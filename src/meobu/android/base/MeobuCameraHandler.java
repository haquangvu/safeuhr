package meobu.android.base;

import java.io.File;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

public class MeobuCameraHandler {
	
	public static final int MeobuCapturePhotoActivity = 2508874;
	public static final int MeobuCaptureVideoActivity = 2508878;
	
	private static File cacheOutputFile;
	
	public static String executeCameraVideoCaptureActivity(Activity activity) {
		cacheOutputFile = null;
    	Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
    	if (intent.resolveActivity(activity.getPackageManager()) != null) {
        	activity.startActivityForResult(intent, MeobuCaptureVideoActivity);
    	}
		return null;
	}
	
	public static String executeCameraPhotoCaptureActivity(Activity activity) {
		executeCameraMediaCaptureActivity(activity, ".png", Environment.DIRECTORY_PICTURES, MediaStore.ACTION_IMAGE_CAPTURE, MeobuCapturePhotoActivity);
		if (cacheOutputFile != null) {
			return cacheOutputFile.getAbsolutePath();
		}
		return null;
	}
	
	private static void executeCameraMediaCaptureActivity(Activity activity, String extension, String directory, String action, int request) {
		//gen file path
		String filename = "timelock" + System.currentTimeMillis() + extension;
		//use external
		File folder = Environment.getExternalStoragePublicDirectory(directory);
		if (folder == null ||
				(Environment.getExternalStorageState() != null && !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))) {
			//use app folder
			folder = activity.getExternalFilesDir(directory);
			if (folder == null) {
				//if not, use internal
				folder = Environment.getDataDirectory();
			}
		}
		cacheOutputFile = new File(folder, filename);
		//start capture
    	Intent intent = new Intent(action);
    	intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cacheOutputFile));
    	activity.startActivityForResult(intent, request);
	}
	
	public static Object onActivityResult(int requestCode, int resultCode,
			Intent data, MeobuActivity activity, String cacheOutputPath) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == MeobuCapturePhotoActivity ||
					requestCode == MeobuCaptureVideoActivity) {
				if (cacheOutputFile != null) {
					return cacheOutputFile;
				}
				if (cacheOutputPath != null) {
					return new File(cacheOutputPath);
				}
				if (data.getExtras() != null) {
					Object output = data.getExtras().get(MediaStore.EXTRA_OUTPUT);
					if (output != null) {
						return output;
					}
				}
				return data.getData();
			}
		}
		return null;
	}

}
