package meobu.android.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MeobuSettingSQLiteOpenHelper extends SQLiteOpenHelper {
	
	//----------//----------//----------//----------//----------//

	private static final String DATABASE_NAME = "mbs.db";
	private static final int DATABASE_VERSION = 1;

	public MeobuSettingSQLiteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	//----------//----------//----------//----------//----------//

	public static final String TABLE_NAME = "settings";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_VALUE = "value";
	public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_NAME, COLUMN_VALUE};

	// Database creation sql statement
	private static final String DATABASE_CREATE =
			"create table " + TABLE_NAME
			+ "("
					+ COLUMN_ID + " integer primary key autoincrement, "
					+ COLUMN_NAME + " text not null,"
					+ COLUMN_VALUE + " text not null"
			+");";

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
	}
	
	//----------//----------//----------//----------//----------//

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
	
	//----------//----------//----------//----------//----------//

}
