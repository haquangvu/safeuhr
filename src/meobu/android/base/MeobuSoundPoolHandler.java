package meobu.android.base;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.SparseIntArray;

public final class MeobuSoundPoolHandler {
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private static MeobuSoundPoolHandler instance;
	public static MeobuSoundPoolHandler getInstance() {
		if (instance == null) {
			instance = new MeobuSoundPoolHandler();
		}
		return instance;
	}
	private boolean activated;
	private MeobuSoundPoolHandler() {
		activated = false;
	}

	SoundPool soundPool;
	SparseIntArray soundPoolMap;
//	SparseBooleanArray soundPoolStatus;
//	SoundPool.OnLoadCompleteListener listener = new SoundPool.OnLoadCompleteListener() {
//		@Override
//		public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
//			Log.d("meobudebug", "sampleid: " + sampleId + "   status: " + status);
//			if (status == 0) {
//				soundPoolStatus.put(sampleId, true);
//			}
//		}
//	};
	void activate(MeobuApplication app) {
		soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
//		soundPool.setOnLoadCompleteListener(listener);
	    soundPoolMap = new SparseIntArray();
//	    soundPoolStatus = new SparseBooleanArray();
	    int[] resources = app.getSoundResourceId();
	    if (resources != null) {
		    for (int i=0; i<resources.length; i++) {
//		    	soundPoolStatus.put(i+1, false);
		    	soundPoolMap.put(i+1, soundPool.load(app, resources[i], 1));
		    }
	    }
	    activated = true;
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	public void play(MeobuApplication app, int index) {
		if (!activated) return;
//		if (!soundPoolStatus.get(index)) {
//			Log.d("meobudebug", "not load");
//		    int[] resources = app.getSoundResourceId();
//			soundPoolMap.put(index, soundPool.load(app, resources[index-1], 1));
//			return;
//		}
		AudioManager audioManager = (AudioManager)app.getSystemService(Context.AUDIO_SERVICE);
        //float curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        //float leftVolume = curVolume/maxVolume;
        //float rightVolume = curVolume/maxVolume;
        int priority = 1;
        int no_loop = 0;
        float normal_playback_rate = 1f;
        //soundPool.play(soundID, leftVolume, rightVolume, priority, no_loop, normal_playback_rate);
        soundPool.play(index, maxVolume, maxVolume, priority, no_loop, normal_playback_rate);
        //soundPool.play(soundPoolMap.get(index), maxVolume, maxVolume, priority, no_loop, normal_playback_rate);
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
}
