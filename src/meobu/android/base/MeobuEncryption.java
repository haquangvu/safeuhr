package meobu.android.base;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;

public class MeobuEncryption {
	
	public static InputStream inStreamDecryptedAES(InputStream is, String pw) throws Exception {
		//#pw.length == 16
		SecretKeySpec sks = new SecretKeySpec(pw.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");
	    cipher.init(Cipher.DECRYPT_MODE, sks);
	    return new CipherInputStream(is, cipher);
	}
	
	public static OutputStream outStreamEncryptedAES(OutputStream os, String pw) throws Exception {
		//#pw.length == 16
		// Length is 16 byte
	    SecretKeySpec sks = new SecretKeySpec(pw.getBytes(), "AES");
	    // Create cipher
	    Cipher cipher = Cipher.getInstance("AES");
	    cipher.init(Cipher.ENCRYPT_MODE, sks);
	    // Wrap the output stream
	    return new CipherOutputStream(os, cipher);
    }
	
	public static String SHA256(String text) throws Exception {
		//Log.d("meobudebug", "input: " + text);
	    MessageDigest md = MessageDigest.getInstance("SHA-256");
	    md.update(text.getBytes("UTF-8"));
	    byte[] digest = md.digest();
	    StringBuilder result = new StringBuilder();
	    for (int i=0; i<digest.length; i++) {
	    	result.append(String.format("%02x", digest[i]));
	    	//result.append(Integer.toHexString(digest[i]));
	    }
	    return result.toString();
	    
//	    String rs = result.toString();
//	    Log.d("meobudebug", "length: " + rs.length() + "   rs: " + rs);
//	    return rs;
	    
//	    String result = new String(digest, "UTF-8");
//	    Log.d("meobudebug", "length: " + digest.length + "   string: " + new String(digest, "ISO-8859-1") + "   result: " + result);
//	    return result;
	    
	    //return Base64.encodeToString(digest, Base64.DEFAULT);
	}
	
}
