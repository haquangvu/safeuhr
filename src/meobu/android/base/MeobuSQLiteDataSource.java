package meobu.android.base;

public interface MeobuSQLiteDataSource {
	public void open();
	public void close();
}
