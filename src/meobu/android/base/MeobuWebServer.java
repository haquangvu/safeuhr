package meobu.android.base;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import org.apache.http.HttpException;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpRequestHandlerRegistry;
import org.apache.http.protocol.HttpService;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseDate;
import org.apache.http.protocol.ResponseServer;

import android.content.Context;

public class MeobuWebServer {

	// ----------//----------//----------//----------//----------//----------//----------//
	
	//private Context context = null;

	private BasicHttpProcessor httpproc = null;
	private BasicHttpContext httpContext = null;
	private HttpService httpService = null;
	private HttpRequestHandlerRegistry registry = null;

	public static final int serverPort = 1234;
	//TODO define supported url
//	private static final String ALL_PATTERN = "*";
//	private static final String EXCEL_PATTERN = "/*.xls";
//	private static final String HOME_PATTERN = "/home.html";
	
	private boolean running = false;
	
	public MeobuWebServer(Context context) {
	    //this.context = context;

	    httpproc = new BasicHttpProcessor();
	    httpContext = new BasicHttpContext();

	    httpproc.addInterceptor(new ResponseDate());
	    httpproc.addInterceptor(new ResponseServer());
	    httpproc.addInterceptor(new ResponseContent());
	    httpproc.addInterceptor(new ResponseConnControl());

	    httpService = new HttpService(httpproc,
	        new DefaultConnectionReuseStrategy(), new DefaultHttpResponseFactory());

	    registry = new HttpRequestHandlerRegistry();
	    //TODO register supported url
	    //registry.register(HOME_PATTERN, new HomeCommandHandler(context));
	    
	    httpService.setHandlerResolver(registry);
	}

	// ----------//----------//----------//----------//----------//----------//----------//
	
	private ServerSocket serverSocket;
	private void runServer() {
	    try {
	        serverSocket = new ServerSocket(serverPort);
	        serverSocket.setReuseAddress(true);

	        while (running) {
	            try {
	                final Socket socket = serverSocket.accept();
	                DefaultHttpServerConnection serverConnection = new DefaultHttpServerConnection();
	                serverConnection.bind(socket, new BasicHttpParams());

	                httpService.handleRequest(serverConnection, httpContext);

	                serverConnection.shutdown();
	            } catch (IOException e) {
	                e.printStackTrace();
	            } catch (HttpException e) {
	                e.printStackTrace();
	            } catch (Exception e) {
	            	e.printStackTrace();
	            }
	        }
	        serverSocket.close();
	    } catch (SocketException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }

	    running = false;
	}

	// ----------//----------//----------//----------//----------//----------//----------//
	
	public synchronized void startServer() {
	    running = true;
	    runServer();
	}

	public synchronized void stopServer() {
	    running = false;
	    if (serverSocket != null) {
	        try {
	            serverSocket.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	    }
	}

	// ----------//----------//----------//----------//----------//----------//----------//
	
}
