package meobu.android.base;

import java.util.ArrayList;
import java.util.Hashtable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

/**
 * Purpose: use for features-enable settings.
 * How:
 * + Override, provide list of supported settings.
 * + Retrieve in Application to get values. Update via Application after billing.
 * Missing:
 * + Error handle
 * @author meobu
 *
 */
public class MeobuSettingsDataSource implements MeobuSQLiteDataSource {

	// ----------//----------//----------//----------//----------//

	SQLiteOpenHelper helper;
	public MeobuSettingsDataSource(Context context) {
		helper = new MeobuSettingSQLiteOpenHelper(context);
	}
	
	@Override
	public void open() {
		//db = helper.getWritableDatabase();
	}
	
	@Override
	public void close() {
		helper.close();
	}

	// ----------//----------//----------//----------//----------//
	
	public static class MeobuSettingsContent {
		public int id = -1;
		public String name = null, value = null;
		public ContentValues toValues() {
			ContentValues values = new ContentValues();
			values.put(MeobuSettingSQLiteOpenHelper.COLUMN_NAME, name);
			values.put(MeobuSettingSQLiteOpenHelper.COLUMN_VALUE, value);
			return values;
		}
		public void fromCursor(Cursor cursor) {
			id = cursor.getInt(0);
			name = cursor.getString(1);
			value = cursor.getString(2);
		}
	}
	
	protected String[] getKeys() {
		return null;
	}
	
	// ----------//----------//----------//----------//----------//
		
	//start app: load all from data
	//save all: for all keys, update if exist & id >= 0, add if null
	//save 1: update if exist & id >= 0, add if null;
	//free add new keys, change keys will reset to default

	public Hashtable<String, MeobuSettingsContent> getAll() {
		Hashtable<String, MeobuSettingsContent> contents = new Hashtable<String, MeobuSettingsContent>();
		SQLiteDatabase db = helper.getReadableDatabase();
		
		String query = SQLiteQueryBuilder.buildQueryString(false,
				MeobuSettingSQLiteOpenHelper.TABLE_NAME,
				MeobuSettingSQLiteOpenHelper.ALL_COLUMNS, null, null, null,
				null, null);
		Cursor cursor = db.rawQuery(query, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			
			MeobuSettingsContent content = new MeobuSettingsContent();
			content.id = cursor.getInt(0);
			content.name = cursor.getString(1);
			content.value = cursor.getString(2);
			contents.put(content.name, content);
			
			cursor.moveToNext();
		}
		
		db.close();
		
		return contents;
	}
	
	public void saveAll(Hashtable<String, MeobuSettingsContent> contents) {
		if (contents == null || contents.isEmpty() || contents.size() <= 0) {
			//empty nothing to do
			return;
		}
		String[] supports = getKeys();
		if (supports == null || supports.length <= 0) {
			//don't support anything
			return;
		}
		
		MeobuSettingsContent content;
		
		ArrayList<MeobuSettingsContent> updateItems = new ArrayList<MeobuSettingsContent>();
		ArrayList<MeobuSettingsContent> addItems = new ArrayList<MeobuSettingsContent>();
		for (int i=0; i<supports.length; i++) {
			content = contents.get(supports[i]);
			if (content == null) {
				
			} else if (content.id < 0) {
				addItems.add(content);
			} else {
				updateItems.add(content);
			}
		}
		
		SQLiteDatabase db = helper.getWritableDatabase();
		try {
		//update 
		for (int i=0; i<updateItems.size(); i++) {
			content = updateItems.get(i);
			
			String table = MeobuSettingSQLiteOpenHelper.TABLE_NAME;
			ContentValues values = content.toValues();
			String whereClause = MeobuSettingSQLiteOpenHelper.COLUMN_ID + "=" + content.id;
			String[] whereArgs = null;
			
			int count = db.update(table, values, whereClause, whereArgs);
			if (count <= 0) {
				//TODO database fail
			} else if (count > 1) {
				//TODO code fail
			} else {
				//TODO correct
			}
		}
		//add & id
		for (int i=0; i<addItems.size(); i++) {
			content = addItems.get(i);
			long id = db.insert(MeobuSettingSQLiteOpenHelper.TABLE_NAME, null, content.toValues());
			if (id < 0) {
				//TODO database fail
			} else {
				content.id = (int)id;
			}
		}
		} catch (Exception e) {
			
		} finally {
			db.close();
		}
	}
	
	// ----------//----------//----------//----------//----------//

}