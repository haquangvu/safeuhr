package meobu.android.base;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.channels.FileChannel;

import com.protectstar.timelock.pro.android.data.service.BackgroundActionStatusUpdate;

import common.view.OneByOnePercentAsyncTask;
import common.view.PercentAsyncTask;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;

public class MeobuFileTools {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static File tempLog(Context c) {
		File temp = null;
		try {
			File folder = c.getExternalCacheDir();
			if (folder.exists()) folder.mkdirs();
			temp = File.createTempFile("tmp", null, folder);
			temp.deleteOnExit();
		} catch (Exception e) {}
		return temp;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static File duplicateTemp(Uri uri, ContentResolver cr) {
		File temp = null;
		//1st approach: openFileDescriptor
		ParcelFileDescriptor pfd = null;
		FileDescriptor fd = null;
		try {
			pfd = cr.openFileDescriptor(uri, "r");
			fd = pfd.getFileDescriptor();
			temp = File.createTempFile("mbu", null);
			copy(fd, temp);
		} catch (Exception e) {
			Log.w("meobuwarning", "MeobuFileTools.duplicateTemp(Uri, ContentResolver)#duplicate failed. " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (pfd != null) {
				try { pfd.close(); } catch (Exception e) { }
			}
		}
//		//2nd approach: openInputStream
//		InputStream is = null;
//		try {
//			is = cr.openInputStream(uri);
//			temp = File.createTempFile("mbu", null);
//			copy(is, temp);
//		} catch (Exception e) {
//			Log.w("meobuwarning", "MeobuFileTools.duplicateTemp(Uri, ContentResolver)#duplicate failed. " + e.getMessage());
//			e.printStackTrace();
//		} finally {
//			if (is != null) {
//				try {is.close();} catch (Exception e) {}
//			}
//		}
		return temp;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	//support String, File, FileDescriptor, InputStream
	public static void copyChangePassword(Object from, File to, String oldPassword, String newPassword) {
		//retrieve inputStream
		InputStream inputFrom = null;
		boolean closeInput = true;
		if (from instanceof String) {
			final File fileFrom = new File((String)from);
			if (fileFrom.exists()) {
				try {
					inputFrom = new FileInputStream(fileFrom);
					closeInput = true;
				} catch (Exception e) {
					Log.w("meobuwarning", "MeobuFileTools.copyChangePassword(Object, File, String, String) ### Un-read-able String input. " + e.getMessage());
				}
			}
		} else if (from instanceof InputStream) {
			inputFrom = (InputStream)from;
			closeInput = false;
		} else if (from instanceof File) {
			try {
				inputFrom = new FileInputStream((File)from);
				closeInput = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copyChangePassword(Object, File, String, String) ### Un-read-able File input. " + e.getMessage());
			}
		} else if (from instanceof FileDescriptor) {
			try {
				inputFrom = new FileInputStream((FileDescriptor)from);
				closeInput = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copyChangePassword(Object, File, String, String) ### Un-read-able FileDescriptor input. " + e.getMessage());
			}
		}
		if (inputFrom == null) {
			Log.e("meobuerror", "MeobuFileTools.copyChangePassword(Object, File, String, String) ### Unsupported input. " + from.getClass().getSimpleName());
			return;
		}
		//decrypt & output & encrypt
		InputStream cis = null;
		OutputStream fos = null;
		OutputStream cos = null;
		try {
			cis = MeobuEncryption.inStreamDecryptedAES(inputFrom, oldPassword);
			fos = new FileOutputStream(to);
			cos = MeobuEncryption.outStreamEncryptedAES(fos, newPassword);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int len = 0;
			while ((len = cis.read(buffer)) != -1) {
			    cos.write(buffer, 0, len);
			}
			cos.flush();
		} catch (Exception e) {
			Log.e("meobuerror", "MeobuFileTools.copyChangePassword(Object, File, String, String) ### Decrypt & Copy & Encrypt file failed. " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {fos.close();} catch (Exception e) {}
			}
			if (cis != null) {
				try {cis.close();} catch (Exception e) {}
			}
			if (inputFrom != null && closeInput) {
				try {inputFrom.close();} catch (Exception e) {}
			}
		}
	}
	
	public static void copyDecryptNew(Object from, File to, String password, BackgroundActionStatusUpdate status, PrintStream log) {
		if (from == null || to == null) {
			if (log != null) try { log.println("[_410]"); } catch (Exception e) { }
			return;
		}
		//retrieve inputStream
		InputStream inputFrom = null;
		boolean closeInput = true;
		long size = -1;
		if (from instanceof String) {
			final File fileFrom = new File((String)from);
			size = fileFrom.length();
			if (fileFrom.exists()) {
				try {
					inputFrom = new FileInputStream(fileFrom);
					closeInput = true;
				} catch (Exception e) {
					if (log != null) {
						try { log.println("[_411]"); } catch (Exception ee) { }
						try { e.printStackTrace(log); } catch (Exception ee) { }
					}
				}
			}
		} else if (from instanceof InputStream) {
			inputFrom = (InputStream)from;
			closeInput = false;
			try {size = inputFrom.available();} catch (Exception e) {}
		} else if (from instanceof File) {
			size = ((File)from).length();
			try {
				inputFrom = new FileInputStream((File)from);
				closeInput = true;
			} catch (Exception e) {
				if (log != null) {
					try { log.println("[_412]"); } catch (Exception ee) { }
					try { e.printStackTrace(log); } catch (Exception ee) { }
				}
			}
		} else if (from instanceof FileDescriptor) {
			try {
				inputFrom = new FileInputStream((FileDescriptor)from);
				closeInput = true;
			} catch (Exception e) {
				if (log != null) {
					try { log.println("[_413]"); } catch (Exception ee) { }
					try { e.printStackTrace(log); } catch (Exception ee) { }
				}
			}
			try {size = inputFrom.available();} catch (Exception e) {}
		}
		if (inputFrom == null) {
			if (log != null) {
				try { log.println("[_414]" + from.getClass().getSimpleName()); } catch (Exception ee) { }
			}
			return;
		}
		//decrypt & output
		InputStream cis = null;
		OutputStream fos = null;
		try {
			cis = MeobuEncryption.inStreamDecryptedAES(inputFrom, password);
			fos = new FileOutputStream(to);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			long writtenBytes = 0;
			int len = 0;
			while ((len = cis.read(buffer)) != -1) {
			    fos.write(buffer, 0, len);
			    writtenBytes += len;
			    if (size > 0 && writtenBytes >= size) size = -1;
			    status.update((int)(size/1024), (int)(writtenBytes/1024));
			}
			fos.flush();
		} catch (Exception e) {
			if (log != null) {
				try { log.println("[_415]"); } catch (Exception ee) { }
				try { e.printStackTrace(log); } catch (Exception ee) { }
			}
		} finally {
			if (fos != null) {
				try {fos.close();} catch (Exception e) {}
			}
			if (cis != null) {
				try {cis.close();} catch (Exception e) {}
			}
			if (inputFrom != null && closeInput) {
				try {inputFrom.close();} catch (Exception e) {}
			}
		}
	}

	//support String, File, FileDescriptor, InputStream
	public static void copyDecrypt(Object from, File to, String password, PercentAsyncTask<?, ?> task) {
		//retrieve inputStream
		InputStream inputFrom = null;
		boolean closeInput = true;
		long size = -1;
		if (from instanceof String) {
			final File fileFrom = new File((String)from);
			size = fileFrom.length();
			if (fileFrom.exists()) {
				try {
					inputFrom = new FileInputStream(fileFrom);
					closeInput = true;
				} catch (Exception e) {
					Log.w("meobuwarning", "MeobuFileTools.copyDecrypt(Object, File, String) ### Un-read-able String input. " + e.getMessage());
				}
			}
		} else if (from instanceof InputStream) {
			inputFrom = (InputStream)from;
			closeInput = false;
			try {size = inputFrom.available();} catch (Exception e) {}
		} else if (from instanceof File) {
			size = ((File)from).length();
			try {
				inputFrom = new FileInputStream((File)from);
				closeInput = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copyDecrypt(Object, File, String) ### Un-read-able File input. " + e.getMessage());
			}
		} else if (from instanceof FileDescriptor) {
			try {
				inputFrom = new FileInputStream((FileDescriptor)from);
				closeInput = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copyDecrypt(Object, File, String) ### Un-read-able FileDescriptor input. " + e.getMessage());
			}
			try {size = inputFrom.available();} catch (Exception e) {}
		}
		if (inputFrom == null) {
			Log.e("meobuerror", "MeobuFileTools.copyDecrypt(Object, File, String) ### Unsupported input. " + from.getClass().getSimpleName());
			return;
		}
		//decrypt & output
		InputStream cis = null;
		OutputStream fos = null;
		try {
			cis = MeobuEncryption.inStreamDecryptedAES(inputFrom, password);
			fos = new FileOutputStream(to);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			long writtenBytes = 0;
			long lastNotify = System.currentTimeMillis();
			int len = 0;
			while ((len = cis.read(buffer)) != -1) {
			    fos.write(buffer, 0, len);
			    writtenBytes += len;
			    if (System.currentTimeMillis() - lastNotify > 1000) {
					lastNotify = System.currentTimeMillis();
				    if (size > 0 && writtenBytes >= size) size = -1;
			    	task.doPublishProgressPercent((int)writtenBytes, (int)size);
			    }
			}
			fos.flush();
		} catch (Exception e) {
			Log.e("meobuerror", "MeobuFileTools.copyDecrypt(Object, File, String) ### Decrypt & Copy file failed. " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {fos.close();} catch (Exception e) {}
			}
			if (cis != null) {
				try {cis.close();} catch (Exception e) {}
			}
			if (inputFrom != null && closeInput) {
				try {inputFrom.close();} catch (Exception e) {}
			}
		}
	}
	
	public static void copyEncryptNew(Object from, File to, String password, BackgroundActionStatusUpdate status, PrintStream log) {
		if (from == null || to == null) {
			if (log != null) try { log.println("[410]"); } catch (Exception e) { }
			return;
		}
		//retrieve inputStream
		InputStream inputFrom = null;
		boolean closeInput = true;
		long size = -1;
		if (from instanceof String) {
			final File fileFrom = new File((String)from);
			size = fileFrom.length();
			if (fileFrom.exists()) {
				try {
					inputFrom = new FileInputStream(fileFrom);
					closeInput = true;
				} catch (Exception e) {
					if (log != null) {
						try { log.println("[411]"); } catch (Exception ee) { }
						try { e.printStackTrace(log); } catch (Exception ee) { }
					}
				}
			}
		} else if (from instanceof InputStream) {
			inputFrom = (InputStream)from;
			try {size = inputFrom.available();}catch(Exception e) {}
			closeInput = false;
		} else if (from instanceof File) {
			size = ((File)from).length();
			try {
				inputFrom = new FileInputStream((File)from);
				closeInput = true;
			} catch (Exception e) {
				if (log != null) {
					try { log.println("[412]"); } catch (Exception ee) { }
					try { e.printStackTrace(log); } catch (Exception ee) { }
				}
			}
		} else if (from instanceof FileDescriptor) {
			try {
				inputFrom = new FileInputStream((FileDescriptor)from);
				closeInput = true;
			} catch (Exception e) {
				if (log != null) {
					try { log.println("[413]"); } catch (Exception ee) { }
					try { e.printStackTrace(log); } catch (Exception ee) { }
				}
			}
			try {size = inputFrom.available();}catch(Exception e) {}
		}
		if (inputFrom == null) {
			if (log != null) try { log.println("[414]" + from.getClass().getSimpleName()); } catch (Exception e) { }
			return;
		}
		//output & encrypt
		OutputStream fos = null;
		OutputStream cos = null;
		try {
			fos = new FileOutputStream(to);
			cos = MeobuEncryption.outStreamEncryptedAES(fos, password);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			long writtenBytes = 0;
			long lastNotify = System.currentTimeMillis();
			int len = 0;
			while ((len = inputFrom.read(buffer)) != -1) {
			    cos.write(buffer, 0, len);
			    writtenBytes += len;
			    if (System.currentTimeMillis() - lastNotify > 1000) {
					lastNotify = System.currentTimeMillis();
				    if (size > 0 && writtenBytes >= size) size = -1;
				    status.update((int)(size/1024), (int)(writtenBytes/1024));
			    }
			}
			cos.flush();
		} catch (Exception e) {
			if (log != null) {
				try { log.println("[415]"); } catch (Exception ee) { }
				try { e.printStackTrace(log); } catch (Exception ee) { }
			}
		} finally {
			if (cos != null) {
				try {cos.close();} catch (Exception e) {}
			}
			if (fos != null) {
				try {fos.close();} catch (Exception e) {}
			}
			if (inputFrom != null && closeInput) {
				try {inputFrom.close();} catch (Exception e) {}
			}
		}
	}
	
	//support String, File, FileDescriptor, InputStream
	public static void copyEncrypt(Object from, File to, String password, OneByOnePercentAsyncTask<?, ?> task) {
		if (from == null) {
			task.doPublishLog(410);
			Log.e("meobuerror", "MeobuFileTools.copyEncrypt(Object, File, String) ### Null input.");
			return;
		}
		//retrieve inputStream
		InputStream inputFrom = null;
		boolean closeInput = true;
		long size = -1;
		if (from instanceof String) {
			final File fileFrom = new File((String)from);
			size = fileFrom.length();
			if (fileFrom.exists()) {
				try {
					inputFrom = new FileInputStream(fileFrom);
					closeInput = true;
				} catch (Exception e) {
					Log.w("meobuwarning", "MeobuFileTools.copyEncrypt(Object, File, String) ### Un-read-able String input. " + e.getMessage());
				}
			}
		} else if (from instanceof InputStream) {
			inputFrom = (InputStream)from;
			try {size = inputFrom.available();}catch(Exception e) {}
			closeInput = false;
		} else if (from instanceof File) {
			size = ((File)from).length();
			try {
				inputFrom = new FileInputStream((File)from);
				closeInput = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copyEncrypt(Object, File, String) ### Un-read-able File input. " + e.getMessage());
			}
		} else if (from instanceof FileDescriptor) {
			try {
				inputFrom = new FileInputStream((FileDescriptor)from);
				closeInput = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copyEncrypt(Object, File, String) ### Un-read-able FileDescriptor input. " + e.getMessage());
			}
			try {size = inputFrom.available();}catch(Exception e) {}
		}
		if (inputFrom == null) {
			task.doPublishLog(411);
			Log.e("meobuerror", "MeobuFileTools.copyEncrypt(Object, File, String) ### Unsupported input. " + from.getClass().getSimpleName());
			return;
		}
		//output & encrypt
		OutputStream fos = null;
		OutputStream cos = null;
		try {
			task.doPublishLog(412);
			fos = new FileOutputStream(to);
			cos = MeobuEncryption.outStreamEncryptedAES(fos, password);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			long writtenBytes = 0;
			long lastNotify = System.currentTimeMillis();
			int len = 0;
			task.doPublishLog(413);
			while ((len = inputFrom.read(buffer)) != -1) {
			    cos.write(buffer, 0, len);
			    writtenBytes += len;
			    if (System.currentTimeMillis() - lastNotify > 1000) {
					lastNotify = System.currentTimeMillis();
				    if (size > 0 && writtenBytes >= size) size = -1;
			    	task.doPublishProgressFull((int)writtenBytes, (int)size);
			    }
			}
			cos.flush();
		} catch (Exception e) {
			Log.e("meobuerror", "MeobuFileTools.copyEncrypt(Object, File, String) ### Copy & Encrypt file failed. " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (cos != null) {
				try {cos.close();} catch (Exception e) {}
			}
			if (fos != null) {
				try {fos.close();} catch (Exception e) {}
			}
			if (inputFrom != null && closeInput) {
				try {inputFrom.close();} catch (Exception e) {}
			}
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	//support String, File, FileDescriptor, FileInputStream, InputStream
	public static void copy(Object from, File to) {
		//retrieve input
		FileInputStream sourceStream = null;
		boolean closeSourceStream = true;
		if (from instanceof String) {
			final File fileFrom = new File((String)from);
			if (fileFrom.exists()) {
				try {
					sourceStream = new FileInputStream(fileFrom);
					closeSourceStream = true;
				} catch (Exception e) {
					Log.w("meobuwarning", "MeobuFileTools.copy(Object, File) ### Un-read-able String input. " + e.getMessage());
				}
			}
		} else if (from instanceof File) {
			try {
				sourceStream = new FileInputStream((File)from);
				closeSourceStream = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copy(Object, File) ### Un-read-able File input. " + e.getMessage());
			}
		} else if (from instanceof FileDescriptor) {
			try {
				sourceStream = new FileInputStream((FileDescriptor)from);
				closeSourceStream = true;
			} catch (Exception e) {
				Log.w("meobuwarning", "MeobuFileTools.copy(Object, File) ### Un-read-able FileDescriptor input. " + e.getMessage());
			}
		} else if (from instanceof FileInputStream) {
			sourceStream = (FileInputStream)from;
			closeSourceStream = false;
		} else if (from instanceof InputStream) {
			copyFromInputStream((InputStream)from, to);
		}
		if (sourceStream == null) {
			if (from instanceof InputStream) {
				//already done
			} else {
				Log.e("meobuerror", "MeobuFileTools.copy(Object, File) ### Unsupported input. " + from.getClass().getSimpleName());
			}
			return;
		}
		//output
		FileOutputStream destinationStream = null;
        FileChannel source = null;
        FileChannel destination = null;
        //process
        try {
	        source = sourceStream.getChannel();
	        destinationStream = new FileOutputStream(to);
	        destination = destinationStream.getChannel();
	        if (destination != null && source != null) {
	            destination.transferFrom(source, 0, source.size());
	        }
        } catch (Exception e) {
			Log.e("meobuerror", "MeobuFileTools.copy(Object, File) ### Copy file failed. " + e.getMessage());
			e.printStackTrace();
        } finally {
        	if (source != null) {
        		try { source.close(); } catch (Exception e) { }
        	}
        	if (sourceStream != null && closeSourceStream) {
        		try { sourceStream.close(); } catch (Exception e) { }
        	}
        	if (destination != null) {
        		try { destination.close(); } catch (Exception e) { }
        	}
        	if (destinationStream != null) {
        		try { destinationStream.close(); } catch (Exception e) { }
        	}
        }
    }

	private static void copyFromInputStream(InputStream from, File to) {
		if (from == null) return;
		OutputStream fos = null;
		OutputStream bos = null;
		try {
			fos = new FileOutputStream(to);
			bos = new BufferedOutputStream(fos); 
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int len = 0;
			while ((len = from.read(buffer)) != -1) {
			    bos.write(buffer, 0, len);
			}
			bos.flush();
		} catch (Exception e) {
			Log.e("meobuerror", "MeobuFileTools.copy(InputStream, File) ### Copy file failed. " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {bos.close();} catch (Exception e) {}
			}
			if (fos != null) {
				try {fos.close();} catch (Exception e) {}
			}
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
}
