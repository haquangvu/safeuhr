package meobu.android.base;

import java.util.Locale;

import android.content.res.Configuration;

public final class MeobuLanguageHandler {
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private static MeobuLanguageHandler instance;
	public static MeobuLanguageHandler getInstance() {
		if (instance == null) {
			instance = new MeobuLanguageHandler();
		}
		return instance;
	}
	private boolean activated;
	private MeobuLanguageHandler() {
		activated = false;
	}

	public int languageIndex = 0;
	public Locale systemLocale;
	public String languageName = null;
	public static final String NAME = "MeobuLanguage";
	void activate(MeobuApplication app) {
		languageIndex = app.getSharedPreferences(NAME, 0).getInt("languageIndex", 0);
		systemLocale = app.getApplicationContext().getResources().getConfiguration().locale;
		languageName = systemLocale.getDisplayName();
		activated = true;
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	public void updateLanguage(MeobuApplication app) {
		if (!activated) return;
		setLanguageIndex(app, languageIndex);
	}

	public void setLanguageIndex(MeobuApplication app, final int index) {
		if (!activated) return;
		languageIndex = index;
		app.getSharedPreferences(NAME, 0).edit().putInt("languageIndex", languageIndex).commit();
		Locale requestLocale = app.getLocaleWithLanguageIndex(index);
		Locale currentLocale = app.getApplicationContext().getResources().getConfiguration().locale;
		if (requestLocale == null) {
			//change to System Locale
			//Log.d("meobudebug", "change to System Locale");
			if (currentLocale.equals(systemLocale)) {//same with System
				//do nothing
				//Log.d("meobudebug", "Current Locale same with System Locale, no change.");
			} else {
				//Log.d("meobudebug", "Current Locale NOT same with System Locale, do change.");
				doChangeLocale(app, systemLocale);
			}
		} else {
			//Log.d("meobudebug", "change to Custom Locale");
			if (currentLocale.equals(requestLocale)) {//same with current
				//do nothing
				//Log.d("meobudebug", "Current Locale same with Selected Custom Locale, no change.");
			} else {
				//Log.d("meobudebug", "Current Locale NOT same with Selected Custom Locale, do change.");
				doChangeLocale(app, requestLocale);
			}
		}
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//

	private void doChangeLocale(MeobuApplication app, Locale requestLocale) {
    	Locale.setDefault(requestLocale);
    	languageName = requestLocale.getDisplayName();
        Configuration config = app.getApplicationContext().getResources().getConfiguration();
        //Log.d("meobudebug", "doChangeLocale   from: " + config.locale.getLanguage() + "   to: " + requestLocale.getLanguage());
        config.locale = requestLocale;
        app.getApplicationContext().getResources().updateConfiguration(config, app.getApplicationContext().getResources().getDisplayMetrics());
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
}
