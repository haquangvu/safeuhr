package meobu.android.base;

import java.util.ArrayList;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

public abstract class MeobuListViewHandler<ViewData extends MeobuListViewData> {
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	AbsListView view;
	int[] children;
	MeobuListViewAdapter adapter = new MeobuListViewAdapter();
	public MeobuListViewHandler(int[] children) {
		this.children = children;
		if (children == null) {
			this.children = new int[0];
		}
	}
	
	public void setListView(AbsListView view) {
		this.view = view;
		view.setAdapter(adapter);
	}
	
	public void refresh() {
		// Save ListView state
		Parcelable state = view.onSaveInstanceState();
		// Set new items
		view.setAdapter(adapter);
		adapter.notifyDataSetInvalidated();
		adapter.notifyDataSetChanged();
		// Restore previous state (including selected item index and scroll position)
		view.onRestoreInstanceState(state);
	}
	
	public void setListView(MeobuActivity act, int viewid) {
		setListView((AbsListView)act.findViewById(viewid));
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//

	public abstract ArrayList<ViewData> getMeobuListData();
	
	public Object getMeobuItem(int position) {
		return adapter.getItem(position);
	}
	
	public View createMeobuView(int position, int type, ViewGroup parent) {
		View view = null;
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		view = inflater.inflate(children[type], parent, false);
		return view;
	}
	
	public abstract void updateMeobuView(int position, int type, View view);
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//

	protected class MeobuListViewAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			ArrayList<ViewData> data = getMeobuListData();
			if (data == null) {
				return 0;
			}
			return data.size();
		}

		public int getMeobuViewTypeCount() {
			return children.length;
		}
		@Override
		public int getViewTypeCount() {
			return getMeobuViewTypeCount();
		}
		
		@SuppressWarnings("unchecked")
		public int getMeobuItemViewType(int position) {
			if (getItem(position) == null) return 0;
			return ((ViewData)getItem(position)).getType();
		}
		@Override
		public int getItemViewType(int position) {
			return getMeobuItemViewType(position);
		};

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			ArrayList<ViewData> data = getMeobuListData();
			if (data == null || position >= data.size() || position < 0) {
				return null;
			}
			return data.get(position);
		}

		@Override
		public View getView(int position, View convert, ViewGroup parent) {
			int type = getItemViewType(position);
			if (convert == null) {
				convert = createMeobuView(position, type, parent);
			}
			updateMeobuView(position, type, convert);
			return convert;
		}
		
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//

}
