package meobu.android.base;

import java.io.File;


import com.nostra13.universalimageloader.core.ImageLoader;
import meobu.android.base.MeobuGalleryHandler.MeobuGalleryItem;
import uk.co.jarofgreen.lib.ShakeDetectActivity;
import uk.co.jarofgreen.lib.ShakeDetectActivityListener;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MeobuActivity extends Activity {
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		updateMeobuLanguage();
		prepareMeobuShakeDetection();
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		meobuCameraMediaCachePath = savedInstanceState.getString("meobuCameraMediaCachePath");
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		resumeMeobuShakeDetection();
		resumeMeobuSQLite();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		pauseMeobuShakeDetection();
		pauseMeobuSQLite();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (meobuCameraMediaCachePath != null) outState.putString("meobuCameraMediaCachePath", meobuCameraMediaCachePath);
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected void displayMeobuImage(String file, ImageView view) {
		if (((MeobuApplication)getApplication()).supportMeobuImageLoader()) { 
			ImageLoader.getInstance().displayImage("file://" + file, view);
		}
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected void setMeobuLanguage(int index) {
		((MeobuApplication)getApplication()).setMeobuLanguage(index);
	}
	protected int getMeobuLanguage() {
		return ((MeobuApplication)getApplication()).getMeobuLanguage();
	}
	private void updateMeobuLanguage() {
		((MeobuApplication)getApplication()).updateMeobuLanguage();
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected void playMeobuSoundPool(int index) {
		((MeobuApplication)getApplication()).playMeobuSoundPool(index);
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected void startMeobuGalleryPhotoSave(String path) {
		MeobuGalleryHandler.executeGalleryPhotoSave(this, path);
	}
	protected void startMeobuGalleryVideoSave(String path) {
		MeobuGalleryHandler.executeGalleryVideoSave(this, path);
	}
	protected void startMeobuGalleryPhotosPick() {
		MeobuGalleryHandler.executeGalleryPhotoPick(this);
	}
	protected void startMeobuGalleryVideosPick() {
		MeobuGalleryHandler.executeGalleryVideosPick(this);
	}
	protected void onMeobuGalleryPhotosPicked(Uri[] uri) { }
	protected void onMeobuGalleryPhotosPicked(File[] file) { }
	protected void onMeobuGalleryPhotosPicked(MeobuGalleryItem[] item) { }
	protected void onMeobuGalleryVideosPicked(Uri[] uri) { }
	protected void onMeobuGalleryVideosPicked(File[] file) { }
	public boolean supportMeobuGalleryDeleteAfterPicking() {
		return false;
	}
//	private void getRealPathFromURIAndNotify(Uri contentUri, int requestCode) {
//		String key = MediaStore.Images.Media.DATA;
//		if (requestCode == MeobuGalleryPickHandler.MeobuGalleryPickVideosActivity) {
//			key = MediaStore.Video.Media.DATA;
//		}
//		String[] proj = { key };
//		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
//			usingCursorLoaderBelow11(contentUri, proj, requestCode);
//		} else {
//			usingCursorLoader11AndOver(contentUri, proj, requestCode);
//		}
//    }
//	private void usingCursorLoaderBelow11(Uri contentUri, String[] proj, int requestCode) {
//		@SuppressWarnings("deprecation")
//		Cursor cursor = managedQuery(contentUri, proj, null, null, null);
//		try {
//			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//			cursor.moveToFirst();
//			String path = cursor.getString(column_index);
//			if (requestCode == MeobuGalleryPickHandler.MeobuGalleryPickPhotosActivity) {
//				onMeobuGalleryPhotosPicked(contentUri, path);
//			} else { //if (requestCode == MeobuGalleryPickHandler.LUMediaStoreVideoLibraryActivity) {
//				onMeobuGalleryVideosPicked(contentUri, path);
//			}
//		} catch (Exception e) {
//			Log.e("meobuerror", "cannot get media path");
//		}
//	}
//	private void usingCursorLoader11AndOver(final Uri contentUri, final String[] proj, final int requestCode) {
////		usingCursorLoaderBelow11(contentUri, proj, requestCode);
//		String tempPath = contentUri.getPath();
//		Log.d("meobudebug", "simple Path retrieve: " + tempPath);
//		//getContentResolver().qu
//		final LoaderCallbacks<Cursor> callback = new LoaderCallbacks<Cursor>() {
//			@Override
//			public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//				switch (id) {
//				case 0:
//					Log.d("meobudebug", "usingCursorLoader11AndOver   onCreate");
//					return new CursorLoader(MeobuActivity.this, contentUri, proj, null, null, null);
//				default:
//					return null;
//				}
//			}
//			@Override
//			public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//				try {
//					int column_index = data.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//					data.moveToFirst();
//					String path = data.getString(column_index);
//					data.getColumnCount();
//					Log.d("meobudebug", "query successful");
//					debugCursor(data);
//					if (requestCode == MeobuGalleryPickHandler.MeobuGalleryPickPhotosActivity) {
//						onMeobuGalleryPhotosPicked(contentUri, path);
//					} else { //if (requestCode == MeobuGalleryPickHandler.LUMediaStoreVideoLibraryActivity) {
//						onMeobuGalleryVideosPicked(contentUri, path);
//					}
//				} catch (Exception e) {
//					Log.e("meobuerror", "cannot get media path");
//				}
//			}
//			@Override
//			public void onLoaderReset(Loader<Cursor> loader) {
//				Log.d("meobudebug", "onLoaderReset");
//			}
//		};
//		//getLoaderManager().initLoader(0, null, callback);
//		getLoaderManager().restartLoader(0, null, callback);
//	}
//	protected static void debugCursor(Cursor cursor) {
//		int count = cursor.getColumnCount();
//		Log.d("meobudebug", "Cursor column count: " + count);
//		for (int i=0; i<count; i++) {
//			switch (cursor.getType(i)) {
//			case Cursor.FIELD_TYPE_NULL:
//				Log.d("meobudebug", i + " - NULL");
//				break;
//			case Cursor.FIELD_TYPE_INTEGER:
//				Log.d("meobudebug", i + " - " + cursor.getInt(i));
//				break;
//			case Cursor.FIELD_TYPE_FLOAT:
//				Log.d("meobudebug", i + " - " + cursor.getFloat(i));
//				break;
//			case Cursor.FIELD_TYPE_STRING:
//				Log.d("meobudebug", i + " - " + cursor.getString(i));
//				break;
//			default:
//				Log.d("meobudebug", i + " - bytes array, no need");
//				break;
//			} 
//		}
//	}
//	private static String getPath(final Context context, final Uri uri) {
//		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
//
//	    // DocumentProvider
//	    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//	    	boolean isExternalStorageDocument = "com.android.externalstorage.documents".equals(uri.getAuthority());
//	    	boolean isDownloadsDocument = "com.android.providers.downloads.documents".equals(uri.getAuthority());
//	    	boolean isMediaDocument = "com.android.providers.media.documents".equals(uri.getAuthority());
//	        // ExternalStorageProvider
//	        if (isExternalStorageDocument) {
//	            final String docId = DocumentsContract.getDocumentId(uri);
//	            final String[] split = docId.split(":");
//	            final String type = split[0];
//
//	            if ("primary".equalsIgnoreCase(type)) {
//	                return Environment.getExternalStorageDirectory() + "/" + split[1];
//	            }
//
//	            // handle non-primary volumes
//	        }
//	        // DownloadsProvider
//	        else if (isDownloadsDocument) {
//
//	            final String id = DocumentsContract.getDocumentId(uri);
//	            final Uri contentUri = ContentUris.withAppendedId(
//	                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//	            return getDataColumn(context, contentUri, null, null);
//	        }
//	        // MediaProvider
//	        else if (isMediaDocument) {
//	            final String docId = DocumentsContract.getDocumentId(uri);
//	            final String[] split = docId.split(":");
//	            final String type = split[0];
//
//	            Uri contentUri = null;
//	            if ("image".equals(type)) {
//	                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//	            } else if ("video".equals(type)) {
//	                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//	            } else if ("audio".equals(type)) {
//	                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//	            }
//
//	            final String selection = "_id=?";
//	            final String[] selectionArgs = new String[] {
//	                    split[1]
//	            };
//
//	            return getDataColumn(context, contentUri, selection, selectionArgs);
//	        }
//	    }
//	    // MediaStore (and general)
//	    if ("content".equalsIgnoreCase(uri.getScheme())) {
//	    	//Toast.makeText(context, "Uri path: " + uri.toString(), Toast.LENGTH_LONG).show();
//	    	boolean isGooglePhotosUri = "com.google.android.apps.photos.content".equals(uri.getAuthority());
//	        // Return the remote address
//	        if (isGooglePhotosUri) {
//	            return uri.getLastPathSegment();
//	        }
//
//	        return getDataColumn(context, uri, null, null);
//	    }
//	    // File
//	    if ("file".equalsIgnoreCase(uri.getScheme())) {
//	        return uri.getPath();
//	    }
//
//	    return null;
//	}
//	private static String getDataColumn(Context context, Uri uri, String selection,
//	        String[] selectionArgs) {
//
//	    Cursor cursor = null;
//	    final String column = "_data";
//	    final String[] projection = {
//	            column
//	    };
//
//	    try {
//	        cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
//	                null);
//	        if (cursor != null && cursor.moveToFirst()) {
//	            final int index = cursor.getColumnIndexOrThrow(column);
//	            return cursor.getString(index);
//	        }
//	    } finally {
//	        if (cursor != null)
//	            cursor.close();
//	    }
//	    return null;
//	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected String meobuCameraMediaCachePath;
	protected void startMeobuCameraPhotoCapture() {
		meobuCameraMediaCachePath = MeobuCameraHandler.executeCameraPhotoCaptureActivity(this);
	}
	protected void startMeobuCameraVideoCapture() {
		meobuCameraMediaCachePath = MeobuCameraHandler.executeCameraVideoCaptureActivity(this);
	}
	protected void onMeobuCameraPhotoCaptured(Uri uri) { }
	protected void onMeobuCameraPhotoCaptured(File file) { }
	protected void onMeobuCameraVideoCaptured(Uri uri) { }
	protected void onMeobuCameraVideoCaptured(File file) { }
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == MeobuGalleryHandler.MeobuGalleryPhotosPick ||
				requestCode == MeobuGalleryHandler.MeobuGalleryVideosPick) {
			if (resultCode == RESULT_OK) {
				Object[] medias = MeobuGalleryHandler.onActivityResult(requestCode, resultCode, data, this);
				if (medias == null) {
					return;
				}
//				debugUris(medias, this);
//				final int count = medias.length;
//				String[] paths = new String[count];
//				final int takeFlags = getIntent().getFlags()
//			            & (Intent.FLAG_GRANT_READ_URI_PERMISSION
//			            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//				for (int i=0; i<count; i++) {
//					try {
//					// Check for the freshest data.
//					getContentResolver().takePersistableUriPermission(medias[i], takeFlags);
//						paths[i] = getPath(this, medias[i]);
//					} catch (Exception e) {
//						Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
//					} finally {
//						if (paths[i] == null) {
//							paths[i] = "empty";
//						}
//					}
//				}
				if (requestCode == MeobuGalleryHandler.MeobuGalleryPhotosPick) {
					if (medias instanceof Uri[]) {
						onMeobuGalleryPhotosPicked((Uri[])medias);
					} else if (medias instanceof File[]) {
						onMeobuGalleryPhotosPicked((File[])medias);
					} else if (medias instanceof MeobuGalleryItem[]) {
						onMeobuGalleryPhotosPicked((MeobuGalleryItem[])medias);
					}
				} else {//requestCode == MeobuGalleryPickHandler.MeobuGalleryPickVideosActivity) {
					if (medias instanceof Uri[]) {
						onMeobuGalleryVideosPicked((Uri[])medias);
					} else if (medias instanceof File[]) {
						onMeobuGalleryVideosPicked((File[])medias);
					}
					//onMeobuGalleryVideosPicked(medias);
				}
				//getRealPathFromURIAndNotify(media, requestCode);
			}
		} else if (requestCode == MeobuCameraHandler.MeobuCapturePhotoActivity ||
				requestCode == MeobuCameraHandler.MeobuCaptureVideoActivity) {
			if (resultCode == RESULT_OK) {
				Object media = MeobuCameraHandler.onActivityResult(requestCode, resultCode, data, this, meobuCameraMediaCachePath);
				if (media != null) {
					if (requestCode == MeobuCameraHandler.MeobuCapturePhotoActivity) {
						//if (media instanceof Uri) {
						//	onMeobuCameraPhotoCaptured((Uri)media);
						//} else
						if (media instanceof File) {
							onMeobuCameraPhotoCaptured((File)media);
						}
					} else if (requestCode == MeobuCameraHandler.MeobuCaptureVideoActivity) {
						if (media instanceof File) {
							onMeobuCameraVideoCaptured((File)media);
						} else if (media instanceof Uri) {
							onMeobuCameraVideoCaptured((Uri)media);
						}
					}
				}
			}
		}
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected boolean supportMeobuSQLite() {
		return false;
	}
	protected MeobuSQLiteDataSource getMeobuDataSource() {
		return null;
	}
	protected void resumeMeobuSQLite() {
		if (supportMeobuSQLite()) {
			try { getMeobuDataSource().open(); } catch (Exception e) { }
		}
	}
	protected void pauseMeobuSQLite() {
		if (supportMeobuSQLite()) {
			try { getMeobuDataSource().close(); } catch (Exception e) { }
		}
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected void restartMeobuActivity() {
		//if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Intent intent = getIntent();
		    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(intent);
			finish();
		//} else {
		//	recreate();
		//}
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected boolean supportMeobuShakeDetection() {
		return false;
	}
	private ShakeDetectActivity detection;
	private void prepareMeobuShakeDetection() {
		if (supportMeobuShakeDetection()) {
			detection = new ShakeDetectActivity(this);
			detection.addListener(new ShakeDetectActivityListener() {
				@Override
				public void shakeDetected() {
					onMeobuShakeDetected();
				}
			});
		}
	}
	private void resumeMeobuShakeDetection() {
		if (supportMeobuShakeDetection()) {
			detection.onResume();
		}
	}
	private void pauseMeobuShakeDetection() {
		if (supportMeobuShakeDetection()) {
			detection.onPause();
		}
	}
	protected void onMeobuShakeDetected() { }
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected static int getViewTag(View view) {
		int id = Integer.MIN_VALUE;
		if (view.getTag() != null) {
			if (view.getTag() instanceof Integer) {
				id = ((Integer)view.getTag()).intValue();
			} else if (view.getTag() instanceof Long) {
				id = ((Long)view.getTag()).intValue();
			}
		}
		return id;
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected static void executeVoidVoidInteger(AsyncTask<Void, ?, ?> task) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeVoidVoidIntegerBelow11(task);
		} else {
			executeVoidVoidInteger11AndOver(task);
		}
	}
	protected static void executeVoidVoidIntegerBelow11(AsyncTask<Void, ?, ?> task) {
		task.execute();
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeVoidVoidInteger11AndOver(AsyncTask<Void, ?, ?> task) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
	
	protected static void executeInteger(AsyncTask<Integer, ?, ?> task, Integer... params) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeIntegerBelow11(task, params);
		} else {
			executeIntegerBelow11(task, params);
		}
	}
	protected static void executeIntegerBelow11(AsyncTask<Integer, ?, ?> task, Integer... params) {
		task.execute(params);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeInteger11AndOver(AsyncTask<Integer, ?, ?> task, Integer... params) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
	}
	
	protected static void executeObject(AsyncTask<Object, ?, ?> task, Object... params) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeObjectBelow11(task, params);
		} else {
			executeObjectBelow11(task, params);
		}
	}
	protected static void executeObjectBelow11(AsyncTask<Object, ?, ?> task, Object... params) {
		task.execute(params);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeObject11AndOver(AsyncTask<Object, ?, ?> task, Object... params) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
	}
	
	protected static void executeLong(AsyncTask<Long, ?, ?> task, Long... params) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeLongBelow11(task, params);
		} else {
			executeLongBelow11(task, params);
		}
	}
	protected static void executeLongBelow11(AsyncTask<Long, ?, ?> task, Long... params) {
		task.execute(params);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeLong11AndOver(AsyncTask<Long, ?, ?> task, Long... params) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
	}
	
	protected static void executeVoidVoidBoolean(AsyncTask<Void, ?, ?> task) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeVoidVoidBooleanBelow11(task);
		} else {
			executeVoidVoidBoolean11AndOver(task);
		}
	}
	protected static void executeVoidVoidBooleanBelow11(AsyncTask<Void, ?, ?> task) {
		task.execute();
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeVoidVoidBoolean11AndOver(AsyncTask<Void, ?, ?> task) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
	
	protected static void executeVoidVoidVoid(AsyncTask<Void, ?, ?> task) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeVoidVoidVoidBelow11(task);
		} else {
			executeVoidVoidVoidAndOver(task);
		}
	}
	protected static void executeVoidVoidVoidBelow11(AsyncTask<Void, ?, ?> task) {
		task.execute();
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeVoidVoidVoidAndOver(AsyncTask<Void, ?, ?> task) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
	
	protected static void executeVoid(AsyncTask<Void, ?, ?> task) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeVoidBelow11(task);
		} else {
			executeVoid11AndOver(task);
		}
	}
	protected static void executeVoidBelow11(AsyncTask<Void, ?, ?> task) {
		task.execute();
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeVoid11AndOver(AsyncTask<Void, ?, ?> task) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected static void debugUris(Uri[] uris, Context context) {
		String[] tests = new String[uris.length];
		for (int i=0; i<tests.length; i++) {
			if (uris[i] != null) {
				tests[i] = uris[i].toString();
			}
		}
		new AlertDialog.Builder(context).setItems(tests, null).create().show();
	}
	
}
