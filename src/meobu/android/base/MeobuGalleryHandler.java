package meobu.android.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;


import com.luminous.pick.Action;
import com.luminous.pick.CustomGalleryActivity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

public class MeobuGalleryHandler {

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	public static class MeobuGalleryItem {
		public long id, date;
		public String path;
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private static void copyFile(File from, File to) {
		FileInputStream sourceStream = null;
		FileOutputStream destinationStream = null;
        FileChannel source = null;
        FileChannel destination = null;
        try {
        	sourceStream = new FileInputStream(from);
	        source = sourceStream.getChannel();
	        destinationStream = new FileOutputStream(to);
	        destination = destinationStream.getChannel();
	        if (destination != null && source != null) {
	            destination.transferFrom(source, 0, source.size());
	        }
        } catch (Exception e) {
			Log.e("meobuerror", "TimeLockMediaFileLayer.copyFile###Copy file failed. " + e.getMessage());
        } finally {
        	if (sourceStream != null) {
        		try { sourceStream.close(); } catch (Exception e) { }
        	}
        	if (source != null) {
        		try { source.close(); } catch (Exception e) { }
        	}
        	if (destinationStream != null) {
        		try { destinationStream.close(); } catch (Exception e) { }
        	}
        	if (destination != null) {
        		try { destination.close(); } catch (Exception e) { }
        	}
        }
	}
	public static void executeGalleryPhotoSave(Activity activity, String path) {
		executeGalleryMediaSave(activity, path, MeobuGalleryPhotosPick);
	}
	public static void executeGalleryVideoSave(Activity activity, String path) {
		executeGalleryMediaSave(activity, path, MeobuGalleryVideosPick);
	}
	private static void executeGalleryMediaSave(Activity activity, String path, int type) {
		//duplicate selected files to public-photo-folder-in-external
		final String filename = "timelock" + System.currentTimeMillis() + ".png";
		File outputFile = null;
		if (type == MeobuGalleryPhotosPick) {
			outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), filename);
		} else {
			outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), filename);
		}
		try {
			outputFile.createNewFile();
		} catch (Exception e) { }
		File inputFile = new File(path);
		copyFile(inputFile, outputFile);
		
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    Uri contentUri = Uri.fromFile(outputFile);
	    mediaScanIntent.setData(contentUri);
	    activity.sendBroadcast(mediaScanIntent);
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//

	public static final int MeobuGalleryPhotosPick = 2508870;
	public static final int MeobuGalleryVideosPick = 2508872;

	public static void executeGalleryVideosPick(Activity activity) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
//			Intent mediaPickerIntent = new Intent(Action.ACTION_MULTIPLE_PICK, null, activity, CustomGalleryActivity.class);
//			mediaPickerIntent.putExtra(Action.EXTRA_VIDEO_ONLY, true);
//			activity.startActivityForResult(mediaPickerIntent, MeobuGalleryPickVideosActivity);
			Intent mediaPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
			mediaPickerIntent.setType("video/*");
			activity.startActivityForResult(mediaPickerIntent, MeobuGalleryVideosPick);
		} else {
			executeGalleryVideoPickKitkatAndOver(activity);
		}
	}
	@TargetApi(19)
	private static void executeGalleryVideoPickKitkatAndOver(Activity activity) {
		Intent mediaPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
		mediaPickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
		mediaPickerIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
		mediaPickerIntent.setType("video/*");
		activity.startActivityForResult(mediaPickerIntent,
				MeobuGalleryVideosPick);
	}

	public static void executeGalleryPhotoPick(Activity activity) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			Intent mediaPickerIntent = new Intent(Action.ACTION_MULTIPLE_PICK, null, activity, CustomGalleryActivity.class);
//			Intent mediaPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
//			mediaPickerIntent.setType("image/*");
			activity.startActivityForResult(mediaPickerIntent, MeobuGalleryPhotosPick);
		} else {
			executePhotoGalleryActivityKitkatAndOver(activity);
		}
	}
	@TargetApi(19)
	private static void executePhotoGalleryActivityKitkatAndOver(Activity activity) {
		Intent mediaPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
		mediaPickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
		mediaPickerIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
		mediaPickerIntent.setType("image/*");
		activity.startActivityForResult(mediaPickerIntent,
				MeobuGalleryPhotosPick);
	}

	public static Object[] onActivityResult(int requestCode, int resultCode,
			Intent data, MeobuActivity activity) {
		if (resultCode == Activity.RESULT_OK) {
			if (data != null) {
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
					if (requestCode == MeobuGalleryPhotosPick) {
						String[] all_path = data.getStringArrayExtra("all_path");
						long[] all_date = data.getLongArrayExtra("all_date");
						long[] all_id = data.getLongArrayExtra("all_id");
						if (all_path == null || all_path.length <= 0) return null;
						//TODO try Uri
//						File[] result = new File[all_path.length];
//						for (int i=0; i<result.length; i++) {
////							result[i] = new Uri.Builder()
////											.path(all_path[i])
////											.appendQueryParameter(MediaStore.Images.Media.DATE_ADDED, Long.toString(all_date[i]))
////											.build();
//							//File a;a.
//							//result[i] = Uri.fromFile(new File(all_path[i]));
//							result[i] = new File(all_path[i]);
//							//Log.d("meobudebug", "selected file date: " + result[i].lastModified());
//						}
						//TODO try new Object
//						Uri[] result = new Uri[all_path.length];
//						for (int i=0; i<result.length; i++) {
//							result[i] = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, all_id[i]);
//						}
						MeobuGalleryItem[] result = new MeobuGalleryItem[all_path.length];
						for (int i=0; i<result.length; i++) {
							MeobuGalleryItem item = new MeobuGalleryItem();
							item.id = all_id[i];
							item.date = all_date[i];
							item.path = all_path[i];
							result[i] = item;
						}
						//TODO deletable, use delete query with id
						return result;
					}
					Uri[] result = new Uri[1];
					result[0] = data.getData();
					//TODO deletable, use content resolver
					return result;
				}//else
				return onActivityResultKitkatAndOver(requestCode, resultCode, data, activity);
//				Uri result = data.getData();
//		        final int takeFlags = data.getFlags()
//		                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
//		                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//		        // Check for the freshest data.
//		        activity.getContentResolver().takePersistableUriPermission(result, takeFlags);
//		        return result;
			}
			return null;
		}
		return null;
	}
	private static Uri[] onActivityResultKitkatAndOver(int requestCode, int resultCode, Intent data, MeobuActivity activity) {
		ClipData clip = data.getClipData();
		if (clip == null) {
//			if (activity.supportMeobuGalleryDeleteAfterPicking()) {
//				deleteUriUsingContentResolver(data.getData(), activity);
//			}
			Uri uri = data.getData();
			try {
			final int takeFlags = data.getFlags()
		            & (Intent.FLAG_GRANT_READ_URI_PERMISSION
		            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
			activity.getContentResolver().takePersistableUriPermission(uri, takeFlags);
			} catch (Exception e) {}
			Uri[] result = new Uri[1];
			result[0] = uri;
			return result;
		}
		final int count = clip.getItemCount();
		Uri[] result = new Uri[count];
		final int takeFlags = data.getFlags()
	            & (Intent.FLAG_GRANT_READ_URI_PERMISSION
	            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		for (int i=0; i<count; i++) {
			ClipData.Item item = clip.getItemAt(i);
			Uri uri = item.getUri();
			try {
			activity.getContentResolver().takePersistableUriPermission(uri, takeFlags);
			} catch (Exception e) { }
			result[i] = uri;
		}
		//TODO deletable, using content resolver
		return result;
	}
	protected static void deleteUriUsingContentResolver(Uri uri, MeobuActivity activity) {//TODO not use
		try {
			DocumentsContract.deleteDocument(activity.getContentResolver(), uri);
			//activity.getContentResolver().delete(uri, null, null);
		} catch (Exception e) {
			Log.d("meobuerror", "delete kitkat media error: " + e.getMessage());
			e.printStackTrace();
		}
	}
	//public static long queryMediaDate(Context context, Uri uri, String key) {
	//public static long queryMediaDate(Context context, Uri uri, int type) {
	public static long queryMediaDate(Context context, Uri uri) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			long result = 0;
			Cursor mediacursor = null;
			try {
				long id = ContentUris.parseId(uri);
				if (id > 0) {
					String[] columns = { MediaStore.Video.Media.DATE_MODIFIED, MediaStore.Video.Media.DATE_ADDED, MediaStore.Video.Media.DATE_TAKEN };//same with MediaStore.Images.Media.DATE_MODIFIED
					String selection = MediaStore.Images.Media._ID + " = ?";
					String[] selectionArgs = new String[] { Long.toString(id) };
					mediacursor = context.getContentResolver().query(uri, columns, selection, selectionArgs, null);
					long temp = 0;
					if (mediacursor.moveToNext()) {
						try {
							int modifyColumnInex = mediacursor.getColumnIndex(columns[0]);
							result = mediacursor.getLong(modifyColumnInex);
						} catch (Exception e) {}

						try {
							int addColumnInex = mediacursor.getColumnIndex(columns[1]);
							temp = mediacursor.getLong(addColumnInex);
							if (temp > result) {
								result = temp;
							}
						} catch (Exception e) {}

						try {
							int takeColumnInex = mediacursor.getColumnIndex(columns[2]);
							temp = mediacursor.getLong(takeColumnInex);
							if (temp > result) {
								result = temp;
							}
						} catch (Exception e) {}
					}
				}
			} catch (Exception e) {
			} finally {
				if (mediacursor != null) try {mediacursor.close();} catch (Exception e) { }
			}
			if (result > 0) {
				return result;
			}
			try {
				String[] columns = { MediaStore.Video.Media.DATE_MODIFIED, MediaStore.Video.Media.DATE_ADDED, MediaStore.Video.Media.DATE_TAKEN };//same with MediaStore.Images.Media.DATE_MODIFIED
				mediacursor = context.getContentResolver().query(uri, columns, null, null, null);
				long temp = 0;
				if (mediacursor.moveToNext()) {
					try {
						int modifyColumnInex = mediacursor.getColumnIndex(columns[0]);
						result = mediacursor.getLong(modifyColumnInex);
					} catch (Exception e) {}

					try {
						int addColumnInex = mediacursor.getColumnIndex(columns[1]);
						temp = mediacursor.getLong(addColumnInex);
						if (temp > result) {
							result = temp;
						}
					} catch (Exception e) {}

					try {
						int takeColumnInex = mediacursor.getColumnIndex(columns[2]);
						temp = mediacursor.getLong(takeColumnInex);
						if (temp > result) {
							result = temp;
						}
					} catch (Exception e) {}
				}
			} catch (Exception e) {
			} finally {
				if (mediacursor != null) try {mediacursor.close();} catch (Exception e) { }
			}
			return result;
		}
		return queryMediaDate16AndOver(context, uri);
	}
	@TargetApi(19)
	public static long queryMediaDate16AndOver(Context context, Uri uri) {
		long result = 0;
		String[] columns = { DocumentsContract.Document.COLUMN_LAST_MODIFIED };
		Cursor mediacursor = context.getContentResolver().query(uri, columns, null, null, null);
		if (mediacursor.moveToNext()) {
			int dateColumnInex = mediacursor.getColumnIndex(columns[0]);
			result = mediacursor.getLong(dateColumnInex);
		}
		try {mediacursor.close();} catch (Exception e) { }
		return result;
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//

}
