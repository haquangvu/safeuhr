package meobu.android.base;

import java.util.ArrayList;

/**
 * Approach: use ListView to display Sections on GridView.
 * Default: desc, 4 items per line
 * Improve: hashtable (header as key, array/index-in-list as value)
 * Improve: how to convert to SectionListView easily
 * Usage: implements Data interface, then add here
 * @author meobu
 */
public class MeobuSectionGridViewDataManager {

	//----------//----------//----------//----------//----------//----------//----------//

	protected final ArrayList<ViewData> listViewData;
	public MeobuSectionGridViewDataManager() {
		listViewData = new ArrayList<ViewData>();
	}

	//----------//----------//----------//----------//----------//----------//----------//PURPOSE method
	
	public ArrayList<ViewData> getListViewData() {
		return listViewData;
	}

	//----------//----------//----------//----------//----------//----------//----------//FUNCTION method
	
	public void add(Data data) {
		if (listViewData.size() <= 1) {
			//just empty view
			//clear & add group
			listViewData.clear();
			addNewHeaderContent(data, listViewData);
		} else {
			//find group
			FindResult find = find(data.getHeaderValue(), listViewData);
//			int startGroup = -1;
//			int nextGroup = -1;
//			for (int i=0; i<listViewData.size(); i++) {
//				if (listViewData.get(i) instanceof HeaderViewData) {
//					ContentViewData cvd = (ContentViewData)listViewData.get(i+1);
//					int result = compare(cvd.getHeaderValue(), data.getHeaderValue());
//					if (result == 0) {
//						//same group
//						//stop here & find
//						startGroup = i;
//						break;
//					} else if (result < 0) {
//						//smaller
//						//stop here & add header content
//						nextGroup = i;
//						break;
//					}
//				}
//			}
			if (find.startGroup < 0) {
				//not exist same group
				if (find.nextGroup < 0) {
					//not exist older group
					//create last group
					addNewHeaderContent(-1, data, listViewData);//add to last
				} else {
					//exist older group
					//create new group above
					addNewHeaderContent(find.nextGroup, data, listViewData);//add to above older group
				}
			} else {
				//found same day group
				//find startItem, indexItem
				FindValueResult findValue = findValue(find.startGroup, data.getValue(), listViewData);
//				int startItem = -1, indexItem = -1;
//				int nextItem = -1;
//				for (int i=find.startGroup+1; i<listViewData.size(); i++) {
//					ViewData vd = listViewData.get(i);
//					if (vd instanceof HeaderViewData) {
//						//out of group
//						nextItem = i;
//						break;
//					}
//					ContentViewData cvd = (ContentViewData)vd;
//					if (compare(cvd.getFirstValue(), data.getValue()) <= 0) {
//						//data is newer
//						//add at 0
//						startItem = i;
//						indexItem = 0;
//						break;
//					}
//					for (int j=1; j<cvd.getData().size(); j++) {
//						if (compare(cvd.getData().get(j).getValue(), data.getValue()) <= 0) {
//							//data is newer
//							//add here
//							startItem = i;
//							indexItem = j;
//							break;
//						}
//					}
//					if (startItem < 0 || indexItem < 0) {
//						if (!full(cvd.getData())) {
//							//not full item
//							//add here
//							startItem = i;
//							indexItem = cvd.getData().size();
//							break;
//						}
//						//contain 4 and the last still newer
//						//go next line
//					} else {
//						//did found slot
//						break;
//					}
//				}
				//check result
				if (findValue.startItem < 0 || findValue.indexItem < 0) {
					//did not found, last line contains 4 and still newer
					//add new Content View Data and DONE
					addNewContent(findValue.nextItem, data, listViewData);
				} else {
					//the position is in the middle of crowd
					//add, pop, add
					Data outItem = data;
					int i = findValue.startItem;
					while (outItem != null && i < listViewData.size() && listViewData.get(i) instanceof ContentViewData) {
						ContentViewData cvd = (ContentViewData)listViewData.get(i);
						if (full(cvd.getData())) {
							cvd.getData().add(findValue.indexItem, outItem);
							outItem = cvd.getData().remove(cvd.getData().size() - 1);
						} else {
							cvd.getData().add(findValue.indexItem, outItem);
							outItem = null;
						}
						findValue.indexItem = 0;
						i++;
					}
					if (outItem == null) {
						//added to last group
					} else if (i >= listViewData.size()) {
						//out of the only 1 group
						//did process above: //did not found, last line contains 4 and still newer
						addNewContent(outItem, listViewData);
					} else {
						//out of a group which is above another group
						//did process above: //did not found, last line contains 4 and still newer
						addNewContent(i, outItem, listViewData);
					}
				}
			}
		}
	}
	
	public void remove(Data data) {
		if (listViewData.size() <= 1) {
			//list data null
			//list data empty or contain only empty view data
			return;
		}
		//find the day
		FindResult find = find(data.getHeaderValue(), listViewData);
		if (find.startGroup < 0) {
			return;
		}
		//find index & item view data
		FindValueResult findValue = findValue(find.startGroup, data.getValue(), listViewData);
		if (findValue.startItem < 0 || findValue.indexItem < 0 || !findValue.exact) {
			//Log.d("meobudebug", "removeListViewData   Cannot find the item line or item index of given item: " + data.getDataId());
			return;
		}
		//remove & re-arrange
		int previousLineIndex = findValue.startItem;
		ArrayList<Data> previousLineItems = ((ContentViewData)listViewData.get(findValue.startItem)).getData();
		previousLineItems.remove(findValue.indexItem);
		for (int i=findValue.startItem+1; i<listViewData.size(); i++) {
			ViewData vd = listViewData.get(i);
			if (vd instanceof HeaderViewData) {
				//done
				break;
			}
			ContentViewData line = (ContentViewData)vd;
			//@condition: items has data, for sure this time 
			Data ivd = line.getData().remove(0);
			previousLineItems.add(ivd);
			previousLineItems = line.getData();
			previousLineIndex = i;
		}
		if (previousLineItems.isEmpty()) {
			//1 line empty
			//remove that lineout
			listViewData.remove(previousLineIndex);
			
			if (listViewData.size() < 2) {
				//no item here, only the last header
				//clear & add empty
				listViewData.clear();
				listViewData.add(new EmptyViewData());
			} else if (previousLineIndex == find.startGroup + 1) {
				//last line of this group is removed
				//remove group header
				listViewData.remove(find.startGroup);
			}
		}
	}

	//----------//----------//----------//----------//----------//----------//----------//PUBLIC class
	
	public static interface Data {
		public int getValue();
		public int getHeaderValue();
	}
	
	public static interface ViewData {
		public int getViewType();
	}
	
	public enum ViewDataType {
		Empty,
		Header,
		Content,
	}
	
	public static interface HeaderViewData extends ViewData {
		public int getHeaderValue();
	}

	public static interface ContentViewData extends ViewData {
		public int getHeaderValue();
		public int getFirstValue();
		public ArrayList<Data> getData();
	}

	//----------//----------//----------//----------//----------//----------//----------//INTERNAL class
	
	protected static class EmptyViewData implements ViewData {
		public int getViewType() {
			return ViewDataType.Empty.ordinal();
		}
	}
	
	protected static class ImplHeaderViewData implements ViewData {
		//----------//----------//----------//----------//----------//
		private final int value;
		protected ImplHeaderViewData(int value) {
			this.value = value;
		}
		//----------//----------//----------//----------//----------//
		public int getViewType() {
			return ViewDataType.Header.ordinal();
		}
		public int getHeaderValue() {
			return value;
		}
		//----------//----------//----------//----------//----------//
	}
	
	protected static class ImplContentViewData implements ContentViewData {
		//----------//----------//----------//----------//----------//
		private final ArrayList<Data> data;
		protected ImplContentViewData() {
			data = new ArrayList<Data>();
		}
		//----------//----------//----------//----------//----------//
		public int getViewType() {
			return ViewDataType.Content.ordinal();
		}
		public int getHeaderValue() {
			if (data.isEmpty()) return 0;
			return data.get(0).getHeaderValue();
		}
		public int getFirstValue() {
			if (data.isEmpty()) return 0;
			return data.get(0).getValue();
		}
		public ArrayList<Data> getData() {
			return data;
		}
		//----------//----------//----------//----------//----------//
	}
	
	//----------//----------//----------//----------//----------//----------//----------//INTERNAL method
	
	private int compare(int value1, int value2) {
		return value1 - value2;
	}
	private boolean full(ArrayList<Data> list) {
		return list.size() >= 4;
	}

	private static ContentViewData addNewHeaderContent(Data data, ArrayList<ViewData> list) {
		return addNewHeaderContent(-1, data, list);
	}
	private static ContentViewData addNewHeaderContent(int index, Data data, ArrayList<ViewData> list) {
		//add group header
		ImplHeaderViewData newHeaderData = new ImplHeaderViewData(data.getHeaderValue());
		//add group content
		ImplContentViewData newContentData = new ImplContentViewData();
		newContentData.getData().add(data);
		if (index < 0 || index > list.size()) {
			list.add(newHeaderData);
			list.add(newContentData);
		} else {
			list.add(index, newContentData);
			list.add(index, newHeaderData);
		}
		return newContentData;
	}
	
	private static ContentViewData addNewContent(Data data, ArrayList<ViewData> list) {
		return addNewContent(-1, data, list);
	}
	private static ContentViewData addNewContent(int index, Data data, ArrayList<ViewData> list) {
		//add group content
		ImplContentViewData newContentData = new ImplContentViewData();
		newContentData.getData().add(data);
		if (index < 0 || index > list.size()) {
			list.add(newContentData);
		} else {
			list.add(index, newContentData);
		}
		return newContentData;
	}
	
//	private int findGroupHeader(Data data, ArrayList<ViewData> list) {
//		return findGroupHeader(data, list, false);
//	}
//	private int findGroupHeader(Data data, ArrayList<ViewData> list, boolean useNearest) {
//		int startGroup = -1;
//		for (int i=0; i<list.size(); i++) {
//			if (list.get(i) instanceof HeaderViewData) {
//				ContentViewData cvd = (ContentViewData)list.get(i+1);
//				int result = compare(cvd.getHeaderValue(), data.getHeaderValue());
//				if (result == 0) {
//					//correct
//					startGroup = i;
//					break;
//				} else if (result < 0 && useNearest) {
//					startGroup = i;
//					break;
//				}
//			}
//		}
//		return startGroup;
//	}
	
	private static class FindResult {
		int startGroup = -1, nextGroup = -1;
	}
	private FindResult find(int header, ArrayList<ViewData> list) {
		FindResult result = new FindResult();
		for (int i=0; i<list.size(); i++) {
			if (list.get(i) instanceof HeaderViewData) {
				ContentViewData cvd = (ContentViewData)list.get(i+1);
				int compare = compare(cvd.getHeaderValue(), header);//cannot go static because of ORDER
				if (compare == 0) {
					//same group
					//stop here & find
					result.startGroup = i;
					break;
				} else if (compare < 0) {
					//smaller
					//stop here & add header content
					result.nextGroup = i;
					break;
				}
			}
		}
		return result;
	}
	
	private static class FindValueResult {
		int startItem = -1, indexItem = -1, nextItem = -1;
		boolean exact = false;
	}
	private FindValueResult findValue(int startGroup, int value, ArrayList<ViewData> list) {
		FindValueResult find = new FindValueResult();
		for (int i=startGroup+1; i<listViewData.size(); i++) {
			ViewData vd = listViewData.get(i);
			if (vd instanceof HeaderViewData) {
				//out of group
				find.nextItem = i;
				break;
			}
			ContentViewData cvd = (ContentViewData)vd;
			for (int j=0; j<cvd.getData().size(); j++) {
				int compare = compare(cvd.getData().get(j).getValue(), value);
				if (compare <= 0) {//CANNOT go static
					//data is newer
					//add here
					find.startItem = i;
					find.indexItem = j;
					find.exact = compare == 0;
					break;
				}
			}
			if (find.startItem < 0 || find.indexItem < 0) {
				if (!full(cvd.getData())) {
					//not full item
					//add here
					find.startItem = i;
					find.indexItem = cvd.getData().size();
					break;
				}
				//contain 4 and the last still newer
				//go next line
			} else {
				//did found slot
				break;
			}
		}
		return find;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
}
