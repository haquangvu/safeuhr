package meobu.android.base;

import java.util.Hashtable;
import java.util.Locale;

import meobu.android.base.MeobuSettingsDataSource.MeobuSettingsContent;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import android.annotation.TargetApi;
import android.app.Application;
import android.os.AsyncTask;
import android.os.Build;

public class MeobuApplication extends Application {
	
//	static MeobuApplication instance;

	@Override
	public void onCreate() {
		super.onCreate();
//		instance = this;
		prepareMeobuLanguage();
		prepareMeobuSoundPool();
		prepareMeobuImageLoader();
		prepareMeobuSettings();
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	public boolean supportMeobuImageLoader() {
		return false;
	}
	private void prepareMeobuImageLoader() {
		if (supportMeobuImageLoader()) {
			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(getApplicationContext());
			if (supportImageLoaderCustomDownloader()) {
				builder.imageDownloader(createCustomDownloader());
			}
			if (supportImageLoaderCacheOnDisc()) {
				DisplayImageOptions dio = new DisplayImageOptions.Builder().cacheOnDisc(true).build();
				builder.defaultDisplayImageOptions(dio);
			}
			ImageLoaderConfiguration config = builder.build();
			ImageLoader.getInstance().init(config);
		}
	}
	protected boolean supportImageLoaderCacheOnDisc() {
		return false;
	}
	protected boolean supportImageLoaderCustomDownloader() {
		return false;
	}
	protected BaseImageDownloader createCustomDownloader() {
		return null;
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private void prepareMeobuLanguage() {
		if (supportMeobuLanguage()) {
			sharedMeobuLanguage().activate(this);
		}
	}
	protected boolean supportMeobuLanguage() {
		return false;
	}
	private MeobuLanguageHandler sharedMeobuLanguage() {
		return MeobuLanguageHandler.getInstance();
	}
	public Locale getLocaleWithLanguageIndex(int index) {
		return null;
	}
	public void updateMeobuLanguage() {
		if (supportMeobuLanguage()) {
			sharedMeobuLanguage().updateLanguage(this);
		}
	}
	public void setMeobuLanguage(int index) {
		sharedMeobuLanguage().setLanguageIndex(this, index);
	}
	public int getMeobuLanguage() {
		return sharedMeobuLanguage().languageIndex;
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private void prepareMeobuSoundPool() {
		if (supportMeobuSoundPool()) {
			sharedMeobuSoundPool().activate(this);
		}
	}
	public boolean supportMeobuSoundPool() {
		return false;
	}
	private MeobuSoundPoolHandler sharedMeobuSoundPool() {
		return MeobuSoundPoolHandler.getInstance();
	}
	public int[] getSoundResourceId() {
		return new int[0];
	}
	public void playMeobuSoundPool(int index) {
		if (supportMeobuSoundPool()) {
			sharedMeobuSoundPool().play(this, index);
		}
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected boolean isSupportMeobuSettings() {
		return false;
	}
	private void prepareMeobuSettings() {
		if (!isSupportMeobuSettings()) return;
		final AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... params) {
				MeobuSettingsDataSource ds = newMeobuSettingsDataSource();
				ds.open();
				setMeobuSettings(ds.getAll());
				ds.close();
				return 0;
			}
		};
		executeVoidVoidInteger(task);
	}
	protected void saveMeobuSettings() {
		if (!isSupportMeobuSettings()) return;
		final AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... params) {
				MeobuSettingsDataSource ds = newMeobuSettingsDataSource();
				ds.open();
				ds.saveAll(getMeobuSettings());
				ds.close();
				return 0;
			}
		};
		executeVoidVoidInteger(task);
	}
	protected MeobuSettingsDataSource newMeobuSettingsDataSource() { return null; }
	protected void setMeobuSettings(Hashtable<String, MeobuSettingsContent> settings) { }
	protected Hashtable<String, MeobuSettingsContent> getMeobuSettings() { return null; }
	public void setMeobuSettings(String key, Object value) {
		if (isSupportMeobuSettings()) {
			if (getMeobuSettings().containsKey(key)) {
				try {
					if (value == null) {
						getMeobuSettings().get(key).value = null;
					} else if (value instanceof String) {
						getMeobuSettings().get(key).value = (String)value;
					} else if (value instanceof Boolean) {
						getMeobuSettings().get(key).value = Boolean.toString(((Boolean)value));
					} else if (value instanceof Integer) {
						getMeobuSettings().get(key).value = Integer.toString(((Integer)value));
					}
				} catch (Exception e) {}
			} else {
				if (value != null) {
					try {
						MeobuSettingsContent content = new MeobuSettingsContent();
						content.name = key;
						if (value instanceof String) {
							content.value = (String)value;
						} else if (value instanceof Boolean) {
							content.value = Boolean.toString((Boolean)value);
						} else if (value instanceof Integer) {
							content.value = Integer.toString((Integer)value);
						}
						getMeobuSettings().put(key, content);
					} catch (Exception e) {}
				}
			}
		}
	}
	public String getMeobuSettingsString(String key) {
		if (isSupportMeobuSettings()) {
			try {
				return getMeobuSettings().get(key).value;
			} catch (Exception e) { }
		}
		return null;
	}
	public boolean getMeobuSettingsBoolean(String key) {
		if (isSupportMeobuSettings()) {
			try {
				return Boolean.parseBoolean(getMeobuSettings().get(key).value);
			} catch (Exception e) { }
		}
		return false;
	}
	public int getMeobuSettingsInt(String key) {
		if (isSupportMeobuSettings()) {
			try {
				return Integer.parseInt(getMeobuSettings().get(key).value);
			} catch (Exception e) { }
		}
		return 0;
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected static void executeVoidVoidInteger(AsyncTask<Void, Void, Integer> task) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			executeVoidVoidIntegerBelow11(task);
		} else {
			executeVoidVoidInteger11AndOver(task);
		}
	}
	protected static void executeVoidVoidIntegerBelow11(AsyncTask<Void, Void, Integer> task) {
		task.execute();
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected static void executeVoidVoidInteger11AndOver(AsyncTask<Void, Void, Integer> task) {
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
	
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
}
