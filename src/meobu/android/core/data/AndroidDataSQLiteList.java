package meobu.android.core.data;

import java.util.ArrayList;

import android.database.Cursor;

public class AndroidDataSQLiteList<T extends AndroidDataSQLite> extends AndroidDataSQLite {
	
	protected ArrayList<T> list;
	protected ArrayList<T> list() {
		if (list == null) {
			list = new ArrayList<T>();
		}
		return list;
	}
	
	public T create() { return null; }
	public void set(Cursor cursor) {
		if (cursor == null || cursor.getCount() <= 0) return;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			T t = create();
			t.set(cursor);
			list().add(t);
		}
	}

}
