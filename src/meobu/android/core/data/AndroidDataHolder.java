package meobu.android.core.data;

import meobu.core.data.MeobuData;
import meobu.core.data.MeobuDataHolder;
import meobu.core.data.MeobuDataLoader;

public class AndroidDataHolder<T extends MeobuData> extends MeobuDataHolder<T> {

	public AndroidDataHolder(MeobuDataLoader<T> loader) {
		super(loader);
	}

	@Override
	protected void doPush2Thread() {
		final Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				set(loader().load());
			}
		});
		thread.start();
	}

}
