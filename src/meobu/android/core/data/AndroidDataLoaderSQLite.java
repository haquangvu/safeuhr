package meobu.android.core.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import meobu.core.data.sqlite.MeobuDataLoaderSQLite;

public abstract class AndroidDataLoaderSQLite<T extends AndroidDataSQLite> extends MeobuDataLoaderSQLite<T> {
	
	private final SQLiteOpenHelper helper;
	public AndroidDataLoaderSQLite(String table, String[] keys, String[] params, SQLiteOpenHelper helper) {
		super(helper.getDatabaseName(), table, keys, params);
		this.helper = helper;
	}

	@Override
	public T load() {
		T data = null;
		
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = helper.getReadableDatabase();
			cursor = db.query(table, keys, params[PARAMS_INDEX_WHERE], null,
					params[PARAMS_INDEX_GROUPBY], params[PARAMS_INDEX_HAVING],
					params[PARAMS_INDEX_ORDER], params[PARAMS_INDEX_LIMIT]);
			data = create();
			data.set(cursor);
		} catch (Exception e) {
		} finally {
			if (cursor != null) try { cursor.close(); } catch (Exception e) { }
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		update(false);
		return data;
	}

}
