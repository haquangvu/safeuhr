package meobu.android.core.data;

import android.database.Cursor;
import meobu.core.data.sqlite.MeobuDataSQLite;

public abstract class AndroidDataSQLite extends MeobuDataSQLite {
	
	public abstract void set(Cursor cursor);
	
}
