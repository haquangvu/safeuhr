package meobu.core.data.sqlite;

import meobu.core.data.MeobuDataLoader;

public abstract class MeobuDataLoaderSQLite<T extends MeobuDataSQLite> extends MeobuDataLoader<T> {

	protected final String database, table;
	protected final String[] keys, params;
	public static final int PARAMS_INDEX_WHERE = 0;
	public static final int PARAMS_INDEX_GROUPBY = 1;
	public static final int PARAMS_INDEX_HAVING = 2;
	public static final int PARAMS_INDEX_ORDER = 3;
	public static final int PARAMS_INDEX_LIMIT = 4;
	public MeobuDataLoaderSQLite(String database, String table, String[] keys, String[] params) {
		this.database = database;
		this.table = table;
		this.keys = keys;
		this.params = params;
	}
	
}
