package meobu.core.data;

public abstract class MeobuDataHolder<T extends MeobuData> {

	private final MeobuDataLoader<T> loader;
	public MeobuDataHolder(MeobuDataLoader<T> loader) {
		this.loader = loader;
		loader.update(false);
	}
	
	private T data;
	public boolean isNull() {
		return data == null;
	}
	public T get() {
		if (data == null) {
			push2thread();
		}
		return data;
	}
	public void set(T data) {
		this.data = data;
	}
	
	public MeobuDataLoader<T> loader() {
		return loader;
	}
	private void push2thread() {
		if (loader.loading()) return;
		loader.update(true);
		doPush2Thread();
	}
	public MeobuDataStatus status() {
		return loader.status();
	}

	//platform override
	protected abstract void doPush2Thread();
}
