package meobu.core.data;

public abstract class MeobuDataLoader<T extends MeobuData> {
	
	private MeobuDataStatus status;
	public MeobuDataStatus status() {
		if (status == null) {
			status = new MeobuDataStatus();
		}
		return status;
	}
	
	protected boolean loading;
	public boolean loading() {
		return loading;
	}
	public void update(boolean loading) {
		this.loading = loading;
	}
	
	protected abstract T create();
	
	//platform override
	//should be a synchronize function
	public abstract T load();
}
