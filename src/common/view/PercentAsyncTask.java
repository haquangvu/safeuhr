package common.view;

import android.os.AsyncTask;

public abstract class PercentAsyncTask<Params, Result> extends AsyncTask<Params, Integer, Result> {
	public void doPublishProgressPercent(int encrypted, int filesize) {
		super.publishProgress(0, encrypted, filesize);
	}
	public void doPublishLog(int line) {
		super.publishProgress(9999, line);
	}
}