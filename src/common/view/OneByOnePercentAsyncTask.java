package common.view;

public abstract class OneByOnePercentAsyncTask<Params, Result> extends PercentAsyncTask<Params, Result> {
	protected int index, total;
	public void doPublishProgressFull(int encrypted, int filesize) {
		super.publishProgress(1, index, total, encrypted, filesize);
	}
	@Override
	public void doPublishProgressPercent(int encrypted, int filesize) {
		super.publishProgress(0, index, total, encrypted, filesize);
	}
}
