package com.luminous.pick;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.protectstar.safeuhr.android.R;

import java.util.ArrayList;
import java.util.Collections;

public class CustomGalleryActivity extends Activity {

	GridView gridGallery;
	Handler handler;
	GalleryAdapter adapter;

	ImageView imgNoMedia;
	Button btnGalleryOk;

	String action;
	boolean video;
    private ImageLoader imageLoader;

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gallery);

		action = getIntent().getAction();
		if (action == null) {
			finish();
		}
		if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Action.EXTRA_VIDEO_ONLY)) {
			video = getIntent().getExtras().getBoolean(Action.EXTRA_VIDEO_ONLY, false);
		}
        initImageLoader();
		init();
	}

    private void initImageLoader() {
//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
//                .cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
//                .bitmapConfig(Bitmap.Config.RGB_565).build();
//        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
//                this).defaultDisplayImageOptions(defaultOptions).memoryCache(
//                new WeakMemoryCache());
//
//        ImageLoaderConfiguration config = builder.build();
        imageLoader = ImageLoader.getInstance();
//        imageLoader.init(config);
    }

	private void init() {
		handler = new Handler();
		gridGallery = (GridView) findViewById(R.id.gridGallery);
		gridGallery.setFastScrollEnabled(true);
		adapter = new GalleryAdapter(getApplicationContext(), imageLoader);

		if (action.equalsIgnoreCase(Action.ACTION_MULTIPLE_PICK)) {

			findViewById(R.id.llBottomContainer).setVisibility(View.VISIBLE);
			gridGallery.setOnItemClickListener(mItemMulClickListener);
			adapter.setMultiplePick(true);

		} else if (action.equalsIgnoreCase(Action.ACTION_PICK)) {

			findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
			gridGallery.setOnItemClickListener(mItemSingleClickListener);
			adapter.setMultiplePick(false);

		}

		gridGallery.setAdapter(adapter);
		imgNoMedia = (ImageView) findViewById(R.id.imgNoMedia);

		btnGalleryOk = (Button) findViewById(R.id.btnGalleryOk);
		btnGalleryOk.setOnClickListener(mOkClickListener);

		new Thread() {

			@Override
			public void run() {
				Looper.prepare();
				handler.post(new Runnable() {

					@Override
					public void run() {
						if (video) {
							adapter.addAll(getGalleryVideos());
						} else {
							adapter.addAll(getGalleryPhotos());
						}
						checkImageStatus();
					}
				});
				Looper.loop();
			};

		}.start();

	}

	private void checkImageStatus() {
		if (adapter.isEmpty()) {
			imgNoMedia.setVisibility(View.VISIBLE);
		} else {
			imgNoMedia.setVisibility(View.GONE);
		}
	}

	View.OnClickListener mOkClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			ArrayList<CustomGallery> selected = adapter.getSelected();

			String[] allPath = new String[selected.size()];
			long[] allDate = new long[selected.size()];
			long[] allId = new long[selected.size()];
			for (int i = 0; i < allPath.length; i++) {
				CustomGallery item = selected.get(i);
				if (item instanceof CustomGallery4Video) {
					allPath[i] = ((CustomGallery4Video)item).videoSDCardPath;
				} else {
					allPath[i] = selected.get(i).sdcardPath;
				}
				allDate[i] = selected.get(i).createdDate;
				allId[i] = selected.get(i).id;
			}

			Intent data = new Intent().putExtra("all_path", allPath).putExtra("all_date", allDate).putExtra("all_id", allId);
			setResult(RESULT_OK, data);
			finish();

		}
	};
	AdapterView.OnItemClickListener mItemMulClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
			adapter.changeSelection(v, position);

		}
	};

	AdapterView.OnItemClickListener mItemSingleClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
			CustomGallery item = adapter.getItem(position);
			Intent data = new Intent().putExtra("single_path", item.sdcardPath);
			setResult(RESULT_OK, data);
			finish();
		}
	};

	private ArrayList<CustomGallery> getGalleryPhotos() {
		ArrayList<CustomGallery> galleryList = new ArrayList<CustomGallery>();

		try {
			final String[] columns = { MediaStore.Images.Media.DATA,
					MediaStore.Images.Media.DATE_TAKEN,
					MediaStore.Images.Media._ID };
			final String orderBy = MediaStore.Images.Media._ID;

			Cursor imagecursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
			//@SuppressWarnings("deprecation")
			//Cursor imagecursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
			if (imagecursor != null && imagecursor.getCount() > 0) {

				while (imagecursor.moveToNext()) {
					CustomGallery item = new CustomGallery();

					int idColumnIndex = imagecursor
							.getColumnIndex(MediaStore.Images.Media._ID);
					item.id = imagecursor.getLong(idColumnIndex);

					int dataColumnIndex = imagecursor
							.getColumnIndex(MediaStore.Images.Media.DATA);
					item.sdcardPath = imagecursor.getString(dataColumnIndex);
					//Log.d("meobudebug", "sdcardpath: " + item.sdcardPath);
					
					int dateColumnInex = imagecursor
							.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
					item.createdDate = imagecursor.getLong(dateColumnInex);
					
					galleryList.add(item);
				}
			}
			imagecursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

        // show newest photo at beginning of the list
		Collections.reverse(galleryList);
        return galleryList;
	}
	
	private ArrayList<CustomGallery> getGalleryVideos() {
		ArrayList<CustomGallery> galleryList = new ArrayList<CustomGallery>();

		try {
			final String[] columns = { MediaStore.Video.Media.DATA,
					MediaStore.Video.Media.DATE_TAKEN,
					MediaStore.Video.Media.MINI_THUMB_MAGIC,
					MediaStore.Video.Media._ID };
			final String orderBy = MediaStore.Video.Media._ID;

			Cursor videocursor = getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
			if (videocursor != null && videocursor.getCount() > 0) {

				while (videocursor.moveToNext()) {
					CustomGallery4Video item = new CustomGallery4Video();

					int idColumnIndex = videocursor
							.getColumnIndex(MediaStore.Images.Media._ID);
					item.id = videocursor.getLong(idColumnIndex);

					int dataColumnIndex = videocursor.getColumnIndex(MediaStore.Video.Media.DATA);
					item.videoSDCardPath = videocursor.getString(dataColumnIndex);
					
					int dateColumnIndex = videocursor.getColumnIndex(MediaStore.Video.Media.DATE_TAKEN);
					item.createdDate = videocursor.getLong(dateColumnIndex);
					
					int thumbColumnIndex = videocursor.getColumnIndex(MediaStore.Video.Media.MINI_THUMB_MAGIC);
					int thumbId = videocursor.getInt(thumbColumnIndex);
					item.sdcardPath = getGalleryVideoThumbnail(thumbId);
					
					galleryList.add(item);
				}
			}
			videocursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

        // show newest photo at beginning of the list
		Collections.reverse(galleryList);
        return galleryList;
	}
	
	private String getGalleryVideoThumbnail(int thumbid) {
		Cursor thumbcursor = null;
		try {
			final Uri uri = MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI;
			final String[] columns = { MediaStore.Video.Thumbnails.DATA };
			String selection = MediaStore.Video.Thumbnails._ID + " = ?";
			String[] args =  new String[]{ Integer.toString(thumbid) };
			thumbcursor = getContentResolver().query(uri, columns, selection, args, null);
			if( thumbcursor.moveToFirst() ){
	            return thumbcursor.getString(thumbcursor.getColumnIndex(MediaStore.Video.Thumbnails.DATA));
			}
		} catch (Exception e) {
			Log.w("meobuwarning", "getGalleryVideoThumbnail:   " + thumbid + "   error:" + e.getMessage());
		} finally {
			if (thumbcursor != null) {
				try { thumbcursor.close(); } catch (Exception e) { }
			}
		}
		return "";
	}

}
