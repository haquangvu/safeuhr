package com.protectstar.timelock.pro.android.activity;

import java.util.Calendar;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.animation.Animation;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.alarm.AlarmService;
import com.protectstar.timelock.pro.android.data.ClockData;

public class ClockActivity3_Alarm extends ClockActivity1_Analog {

	private TimeInfo updatingInfo;
	protected TimeInfo getUpdatingInfo() {
		if (updatingInfo == null) {
			updatingInfo = new TimeInfo();
		}
		return updatingInfo;
	}
	protected boolean alarmState;
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//

	private float alarmCurrentValue;
	protected void updateAlarm(float alarmNew, float alarmOld) {
		if (alarmOld < 0) {
			//alarm moving
			if (alarmCurrentValue == alarmNew) {
				//do nothing
				onUpdateFinish();
			} else {
				//do update, set value
				doUpdateAlarm(alarmNew, -1);
				alarmCurrentValue = alarmNew;
			}
		} else {
			//alarm animating
			doUpdateAlarm(alarmNew, alarmOld);
			alarmCurrentValue = alarmNew;

			updateAlarmNotification();
		}
		float alarmNewRound = Math.round(alarmNew);
		getUpdatingInfo().am = true;
		getUpdatingInfo().hour = (float)Math.floor(alarmNewRound / 5);
		getUpdatingInfo().minute = (alarmNewRound % 5) * 12;
		getUpdatingInfo().second = 0;
		doUpdateNumbers(getUpdatingInfo());
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	private void updateAlarmNotification() {
		if (alarmState) {
			long notificationDate = nextAlarmByMinuteIndicator((int)alarmCurrentValue);
			setAlarmNotification(notificationDate);
			//always call because Alarm Service did check if the same value is 
		} else {
			setAlarmNotification(0);
		}
	}
	
	private long nextAlarmByMinuteIndicator(int value) {
		int hour = (int)Math.floor(alarmCurrentValue / 5);
		int minute = (int)(alarmCurrentValue % 5) * 12;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		//not am
		if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
			calendar.set(Calendar.HOUR_OF_DAY, hour + 12);
			//not pm
			if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
				//tomorrow
				calendar.set(Calendar.HOUR_OF_DAY, hour);
				long nextday = calendar.getTimeInMillis() + 86400000l;//= 1000l*60*60*24;
				return nextday;
			}
		}
		return calendar.getTimeInMillis();
	}
	
	private void setAlarmNotification(long time) {
        Intent service = new Intent(this, AlarmService.class);
        if (time <= 0) {
        	service.setAction(AlarmService.CANCEL);
        } else {
        	service.setAction(AlarmService.CREATE);
        }
        service.putExtra("notificationDate", time);
        this.startService(service);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	void doUpdateAlarm(float alarmNew, float alarmOld) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			doUpdateAlarmBelow11(alarmNew, alarmOld);
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			doUpdateAlarmBelow12(alarmNew, alarmOld);
		} else { //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			doUpdateAlarm12AndOver(alarmNew, alarmOld);
		}
	}
	private void doUpdateAlarmBelow11(float alarmNew, float alarmOld) {
		float minute = ClockData.minuteDegree(alarmNew) + 90;
		float minutePast = minute;
		int duration = 0;
		if (alarmOld >= 0) {
			minutePast = ClockData.minuteDegree(alarmOld) + 90;
			duration = 300;
		}
		Animation minuteAnim = createAnimation(minutePast, minute, getCenterPosition(), getCenterPositionY(), duration);
		minuteAnim.setAnimationListener(animListener);
		View alarmView = findViewById(R.id.clockAlarmView);
		if (alarmView.getVisibility() != View.GONE) {
			alarmView.startAnimation(minuteAnim);
		}
	}
	private void doUpdateAlarmBelow12(float alarmNew, float alarmOld) {
		doUpdateAlarmBelow11(alarmNew, alarmOld);
	}
	private void doUpdateAlarm12AndOver(float alarmNew, float alarmOld) {
		doUpdateAlarmBelow11(alarmNew, alarmOld);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
}
