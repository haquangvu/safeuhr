package com.protectstar.timelock.pro.android.activity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;

import com.protectstar.timelock.pro.android.ActivityVoiceRecord;
import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.TimeLockActivity;

public class ActivityVoiceRecord_Record extends TimeLockActivity implements View.OnClickListener {
	
	//----------//----------//----------//----------//----------//
	
	public static final String VOICE_NAME = "voicename";
	public static final String VOICE_PATH = "voicepath";
	public static String getPath(Intent data) {
		return data.getStringExtra(VOICE_PATH);
	}
	public static String getName(Intent data) {
		return data.getStringExtra(VOICE_NAME);
	}
	public static Intent createIntent(Context context) {
		return createIntent(context, null, false);
	}
	public static Intent createIntent(Context context, String path) {
		return createIntent(context, path, false);
	}
	public static Intent createIntent(Context context, boolean inputName) {
		return createIntent(context, null, inputName);
	}
	public static Intent createIntent(Context context, String path, boolean inputName) {
		Intent intent = new Intent(context, ActivityVoiceRecord.class);
		if (path != null && validateVoicePath(path)) {
			intent.putExtra(VOICE_PATH, path);
		}
		intent.putExtra(VOICE_NAME, inputName);
		return intent;
	}
	private static boolean validateVoicePath(String path) {
		return true;
	}
	
	//----------//----------//----------//----------//----------//

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//
	
	protected enum ScreenState {
		StartANewRecord,
		Prepared,
		Started,
		Stopped
	}
	private void setState(ScreenState state) {
		Chronometer duration = (Chronometer)findViewById(R.id.duration);
		switch (state) {
		case StartANewRecord:
			findViewById(R.id.record).setVisibility(View.INVISIBLE);
			findViewById(R.id.stop).setVisibility(View.INVISIBLE);
			findViewById(R.id.loading).setVisibility(View.VISIBLE);
			duration.setVisibility(View.INVISIBLE);
			break;
		case Prepared:
			findViewById(R.id.record).setVisibility(View.VISIBLE);
			findViewById(R.id.stop).setVisibility(View.INVISIBLE);
			findViewById(R.id.loading).setVisibility(View.INVISIBLE);
			duration.setBase(SystemClock.elapsedRealtime());
			duration.setVisibility(View.VISIBLE);
			break;
		case Started:
			findViewById(R.id.record).setVisibility(View.INVISIBLE);
			findViewById(R.id.stop).setVisibility(View.VISIBLE);
			findViewById(R.id.loading).setVisibility(View.VISIBLE);
			duration.setBase(SystemClock.elapsedRealtime());
			duration.start();
			break;
		case Stopped:
		default:
			findViewById(R.id.record).setVisibility(View.INVISIBLE);
			findViewById(R.id.stop).setVisibility(View.INVISIBLE);
			findViewById(R.id.loading).setVisibility(View.INVISIBLE);
			duration.stop();
			break;
		}
	}
	
	//----------//----------//----------//----------//----------//
	
	private boolean isInputName() {
		try {
			return getIntent().getBooleanExtra(VOICE_NAME, false);
		} catch (Exception e) { }
		return false;
	}
	
	String pathVoice;
	private String getFilePath() {
		if (pathVoice == null) {
			String path = null;
			try {
				path = getIntent().getStringExtra(VOICE_PATH);
			} catch (Exception e) { }
			if (path == null) {
				try {
					File fileVoice = new File(getExternalCacheDir(), System.currentTimeMillis() + ".voice");
					if (!fileVoice.createNewFile()) {
						fileVoice.createNewFile();
					}
					path = fileVoice.getAbsolutePath();
				} catch (Exception e) { }
				if (path == null) {
					try {
						File fileVoice = new File(getCacheDir(), System.currentTimeMillis() + ".voice");
						if (!fileVoice.createNewFile()) {
							fileVoice.createNewFile();
						}
						path = fileVoice.getAbsolutePath();
					} catch (Exception e) { }
				}
			}
			pathVoice = path;
		}
		return pathVoice;
	}
	
	protected int getContentViewLayout() {
		return R.layout.activity_voice_record;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getContentViewLayout());
		if (isInputName()) {
			TextView name = (TextView)findViewById(R.id.name);
			name.setVisibility(View.VISIBLE);
			name.setText(getFormat().format(new Date()));
		} else {
			findViewById(R.id.name).setVisibility(View.GONE);
		}
		findViewById(R.id.record).setOnClickListener(this);
		findViewById(R.id.stop).setOnClickListener(this);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected void onResume() {
		super.onResume();
		startANewRecord();
	}
	
	private void startANewRecord() {
		setState(ScreenState.StartANewRecord);
		boolean prepareResult = doPrepare();
		if (prepareResult) {
			setState(ScreenState.Prepared);
		} else {
			DialogInterface.OnClickListener retry = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					doReset();
					startANewRecord();
				}
			};
			DialogInterface.OnClickListener cancel = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					cancel();
				}
			};
			new AlertDialog.Builder(this)
				.setMessage("Prepare failed. Try again later.")
				.setPositiveButton("Retry now", retry)
				.setNegativeButton("Cancel record", cancel)
				.create().show();
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.record:
			start();
			break;
		case R.id.stop:
		default:
			stop();
			break;
		}
	}
	
	private void start() {
		boolean startResult = doStart();
		if (startResult) {
			setState(ScreenState.Started);
		} else {
			DialogInterface.OnClickListener retry = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					doReset();
					startANewRecord();
				}
			};
			DialogInterface.OnClickListener cancel = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					cancel();
				}
			};
			new AlertDialog.Builder(this)
				.setMessage("Start failed. Problem with recorder. Try again later.")
				.setPositiveButton("", retry)
				.setNegativeButton("", cancel)
				.create().show();
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancel();
	}
	
	private void cancel() {
		doRelease();
		setResult(RESULT_CANCELED);
		goBack();
	}
	
	private void stop() {
		setState(ScreenState.Stopped);
		doRelease();
		Intent intent = new Intent();
		//name
		String name = "";
		if (isInputName()) {
			try {
				name = ((EditText)findViewById(R.id.name)).getText().toString();
			} catch (Exception e) { }
		}
		intent.putExtra(VOICE_NAME, name);
		//path
		intent.putExtra(VOICE_PATH, getFilePath());
		setResult(RESULT_OK, intent);
		goBack();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	MediaRecorder recorder;
	private boolean doPrepare() {
		try {
			if (recorder == null) {
				recorder = new MediaRecorder();
			}
			recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			recorder.setOutputFile(getFilePath());
			recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			recorder.prepare();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean doStart() {
		try {
			recorder.start();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private void doReset() {
		try {
			recorder.reset();
		} catch (Exception e) {
			doRelease();
		}
	}
	
	private void doRelease() {
		try {
			recorder.stop();
		} catch (Exception e) { }
		try {
			recorder.release();
		} catch (Exception e) { }
		recorder = null;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	private SimpleDateFormat format;
	private SimpleDateFormat getFormat() {
		if (format == null) {
			format = new SimpleDateFormat("hh:mm:ss a ");
		}
		return format;
	}

}
