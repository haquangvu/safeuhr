package com.protectstar.timelock.pro.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.TimeLockVoicesDataSource;

public class VoicesActivity_Delete extends VoicesActivity_Play {

	//----------//----------//----------//----------//----------//
	
	@Override
	protected void setupList() {
		super.setupList();
		((ListView)findViewById(R.id.list)).setOnItemLongClickListener(itemClick);
	}
	
	private final OnItemLongClickListener itemClick = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> list, View view, int position, long id) {
			//item click
			int noteIndex = -1;
			try {
				if (position == id) {
					noteIndex = position;
				} else {
					noteIndex = (int)id;
				}
			} catch (Exception e) { }
			if (noteIndex < 0) {
				return false;
			}
			onTouch(noteIndex);
			return true;
		}
	};
	
	private void onTouch(final int index) {
		DialogInterface.OnClickListener click = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				((TimeLockVoicesDataSource)getMeobuDataSource()).remove(getUseList().get(index));
				doRefreshList();
			}
		};
		new AlertDialog.Builder(this)
			.setMessage("Do you want to delete?")
			.setPositiveButton("Delete", click)
			.setNegativeButton("No", null)
			.create().show();
	}

	//----------//----------//----------//----------//----------//
	
}
