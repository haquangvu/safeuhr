package com.protectstar.timelock.pro.android.activity;

import meobu.android.base.MeobuSQLiteDataSource;
import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.TextView;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.TimeLockActivity;
import com.protectstar.timelock.pro.android.data.TimeLockVoicesDataSource;

public class VoicesActivity_Base extends TimeLockActivity {
	
	//----------//----------//----------//----------//----------//----------//----------//

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notes);
		((TextView)findViewById(R.id.title)).setText(R.string.folder_voice_title);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected void onResume() {
		super.onResume();
		doResume();
	}
	
	protected void doResume() {
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	protected AlertDialog loading;
	protected AlertDialog getLoading() {
		if (loading == null) {
			loading = new AlertDialog.Builder(this).setMessage(R.string.safe_add_processing).setCancelable(false).create();
		}
		return loading;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (getLoading().isShowing()) {
			getLoading().dismiss();
			onHideLoading();
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected boolean supportMeobuSQLite() {
		return true;
	}
	TimeLockVoicesDataSource datasource;
	private TimeLockVoicesDataSource getDataSource() {
		if (datasource == null) {
			datasource = new TimeLockVoicesDataSource(this);
		}
		return datasource;
	}
	@Override
	protected MeobuSQLiteDataSource getMeobuDataSource() {
		return getDataSource();
	}

	//----------//----------//----------//----------//----------//----------//----------//
	
}
