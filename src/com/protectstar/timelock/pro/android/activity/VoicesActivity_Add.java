package com.protectstar.timelock.pro.android.activity;

import java.io.File;

import meobu.android.base.MeobuFileTools;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.protectstar.timelock.pro.android.ActivityVoiceRecord;
import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.TimeLockApplication;
import com.protectstar.timelock.pro.android.data.TimeLockMediaFileLayer;
import com.protectstar.timelock.pro.android.data.TimeLockVoice;
import com.protectstar.timelock.pro.android.data.TimeLockVoicesDataSource;

import common.view.OneByOnePercentAsyncTask;

public class VoicesActivity_Add extends VoicesActivity_List {

	//----------//----------//----------//----------//----------//
	
	private void goVoiceRecord() {
		Intent intent = ActivityVoiceRecord.createIntent(this, true);
		startActivityForResult(intent, 100);
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
	}
	
	private String onVoiceRecordedPath = null, onVoiceRecordedName = null;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			String path = ActivityVoiceRecord.getPath(data);
			String name = ActivityVoiceRecord.getName(data);
			if (path != null && path.length() > 0) {
				onVoiceRecordedPath = path;
				onVoiceRecordedName = name;
			}
		}
	}
	
	@Override
	protected void doResume() {
		if (onVoiceRecordedPath != null && onVoiceRecordedPath.length() > 0) {
			doVoiceAdded(onVoiceRecordedPath, onVoiceRecordedName);
			onVoiceRecordedPath = null;
			onVoiceRecordedName = null;
		} else {
			super.doResume();
		}
	}
	
	private void doVoiceAdded(final String path, final String name) {
		getLoading().setMessage(getString(R.string.safe_add_processing));
		getLoading().show();
		onShowLoading();
		task = new OneByOnePercentAsyncTask<Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				if (path == null) return 0;
				int errorCount = 0;
				total = 1;
				boolean external = ((TimeLockApplication)getApplication()).getExternalStoreEnable();
					index = 1;
					doPublishProgressFull(-1,-1);
					File source = new File(path);
					String[] paths = TimeLockMediaFileLayer.generateSecretPaths(VoicesActivity_Add.this, external);
					File destination = new File(paths[0]);
					
					try {
						doPublishLog(41);
						MeobuFileTools.copyEncrypt(source, destination, getCachePasscode(), this);
						TimeLockVoice item = new TimeLockVoice();
						item.setDate(System.currentTimeMillis());
						if (name == null || name.length() <= 0) {
							item.setName("test name");
						} else {
							item.setName(name);
						}
						item.setPath(destination.getAbsolutePath());
						long id = ((TimeLockVoicesDataSource)getMeobuDataSource()).put(item);
						if (id < 0) {
							errorCount++;
						}
						
						try { source.delete(); } catch (Exception e) { }
					} catch (Exception e) {
						e.printStackTrace();
						errorCount++;
						try { destination.delete(); } catch (Exception ee) {}
					}
				return errorCount;
			}
			
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (values[0] > 8888) {
					return;
				}
				if (getLoading().isShowing()) {
					String message = String.format(getString(R.string.safe_add_percentage_processing), values[1], values[2]);
					if (values[3] >= 0 || values[4] >= 0) {
						String value3 = String.format("%.2f", values[3] / 1048576f);
						String value4 = String.format("%.2f", values[4] / 1048576f);
						message = message + "\n" + (values[3]<0?"?":value3) + "/" + (values[4]<0?"?":value4);
					}
					getLoading().setMessage(message);
				}
			}

	        @Override
	        protected void onPostExecute(Integer result) {
	    		if (getLoading().isShowing()) {
	    			getLoading().dismiss();
	    			onHideLoading();
	    		}
//	    		if (result > 0) {
//    				Toast.makeText(VoicesActivity.this, R.string.safe_add_notsupport, Toast.LENGTH_SHORT).show();//TODO error message, ignore error
//	    		} else {
//	    		}
	    		if (!isCancelled()) {
					try { new File(path).delete(); } catch (Exception e) { }
		    		doRefreshList();
	    		}
	        }
		};
		executeVoid(task);
	}	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupButtons();
	}
	
	private void setupButtons() {
		findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				goVoiceRecord();
			}
		});
	}

	//----------//----------//----------//----------//----------//
	
}
