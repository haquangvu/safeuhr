package com.protectstar.timelock.pro.android.activity;

import java.util.Calendar;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.protectstar.safeuhr.android.R;

import meobu.android.base.MeobuActivity;

public class ClockActivity2_Digital extends MeobuActivity {
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//

	protected void doUpdateNumbers(TimeInfo infoNew) {
		int hour = Math.round(infoNew.hour) + ((infoNew.am)?0:12);
		if (infoNew.am) {
			if (hour > 11) {
				hour -= 12;
			}
		}
		int minute = Math.round(infoNew.minute);
		if (minute > 59) {
			minute -= 60;
		}
		int second = Math.round(infoNew.second);
		if (second > 59) {
			second -= 60;
		}
		setNumbers(hour/10, hour%10, minute/10, minute%10, second/10, second%10);
	}

	protected void onNumbersChanged() { }//play sound after
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	private int numberHour1 = -1, numberHour2 = -1, numberMin1 = -1, numberMin2 = -1, numberSec1 = -1, numberSec2 = -1;
	private void setNumbers(int hour1, int hour2, int min1, int min2, int sec1, int sec2) {
		boolean changed = false;
		if (hour1 != this.numberHour1) {
			setNumber((ImageView)findViewById(R.id.clockHour0), hour1);
			changed = true;
			this.numberHour1 = hour1;
		}
		if (hour2 != this.numberHour2) {
			setNumber((ImageView)findViewById(R.id.clockHour1), hour2);
			changed = true;
			this.numberHour2 = hour2;
		}
		if (min1 != this.numberMin1) {
			setNumber((ImageView)findViewById(R.id.clockMinute0), min1);
			changed = true;
			this.numberMin1 = min1;
		}
		if (min2 != this.numberMin2) {
			setNumber((ImageView)findViewById(R.id.clockMinute1), min2);
			changed = true;
			this.numberMin2 = min2;
		}
		if (sec1 != this.numberSec1) {
			setNumber((ImageView)findViewById(R.id.clockSecond0), sec1);
			changed = true;
			this.numberSec1 = sec1;
		}
		if (sec2 != this.numberSec2) {
			setNumber((ImageView)findViewById(R.id.clockSecond1), sec2);
			changed = true;
			this.numberSec2 = sec2;
		}
		if (changed) {
			onNumbersChanged();
		}
	}
	
	private static final int[] numberResources = {
		R.drawable.num_0,
		R.drawable.num_1,
		R.drawable.num_2,
		R.drawable.num_3,
		R.drawable.num_4,
		R.drawable.num_5,
		R.drawable.num_6,
		R.drawable.num_7,
		R.drawable.num_8,
		R.drawable.num_9 };
	private Drawable[] numberDrawables;
	private Drawable[] getNumberDrawables() {
		if (numberDrawables == null) {
			numberDrawables = new Drawable[numberResources.length];
			for (int i=0; i<numberResources.length; i++) {
				numberDrawables[i] = getResources().getDrawable(numberResources[i]);
			}
		}
		return numberDrawables;
	}
	private void setNumber(ImageView view, int number) {
		view.setImageDrawable(getNumberDrawables()[number]);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static class TimeInfo {
		//public int hour, minute, second;
		public float hour, minute, second;
		public boolean am;
		public boolean percent;
		void set(TimeInfo info) {
			hour = info.hour;
			minute = info.minute;
			second = info.second;
			am = info.am;
			percent = info.percent;
		}
		void set(int hour, int minute, int second, boolean am, boolean percent) {
			this.hour = hour;
			this.minute = minute;
			this.second = second;
			this.am = am;
			this.percent = percent;
		}
		void set(Calendar calendar, boolean percent) {
			this.hour = calendar.get(Calendar.HOUR);
			this.minute = calendar.get(Calendar.MINUTE);
			this.second = calendar.get(Calendar.SECOND);
			this.am = calendar.get(Calendar.AM_PM) == Calendar.AM;
			this.percent = percent;
		}
		static TimeInfo create(int hour, int minute, int second, boolean am, boolean percent) {
			TimeInfo ti = new TimeInfo();
			ti.set(hour, minute, second, am, percent);
			return ti;
		}
		static TimeInfo create(Calendar calendar, boolean percent) {
			TimeInfo ti = new TimeInfo();
			ti.set(calendar, percent);
			return ti;
		}
		boolean equals(TimeInfo object) {
			if (hour == object.hour && minute == object.minute && second == object.second
					&& am == object.am && percent == object.percent) {
				return true;
			}
			return false;
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
}
