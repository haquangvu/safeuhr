package com.protectstar.timelock.pro.android.activity;

import android.os.Build;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.Animation.AnimationListener;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.ClockData;

public class ClockActivity1_Analog extends ClockActivity2_Digital {

	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//

	private TimeInfo currentInfo;
	private TimeInfo getCurrentInfo() {
		if (currentInfo == null) {
			currentInfo = new TimeInfo();
		}
		return currentInfo;
	}
	
	protected void updateTime(TimeInfo infoNew, TimeInfo infoOld) {
		if (infoNew == null) return;
		if (infoOld == null) {
			if (getCurrentInfo().equals(infoNew)) {
				//do nothing
				onUpdateFinish();
			} else {
				//do update, set value
				doUpdateTimeView(infoNew, null);
				getCurrentInfo().set(infoNew);
			}
		} else {
			if (getCurrentInfo().equals(infoNew) && infoNew.equals(infoOld)) {
				//do nothing
				onUpdateFinish();
			} else {
				//do anim, set value
				doUpdateTimeView(infoNew, infoOld);
				getCurrentInfo().set(infoNew);
			}
		}
	}

	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	private void doUpdateTimeView(TimeInfo infoNew, TimeInfo infoOld) {
		if (infoNew == null) return;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			doUpdateTimeViewBelow11(infoNew, infoOld);
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			doUpdateTimeViewBelow12(infoNew, infoOld);
		} else {
			doUpdateTimeView12AndOver(infoNew, infoOld);
		}
		doUpdateNumbers(infoNew);
	}
	
	protected void onUpdateFinish() { }//do update later

	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//

	private void doUpdateTimeViewBelow11(TimeInfo infoNew, TimeInfo infoOld) {
		if (infoNew == null) return;
		float second = ClockData.secondDegree(infoNew.second) + 90;
		float minute = ClockData.minuteDegree(infoNew.minute) + 90;
		float hour = ClockData.hourDegree(infoNew.hour) + 90;
		if (infoNew.percent) {
			minute = ClockData.minuteDegree(infoNew.minute, infoNew.second) + 90;
			hour = ClockData.hourDegree(infoNew.hour, infoNew.minute) + 90;
		}
		float secondPast = second;
		float minutePast = minute;
		float hourPast = hour;
		int duration = 0;
		if (infoOld != null && !infoOld.equals(infoNew)) {
			secondPast = infoOld.second * ClockData.DEGREE_PER_SECOND + 90;
			minutePast = infoOld.minute * ClockData.DEGREE_PER_SECOND + 90;
			hourPast = infoOld.hour * 5 * ClockData.DEGREE_PER_SECOND + 90;
			if (infoOld.percent) {
				minutePast += (infoOld.second / 60.0f) * ClockData.DEGREE_PER_SECOND;
				hourPast += (infoOld.minute / 60.0f) * 5 * ClockData.DEGREE_PER_SECOND;
			}
			duration = 300;
		}
		Animation secondAnim = createAnimation(secondPast, second, getCenterPosition(), getCenterPositionY(), duration);
		Animation minuteAnim = createAnimation(minutePast, minute, getCenterPosition(), getCenterPositionY(), duration);
		Animation hourAnim = createAnimation(hourPast, hour, getCenterPosition(), getCenterPositionY(), duration);
		hourAnim.setAnimationListener(animListener);
		View secondView = findViewById(R.id.clockSecondView);
		if (secondView.getVisibility() != View.GONE) {
			secondView.startAnimation(secondAnim);
		}
		findViewById(R.id.clockMinuteView).startAnimation(minuteAnim);
		findViewById(R.id.clockHourView).startAnimation(hourAnim);
	}
	private float centerPosition = -1, centerPositionY = -1;
	protected float getCenterPosition() {
		if (centerPosition <= 0) {
			centerPosition = findViewById(R.id.clockBaseView).getWidth() / 2.0f;
		}
		return centerPosition;
	}
	protected float getCenterPositionY() {
		if (centerPositionY <= 0) {
			centerPositionY = findViewById(R.id.clockHourView).getHeight() / 2.0f;
		}
		return centerPositionY;
	}
	protected Animation createAnimation(float from, float to, float pivotX, float pivotY, int duration) {
		if (from - to > 360 + to - from) {
			from -= 360;
		}
		return doCreateAnimation(from, to, pivotX, pivotY, duration);
	}
	private Animation doCreateAnimation(float from, float to, float pivotX, float pivotY, int duration) {
		Animation anim = new RotateAnimation(from, to, pivotX, pivotY);
		anim.setFillAfter(true);
		anim.setDuration(duration);
		return anim;
	}
	protected final AnimationListener animListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation anim) { }
		@Override
		public void onAnimationRepeat(Animation anim) { }
		@Override
		public void onAnimationEnd(Animation anim) {
			onUpdateFinish();
		}
	};
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	private void doUpdateTimeViewBelow12(TimeInfo infoNew, TimeInfo infoOld) {
		doUpdateTimeViewBelow11(infoNew, infoOld);
	}
	
	private void doUpdateTimeView12AndOver(TimeInfo infoNew, TimeInfo infoOld) {
		doUpdateTimeViewBelow11(infoNew, infoOld);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//

}
