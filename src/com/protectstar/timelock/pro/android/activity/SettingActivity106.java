package com.protectstar.timelock.pro.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.protectstar.timelock.pro.android.CodeActivity;
import com.protectstar.timelock.pro.android.DestructionActivity;
import com.protectstar.timelock.pro.android.FullGuideActivity;
import com.protectstar.timelock.pro.android.IFileActivity;
import com.protectstar.timelock.pro.android.LanguageActivity;
import com.protectstar.timelock.pro.android.LockActivity;
import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.SecureActivity;
import com.protectstar.timelock.pro.android.SoundActivity;
import com.protectstar.timelock.pro.android.SpyActivity;
import com.protectstar.timelock.pro.android.TimeLockActivity;
import com.protectstar.timelock.pro.android.TimeLockApplication;
import com.protectstar.timelock.pro.android.data.TimeLockSettingDataSource;

public class SettingActivity106 extends TimeLockActivity {
	
	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_right,
				R.anim.hold_fade_out);
	}
	
	private void pushActivity(Class<?> activityClass) {
		Intent intent = new Intent(this, activityClass);
		intent.putExtra("intent_cache_passcode", getCachePasscode());
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
	}
	
	private void pushLanguageActivity() {
		finish();
		Intent intent = new Intent(this, LanguageActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
	}
	
	private void showGuide() {
		Intent intent = new Intent(this, FullGuideActivity.class);
		intent.putExtra(FullGuideActivity.FIRST_GUIDE_KEY, false);
		startActivity(intent);
		overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
	}

	//----------//----------//----------//----------//----------//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		setupButtons();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		updateSwitches();
		updateSecurityButtons();
		TimeLockApplication app = (TimeLockApplication)getApplication();
		currentNotesFolder = app.getNotesFolderVisible();
		updateNotesButtons();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		saveSwitches();
		TimeLockApplication app = (TimeLockApplication)getApplication();
		boolean notesPackage = app.getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[3]);
		boolean notes = app.getNotesFolderVisible();
		if (notesPackage && currentNotesFolder != notes) {
			app.setNotesFolderVisible(currentNotesFolder);
			app.setFeaturedFolderVisibility4Views(TimeLockApplication.NOTE_FOLDER_ID, currentNotesFolder);
		}
	}

	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//

	private static int[] buttonids = { R.id.settingSound, R.id.settingPasscode,
			R.id.settingDestruction, R.id.settingLanguage, R.id.settingIFile,
			R.id.settingHelp, R.id.settingSupport, R.id.settingLock,
			R.id.settingSpy, R.id.settingSecured};
	private final View.OnClickListener buttonClick = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			int index = getViewTag(view);
			onButtonClick(index);
		}
	};

	private void setupButtons() {
		for (int i=0; i<buttonids.length; i++) {
			View view = findViewById(buttonids[i]);
			view.setTag(i);
			view.setOnClickListener(buttonClick);
		}
		if (((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[0])) {
			findViewById(R.id.settingDestruction).setVisibility(View.VISIBLE);
			//findViewById(R.id.settingDestructionLine).setVisibility(View.VISIBLE);
		}
		findViewById(R.id.settingExternal).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				currentExternalStore = !currentExternalStore;
				setSwitches();
				if (currentExternalStore) {
					Toast.makeText(SettingActivity106.this, R.string.safe_external_enable, Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(SettingActivity106.this, R.string.safe_external_disable, Toast.LENGTH_SHORT).show();
				}
			}
		});
		//TODO enable new features after purchased
		setupSecurityButtons();
		setupNotesButtons();
	}
	
	boolean currentExternalStore;
	private void updateSwitches() {
		TimeLockApplication app = (TimeLockApplication)getApplication();
		currentExternalStore = app.getExternalStoreEnable();
		setSwitches();
	}
	
	private void setSwitches() {
		if (currentExternalStore) {
			((TextView)findViewById(R.id.settingExternal)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sdcard, 0, R.drawable.switch_on, 0);
		} else {
			((TextView)findViewById(R.id.settingExternal)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sdcard, 0, R.drawable.switch_off, 0);
		}
	}
	
	private void saveSwitches() {
		TimeLockApplication app = (TimeLockApplication)getApplication();
		if (currentExternalStore != app.getExternalStoreEnable()) {
			app.setExternalStoreEnable(currentExternalStore);
		}
	}
	
	//----------//----------//----------//----------//----------//
	
	private void setupSecurityButtons() {
		findViewById(R.id.settingSecuritySegment).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				boolean securedFolder = ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]);
				boolean spyCamera = ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]);
				if (!securedFolder && !spyCamera) {
					showSecurityPackagePopup();
				}
			}
		});
	}
	private void setupNotesButtons() {
		findViewById(R.id.settingNotesSegment).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				TimeLockApplication app = ((TimeLockApplication)getApplication());
				boolean notesPackage = app.getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[3]);
				if (!notesPackage) {
					showNotesPackagePopup();
				} else {
					currentNotesFolder = !currentNotesFolder;
					updateNotesButtons();
				}
			}
		});
	}
	private void onSecurityButtonsClick(int index) {
		boolean securedFolder = ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]);
		boolean spyCamera = ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]);
		if (!securedFolder && !spyCamera) {
			showSecurityPackagePopup();
		} else {
			if (index == 8) {
				pushActivity(SpyActivity.class);
			} else {
				pushActivity(SecureActivity.class);
			}
		}
	}
	private void showNotesPackagePopup() {
		DialogInterface.OnClickListener buyClick = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface paramDialogInterface, int paramInt) {
				buyNotes();
			}
		};
		new AlertDialog.Builder(this)
			.setTitle(R.string.setting_header_51)
			.setMessage(R.string.note_package_description)
			.setNegativeButton(R.string.btn_cancel, null)
			.setPositiveButton(R.string.btn_buy, buyClick)
			.create().show();
	}
	private void showSecurityPackagePopup() {
		Log.d("meobudebug", "showSecurityPackagePopup");
		View view = getLayoutInflater().inflate(R.layout.activity_setting_package_security, null);
		DialogInterface.OnClickListener buyClick = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface paramDialogInterface, int paramInt) {
				buySecurity();
			}
		};
		new AlertDialog.Builder(this)
			.setTitle(R.string.package_security_title)
			.setView(view)
			.setNegativeButton(R.string.btn_cancel, null)
			.setPositiveButton(R.string.btn_buy, buyClick)
			.create().show();
	}
	protected void buySecurity() {
		
	}
	protected void buyNotes() {
		
	}
	private boolean currentNotesFolder;
	protected void updateNotesButtons() {
		boolean notesPackage = ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[3]);
		if (notesPackage) {
			//bought
			TextView title = (TextView)findViewById(R.id.settingNotesSegmentTitle);
			title.setText(R.string.setting_header_5);
			title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			
			if (currentNotesFolder) {
				((TextView)findViewById(R.id.settingNotes)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_notes, 0, R.drawable.switch_on, 0);
			} else {
				((TextView)findViewById(R.id.settingNotes)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_notes, 0, R.drawable.switch_off, 0);
			}
			
		} else {
			TextView title = (TextView)findViewById(R.id.settingNotesSegmentTitle);
			title.setText(R.string.setting_header_51);
			title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_purchase, 0);
			
			((TextView)findViewById(R.id.settingNotes)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
	}
	protected void updateSecurityButtons() {
		boolean securedFolder = ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]);
		boolean spyCamera = ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]);
		if (securedFolder || spyCamera) {
//			findViewById(R.id.settingSecuritySegment).setVisibility(View.VISIBLE);
//			if (securedFolder) {
//				findViewById(R.id.settingSecured).setVisibility(View.VISIBLE);
//			}
//			if (spyCamera) {
//				findViewById(R.id.settingSpy).setVisibility(View.VISIBLE);
//			}
//			if (securedFolder && spyCamera) {
//				findViewById(R.id.settingSecuritySegmentLine).setVisibility(View.VISIBLE);
//			}
			TextView title = (TextView)findViewById(R.id.settingSecuritySegmentTitle);
			title.setText(R.string.setting_header_4);
			title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			
			((TextView)findViewById(R.id.settingSecured)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_securedfolder, 0, R.drawable.arrow, 0);
			((TextView)findViewById(R.id.settingSpy)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_intruder, 0, R.drawable.arrow, 0);
		} else {
//			findViewById(R.id.settingSecuritySegment).setVisibility(View.GONE);
			TextView title = (TextView)findViewById(R.id.settingSecuritySegmentTitle);
			title.setText(R.string.setting_header_41);
			title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_purchase, 0);
			
			((TextView)findViewById(R.id.settingSecured)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			((TextView)findViewById(R.id.settingSpy)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
	}
	
	//----------//----------//----------//----------//----------//

	private void onButtonClick(int index) {
		switch (index) {
		case 0:// sound
			pushActivity(SoundActivity.class);
			break;
		case 1:// passcode
			pushActivity(CodeActivity.class);
			break;
		case 2:// destruction
			pushActivity(DestructionActivity.class);
			break;
		case 3:// language
			pushLanguageActivity();
			break;
		case 4:// ifile
			pushActivity(IFileActivity.class);
			break;
		case 5:// help
			showGuide();
			break;
		case 6:// support
			supportEmail();
			break;
		case 7:// lock
			pushActivity(LockActivity.class);
			break;
		case 8://spy
		case 9://secured
			onSecurityButtonsClick(index);
			break;
		default:
			Toast.makeText(this, "Not implement yet.", Toast.LENGTH_SHORT).show();
			break;
		}
	}

	//----------//----------//----------//----------//----------//
	
	private void supportEmail() {
		String emailAddress = getString(R.string.settingSupportEmailTo);// "support@protectstar.com";
		String subject = getString(R.string.settingSupportEmailSubject);// "[iShredder 3 Standard - 3.0][Support]";
		String content = "";// add content
		content += getString(R.string.settingSupportEmailContent1) + android.os.Build.MODEL;// device
		// content += getString(R.string.settingSupportEmailContent2) +
		// //language
		content += getString(R.string.settingSupportEmailContent3)
				+ android.os.Build.VERSION.RELEASE;// version
		content += getString(R.string.settingSupportEmailContent4);

//		String uriText = "mailto:" + emailAddress + "?subject="
//				+ Uri.encode(subject) + "&body=" + Uri.encode(content);
//		Uri uri = Uri.parse(uriText);
//		Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
//		sendIntent.setData(uri);
//		startActivity(Intent.createChooser(sendIntent, "Send email"));
		

		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("message/rfc822");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, content);
		startActivity(Intent.createChooser(emailIntent, "Send email"));//emailIntent);// 
	}

}
