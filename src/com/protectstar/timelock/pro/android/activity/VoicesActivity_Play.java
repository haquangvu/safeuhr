package com.protectstar.timelock.pro.android.activity;

import java.io.File;

import meobu.android.base.MeobuFileTools;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.TimeLockVoice;

import common.view.PercentAsyncTask;

import android.app.AlertDialog;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class VoicesActivity_Play extends VoicesActivity_Add {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected void onPause() {
		super.onPause();
    	if (getLoadingPlay().isShowing()) {
    		getLoadingPlay().dismiss();
    		onHideLoading();
    	}
    	if (currentPlayingData != null) {
    		stopPlaying();
    	}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected void setupList() {
		super.setupList();
		((ListView)findViewById(R.id.list)).setOnItemClickListener(itemClick);
	}
	
	private final OnItemClickListener itemClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			//item click
			int noteIndex = -1;
			try {
				if (position == id) {
					noteIndex = position;
				} else {
					noteIndex = (int)id;
				}
			} catch (Exception e) { }
			if (noteIndex < 0) {
				return;
			}
			onTouch(noteIndex);
		}
	};
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	private TimeLockVoice currentPlayingData = null;
	private void onTouch(int index) {
		TimeLockVoice data = getUseList().get(index);
		if (currentPlayingData == null || currentPlayingData.getId() != data.getId()) {
			stopPlaying();
			currentPlayingData = data;
			startPlaying();
		} else {
			stopPlaying();
			currentPlayingData = null;
		}
		refreshList();
	}
	
	@Override
	protected void updateMeobuView4Playing(int position, int type, View view) {
		TextView txt = (TextView)view.findViewById(R.id.date);
		if (currentPlayingData != null && getUseList().get(position).getId() == currentPlayingData.getId()) {
			//playing...
			txt.setCompoundDrawablesWithIntrinsicBounds(null, null, getPlaying(), null);
		} else {
			//playable
			txt.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
		}
	}
	
	private Drawable playing;
	private Drawable getPlaying() {
		if (playing == null) {
			playing = getResources().getDrawable(R.drawable.play50);
		}
		return playing;
	}
	
	private void startPlaying() {
		checkNdecrypt();
	}
	
	private AlertDialog loadingPlay;
	private AlertDialog getLoadingPlay() {
		if (loadingPlay == null) {
			loadingPlay = new AlertDialog.Builder(this).setMessage(R.string.view_play_preprocessing).setCancelable(false).create();
		}
		return loadingPlay;
	}
	private void checkNdecrypt() {
		if (currentPlayingData == null) return;
		if (currentPlayingData.getDecryptedPath() == null) {
			getLoadingPlay().setMessage(getString(R.string.view_play_preprocessing));
			getLoadingPlay().show();
			onShowLoading();
			final PercentAsyncTask<Void, Integer> task = new PercentAsyncTask<Void, Integer>() {
				@Override
				protected Integer doInBackground(Void... arg0) {
					int result = 0;
					try {
						File temp = File.createTempFile("voice", null);
						currentPlayingData.setDecryptedPath(temp.getAbsolutePath());
						temp.deleteOnExit();
						MeobuFileTools.copyDecrypt(currentPlayingData.getPath(), temp, getCachePasscode(), this);
					} catch (Exception e) {
						result = -1;
		        		currentPlayingData.setDecryptedPath(null);
					}
					return result;
				}
				@Override
				protected void onProgressUpdate(Integer... values) {
					super.onProgressUpdate(values);
					if (values[0] > 8888) {
						return;
					}
					if (getLoadingPlay().isShowing()) {
						String message = getString(R.string.view_play_preprocessing);
						if (values[1] >= 0 || values[2] >= 0) {
							String value1 = String.format("%.2f", values[1] / 1048576f);
							String value2 = String.format("%.2f", values[2] / 1048576f);
							message = message + "\n" + (values[1]<0?"?":value1) + "/" + (values[2]<0?"?":value2);
						}
						getLoadingPlay().setMessage(message);
					}
				}
		        @Override
		        protected void onPostExecute(Integer result) {
		        	if (getLoadingPlay().isShowing()) {
		        		getLoadingPlay().dismiss();
		        		onHideLoading();
			        	if (result < 0) {
			        		new AlertDialog.Builder(VoicesActivity_Play.this).setNeutralButton(R.string.btn_ok1, null).setMessage(R.string.view_decrypt_fail).create().show();
			        	} else {
			        		checkNplay();
			        	}
		        	}
		        }
			};
			executeVoidVoidInteger(task);
		} else {
			checkNplay();
		}
	}
	MediaPlayer player = null;
	private void checkNplay() {
		Uri uri = Uri.fromFile(new File(currentPlayingData.getDecryptedPath()));
		player = MediaPlayer.create(this, uri);
		player.start();
	}
	
	private void stopPlaying() {
		try { player.release(); } catch (Exception e) { }
		player = null;
		currentPlayingData = null;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
}
