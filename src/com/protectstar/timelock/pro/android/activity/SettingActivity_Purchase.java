package com.protectstar.timelock.pro.android.activity;

/**
 * add purchase Security Package
 * @author meobu
 */
public class SettingActivity_Purchase extends SettingActivity106 {
//	
//	//----------//----------//----------//----------//----------//
//
//	final ServiceConnection mServiceConn = new ServiceConnection() {
//		@Override
//		public void onServiceDisconnected(ComponentName name) {
//			mService = null;
//		}
//		@Override
//		public void onServiceConnected(ComponentName name, IBinder service) {
//			mService = IInAppBillingService.Stub.asInterface(service);
//			if (requestAction == 25080) {
//				purchaseSecurity();
//			} else if (requestAction == 25081) {
//				purchaseNotes();
//			}
//			requestAction = 0;
//		}
//	};
//	IInAppBillingService mService;
//	int requestAction = 0;
//	private void bindBillingService(int requestAction) {
//		//remove because of implicit intent
////        bindService(new Intent("com.android.vending.billing.InAppBillingService.BIND"), mServiceConn, Context.BIND_AUTO_CREATE);
//		ResolveInfo info = isBillingAvailable(this);
//		if (info == null) {
//			Toast.makeText(this, "Service is not available at this time.", Toast.LENGTH_SHORT).show();
//			return;
//		}
//		this.requestAction = requestAction;
//		ServiceInfo servInfo = info.serviceInfo;
//        ComponentName name = new ComponentName(servInfo.applicationInfo.packageName, servInfo.name);
//        Intent i2 = new Intent();
//        i2.setComponent(name);
//        bindService(i2, mServiceConn, Context.BIND_AUTO_CREATE);
//	}
//	public static ResolveInfo isBillingAvailable(Context context) {
//	    final PackageManager packageManager = context.getPackageManager();
//	    final Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
//	    List<ResolveInfo> list = packageManager.queryIntentServices(intent, 0);
//	    Log.d("meobudebug", "billing service count: " + list);
//	    if (list.size() > 0) {
//	    	return list.get(0);
//	    }
//	    return null;
//	}
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		//bindBillingService();
//	}
//	private void unbindBillingService() {
//		if (mService != null) {
//			unbindService(mServiceConn);
//			mService = null;
//		}
//	}
//	@Override
//	protected void onPause() {
//		super.onPause();
//		unbindBillingService();
//	}
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//		//unbindBillingService();
//	}
//	
//	//----------//----------//----------//----------//----------//
//	
//	@Override
//	protected void buySecurity() {
//		if (mService == null) {
//			bindBillingService(25080);
//		} else {
//			purchaseSecurity();
//		}
//	}
//	@Override
//	protected void buyNotes() {
//		if (mService == null) {
//			bindBillingService(25081);
//		} else {
//			purchaseNotes();
//		}
//	}
//	private void purchaseNotes() {
//		purchase("com.projectstar.timelock.android.package_notes", 25081, "notes");
////		purchase("android.test.purchased", 25081, "notes");
//	}
//	private void purchaseSecurity() {
//		purchase("package_security", 25080, "security");
//		//purchase("android.test.purchased", 2508, "security");
//		//android.test.item_unavailable
//		//android.test.canceled
//		//android.test.refunded
//	}
//	private void purchase(String productId, int requestCode, String payloadSeed) {
//		//String productId = "";
//		Log.d("meobudebug", "productId: " + productId);
//		String success = null;
//		try {
//			String developerPayload = MeobuEncryption.SHA256("meobu" + requestCode + payloadSeed + productId);
//			Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(), productId, "inapp", developerPayload);
//            int response = getResponseCodeFromBundle(buyIntentBundle);
//            if (response == 7) {
//            	//already buy
//            	Log.d("meobudebug", "Already bought!");
//            	onPurchaseSuccess(requestCode);
//            	if (requestCode == 25080) {
//            		updateSecurityButtons();
//            	} else if (requestCode == 25081) {
//            		updateNotesButtons();
//            	}
//                return;
//            }
//            if (response != 0) {
//            	Toast.makeText(this, "Unable to buy item, Error response: " + getResponseDesc(response), Toast.LENGTH_SHORT).show();
//            	return;
//            }
//			PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
//			startIntentSenderForResult(pendingIntent.getIntentSender(),
//					requestCode, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
//						   Integer.valueOf(0));
//		} catch (RemoteException e) {
//			e.printStackTrace();
//			success = e.toString();
//		} catch (SendIntentException e) {
//			e.printStackTrace();
//			success = e.toString();
//		} catch (Exception e) {
//			e.printStackTrace();
//			success = e.toString();
//		}
//		if (success != null) {
//			new AlertDialog.Builder(this)
//				.setMessage("Item is not available this time.\n" + success)
//				.setNeutralButton(R.string.btn_ok1, null)
//				.create().show();
//		}
//	}
//	private void onPurchaseSuccess(int requestCode) {
//		TimeLockApplication app = (TimeLockApplication)getApplication();
//		if (requestCode == 25080) {
//			app.setSecurityPackagePurchased();
//			//add to views
//			app.setFeaturedFolderVisibility4Views(TimeLockApplication.SECURED_FOLDER_ID, true);
//			app.setFeaturedFolderVisibility4Views(TimeLockApplication.SPY_FOLDER_ID, true);
//		} else if (requestCode == 25081) {
//			app.setNotesPackagePurchased();
//			app.setFeaturedFolderVisibility4Views(TimeLockApplication.NOTE_FOLDER_ID, true);
//		}
//	}
//
//	//----------//----------//----------//----------//----------//
//	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		if (requestCode == 25080 || requestCode == 25081) {
//			if (resultCode == RESULT_OK) {
//				String success = null;
//				try {
//					int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
//					String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
//					String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
//					Log.d("meobudebug", "onActivityResult   responseCode:" + responseCode + "   purchaseData:" + purchaseData + "   dataSignature:" + dataSignature);
//					JSONObject jo = new JSONObject(purchaseData);
//		    		String productId = jo.getString("productId");
//		    		String developerPayload = jo.getString("developerPayload");
//		    		Log.d("meobudebug", "onActivityResult[OK]   productId:" + productId + "   developerPayload:" + developerPayload);
//				} catch (Exception e) {
//					e.printStackTrace();
//					success = e.toString();
//				}
//				if (success != null) {
//					new AlertDialog.Builder(this)
//						.setMessage("Purchase not complete. " + success)
//						.setNeutralButton(R.string.btn_ok1, null)
//						.create().show();
//				} else {
//					Toast.makeText(this, "Purchase success", Toast.LENGTH_SHORT).show();
//					onPurchaseSuccess(requestCode);
//				}
//			//} else {
//			//	Toast.makeText(this, "Process has been cancelled.", Toast.LENGTH_SHORT).show();
//			}
//		}
//	}
//	
//	//----------//----------//----------//----------//----------//
//
//    // Workaround to bug where sometimes response codes come as Long instead of Integer
//    private int getResponseCodeFromBundle(Bundle b) {
//        Object o = b.get("RESPONSE_CODE");
//        if (o == null) {
//            //logDebug("Bundle with null response code, assuming OK (known issue)");
//            return 0;
//        }
//        else if (o instanceof Integer) return ((Integer)o).intValue();
//        else if (o instanceof Long) return (int)((Long)o).longValue();
//        else {
//            Log.d("meobudebug","Unexpected type for bundle response code. " + o.getClass().getName());
//            throw new RuntimeException("Unexpected type for bundle response code: " + o.getClass().getName());
//        }
//    }
//    public static String getResponseDesc(int code) {
//    	String[] iab_msgs = {
//    					"0:OK",
//    					"1:User Canceled",
//    					"2:Unknown",
//    					"3:Billing Unavailable",
//    					"4:Item unavailable",
//    					"5:Developer Error",
//    					"6:Error",
//    					"7:Item Already Owned",
//    					"8:Item not owned"};
////        String[] iab_msgs = ("0:OK/1:User Canceled/2:Unknown/" +
////                "3:Billing Unavailable/4:Item unavailable/" +
////                "5:Developer Error/6:Error/7:Item Already Owned/" +
////                "8:Item not owned").split("/");
//        String[] iabhelper_msgs = ("0:OK/-1001:Remote exception during initialization/" +
//                                   "-1002:Bad response received/" +
//                                   "-1003:Purchase signature verification failed/" +
//                                   "-1004:Send intent failed/" +
//                                   "-1005:User cancelled/" +
//                                   "-1006:Unknown purchase response/" +
//                                   "-1007:Missing token/" +
//                                   "-1008:Unknown error/" +
//                                   "-1009:Subscriptions not available/" +
//                                   "-1010:Invalid consumption attempt").split("/");
//
//        if (code <= -1000) {
//            int index = -1000 - code;
//            if (index >= 0 && index < iabhelper_msgs.length) return iabhelper_msgs[index];
//            else return String.valueOf(code) + ":Unknown IAB Helper Error";
//        }
//        else if (code < 0 || code >= iab_msgs.length)
//            return String.valueOf(code) + ":Unknown";
//        else
//            return iab_msgs[code];
//    }	
	
}