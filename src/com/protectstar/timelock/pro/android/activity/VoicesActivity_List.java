package com.protectstar.timelock.pro.android.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import meobu.android.base.MeobuListViewHandler;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.TimeLockVoice;
import com.protectstar.timelock.pro.android.data.TimeLockVoicesDataSource;

import common.view.OneByOnePercentAsyncTask;

public class VoicesActivity_List extends VoicesActivity_Base {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupList();
	}
	
	protected void doResume() {
		doRefreshList();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (task != null) {
			task.cancel(false);
			task = null;
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	OneByOnePercentAsyncTask<Void, Integer> task = null;
	protected void doRefreshList() {
		getLoading().setMessage(getString(R.string.view_play_preprocessing));
		getLoading().show();
		onShowLoading();
		task = new OneByOnePercentAsyncTask<Void, Integer>() {
			ArrayList<TimeLockVoice> list = null;
			@Override
			protected Integer doInBackground(Void... params) {
				list = ((TimeLockVoicesDataSource)getMeobuDataSource()).retrieve(0, Integer.MAX_VALUE);
				return 0;
			}
			@Override
			protected void onPostExecute(Integer result) {
	    		if (getLoading().isShowing()) {
	    			getLoading().dismiss();
	    			onHideLoading();
	    		}
				if (!isCancelled()) {
					useList = list;
					refreshList();
				}
			}
		};
		executeVoid(task);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	private ArrayList<TimeLockVoice> useList;
	protected ArrayList<TimeLockVoice> getUseList() {
		if (useList == null) {
			useList = new ArrayList<TimeLockVoice>();
		}
		return useList;
	}
	protected void setupList() {
		listHandler.setListView(this, R.id.list);
	}
	protected void refreshList() {
		listHandler.refresh();
	}
	private static final int[] listChildrenLayoutIds = {R.layout.activity_notes_item};
	private final MeobuListViewHandler<TimeLockVoice> listHandler = new MeobuListViewHandler<TimeLockVoice>(listChildrenLayoutIds) {
		@Override
		public ArrayList<TimeLockVoice> getMeobuListData() {
			return getUseList();
		}
		@Override
		public void updateMeobuView(int position, int type, View view) {
			TimeLockVoice note = getUseList().get(position);
			String title = note.getName();
			if (title == null || title.length() <= 0) {
				title = getString(R.string.note_item_notitle);
			}
			((TextView)view.findViewById(R.id.title)).setText(title);
			String date = convertDateLong(note.getDate());
			((TextView)view.findViewById(R.id.date)).setText(date);
			updateMeobuView4Playing(position, type, view);
		}
	};
	protected void updateMeobuView4Playing(int position, int type, View view) {
		
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	private final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
	private String convertDateLong(long date) {
		return format.format(new Date(date));
	}

	//----------//----------//----------//----------//----------//----------//----------//
	
}
