package com.protectstar.timelock.pro.android.alarm;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class AlarmService extends IntentService {
    
   public static final String CREATE = "CREATE";
   public static final String CANCEL = "CANCEL";
    
   private IntentFilter matcher;

   public AlarmService() {
       super("AlarmService");
       matcher = new IntentFilter();
       matcher.addAction(CREATE);
       matcher.addAction(CANCEL);
   }
   
   long lastNotificationDate = -1;
   long getLastNotificationDate() {
	   if (lastNotificationDate < 0) {
		   lastNotificationDate = getSharedPreferences("alarmservice", 0).getLong("notificationDate", 0);
	   }
	   return lastNotificationDate;
   }
   void setLastNotificationDate(long notificationDate) {
	   this.lastNotificationDate = notificationDate;
	   getSharedPreferences("alarmservice", 0).edit().putLong("notificationDate", notificationDate).commit();
   }

   @Override
   protected void onHandleIntent(Intent intent) {
       String action = intent.getAction();
       //String notificationId = intent.getStringExtra("notificationId");
       long notificationDate = intent.getLongExtra("notificationDate", 0);
       if (matcher.matchAction(action)) {
           execute(action, notificationDate);
       }
   }

   private void execute(String action, long notificationDate) {
	   //Log.d("meobudebug", "test again! time: " + notificationDate);
	    AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(this, AlarmReceiver.class);
        //i.putExtra("id", c.getLong(c.getColumnIndex(Notification.COL_ID)));
        //i.putExtra("msg", c.getString(c.getColumnIndex(Notification.COL_MSG)));

        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        if (CREATE.equals(action) && notificationDate > 0) {
	        if (notificationDate == getLastNotificationDate()) {
	        	//do nothing
	        } else {
	        	//Log.d("meobudebug", "service set!!!");
	        	am.set(AlarmManager.RTC_WAKEUP, notificationDate, pi);
	        	setLastNotificationDate(notificationDate);
	        }

        } else if (CANCEL.equals(action)) {
//        	if (notificationDate == getLastNotificationDate()) {
//        		//do nothing
//        	} else {
        		am.cancel(pi);
        		setLastNotificationDate(notificationDate);
//        	}
        }
	}

}