package com.protectstar.timelock.pro.android.alarm;

import com.protectstar.safeuhr.android.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

public class AlarmReceiver extends BroadcastReceiver {
 
    @Override
    public void onReceive(Context context, Intent intent) {
        long id = intent.getLongExtra("id", 0);
        String msg = intent.getStringExtra("msg");
         
        Notification n = createNotification(context, msg);       
         
        NotificationManager nm = (NotificationManager) 
                                    context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify((int)id, n);
    }
    
    private Notification createNotification(Context context, String message) {
    	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
    		return createNotificationLower11(context, message);
    	}
    	return createNotification11AndOver(context, message);
    }
    
    @SuppressWarnings("deprecation")
	private Notification createNotificationLower11(Context context, String message) {
    	Notification n = new Notification(R.drawable.ic_launcher, message, System.currentTimeMillis());
		PendingIntent pi = PendingIntent.getActivity(context, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
		//TODO activity to open app
		
		n.setLatestEventInfo(context, context.getString(R.string.app_name), message, pi);
		// check user preferences
		n.defaults |= Notification.DEFAULT_VIBRATE;
		n.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.alarm);
		//n.defaults |= Notification.DEFAULT_SOUND;
		n.flags |= Notification.FLAG_AUTO_CANCEL;
    	return n;
    }
    
    private Notification createNotification11AndOver(Context context, String message) {
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
//    	Log.d("meobudebug", "notificationnnnnnnnnnnnnnnnnnnnnnn");
    	return createNotificationLower11(context, message);
    }
}