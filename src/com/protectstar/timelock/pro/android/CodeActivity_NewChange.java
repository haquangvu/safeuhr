package com.protectstar.timelock.pro.android;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionChangeCode;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionFactory;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionStatus;
import com.protectstar.timelock.pro.android.data.service.BackgroundService;
import com.protectstar.timelock.pro.android.data.service.BackgroundService.BackgroundActionInterface;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

public class CodeActivity_NewChange extends CodeActivity_Full {

	private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getBooleanExtra("working", false)) {
				if (!getLoading().isShowing()) {
					showChangingLoading();
				}
				BackgroundActionStatus status = BackgroundActionStatus.from(intent);
				onUpdateChangingLoading(status.listTotal, status.listCurrent, status.itemTotal, status.itemCurrent);
				loop();
			} else {
				BackgroundActionInterface action = BackgroundActionFactory.from(CodeActivity_NewChange.this, intent);
				if (action != null && action instanceof BackgroundActionChangeCode) {
					onChangingSuccess((BackgroundActionChangeCode)action);
				} else {
					onChangingSuccess(null);
				}
			}
		}
	};
	
	private boolean loopRunning = false;
	private final Runnable loop = new Runnable() {
		@Override
		public void run() {
			BackgroundService.updateStatus(CodeActivity_NewChange.this);
		}
	};
	
	@Override
	protected void showChangingLoading() {
		super.showChangingLoading();
		LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver, new IntentFilter("my-event"));
		//start loop
		loopRunning = true;
		loop();
	}
	
	private void loop() {
		if (loopRunning) {
			if (findViewById(R.id.main_header) != null) {
				findViewById(R.id.main_header).postDelayed(loop, 300);
			}
		}
	}
	
	@Override
	protected void hideChangingLoading() {
		super.hideChangingLoading();
		LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
		//end loop
		loopRunning = false;
	}
	
	private void onUpdateChangingLoading(int listCount, int listCurrent, int itemSize, int itemCurrent) {
		if (getLoading().isShowing()) {
			String message = String.format(getString(R.string.safe_add_percentage_processing), listCurrent, listCount);
			if (itemCurrent >= 0 || itemSize >= 0) {
				String value3 = String.format("%.2f", itemCurrent / 1024f);
				String value4 = String.format("%.2f", itemSize / 1024f);
				message = message + "\n" + (itemCurrent<0?"?":value3) + "/" + (itemSize<0?"?":value4);
			}
			getLoading().setMessage(message);
		}
	}
	
	private void onChangingSuccess(BackgroundActionChangeCode action) {
		hideChangingLoading();
		if (action == null) return;
		if (action.errorCount > 0) {
			Toast.makeText(CodeActivity_NewChange.this, "Cannot convert " + action.errorCount + " files.", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(CodeActivity_NewChange.this, "Change passcode success!!!", Toast.LENGTH_SHORT).show();
		}
		((TimeLockApplication)getApplication()).setNewPasscode(action.hour, action.minute, action.second);
		doUpdateNumbers(action.hour, action.minute, action.second);
		//prompt alert to restart
		DialogInterface.OnClickListener restart = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				goClock();
			}
		};
		new AlertDialog.Builder(this)
			.setMessage(R.string.changepasscode_restart)
			.setNeutralButton(R.string.changepasscode_restart_ok, restart)
			.create().show();
	}
	
	@Override
	protected void updateNewPassword(int newHour, int newMinute, int newSecond) {
		String newPassword = TimeLockApplication.buildPasscode(newHour, newMinute, newSecond);
		BackgroundService.startChangeCode(this, getCachePasscode(), newPassword, newHour, newMinute, newSecond);
		showChangingLoading();
	}
	
}
