package com.protectstar.timelock.pro.android;

import java.io.File;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData.ItemViewDataType;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionChangeCode;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionFactory;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionImport;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionImportItem;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionStatus;
import com.protectstar.timelock.pro.android.data.service.BackgroundService;
import com.protectstar.timelock.pro.android.data.service.BackgroundService.BackgroundActionInterface;

import meobu.android.base.MeobuGalleryHandler.MeobuGalleryItem;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
//import android.util.Log;
import android.widget.Toast;

public class SafeActivity_NewAdd extends SafeActivity_Full {
	
	boolean postAddingActionReloadData = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		postAddingActionReloadData = true;
	}
	
	@Override
	protected void doResume() {
		if (postAddingActionReloadData) {
			checkAdding();
		} else {
			doRefreshOnResume();
		}
	}
	
	private void checkAdding() {
		showAddingLoading();
		BackgroundService.updateStatus(SafeActivity_NewAdd.this);
	}
	
	private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getBooleanExtra("working", false)) {
//				Log.d("meobudebug", "receive status");
				if (!getLoading().isShowing()) {
					//first time will come here
					showAddingLoading();
				}
				BackgroundActionStatus status = BackgroundActionStatus.from(intent);
				onUpdateAddingLoading(status.listTotal, status.listCurrent, status.itemTotal, status.itemCurrent);
				loop();
			} else {
				BackgroundActionInterface action = BackgroundActionFactory.from(SafeActivity_NewAdd.this, intent);
				boolean justDismiss = false;
				if (action != null) {
					if (action instanceof BackgroundActionImport) {
						onAddingSuccess((BackgroundActionImport)action);
					} else if (action instanceof BackgroundActionChangeCode) {
						onChangingSuccess((BackgroundActionChangeCode)action);
					} else {
						justDismiss = true;
					}
				} else {
					justDismiss = true;
				}
				if (justDismiss) {
					onAddingSuccess(null);
				}
			}
		}
	};
	
	private boolean loopRunning = false;
	private final Runnable loop = new Runnable() {
		@Override
		public void run() {
			BackgroundService.updateStatus(SafeActivity_NewAdd.this);
		}
	};
	
	@Override
	protected void showAddingLoading() {
		super.showAddingLoading();
		LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver, new IntentFilter("my-event"));
		//start loop
		loopRunning = true;
		loop();
	}
	
	private void loop() {
		if (loopRunning) {
			if (findViewById(R.id.main_header) != null) {
				findViewById(R.id.main_header).postDelayed(loop, 300);
			}
		}
	}
	
	@Override
	protected void hideAddingLoading() {
		super.hideAddingLoading();
		LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
		//end loop
		loopRunning = false;
	}
	
	private void onUpdateAddingLoading(int listCount, int listCurrent, int itemSize, int itemCurrent) {
		if (getLoading().isShowing()) {
			String message = String.format(getString(R.string.safe_add_percentage_processing), listCurrent, listCount);
			if (itemCurrent >= 0 || itemSize >= 0) {
				String value3 = String.format("%.2f", itemCurrent / 1024f);
				String value4 = String.format("%.2f", itemSize / 1024f);
				message = message + "\n" + (itemCurrent<0?"?":value3) + "/" + (itemSize<0?"?":value4);
			}
			getLoading().setMessage(message);
		}
	}
	
	private void onChangingSuccess(BackgroundActionChangeCode action) {
		hideAddingLoading();
		if (action == null) return;
		if (action.errorCount > 0) {
			Toast.makeText(SafeActivity_NewAdd.this, "Cannot convert " + action.errorCount + " files.", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(SafeActivity_NewAdd.this, "Change passcode success!!!", Toast.LENGTH_SHORT).show();
		}
		((TimeLockApplication)getApplication()).setNewPasscode(action.hour, action.minute, action.second);
		//prompt alert to restart
		DialogInterface.OnClickListener restart = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				goClock();
			}
		};
		new AlertDialog.Builder(this)
			.setMessage(R.string.changepasscode_restart)
			.setNeutralButton(R.string.changepasscode_restart_ok, restart)
			.create().show();
	}
	
	private void onAddingSuccess(BackgroundActionImport action) {
		hideAddingLoading();
		if (action == null) {
			if (postAddingActionReloadData) {
				postAddingActionReloadData = false;
				doRefreshOnResume();
			}
			return;
		}
		if (action.errorCount > 0) {
			boolean notSupport = false;
			if (action.errorCount > 7524) {
				new AlertDialog.Builder(SafeActivity_NewAdd.this)
					.setMessage(R.string.safe_add_google_offline)
					.setNeutralButton(R.string.btn_ok1, null)
					.create().show();
				action.errorCount -= 7524;
			} else if (action.errorCount > 5016) {
				Toast.makeText(SafeActivity_NewAdd.this, R.string.safe_add_notsupport_filebrowse, Toast.LENGTH_SHORT).show();
				action.errorCount -= 5016;
			} else if (action.errorCount > 2508) {
				new AlertDialog.Builder(SafeActivity_NewAdd.this)
					.setMessage(R.string.safe_add_google_offline)
					.setNeutralButton(R.string.btn_ok1, null)
					.create().show();
				action.errorCount -= 2508;
			} else if (action.errorCount == 1) {
				notSupport = true;
			} else {
				notSupport = true;
			}
			if (action.errorCount < action.items.size() && !postAddingActionReloadData) {
	    		refreshListData();
			}
			if (notSupport) {
				messageImpportSupport(new File(action.logPath), action.errorCount);
			}
		} else {
			if (!postAddingActionReloadData) {
				refreshListData();
			}
    		messageDeleteImported(action);
		}
		if (postAddingActionReloadData) {
			postAddingActionReloadData = false;
			doRefreshOnResume();
		}
	}
	
	private void messageDeleteImported(final BackgroundActionImport action) {
		if (!action.delete && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT && action.items != null) {
			DialogInterface.OnClickListener delete = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					for (int i=0; i<action.items.size(); i++) {
						BackgroundActionImportItem item = action.items.get(i);
						if (action.actionSub == BackgroundActionImport.ACTION_SUB_IMPORT_FILE) {
							try {new File(item.path).delete();} catch (Exception e) {}
						} else if (action.actionSub == BackgroundActionImport.ACTION_SUB_IMPORT_URI) {
							try {
								if (item.path.startsWith("file")) {
									new File(item.path).delete();
								} else {
									long id = -1;
									Uri uri = Uri.parse(item.path);
									try { id = ContentUris.parseId(uri); } catch (Exception e) {}
									if (id < 0) {//from somewhere
										getContentResolver().delete(uri, null, null);
									} else {
										Uri deleteUri = null;
										if (action.type == ItemViewDataType.Photo.ordinal()) {
											deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
										} else {
											deleteUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id);
										}
									    getContentResolver().delete(deleteUri, null, null);
									}
								}
							} catch (Exception e) { }
						} else if (action.actionSub == BackgroundActionImport.ACTION_SUB_IMPORT_GALLERY) {
							try {
								Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, item.id);
							    getContentResolver().delete(deleteUri, null, null);
							} catch (Exception e) {}
							try {new File(item.path).delete();} catch (Exception e) {}
						}
					}
				}
			};
			new AlertDialog.Builder(this)
				.setMessage(R.string.safe_import_delete_source)
				.setPositiveButton(R.string.safe_delete_confirm_yes, delete)
				.setNegativeButton(R.string.safe_delete_confirm_no, null)
				.create().show();
		}
	}
	
	private boolean getExternal() {
		return ((TimeLockApplication)getApplication()).getExternalStoreEnable();
	}
	
	@Override
	protected void startAddItem(File[] mediaFiles, int type, boolean delete, int folder) {
		BackgroundService.startImport(this, mediaFiles, type, folder, getExternal(), getCachePasscode(), delete);
		showAddingLoading();
	}
	
	@Override
	protected void startAddItem(Uri[] mediaUris, int type, boolean delete, int folder) {
		BackgroundService.startImport(this, mediaUris, type, folder, getExternal(), getCachePasscode(), delete);
		showAddingLoading();
	}
	
	@Override
	protected void startAddItem(MeobuGalleryItem[] mediaItems, int type, boolean delete) {
		BackgroundService.startImport(this, mediaItems, type, getFolder(), getExternal(), getCachePasscode(), delete);
		showAddingLoading();
	}
	
}
