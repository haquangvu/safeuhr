package com.protectstar.timelock.pro.android;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.SettingActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LanguageActivity extends TimeLockActivity {
	
	private void goBack() {
		finish();
		Intent intent = new Intent(this, SettingActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}

	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_language);
		setupCheck();
	}

	@Override
	protected void onResume() {
		super.onResume();
		resetCheck();
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	public static final int[] language_ids = { R.id.languageSystem,
			R.id.languageEnglish, R.id.languageEspanol,
			R.id.languageFrancais, R.id.languageDeutsch,
			R.id.languagePortuguese, R.id.languageItalian};
	public static final int[] language_icons = { 0,
			R.drawable.flag_en, R.drawable.flag_es,
			R.drawable.flag_fr, R.drawable.flag_de,
			R.drawable.flag_pt_br, R.drawable.flag_it};

	private void setupCheck() {
		for (int i = 0; i < language_ids.length; i++) {
			View view = findViewById(language_ids[i]);
			view.setTag(i);
			view.setOnClickListener(onLineClickListener);
		}
	}
	private void resetCheck() {
		for (int i = 0; i < language_ids.length; i++) {
			TextView view = (TextView) findViewById(language_ids[i]);
			if (i == getMeobuLanguage()) {
				view.setCompoundDrawablesWithIntrinsicBounds(language_icons[i], 0, R.drawable.radio_on, 0);
			} else {
				view.setCompoundDrawablesWithIntrinsicBounds(language_icons[i], 0, R.drawable.radio_off, 0);
			}
		}
	}

	final View.OnClickListener onLineClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			int index = getViewTag(v);
			setLanguageIndex(index);
		}
	};

	private void setLanguageIndex(final int selectedIndex) {
		((TimeLockApplication)getApplication()).refreshViewData4Language();
		setMeobuLanguage(selectedIndex);
		restartMeobuActivity();
	}

}
