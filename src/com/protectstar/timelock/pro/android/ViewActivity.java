package com.protectstar.timelock.pro.android;

import java.io.File;
import java.util.ArrayList;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.TimeLockApplication.Data;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData.ItemViewDataType;
import com.protectstar.timelock.pro.android.data.TimeLockContentsDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockImageDownloader;

import common.view.HackyViewPager;
import common.view.PercentAsyncTask;

import uk.co.senab.photoview.PhotoView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import meobu.android.base.MeobuApplication;
import meobu.android.base.MeobuFileTools;
import meobu.android.base.MeobuSQLiteDataSource;

public class ViewActivity extends TimeLockActivity {
	
	private void goBack() {
		finish();
		overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
	}
	
	//----------//----------//----------//----------//----------//

	public static String PARAM_INDEX_KEY = "INDEX_KEY";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view);
		
		pager = (HackyViewPager)findViewById(R.id.pager);
		pager.setOnPageChangeListener(listener);
		pager.setAdapter(adapter);
		
		findViewById(R.id.stopSlideshow).setOnClickListener(slideshowClick);
		findViewById(R.id.share).setOnClickListener(shareClick);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}
	
	private int getFolder() {
		return getIntent().getIntExtra("folder", 0);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		//cleanSharedFile();
		useList = new ArrayList<Data>(((TimeLockApplication)getApplication()).getListData(getFolder()));
		adapter.notifyDataSetChanged();
		int index = getIntent().getIntExtra(PARAM_INDEX_KEY, 0);
		pager.setCurrentItem(index);
		updateTitle();
//		pager.post(new Runnable() {
//			@Override
//			public void run() {
//				int index = getIntent().getIntExtra(PARAM_INDEX_KEY, 0);
////				int index = 0;
////				for (int i=0; i<useList.size(); i++) {
////					if (useList.get(i).getDataId() == id) {
////						index = i;
////						break;
////					}
////				}
//				pager.setCurrentItem(index);
//			}
//		});
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		onHideLoading();
		if (currentVideoView != null) {
			currentVideoView.stopPlayback();
			currentVideoView = null;
		}
		//Log.d("meobudebug", "on pause: true");
		if (getLoadingSaveBack().isShowing()) getLoadingSaveBack().dismiss();
		if (getLoadingDelete().isShowing()) getLoadingDelete().dismiss();
		if (getLoadingPlay().isShowing()) getLoadingPlay().dismiss();
		if (getLoadingPrepare().isShowing()) getLoadingPrepare().dismiss();
		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}
	
	@Override
	protected void displayMeobuImage(String file, ImageView view) {
		if (((MeobuApplication)getApplication()).supportMeobuImageLoader()) { 
			doDisplayMeobuImage(file, view, false);
		}
	}
	private void doDisplayMeobuImage(String file, ImageView view, boolean shouldDecrypte) {
		if (shouldDecrypte) {
			ImageLoader.getInstance().displayImage(TimeLockImageDownloader.NEW_SUPPORTED_SCHEME + file, view);
		} else {
			ImageLoader.getInstance().displayImage("file://" + file, view);
		}
	}

	//----------//----------//----------//----------//----------//
	
	private void resetPager(int index) {
		if (index >= useList.size()) index--;
		pager.setAdapter(adapter);
		pager.setCurrentItem(index);
		updateTitle();
	}

	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onMeobuShakeDetected() {
		if (currentVideoView != null) {
			currentVideoView.stopPlayback();
			currentVideoView = null;
		}
		super.onMeobuShakeDetected();
	}
	
	//----------//----------//----------//----------//----------//

	@Override
	protected boolean supportMeobuSQLite() {
		return true;
	}
	TimeLockContentsDataSource datasource;
	private TimeLockContentsDataSource getDataSource() {
		if (datasource == null) {
			datasource = new TimeLockContentsDataSource(this);
		}
		return datasource;
	}
	@Override
	protected MeobuSQLiteDataSource getMeobuDataSource() {
		return getDataSource();
	}
	
	//----------//----------//----------//----------//----------//
	
	private final View.OnClickListener shareClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			onShare();
		}
	};
	private void onShare() {
		new AlertDialog.Builder(this).setTitle(R.string.view_share_title)
		.setItems(R.array.view_share_options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onHideLoading();
				if (which == 0) {
					slideshow();
				} else if (which == 1) {
					delete(pager.getCurrentItem());

				} else if (which == 2) {
					final Data itemData = useList.get(pager.getCurrentItem());
					saveBackGallery(itemData);
				} else {//if (which == 3) {
					//attach to an email.
					Data data = useList.get(pager.getCurrentItem());
					sendEmail(data);
				}
			}
		})
		.create().show();
		onShowLoading();
	}
	
	//----------//----------//----------//----------//----------//

	private void slideshow() {
		//hide header, show stop, start loop
		findViewById(R.id.header).setVisibility(View.GONE);
		findViewById(R.id.stopSlideshow).setVisibility(View.VISIBLE);
		slideshowRun = true;
		slideshowLoop();
	}
	private View.OnClickListener slideshowClick = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			slideshowStop();
		}
	};
	private void slideshowStop() {
		slideshowRun = false;
		findViewById(R.id.header).setVisibility(View.VISIBLE);
		findViewById(R.id.stopSlideshow).setVisibility(View.GONE);
	}
	private void slideshowLoop() {
		pager.postDelayed(slideshowNext, 3000);//3s
	}
	private boolean slideshowRun = false;
	private Runnable slideshowNext = new Runnable() {
		@Override
		public void run() {
			if (slideshowRun) {
				int index = pager.getCurrentItem();
				if (index < adapter.getCount() - 1) {
					pager.setCurrentItem(index + 1, true);
				} else {
					pager.setCurrentItem(0, true);
				}
				slideshowLoop();
			}
		}
	};
	
	//----------//----------//----------//----------//----------//
	
	private void saveBackGallery(final Data itemData) {
		if (Environment.getExternalStorageState() == null || !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			new AlertDialog.Builder(this).setNeutralButton(R.string.btn_ok1, null).setMessage(R.string.view_saveback_sdcard).create().show();
			return;
		}
		getLoadingSaveBack().setMessage(getString(R.string.view_save_back_processing));
		getLoadingSaveBack().show();
		onShowLoading();
		final PercentAsyncTask<Void, Integer> task = new PercentAsyncTask<Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				//save file to gallery
				String filename = "timelock" + System.currentTimeMillis();// + ".png";
				File outputFile = null;
				if (itemData.getDataType() == ItemViewDataType.Photo.ordinal()) {
					//getd
					filename += ".png";
					outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), filename);
				} else {
					filename += ".mp4";
					outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), filename);
				}
				try {
					boolean created = outputFile.createNewFile();
					if (!created) {
						outputFile.delete();
						created = outputFile.createNewFile();
						if (!created) {
							deleteFile(outputFile.getAbsolutePath());
							outputFile.createNewFile();
						}
					}
				} catch (Exception e) { }
				File inputFile = new File(itemData.getDataPath());
				MeobuFileTools.copyDecrypt(inputFile, outputFile, getCachePasscode(), this);
				//scan to gallery
				Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			    Uri contentUri = Uri.fromFile(outputFile);
			    mediaScanIntent.setData(contentUri);
			    sendBroadcast(mediaScanIntent);
			    return 1;
			}
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (values[0] > 8888) {
					//messageFile.append(values[1]).append(' ');
					return;
				}
				if (getLoadingSaveBack().isShowing()) {
					String message = getString(R.string.view_save_back_processing);
					if (values[1] >= 0 || values[2] >= 0) {
						String value1 = String.format("%.2f", values[1] / 1048576f);
						String value2 = String.format("%.2f", values[2] / 1048576f);
						message = message + "\n" + (values[1]<0?"?":value1) + "/" + (values[2]<0?"?":value2);
					}
					getLoadingSaveBack().setMessage(message);
				}
			}
	        @Override
	        protected void onPostExecute(Integer result) {
	        	Log.d("meobudebug", "saveBackGallery finish.");
	        	if (getLoadingSaveBack().isShowing()) {
	        		getLoadingSaveBack().dismiss();
	        		onHideLoading();
		    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	        	}
	        }
		};
		executeVoidVoidInteger(task);
    }
	String lastSharedFile;
	private void prepareSharedFile(int type) {
		try {
			File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
			File temp = null;
			if (type == ItemViewDataType.Photo.ordinal()) {
				temp = File.createTempFile("tmv", ".png", folder);
			} else {
				temp = File.createTempFile("tmv", ".mp4", folder);//3gp, 
			}
			temp.deleteOnExit();
			lastSharedFile = temp.getAbsolutePath();
		} catch (Exception e) {}
	}
	private void sendEmail(final Data data) {
		if (Environment.getExternalStorageState() == null || !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			new AlertDialog.Builder(this).setNeutralButton(R.string.btn_ok1, null).setMessage(R.string.view_email_sdcard).create().show();
			return;
		}
		getLoadingPrepare().setMessage(getString(R.string.view_email_preparing));
		getLoadingPrepare().show();
		onShowLoading();
		final PercentAsyncTask<Void, Integer> task = new PercentAsyncTask<Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				try {
					prepareSharedFile(data.getDataType());
					if (lastSharedFile == null) {
						return -1;
					}
					MeobuFileTools.copyDecrypt(data.getDataPath(), new File(lastSharedFile), getCachePasscode(), this);
				} catch (Exception e) {
					return -2;
				}
				return 1;
			}
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (values[0] > 8888) {
					//messageFile.append(values[1]).append(' ');
					return;
				}
				if (getLoadingPrepare().isShowing()) {
					String message = getString(R.string.view_email_preparing);
					if (values[1] >= 0 || values[2] >= 0) {
						String value1 = String.format("%.2f", values[1] / 1048576f);
						String value2 = String.format("%.2f", values[2] / 1048576f);
						message = message + "\n" + (values[1]<0?"?":value1) + "/" + (values[2]<0?"?":value2);
					}
					getLoadingPrepare().setMessage(message);
				}
			}
	        @Override
	        protected void onPostExecute(Integer result) {
	        	if (getLoadingPrepare().isShowing()) {
	        		getLoadingPrepare().dismiss();
	        		onHideLoading();
		    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		    		if (result > 0) {
			    		Intent emailIntent = new Intent(Intent.ACTION_SEND);
			    		emailIntent.setType("message/rfc822");
			    		emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.view_share_email_content));
			    		emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(lastSharedFile)));
			    		//Log.d("meobudebug", "go pause now!!!");
			    		startActivity(Intent.createChooser(emailIntent, "Send email"));//emailIntent);//
		    		} else if (result == -1) {
		    			new AlertDialog.Builder(ViewActivity.this)
		    				.setMessage(R.string.view_email_prepare_fail_external)
		    				.setNeutralButton(R.string.btn_ok1, null)
		    				.create().show();
		    		}
	        	}
	        }
		};
		executeVoidVoidInteger(task);
	}
	private AlertDialog loadingDelete;
	private AlertDialog getLoadingDelete() {
		if (loadingDelete == null) {
			loadingDelete = new AlertDialog.Builder(this).setMessage(R.string.view_share_delete_processing).setCancelable(false).create();
		}
		return loadingDelete;
	}
	private AlertDialog loadingSaveBack;
	private AlertDialog getLoadingSaveBack() {
		if (loadingSaveBack == null) {
			loadingSaveBack = new AlertDialog.Builder(this).setMessage(R.string.view_save_back_processing).setCancelable(false).create();
		}
		return loadingSaveBack;
	}
	private AlertDialog loadingPlay;
	private AlertDialog getLoadingPlay() {
		if (loadingPlay == null) {
			loadingPlay = new AlertDialog.Builder(this).setMessage(R.string.view_play_preprocessing).setCancelable(false).create();
		}
		return loadingPlay;
	}
	private AlertDialog loadingPrepare;
	private AlertDialog getLoadingPrepare() {
		if (loadingPrepare == null) {
			loadingPrepare = new AlertDialog.Builder(this).setMessage(R.string.view_email_preparing).setCancelable(false).create();
		}
		return loadingPrepare;
	}
	private void delete(final int index) {
		getLoadingDelete().show();
		onShowLoading();
		final AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				//delete in database, thumbnail, full file
				Data data = useList.get(index);
				((TimeLockApplication)getApplication()).removeData(data, getDataSource(), getFolder());
				return 1;
			}

	        @Override
	        protected void onPostExecute(Integer result) {
	        	if (getLoadingDelete().isShowing()) {
	        		getLoadingDelete().dismiss();
	        		onHideLoading();
		        	if (((TimeLockApplication)getApplication()).getSoundDeletion()) {
		        		playMeobuSoundPool(5);//reference TimeLockApplication soundpool
		        	}
		    		if (result > 0) {
		    			useList.remove(index);
		    			adapter.notifyDataSetChanged();
		    			if (useList.isEmpty()) {
		    				goBack();
		    			} else {
				    		resetPager(index);
				    		//update player and clear if any
				    		if (currentVideoIndex > pager.getCurrentItem()) {
				    			currentVideoIndex--;
				    		} else if (currentVideoIndex == pager.getCurrentItem()) {
				    			currentVideoIndex = -1;
				    			//release player
				    			if (currentVideoView != null) {
				    				currentVideoView.stopPlayback();
				    				currentVideoView = null;
				    			}
				    		}
		    			}
		    		}
	        	}
	        }
		};
		executeVoidVoidInteger(task);
	}
	//----------//----------//----------//----------//----------//
	
	private void updateTitle() {
		int count = 0;
		if (useList != null) {
			count = useList.size();
		}
		if (count == 0) {
			((TextView)findViewById(R.id.title)).setText("0/0");
		} else {
			String title = String.format("%d/%d", pager.getCurrentItem() + 1, count);
			((TextView)findViewById(R.id.title)).setText(title);
		}
	}
	
	//----------//----------//----------//----------//----------//
	
	ArrayList<Data> useList;
	HackyViewPager pager;
	int currentVideoIndex = -1;
	VideoView currentVideoView;
	View currentVideoGroup;
	MediaController videoController;//use for all
	public MediaController getVideoController() {
		if (videoController == null) {
			videoController = new MediaController(this);
		}
		return videoController;
	}
	private void checkNdecrypt() {
		final int index = pager.getCurrentItem();
		final Data data = useList.get(index);
		//Log.d("meobudebug", "checkNdecrypt   " + data.getDataPathDecrypted());
		if (data.getDataPathDecrypted() == null) {
			getLoadingPlay().setMessage(getString(R.string.view_play_preprocessing));
			getLoadingPlay().show();
			onShowLoading();
			final PercentAsyncTask<Void, Integer> task = new PercentAsyncTask<Void, Integer>() {
				@Override
				protected Integer doInBackground(Void... arg0) {
					int result = 0;
					try {
						File temp = File.createTempFile("tmv", null);
						data.setDataPathDecrypted(temp.getAbsolutePath());
						temp.deleteOnExit();
						MeobuFileTools.copyDecrypt(data.getDataPath(), temp, getCachePasscode(), this);
					} catch (Exception e) {
						result = -1;
					}
					return result;
				}
				@Override
				protected void onProgressUpdate(Integer... values) {
					super.onProgressUpdate(values);
					if (values[0] > 8888) {
						//messageFile.append(values[1]).append(' ');
						return;
					}
					if (getLoadingPlay().isShowing()) {
						String message = getString(R.string.view_play_preprocessing);
						if (values[1] >= 0 || values[2] >= 0) {
							String value1 = String.format("%.2f", values[1] / 1048576f);
							String value2 = String.format("%.2f", values[2] / 1048576f);
							message = message + "\n" + (values[1]<0?"?":value1) + "/" + (values[2]<0?"?":value2);
						}
						getLoadingPlay().setMessage(message);
					}
				}
		        @Override
		        protected void onPostExecute(Integer result) {
		        	if (getLoadingPlay().isShowing()) {
		        		getLoadingPlay().dismiss();
		        		onHideLoading();
			    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			        	if (result < 0) {
			        		//show message
			        		new AlertDialog.Builder(ViewActivity.this).setNeutralButton(R.string.btn_ok1, null).setMessage(R.string.view_decrypt_fail).create().show();
			        		//clean data
			        		data.setDataPathDecrypted(null);
			        	} else {
			        		checkNplay();
			        	}
		        	}
		        }
			};
			executeVoidVoidInteger(task);
		} else {
			checkNplay();
		}
	}
	private void tickVideoViewNMediaController(VideoView view, MediaController controller) {
		controller.setAnchorView(view);
		controller.setMediaPlayer(view);
		view.setMediaController(controller);
	}
	private void setItemViewState(boolean playing, View view) {
		if (playing) {
			view.findViewById(R.id.thumbnail).setVisibility(View.GONE);
			view.findViewById(R.id.play).setVisibility(View.GONE);
			view.findViewById(R.id.videoClickable).setVisibility(View.GONE);
		} else {
			view.findViewById(R.id.thumbnail).setVisibility(View.VISIBLE);
			view.findViewById(R.id.play).setVisibility(View.VISIBLE);
			view.findViewById(R.id.videoClickable).setVisibility(View.VISIBLE);
		}
	}
	private void checkNplay() {//always in main thread
		Data data = useList.get(pager.getCurrentItem());
		currentVideoIndex = pager.getCurrentItem();
		currentVideoGroup = pager.findViewById(currentVideoIndex);
		currentVideoView = (VideoView)(currentVideoGroup.findViewById(R.id.video));
		
		//prepare
		currentVideoView.setVideoPath(data.getDataPathDecrypted());
		//always success
		setItemViewState(true, currentVideoGroup);
		tickVideoViewNMediaController(currentVideoView, getVideoController());
		currentVideoView.start();
	}
	private final View.OnClickListener click = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			checkNdecrypt();
		}
	};
	private final OnPageChangeListener listener = new OnPageChangeListener() {
		@Override
		public void onPageScrollStateChanged(int state) {
			if (state == ViewPager.SCROLL_STATE_IDLE) {//after animation
				//update title
				updateTitle();
			} else if (state == ViewPager.SCROLL_STATE_DRAGGING) {//start dragging
				//pause video
				if (currentVideoView != null) {
					getVideoController().hide();
					currentVideoView.stopPlayback();
					setItemViewState(false, currentVideoGroup);
				}
			}
		}
		@Override
		public void onPageSelected(int position) { }
		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
	};
	private final PagerAdapter adapter = new PagerAdapter() {
		@Override
		public int getCount() {
			if (useList == null) return 0;
			return useList.size();
		}
		@Override
		public void startUpdate(ViewGroup container) {
			super.startUpdate(container);
			Log.d("meobudebug", "");
		}
		@Override
		public View instantiateItem(ViewGroup container, int position) {
			//Log.d("meobudebug", "instantiateItem: " + position);
			Data data = null;
			if (useList != null) {
				data = useList.get(position);
				View newView = null;
				if (data.getDataType() == ItemViewDataType.Photo.ordinal()) {
					PhotoView photoView = new PhotoView(container.getContext());
					doDisplayMeobuImage(useList.get(position).getDataPath(), photoView, true);
					newView = photoView;
				} else if (data.getDataType() == ItemViewDataType.Video.ordinal()) {
					LayoutInflater li = LayoutInflater.from(container.getContext());
					View view = li.inflate(R.layout.activity_view_video, container, false);
					//click
					view.findViewById(R.id.videoClickable).setOnClickListener(click);
					//thumb
					ImageView iv = (ImageView)view.findViewById(R.id.thumbnail);
					doDisplayMeobuImage(useList.get(position).getDataThumbnail(), iv, false);
					//view
					newView = view;
//				} else {//audio
//					LayoutInflater li = LayoutInflater.from(container.getContext());
//					View view = li.inflate(R.layout.activity_view_audio, container, false);
//					view.setOnClickListener(click);
//					newView = view;
				}
				newView.setTag(position);
				newView.setId(position);
				container.addView(newView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
				return newView;
			}
			return null;
		}
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}
		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	};
	
	//----------//----------//----------//----------//----------//

}
