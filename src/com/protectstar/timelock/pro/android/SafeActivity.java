package com.protectstar.timelock.pro.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.protectstar.safeuhr.android.R;
import common.view.OneByOnePercentAsyncTask;

public class SafeActivity extends SafeActivity7 {
	
	private void showFolderNameInput(String message, final int action, final int folderid) {
		final View input = LayoutInflater.from(this).inflate(R.layout.activity_safe_folder_name, null);
		if (input instanceof EditText) {
			((EditText)input).setText(message);
		}
		final AlertDialog ad = new AlertDialog.Builder(this)
			.setTitle(R.string.safe_folder_name_title)
			.setView(input)
			.setNeutralButton(R.string.btn_cancel, null)
			.setPositiveButton(R.string.btn_ok, null)
			.create();
		final View.OnClickListener done = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = null;
				if (input != null && input instanceof EditText) {
					name = ((EditText)input).getText().toString();
				}
				if (name == null || name.length() <= 0) {
					Toast.makeText(SafeActivity.this, R.string.safe_folder_name_empty, Toast.LENGTH_SHORT).show();
					return;
				}
				switch (action) {
				case 0://add
					doCreateFolder(name);
					break;
				case 1://edit
					doEditFolder(name, folderid);
				default:
					break;
				}
				ad.dismiss();
			}
		};
		ad.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface paramDialogInterface) {
				((AlertDialog)paramDialogInterface).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(done);
			}
		});
		ad.show();
	}
	
	@Override
	protected void createFolder() {
		Log.d("meobudebug", "create folder!!!");
		showFolderNameInput("", 0, 0);
	}
	private void doCreateFolder(final String name) {
		final OneByOnePercentAsyncTask<Void, Integer> task = new OneByOnePercentAsyncTask<Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				((TimeLockApplication)getApplication()).addFolder(name, getFolder(), getDataSourceFolder());
				return 0;
			}
			@Override
			protected void onPostExecute(Integer result) {
				super.onPostExecute(result);
				refreshListData();
			}
		};
		executeVoid(task);
	}
	
	@Override
	protected boolean editFolder(final int folderid) {
		if (folderid == TimeLockApplication.SECURED_FOLDER_ID ||
				folderid == TimeLockApplication.NOTE_FOLDER_ID ||
				folderid == TimeLockApplication.SPY_FOLDER_ID) {
			Log.d("meobudebug", "try to edit special folder name, failed!!!");
			return true;
		}
		final String[] texts = getResources().getStringArray(R.array.safe_folder_options);
//		String name = null;
//		try {name = ((TimeLockApplication)getApplication()).getHashNodeForSafeActivityFolderName().get(folderid).getName();} catch (Exception e) {}
//		texts[0] = String.format(texts[0], name);
//		texts[1] = String.format(texts[1], name);
		ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, android.R.id.text1, texts){
	        public View getView(int position, View convertView, ViewGroup parent) {
	            View v = super.getView(position, convertView, parent);
	            TextView tv = (TextView)v.findViewById(android.R.id.text1);
	            tv.setBackgroundColor(0xff545454);
	            tv.setTextColor(0xfff7f7f7);
	            int dp10 = (int) (10 * getResources().getDisplayMetrics().density + 0.5f);
	            tv.setCompoundDrawablePadding(dp10);
	            return v;
	        }
	    };
		new AlertDialog.Builder(this).setTitle(R.string.safe_folder_message)
		.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onHideLoading();
				if (which == 0) {
					String name = null;
					try {name = ((TimeLockApplication)getApplication()).getHashNodeForSafeActivityFolderName().get(folderid).getName();} catch (Exception e) {}
					showFolderNameInput(name, 1, folderid);
				} else {//if (which == 1) {
					deleteFolder(folderid);
				}
			}
		})
		.create().show();
		onShowLoading();
		return true;
	}
	
	private void doEditFolder(final String name, final int folderid) {
		final OneByOnePercentAsyncTask<Void, Integer> task = new OneByOnePercentAsyncTask<Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				((TimeLockApplication)getApplication()).renameFolder(name, folderid, getFolder(), getDataSourceFolder());
				return 0;
			}
			@Override
			protected void onPostExecute(Integer result) {
				super.onPostExecute(result);
				refreshListData();
			}
		};
		executeVoid(task);
	}
	
	protected void deleteFolder(final int folderid) {
		DialogInterface.OnClickListener delete = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onHideLoading();
				int[] selected = new int[1];
				selected[0] = -folderid;
				delete(selected);
			}
		};
		String name = null;
		try {name = ((TimeLockApplication)getApplication()).getHashNodeForSafeActivityFolderName().get(folderid).getName();} catch (Exception e) {}
		String message = String.format(getString(R.string.safe_folder_delete_confirm), name);
		new AlertDialog.Builder(this)
				.setNegativeButton(R.string.safe_delete_confirm_no, null)
				.setPositiveButton(R.string.safe_delete_confirm_yes, delete)
				.setMessage(message)
				.create().show();
		onShowLoading();
	}
	
	@Override
	protected void onSelectFolderInMultipleMode(int folderid) {
		Log.d("meobudebug", "select folder in multiple mode");
	}

}
