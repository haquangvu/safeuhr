package com.protectstar.timelock.pro.android;

import com.protectstar.timelock.pro.android.activity.VoicesActivity_Delete;

public class VoicesActivity extends VoicesActivity_Delete {

//	private void goBack() {
//		finish();
//		overridePendingTransition(R.anim.slide_in_from_left,
//				R.anim.slide_out_to_right);
//	}
//	
//	private void goVoiceRecord() {
//		Intent intent = ActivityVoiceRecord.createIntent(this);
//		startActivityForResult(intent, 100);
//		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
//	}
//	
//	private String onVoiceRecordedPath = null, onVoiceRecordedName = null;
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		if (resultCode == RESULT_OK) {
//			String path = ActivityVoiceRecord.getPath(data);
//			String name = ActivityVoiceRecord.getName(data);
//			if (path != null && path.length() > 0) {
//				onVoiceRecordedPath = path;
//				onVoiceRecordedName = name;
//			}
//		}
//	}
//	
//	//----------//----------//----------//----------//----------//
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_notes);
//		((TextView)findViewById(R.id.title)).setText(R.string.folder_voice_title);
//		setupList();
//		setupButtons();
//	}
//	
//	OneByOnePercentAsyncTask<Void, Integer> task = null;
//	@Override
//	protected void onResume() {
//		super.onResume();
//		if (onVoiceRecordedPath != null && onVoiceRecordedPath.length() > 0) {
//			doVoiceAdded(onVoiceRecordedPath, onVoiceRecordedName);
//			onVoiceRecordedPath = null;
//			onVoiceRecordedName = null;
//		} else {
//			doRefreshList();
//		}
//	}
//	
//	private AlertDialog loading;
//	private AlertDialog getLoading() {
//		if (loading == null) {
//			loading = new AlertDialog.Builder(this).setMessage(R.string.safe_add_processing).setCancelable(false).create();
//		}
//		return loading;
//	}
//	private void doVoiceAdded(final String path, final String name) {
//		getLoading().setMessage(getString(R.string.safe_add_processing));
//		getLoading().show();
//		onShowLoading();
//		task = new OneByOnePercentAsyncTask<Void, Integer>() {
//			@Override
//			protected Integer doInBackground(Void... arg0) {
//				if (path == null) return 0;
//				int errorCount = 0;
//				total = 1;
//				boolean external = ((TimeLockApplication)getApplication()).getExternalStoreEnable();
//					index = 1;
//					doPublishProgressFull(-1,-1);
//					File source = new File(path);
//					Log.d("meobudebug", "source   length:" + source.length() + "   path:" + source.getAbsolutePath());
//					String[] paths = TimeLockMediaFileLayer.generateSecretPaths(VoicesActivity.this, external);
//					File destination = new File(paths[0]);
//					Log.d("meobudebug", "destination   path:" + destination.getAbsolutePath());
//					
//					try {
//						doPublishLog(41);
//						MeobuFileTools.copyEncrypt(source, destination, TimeLockApplication.cachePasscode(), this);
//						TimeLockVoice item = new TimeLockVoice();
//						item.setDate(System.currentTimeMillis());
//						if (name == null || name.length() <= 0) {
//							item.setName("test name");
//						} else {
//							item.setName(name);
//						}
//						item.setPath(destination.getAbsolutePath());
//						long id = getDataSource().put(item);
//						if (id < 0) {
//							errorCount++;
//						}
//						
//						try { source.delete(); } catch (Exception e) { }
//					} catch (Exception e) {
//						e.printStackTrace();
//						errorCount++;
//						try { destination.delete(); } catch (Exception ee) {}
//					}
//				return errorCount;
//			}
//			
//			@Override
//			protected void onProgressUpdate(Integer... values) {
//				super.onProgressUpdate(values);
//				if (values[0] > 8888) {
//					return;
//				}
//				if (getLoading().isShowing()) {
//					String message = String.format(getString(R.string.safe_add_percentage_processing), values[1], values[2]);
//					if (values[3] >= 0 || values[4] >= 0) {
//						String value3 = String.format("%.2f", values[3] / 1048576f);
//						String value4 = String.format("%.2f", values[4] / 1048576f);
//						message = message + "\n" + (values[3]<0?"?":value3) + "/" + (values[4]<0?"?":value4);
//					}
//					getLoading().setMessage(message);
//				}
//			}
//
//	        @Override
//	        protected void onPostExecute(Integer result) {
//	    		if (getLoading().isShowing()) {
//	    			getLoading().dismiss();
//	    			onHideLoading();
//	    		}
////	    		if (result > 0) {
////    				Toast.makeText(VoicesActivity.this, R.string.safe_add_notsupport, Toast.LENGTH_SHORT).show();//TODO error message, ignore error
////	    		} else {
////	    		}
//				try { new File(path).delete(); } catch (Exception e) { }
//	    		doRefreshList();
//	        }
//		};
//		executeVoid(task);
//	}
//	private void doRefreshList() {
//		getLoading().setMessage(getString(R.string.view_play_preprocessing));
//		getLoading().show();
//		onShowLoading();
//		task = new OneByOnePercentAsyncTask<Void, Integer>() {
//			ArrayList<TimeLockVoice> list = null;
//			@Override
//			protected Integer doInBackground(Void... params) {
//				list = getDataSource().retrieve(0, Integer.MAX_VALUE);
//				return 0;
//			}
//			@Override
//			protected void onPostExecute(Integer result) {
//	    		if (getLoading().isShowing()) {
//	    			getLoading().dismiss();
//	    			onHideLoading();
//	    		}
//				if (!isCancelled()) {
//					useList = list;
//					listHandler.refresh();
//				}
//			}
//		};
//		executeVoid(task);
//	}
//	
//	@Override
//	protected void onPause() {
//		super.onPause();
//		if (task != null) {
//			task.cancel(false);
//			task = null;
//		}
//	}
//	
//	@Override
//	public void onBackPressed() {
//		goBack();
//	}
//
//	//----------//----------//----------//----------//----------//
//	
//	private void setupButtons() {
//		findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View arg0) {
//				goVoiceRecord();
//			}
//		});
//	}
//
//	//----------//----------//----------//----------//----------//
//	
//	ArrayList<TimeLockVoice> useList;
//	ArrayList<TimeLockVoice> getUseList() {
//		if (useList == null) {
//			useList = new ArrayList<TimeLockVoice>();
//		}
//		return useList;
//	}
//	private void setupList() {
//		((ListView)findViewById(R.id.list)).setOnItemClickListener(itemClick);
//		listHandler.setListView(this, R.id.list);
//	}
//	final OnItemClickListener itemClick = new OnItemClickListener() {
//		@Override
//		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//			//item click
//			long noteid = -1;
//			try {
//				if (position == id) {
//					noteid = useList.get(position).getId();
//				} else {
//					noteid = useList.get((int)id).getId();
//				}
//			} catch (Exception e) { }
//			if (noteid < 0) {
//				return;
//			}
//			//goVoiceRecord(noteid);//TODO play here
//		}
//	};
//	static final int[] listChildrenLayoutIds = {R.layout.activity_notes_item};
//	final MeobuListViewHandler<TimeLockVoice> listHandler = new MeobuListViewHandler<TimeLockVoice>(listChildrenLayoutIds) {
//		@Override
//		public ArrayList<TimeLockVoice> getMeobuListData() {
//			return getUseList();
//		}
//		@Override
//		public void updateMeobuView(int position, int type, View view) {
//			TimeLockVoice note = getUseList().get(position);
//			String title = note.getName();
//			if (title == null || title.length() <= 0) {
//				title = getString(R.string.note_item_notitle);
//			}
//			((TextView)view.findViewById(R.id.title)).setText(title);
//			String date = convertDateLong(note.getDate());
//			((TextView)view.findViewById(R.id.date)).setText(date);
//		}
//	};
//	
//	//----------//----------//----------//----------//----------//
//	
//	final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
//	String convertDateLong(long date) {
//		return format.format(new Date(date));
//	}
//
//	//----------//----------//----------//----------//----------//
//	
//	@Override
//	protected boolean supportMeobuSQLite() {
//		return true;
//	}
//	TimeLockVoicesDataSource datasource;
//	private TimeLockVoicesDataSource getDataSource() {
//		if (datasource == null) {
//			datasource = new TimeLockVoicesDataSource(this);
//		}
//		return datasource;
//	}
//	@Override
//	protected MeobuSQLiteDataSource getMeobuDataSource() {
//		return getDataSource();
//	}
//
//	//----------//----------//----------//----------//----------//
	
}
