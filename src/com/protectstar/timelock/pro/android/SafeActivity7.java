package com.protectstar.timelock.pro.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;
import com.protectstar.safeuhr.android.R;

/**
 * SecuredFolder: importing photo to vault
 * @require startAddItem method
 * @author meobu
 */
public class SafeActivity7 extends SafeActivity6 {
	
	@Override
	protected void onClick(int id) {
		if (-id == TimeLockApplication.SECURED_FOLDER_ID) {
			onSecuredFolderClick();
		} else {
			super.onClick(id);
		}
	}
	
	private void onSecuredFolderClick() {
		if (((TimeLockApplication)getApplication()).getSecuredFolderPassword()) {
			inputPasswordNCheck();
		} else {
			goSecuredFolder();
		}
	}
	private void inputPasswordNCheck() {
		final EditText input = new EditText(this);
		input.setInputType(EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS | EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
		input.setHint(R.string.safe_folder_secured_input_hint);
		final AlertDialog alert = new AlertDialog.Builder(this)
				.setTitle(R.string.safe_folder_secured_input)
				.setView(input)
				.setNegativeButton(R.string.changecode_button_cancel, null)
				.setPositiveButton(R.string.btn_ok1, null)
				.create();
		final View.OnClickListener onClick = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
		//final DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {
		//	@Override
		//	public void onClick(DialogInterface dialog, int button) {
				String securepass = input.getText().toString();
				compare2go(securepass, alert);
			}
		};
		alert.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				((AlertDialog)dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(onClick);
			}
		});
		alert.show();
	}
	
	private void compare2go(String input, DialogInterface dialog) {
		if (SecureActivity.compare(getApplicationContext(), input)) {
			dialog.dismiss();
			goSecuredFolder();
		} else {
			Toast.makeText(this, R.string.safe_folder_secured_password_wrong, Toast.LENGTH_SHORT).show();
		}
	}
	
	private void goSecuredFolder() {
		super.onClick(-TimeLockApplication.SECURED_FOLDER_ID);
	}

}
