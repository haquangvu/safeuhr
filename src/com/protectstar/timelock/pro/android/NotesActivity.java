package com.protectstar.timelock.pro.android;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import meobu.android.base.MeobuListViewHandler;
import meobu.android.base.MeobuSQLiteDataSource;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.TimeLockNote;
import com.protectstar.timelock.pro.android.data.TimeLockNotesDataSource;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.TextView;

public class NotesActivity extends TimeLockActivity {

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	private void goNoteDetail(long id) {
		Intent intent = ActivityNoteDetail.createIntent(this, id);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notes);
		setupList();
		setupButtons();
	}
	
	AsyncTask<Void, Void, Boolean> task = null;
	@Override
	protected void onResume() {
		super.onResume();
		AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
			ArrayList<TimeLockNote> list = null;
			@Override
			protected Boolean doInBackground(Void... params) {
				list = getDataSource().retrieve(0, Integer.MAX_VALUE);
				for (TimeLockNote note : list) {
					if (note.getName() == null || note.getName().length() <= 0) {
						note.setNameReadable(null);
					} else {
						note.setNameReadable(note.getName(), getCachePasscode());
					}
					if (note.getContent() == null || note.getContent().length() <= 0) {
						note.setContentReadable(null);
					} else {
						note.setContentReadable(note.getContent(), getCachePasscode());
					}
				}
				return true;
			}
			@Override
			protected void onPostExecute(Boolean result) {
				if (!isCancelled()) {
					useList = list;
					listHandler.refresh();
				}
			}
		};
		executeVoidVoidBoolean(task);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (task != null) {
			task.cancel(true);
			task = null;
		}
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	private void setupButtons() {
		findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				TimeLockNote note = new TimeLockNote();
				note.setDate(System.currentTimeMillis());
				long id = getDataSource().put(note);
				if (id < 0) {
					//TODO create note fail
					return;
				}
				note.setId(id);
				goNoteDetail(id);
			}
		});
	}

	//----------//----------//----------//----------//----------//
	
	ArrayList<TimeLockNote> useList;
	ArrayList<TimeLockNote> getUseList() {
		if (useList == null) {
			useList = new ArrayList<TimeLockNote>();
		}
		return useList;
	}
	private void setupList() {
		((AbsListView)findViewById(R.id.list)).setOnItemClickListener(itemClick);
		listHandler.setListView(this, R.id.list);
	}
	final OnItemClickListener itemClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			//item click
			long noteid = -1;
			try {
				if (position == id) {
					noteid = useList.get(position).getId();
				} else {
					noteid = useList.get((int)id).getId();
				}
			} catch (Exception e) { }
			if (noteid < 0) {
				//TODO on click fail
				return;
			}
			goNoteDetail(noteid);
		}
	};
	static final int[] listChildrenLayoutIds = {R.layout.activity_notes_item_grid};
	final MeobuListViewHandler<TimeLockNote> listHandler = new MeobuListViewHandler<TimeLockNote>(listChildrenLayoutIds) {
		@Override
		public ArrayList<TimeLockNote> getMeobuListData() {
			return getUseList();
		}
		@Override
		public void updateMeobuView(int position, int type, View view) {
			TimeLockNote note = getUseList().get(position);
			//title
			String title = note.getNameReadable();
			if (title == null || title.length() <= 0) {
				title = getString(R.string.note_item_notitle);
			}
			((TextView)view.findViewById(R.id.title)).setText(title);
			//content
			String content = note.getContentReadable();
			if (content == null) content = "";
			((TextView)view.findViewById(R.id.content)).setText(content);
			//date
			String date = convertDateLong(note.getDate());
			((TextView)view.findViewById(R.id.date)).setText(date);
		}
	};
	
	//----------//----------//----------//----------//----------//
	
	final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
	String convertDateLong(long date) {
		return format.format(new Date(date));
	}

	//----------//----------//----------//----------//----------//
	
	@Override
	protected boolean supportMeobuSQLite() {
		return true;
	}
	TimeLockNotesDataSource datasource;
	private TimeLockNotesDataSource getDataSource() {
		if (datasource == null) {
			datasource = new TimeLockNotesDataSource(this);
		}
		return datasource;
	}
	@Override
	protected MeobuSQLiteDataSource getMeobuDataSource() {
		return getDataSource();
	}

	//----------//----------//----------//----------//----------//
	
}
