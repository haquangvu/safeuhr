package com.protectstar.timelock.pro.android;

import java.io.File;

import org.json.JSONArray;

import android.widget.Toast;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData.ItemViewDataType;
import com.protectstar.timelock.pro.android.data.TimeLockSettingDataSource;

/**
 * SpyCamera: importing photo to vault
 * @require startAddItem method
 * @author meobu
 */
public class SafeActivity6 extends SafeActivity_NewAdd {//SafeActivity { //
	
	@Override
	protected void onPostUpdateData() {
		super.onPostUpdateData();
		if (!((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4])) return;
		try {
			String intrudersString = getApplication().getSharedPreferences("intruder", 0).getString("intruders", "[]");
			JSONArray intruders = new JSONArray(intrudersString);
			if (intruders.length() > 0) {
				File[] photos = new File[intruders.length()]; 
				for (int i=0; i<intruders.length(); i++) {
					photos[i] = new File(intruders.getString(i));
				}
				startAddItem(photos, ItemViewDataType.Photo.ordinal(), true, TimeLockApplication.SPY_FOLDER_ID);
				Toast.makeText(this, R.string.safe_importing_spycamera, Toast.LENGTH_SHORT).show();
				//getApplication().getSharedPreferences("intruder", 0).edit().putString("intruders", "[]").commit();
				getApplication().getSharedPreferences("intruder", 0).edit().putString("intruders", "[]").commit();
			}
		} catch (Exception e) {}
	}

}
