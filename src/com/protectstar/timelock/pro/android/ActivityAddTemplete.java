package com.protectstar.timelock.pro.android;

import java.io.File;

import meobu.android.base.MeobuCameraHandler;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.activity.SettingActivity106;
import com.protectstar.timelock.pro.android.data.TimeLockSettingDataSource;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityAddTemplete extends TimeLockActivity {

	private static int[] buttonids = { R.id.templeteScan, R.id.templeteAddAFile,
		R.id.templeteWebAccount, R.id.templeteInstantMessager, R.id.templeteEmailAccount,
		R.id.templeteComputerAccount, R.id.templeteNetwork};

	public ActivityAddTemplete() {
		// TODO Auto-generated constructor stub
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_addtemplete);

		setupButtons();
	}

	private final View.OnClickListener buttonClick = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			int index = getViewTag(view);
			onButtonClick(index);
		}
	};

	private void setupButtons() {
		for (int i=0; i<buttonids.length; i++) {
			View view = findViewById(buttonids[i]);
			view.setTag(i);
			view.setOnClickListener(buttonClick);
		}
	}

	//----------//----------//----------//----------//----------//

	private void onButtonClick(int index) {
		
		switch (index) {
		case 0:// scan file
			//				pushActivity(SoundActivity.class);
			startMeobuCameraPhotoCapture();
			break;
		case 1:// passcode
			//				pushActivity(CodeActivity.class);
			break;
		case 2:// destruction
			//				pushActivity(DestructionActivity.class);
			break;
		case 3:// language
			//				pushLanguageActivity();
			break;
		case 4:// ifile
			//				pushActivity(IFileActivity.class);
			break;
		case 5:// help
			//				showGuide();
			break;
		case 6:// support
			//				supportEmail();
			break;
		default:
			Toast.makeText(this, "Not implement yet.", Toast.LENGTH_SHORT).show();
			break;
		}
	}

	protected String meobuCameraMediaCachePath;
	protected void startMeobuCameraPhotoCapture() {
		meobuCameraMediaCachePath = MeobuCameraHandler.executeCameraPhotoCaptureActivity(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == MeobuCameraHandler.MeobuCapturePhotoActivity ||
				requestCode == MeobuCameraHandler.MeobuCaptureVideoActivity) {
			if (resultCode == RESULT_OK) {
				Object media = MeobuCameraHandler.onActivityResult(requestCode, resultCode, data, this, meobuCameraMediaCachePath);
				if (media != null) {
					if (requestCode == MeobuCameraHandler.MeobuCapturePhotoActivity) {
						if (media instanceof File) {
							Log.e("vuhadebug","path : " +((File)media).getPath() );
//							onMeobuCameraPhotoCaptured((File)media);
							Intent intent = new Intent(this, ActivityAddTemplete.class);
							Bundle bundle=new Bundle();
							bundle.putString("path", ((File)media).getPath());
							intent.putExtra("MyPackage", bundle);
							startActivity(intent);
						}
					}
				}
			}

		}
	}
}
