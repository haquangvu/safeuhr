package com.protectstar.timelock.pro.android;

import meobu.android.base.MeobuActivity;

import com.protectstar.safeuhr.android.R;

import android.os.Bundle;
import android.content.Intent;

public class MainActivity extends MeobuActivity {
	
	private void goNext() {
        if (((TimeLockApplication)getApplication()).shouldGuide()) {
			finish();
			Intent intent = new Intent(this, FullGuideActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
        } else {
			finish();
			Intent intent = new Intent(this, ClockActivity6.class);
			startActivity(intent);
			overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
        }
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    	goNext();
    }
    
}
