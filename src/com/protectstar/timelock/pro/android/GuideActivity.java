package com.protectstar.timelock.pro.android;

import com.protectstar.safeuhr.android.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import meobu.android.base.MeobuActivity;

public class GuideActivity extends MeobuActivity {

	public static final String FIRST_GUIDE_KEY = "FIRSTGUIDE";
	public static final String GUIDE_INDEX_KEY = "GUIDEINDEX";
	//public static final int[] guideLayoutIds = {R.layout.activity_guide1, R.layout.activity_guide2, R.layout.activity_guide3, R.layout.activity_guide4, R.layout.activity_guide5, R.layout.activity_guide6, R.layout.activity_guide7};
	public static final int[] guideLayoutIds = {R.layout.activity_guide1, R.layout.activity_guide2, R.layout.activity_guide3, R.layout.activity_guide4, R.layout.activity_guide6, R.layout.activity_guide7};
	boolean firstGuide;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		firstGuide = getIntent().getBooleanExtra(FIRST_GUIDE_KEY, true);
		final int layoutindex = getIntent().getIntExtra(GUIDE_INDEX_KEY, 0);
		if (firstGuide) {
			((TimeLockApplication)getApplication()).offGuide();
		}
		setContentView(guideLayoutIds[layoutindex]);
		findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				goClose();
			}
		});
		findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if ((firstGuide && layoutindex + 1 >= guideLayoutIds.length) ||
						(!firstGuide && layoutindex + 2 >= guideLayoutIds.length)) {
					goClose();
//				if (firstGuide && layoutindex + 1 >= guideLayoutIds.length) {
//					//end first, go clock
//					finish();
//					Intent intent = new Intent(GuideActivity.this, ClockActivity.class);
//					startActivity(intent);
//					overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
//				} else if (!firstGuide && layoutindex + 2 >= guideLayoutIds.length) {
//					//end from setting, back
//					finish();
//					overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
				} else {
					//next guide
					finish();
					Intent intent = new Intent(GuideActivity.this, GuideActivity.class);
					intent.putExtra(FIRST_GUIDE_KEY, firstGuide);
					intent.putExtra(GUIDE_INDEX_KEY, layoutindex + 1);
					startActivity(intent);
					overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
				}
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		firstGuide = getIntent().getBooleanExtra(FIRST_GUIDE_KEY, true);
	}
	
	@Override
	public void onBackPressed() {
		goClose();
	}
	
	private void goClose() {
		if (firstGuide) {
			finish();
			Intent intent = new Intent(GuideActivity.this, ClockActivity6.class);
			startActivity(intent);
			overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
		} else {
			finish();
			overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
		}
	}
	
}
