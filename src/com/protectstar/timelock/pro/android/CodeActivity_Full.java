package com.protectstar.timelock.pro.android;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;

import kankan.wheel.widget.WheelView;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.TimeLockApplication.Data;
import com.protectstar.timelock.pro.android.data.TimeLockMediaFileLayer;
import com.protectstar.timelock.pro.android.data.TimeLockNode;

import common.view.OneByOnePercentAsyncTask;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import meobu.android.base.MeobuFileTools;

public class CodeActivity_Full extends TimeLockActivity {

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_changepasscode);
		setButtons();
		setupWheels();
	}
	
	int hour, minute, second;
	@Override
	protected void onResume() {
		super.onResume();
		reset();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		onHideLoading();
		hideChangingLoading();
		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	kankan.wheel.widget.OnWheelChangedListener listener = new kankan.wheel.widget.OnWheelChangedListener() {
		@Override
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			if (wheel == secondWheel) {
				second = newValue;
			} else if (wheel == hourWheel) {
				hour = newValue;
			} else {
				minute = newValue;
			}
		}
	};
	
	private WheelView hourWheel, minuteWheel, secondWheel;
	private void setupWheels() {
		hourWheel = (WheelView)findViewById(R.id.inputHourWheel);
		hourWheel.setBackgroundResource(0);
        DateArrayAdapter hourAdapter = new DateArrayAdapter(this, 0);
        hourWheel.setViewAdapter(hourAdapter);
        hourWheel.addChangingListener(listener);

        minuteWheel = (WheelView)findViewById(R.id.inputMinuteWheel);
        minuteWheel.setBackgroundResource(0);
        DateArrayAdapter minuteAdapter = new DateArrayAdapter(this, 1);
        minuteWheel.setViewAdapter(minuteAdapter);
        minuteWheel.addChangingListener(listener);
        
        secondWheel = (WheelView)findViewById(R.id.inputSecondWheel);
        secondWheel.setBackgroundResource(0);
        DateArrayAdapter secondAdapter = new DateArrayAdapter(this, 1);
        secondWheel.setViewAdapter(secondAdapter);
        secondWheel.addChangingListener(listener);

	}
	private static class DateArrayAdapter extends kankan.wheel.widget.adapters.ArrayWheelAdapter<String> {
		private static final String[][] values =
			{{"00","01","02","03","04","05","06","07","08","09","10","11"},
			{"00","01","02","03","04","05","06","07","08","09",
				"10","11","12","13","14","15","16","17","18","19",
				"20","21","22","23","24","25","26","27","28","29",
				"30","31","32","33","34","35","36","37","38","39",
				"40","41","42","43","44","45","46","47","48","49",
				"50","51","52","53","54","55","56","57","58","59"}};
        public DateArrayAdapter(Context context, int index) {
            super(context, values[index]);
        }
        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, view.getContext().getResources().getDisplayMetrics());
            view.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, height));
            view.setTextColor(0xFF888888);
            view.setGravity(Gravity.CENTER);
            view.setTypeface(Typeface.DEFAULT);
            view.setTextAppearance(view.getContext(), android.R.attr.textAppearanceMedium);
        }
    }

	//----------//----------//----------//----------//----------//
	
	private void reset() {
		hour = ((TimeLockApplication)getApplication()).getCachePasscodeHour();
		minute = ((TimeLockApplication)getApplication()).getCachePasscodeMinute();
		second = ((TimeLockApplication)getApplication()).getCachePasscodeSecond();
		updateViews();
	}
	
	private void updateViews() {
		doUpdateNumbers(hour, minute, second);
		doUpdateHour(hour);
		doUpdateMinute(minute);
		doUpdateSecond(second);
	}
	
	private void setButtons() {
		findViewById(R.id.cancel).setOnClickListener(cancelClick);
		findViewById(R.id.set).setOnClickListener(setClick);
		
		//findViewById(R.id.inputHourPrev).setOnClickListener(decreaseHourClick);
		findViewById(R.id.inputHourPrevIcon).setOnClickListener(decreaseHourClick);
		//findViewById(R.id.inputHourNext).setOnClickListener(increaseHourClick);
		findViewById(R.id.inputHourNextIcon).setOnClickListener(increaseHourClick);
		
		//findViewById(R.id.inputMinutePrev).setOnClickListener(decreaseMinuteClick);
		findViewById(R.id.inputMinutePrevIcon).setOnClickListener(decreaseMinuteClick);
		//findViewById(R.id.inputMinuteNext).setOnClickListener(increaseMinuteClick);
		findViewById(R.id.inputMinuteNextIcon).setOnClickListener(increaseMinuteClick);
		
		//findViewById(R.id.inputSecondPrev).setOnClickListener(decreaseSecondClick);
		findViewById(R.id.inputSecondPrevIcon).setOnClickListener(decreaseSecondClick);
		//findViewById(R.id.inputSecondNext).setOnClickListener(increaseSecondClick);
		findViewById(R.id.inputSecondNextIcon).setOnClickListener(increaseSecondClick);
	}
	
	private AlertDialog loading;
	protected AlertDialog getLoading() {
		if (loading == null) {
			loading = new AlertDialog.Builder(this).setCancelable(false).setMessage(R.string.changecode_changepass_processing).create();
		}
		return loading;
	}
	protected void showChangingLoading() {
		getLoading().setMessage(getString(R.string.changecode_changepass_processing));
		getLoading().show();
		onShowLoading();
	}
	protected void hideChangingLoading() {
    	if (getLoading().isShowing()) {
    		getLoading().dismiss();
    		onHideLoading();
    	}
	}
	
	protected void updateNewPassword(final int newHour, final int newMinute, final int newSecond) {
		//final AlertDialog loading = new AlertDialog.Builder(this).setCancelable(false).setMessage(R.string.changecode_changepass_processing).create();
		showChangingLoading();
//		getLoading().setMessage(getString(R.string.changecode_changepass_processing));
//		getLoading().show();
//		onShowLoading();
		Toast.makeText(this, "Please stay here until the process finish!", Toast.LENGTH_LONG).show();
		final OneByOnePercentAsyncTask<Void, Integer> task = new OneByOnePercentAsyncTask<Void, Integer>() {
		//};
		//final AsyncTask<Void, Integer, Integer> task = new AsyncTask<Void, Integer, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				String oldPassword = getCachePasscode();
				String newPassword = TimeLockApplication.buildPasscode(newHour, newMinute, newSecond);
				//Log.d("meobudebug", "oldPassword: " + oldPassword + "   newPassword: " + newPassword);
				//build list
				ArrayList<Data> list = new ArrayList<Data>();
				Enumeration<TimeLockNode> elements = ((TimeLockApplication)getApplication()).getHashNodeForCodeActivityOnly().elements();
				while (elements.hasMoreElements()) {
					list.addAll(elements.nextElement().getContents());
				}
				//ArrayList<Data> list = ((TimeLockApplication)getApplication()).getListData();
				//do convert
				if (list == null || list.isEmpty() || list.size() <= 0) return 1;
				total = list.size();
				String[] tempPaths = TimeLockMediaFileLayer.generateSecretPaths(CodeActivity_Full.this, false);
				File tempFile = new File(tempPaths[0]);
				int errorCount = 0;
				for (int i=0; i<list.size(); i++) {
					index = i+1;
					doPublishProgressFull(-1,-1);
					Data item = list.get(i);
					try {
						//clean cache if any
						if (item.getDataPathDecrypted() != null) {
							try {new File(item.getDataPathDecrypted()).delete();} catch (Exception e) {}
							item.setDataPathDecrypted(null);
						}
//						//1st approach, rename, resave old name failed!!!
//						//rename the file
//						String fullpath = item.getDataPath();
//						File file = new File(fullpath);
//						if (tempFile.exists()) {
//							tempFile.delete();
//						}
//						Log.d("meobudebug", "rename 0000000: " + file.length() + "   " + tempFile.length());
//						Log.d("meobudebug", "00000 rename:   " + file.getName() + "   " + tempFile.getName());
//						if (file.renameTo(tempFile)) {
//							//re-save new password
//							//file = new File(fullpath);
//							//tempFile = new File(tempPaths[0]);
//							Log.d("meobudebug", "rename success: " + file.length() + "   " + tempFile.length());
//							Log.d("meobudebug", "after rename:   " + file.getName() + "   " + tempFile.getName());
//							MeobuFileTools.copyChangePassword(tempFile, file, oldPassword, newPassword);
//							Log.d("meobudebug", "copycp success: " + file.length() + "   " + tempFile.length());
//							//MeobuFileTools.copyDecrypt(tempFile, file, oldPassword);
//						} else {
//							//error
//							errorCount++;
//							Log.d("meobuwarning", "Cannot rename from: " + file.getPath() + "   to: " + tempFile.getPath() + "   without reason.");
//						}
						//2nd approach, decrypt to temp, re-encrypt to old name
						MeobuFileTools.copyDecrypt(item.getDataPath(), tempFile, oldPassword, this);
						MeobuFileTools.copyEncrypt(tempFile, new File(item.getDataPath()), newPassword, this);
					} catch (Exception e) {
						errorCount++;
						//Log.d("meobuwarning", "Cannot change password. " + e.getMessage());
						e.printStackTrace();
					}
				}
				try { tempFile.delete(); } catch (Exception e) { }
				if (errorCount > 0) {
					return -errorCount;
				}
				return 1;
			}
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (getLoading().isShowing()) {
					if (values[0] > 8888) {
						//messageFile.append(values[1]).append(' ');
						return;
					}
					String message = String.format(getString(R.string.changecode_changepass_percentage_processing), values[1], values[2]);
					if (values[3] >= 0 || values[4] >= 0) {
						int value3i = values[3];
						int value4i = values[4];
						if (values[0] > 0) {
							value3i += value4i;
						}
						value4i += value4i;
						String value3 = String.format("%.2f", value3i / 1048576f);
						String value4 = String.format("%.2f", value4i / 1048576f);
						message = message + "\n" + (values[3]<0?"?":value3) + "/" + (values[4]<0?"?":value4);
					}
					getLoading().setMessage(message);
				}
			}
	        @Override
	        protected void onPostExecute(Integer result) {
	        	hideChangingLoading();
//	        	if (getLoading().isShowing()) {
//	        		getLoading().dismiss();
//	        		onHideLoading();
//		    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//		    		//TODO skip passcode
//	        	}
	    		if (result >= 0) {
	    			Toast.makeText(CodeActivity_Full.this, "Change passcode success!!!", Toast.LENGTH_SHORT).show();
	    		} else {
	    			Toast.makeText(CodeActivity_Full.this, "Cannot convert " + (-result) + " files.", Toast.LENGTH_SHORT).show();
	    		}
				((TimeLockApplication)getApplication()).setNewPasscode(newHour, newMinute, newSecond);
    			doUpdateNumbers(hour, minute, second);
	        }
		};
		executeVoidVoidInteger(task);
	}
	
	private final View.OnClickListener setClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			updateNewPassword(hour, minute, second);
		}
	};
	private final View.OnClickListener cancelClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			reset();
		}
	};
	private final View.OnClickListener increaseHourClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			hour++;
			if (hour >= 12) {
				hour = 0;
			}
			doUpdateHour(hour);
		}
	};
	private final View.OnClickListener decreaseHourClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			hour--;
			if (hour < 0) {
				hour = 11;
			}
			doUpdateHour(hour);
		}
	};
	private final View.OnClickListener increaseMinuteClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			minute++;
			if (minute >= 60) {
				minute = 0;
			}
			doUpdateMinute(minute);
		}
	};
	private final View.OnClickListener decreaseMinuteClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			minute--;
			if (minute < 0) {
				minute = 59;
			}
			doUpdateMinute(minute);
		}
	};
	private final View.OnClickListener increaseSecondClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			second++;
			if (second >= 60) {
				second = 0;
			}
			doUpdateSecond(second);
		}
	};
	private final View.OnClickListener decreaseSecondClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			second--;
			if (second < 0) {
				second = 59;
			}
			doUpdateSecond(second);
		}
	};

	//----------//----------//----------//----------//----------//
	
	String int2String(int value) {
		if (value < 10) {
			return "0" + Integer.toString(value);
		}
		return Integer.toString(value);
	}
	void doUpdateHour(int hour) {
		hourWheel.setCurrentItem(hour);
	}
	void doUpdateMinute(int minute) {
		minuteWheel.setCurrentItem(minute);
	}
	void doUpdateSecond(int second) {
		secondWheel.setCurrentItem(second);
	}

	//----------//----------//----------//----------//----------//
	
	void doUpdateNumbers(int hour, int minute, int second) {
		setNumber((ImageView)findViewById(R.id.clockHour0), hour / 10);
		setNumber((ImageView)findViewById(R.id.clockHour1), hour % 10);
		setNumber((ImageView)findViewById(R.id.clockMinute0), minute / 10);
		setNumber((ImageView)findViewById(R.id.clockMinute1), minute % 10);
		setNumber((ImageView)findViewById(R.id.clockSecond0), second / 10);
		setNumber((ImageView)findViewById(R.id.clockSecond1), second % 10);
	}
	
	static final int[] numberResources = {
		R.drawable.num_0,
		R.drawable.num_1,
		R.drawable.num_2,
		R.drawable.num_3,
		R.drawable.num_4,
		R.drawable.num_5,
		R.drawable.num_6,
		R.drawable.num_7,
		R.drawable.num_8,
		R.drawable.num_9 };
	Drawable[] numberDrawables;
	Drawable[] getNumberDrawables() {
		if (numberDrawables == null) {
			numberDrawables = new Drawable[numberResources.length];
			for (int i=0; i<numberResources.length; i++) {
				numberDrawables[i] = getResources().getDrawable(numberResources[i]);
			}
		}
		return numberDrawables;
	}
	void setNumber(ImageView view, int number) {
		//view.setImageResource(numberResources[number]);
		view.setImageDrawable(getNumberDrawables()[number]);
	}

	//----------//----------//----------//----------//----------//
	
}
