package com.protectstar.timelock.pro.android;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import com.protectstar.safeuhr.android.R;

import meobu.android.base.MeobuActivity;

public class TimeLockActivity extends MeobuActivity {
	
	//----------//----------//----------//----------//----------//
	
	protected String getCachePasscode() {
		if (TimeLockApplication.cachePasscode() != null && TimeLockApplication.cachePasscode().length() > 0) {
			return TimeLockApplication.cachePasscode();
		}
		String passcode = getIntent().getStringExtra("intent_cache_passcode");
		if (passcode != null && passcode.length() > 0) {
			return passcode;
		}
		return "";
	}
	
	//----------//----------//----------//----------//----------//
	
	//TODO that bai cua tao hoa Screen ON/OFF
//	BroadcastReceiver receiverScreenOff = new BroadcastReceiver() {
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			Log.d("meobudebug", "onReceive: " + intent);
//			onScreenOff();
//		}
//	};
//	private final IntentFilter filterScreenOff = new IntentFilter(Intent.ACTION_SCREEN_OFF);
//	private void registerScreenOff() {
//		Intent result = getApplicationContext().registerReceiver(receiverScreenOff, filterScreenOff);
//		Log.d("meobudebug", "#registerScreenOff: " + result);
//	}
//	private void unregisterScreenOff() {
//		getApplicationContext().unregisterReceiver(receiverScreenOff);
//		Log.d("meobudebug", "unregisterScreenOff#");
//	}
//	private void onScreenOff() {
//		Log.d("meobudebug", "onScreenOff");
//		goClock();
//	}
	private void detectScreenOff() {
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
	    boolean isScreenOn = powerManager.isScreenOn();
	    Log.d("meobudebug", "onPause: screen on? " + isScreenOn);
	    if (!isScreenOn) {
	    	goClock();
	    }
	}
	
	//----------//----------//----------//----------//----------//
	
	public static final int[] DURATIONS_TIMEOUT = {120000, 300000, 600000, 1200000, -1};
	public static int CURRENT_DURATION_TIMEOUT = 0;
	public static void setLockOptionIndex(int index) {
		CURRENT_DURATION_TIMEOUT = DURATIONS_TIMEOUT[index];
	}
	public static final int DURATION_TIMEOUT_BUFFER = 1000;
	public long lastInteraction = 0;
	public boolean shouldStop = false;
	
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		lastInteraction = System.currentTimeMillis();
	}
	
	private boolean isLoading;
	protected void onShowLoading() {
		isLoading = true;
	}
	protected void onHideLoading() {
		isLoading = false;
		lastInteraction = System.currentTimeMillis();
	}
	
	private View getAView() {
		View view = findViewById(R.id.header);
		if (view != null) return view;
		view = findViewById(R.id.main_header);
		return view;
	}
	
	private Runnable checkRunnable = new Runnable() {
		@Override
		public void run() {
			setLockOptionIndex(((TimeLockApplication)getApplication()).getLockOptionIndex());
			if (shouldStop) return;
			if (isLoading) {
				lastInteraction = System.currentTimeMillis();
			}
			if (System.currentTimeMillis() - lastInteraction >= CURRENT_DURATION_TIMEOUT) {
				goClock();
			} else {
				postRunnable(CURRENT_DURATION_TIMEOUT - System.currentTimeMillis() + lastInteraction);
			}
		}
	};
	private void postRunnable(long delay) {
		if (delay < 0) return;
		getAView().postDelayed(checkRunnable, delay + DURATION_TIMEOUT_BUFFER);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setLockOptionIndex(((TimeLockApplication)getApplication()).getLockOptionIndex());
		lastInteraction = System.currentTimeMillis();
		shouldStop = false;
		postRunnable(CURRENT_DURATION_TIMEOUT);
//		registerScreenOff();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		shouldStop = true;
		getAView().removeCallbacks(checkRunnable);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		detectScreenOff();
//		unregisterScreenOff();
	}
	
	//----------//----------//----------//----------//----------//
	
	protected void goClock() {
		((TimeLockApplication)getApplication()).cleanCache();
		finish();
		Intent intent = new Intent(this, ClockActivity6.class);
		addFlag1Task1Top(intent);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_from_top, R.anim.hold_fade_out);
		((TimeLockApplication)getApplication()).cleanCache();
	}
	private void addFlag1Task1Top(Intent intent) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			addFlag1Task1TopBelow11(intent);
		} else {
			addFlag1Task1Top11AndOver(intent);
		}
	}
	private void addFlag1Task1TopBelow11(Intent intent) {
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void addFlag1Task1Top11AndOver(Intent intent) {
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
	}

	//----------//----------//----------//----------//----------//
	
	@Override
	protected boolean supportMeobuShakeDetection() {
		return true;
	}
	@Override
	protected void onMeobuShakeDetected() {
		goClock();
	}

	//----------//----------//----------//----------//----------//
	
}
