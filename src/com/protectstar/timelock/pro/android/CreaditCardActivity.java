package com.protectstar.timelock.pro.android;

import com.protectstar.safeuhr.android.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CreaditCardActivity extends TimeLockActivity {

	public CreaditCardActivity() {
		// TODO Auto-generated constructor stub
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_safe_folder);
		((TextView)findViewById(R.id.title)).setText(R.string.safe_folder_templete_title);
	    // TODO Auto-generated method stub
		findViewById(R.id.add).setVisibility(View.VISIBLE);
		
		setupButton();
	}
	
	private void setupButton() {
		findViewById(R.id.add).setOnClickListener(addClick);
	}
	
	private final View.OnClickListener addClick = new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			onAddClick();
		}
	};
	
	private void onAddClick() {
		Intent intent = new Intent(this, ActivityAddTemplete.class);
		startActivity(intent);
		overridePendingTransition(R.anim.hold_fade_in, R.anim.slide_out_to_left);
	}
}

