package com.protectstar.timelock.pro.android;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Random;

import meobu.android.base.MeobuActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.Toast;
import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.alarm.AlarmService;
import com.protectstar.timelock.pro.android.data.ClockData;
import com.protectstar.timelock.pro.android.data.TimeLockContentsDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockFoldersDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockNotesDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockSettingDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockVoicesDataSource;

public class ClockActivity extends MeobuActivity {
	
	private void changeNext() {
		Intent intent = new Intent(this, SafeActivity.class);
		intent.putExtra("intent_cache_passcode", TimeLockApplication.cachePasscode());
		startActivity(intent);
		overridePendingTransition(R.anim.hold_fade_in, R.anim.slide_out_to_top);
		if (((TimeLockApplication)getApplication()).getSoundSafe()) {
			playMeobuSoundPool(4);//reference TimeLockApplication soundpool
		}
	}
	
	private void changeBackWizard() {
		Intent intent = new Intent(this, FullGuideActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
		finish();
	}
	
	private void setAlarmNotification(long time) {
        Intent service = new Intent(this, AlarmService.class);
        if (time <= 0) {
        	service.setAction(AlarmService.CANCEL);
        } else {
        	service.setAction(AlarmService.CREATE);
        }
        service.putExtra("notificationDate", time);
        this.startService(service);
	}

	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static final String md5(final String s) {
	    try {
	        // Create MD5 Hash
	        MessageDigest digest = java.security.MessageDigest
	                .getInstance("MD5");
	        digest.update(s.getBytes());
	        byte messageDigest[] = digest.digest();

	        // Create Hex String
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < messageDigest.length; i++) {
	            String h = Integer.toHexString(0xFF & messageDigest[i]);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        }
	        return hexString.toString();

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return "";
	}
	
	public static void save(TimeLockApplication app, String password) {
		app.getSharedPreferences("passcode", 0).edit()
			.putString("storedPassword", password)
			.commit();
	}
	public static String generateNew(int seed1, int seed2, int seed3) {
		int value1 = new Random(seed1).nextInt();
		int value2 = new Random(seed2).nextInt();
		int value3 = new Random(seed3).nextInt();
		String core = Integer.toString(value1) + Integer.toString(value2) + Integer.toString(value3);
		return md5(core);
	}
//	private static String generate(int seed1, int seed2) {
//		int value1 = new Random(seed1).nextInt();
//		int value2 = new Random(seed2).nextInt();
//		String core = Integer.toString(value1) + Integer.toString(value2);
//		return md5(core);
//	}

	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
//	private void changeToState(int state) {
//		//default state, no alarm, no input
//		//hour, minute, second
//		//no clock touch, alarm dark,
//		//update current
//		if (state == 0) {
//			
//		//alarm state
//		//hour, minute, alarm
//		//clock touch, alarm light
//		//default: update current, ontouch: update alarm time
//		} else if (state == 1) {
//			
//		//input state
//		//hour, minute
//		//clock touch, alarm dark
//		//default: nothing, ontouch: update touch time
//		} else {
//			
//		}
//	}
	//when state change
	//1. prepare views: turn off animation, ...
	//2. splash animation
	//3. UPDATING...
	//4. ending animation
	//5. update views: 
	
	boolean alarmState;
	int alarmValue;
	private void setupAlarmButton() {
		//ons
		findViewById(R.id.alarmClick).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				changeAlarmState(!alarmState);
			}
		});
	}
	private void changeAlarmState(boolean alarmState) {
		if (!manualUpdating) {
			if (!alarmState) {
				NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
				nm.cancel(0);
			}
			this.alarmState = alarmState;
			updateAlarmStateUI();
			updateAlarmNotification();
		}
	}
	private void updateAlarmStateUI() {
		if (alarmState) {
			findViewById(R.id.clockBaseView).setOnTouchListener(clockTouch);
			findViewById(R.id.alarmView).setEnabled(true);
			turnSecondIndicator(false);
			turnAlarmIndicator(true);
			alarmUpdateValue = alarmCurrentValue;
			changeAction(UpdatePostActionId.RequestCurrentAlarmTouchUp.ordinal());
		} else {
			findViewById(R.id.clockBaseView).setOnTouchListener(null);
			findViewById(R.id.alarmView).setEnabled(false);
			turnAlarmIndicator(false);
			turnSecondIndicator(true);
			changeAction(UpdatePostActionId.RequestUpdateTime.ordinal());
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		DisplayMetrics metrics = getResources().getDisplayMetrics();
//		if (metrics.widthPixels < metrics.heightPixels) {
			setContentView(R.layout.activity_clock);
//		} else {
//			setContentView(R.layout.activity_clockh);
//		}
		updateViews1(this);
		setupAlarmButton();
		findViewById(R.id.clockCenterButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				switchManualUpdating();
			}
		});
	}
	private String storedPassword;
	private String storedPasswordCache;
	private int passwordFailCount;
	private int passwordAttemptCount4Delay;
	private long passwordAttemptDate4Delay;
	@Override
	protected void onResume() {
		super.onResume();
		((TimeLockApplication)getApplication()).cleanCache();
		((TimeLockApplication)getApplication()).clearCachePasscode();
		startUpdateLoop();
		//alarm params
		alarmState = getSharedPreferences("alarm", 0).getBoolean("alarmState", false);
		alarmCurrentValue = getSharedPreferences("alarm", 0).getFloat("alarmCurrentValue", 0);
		updateAlarmStateUI();
		updateAlarmNotification();
		
		//password params
		storedPassword = getSharedPreferences("passcode", 0).getString("storedPassword", "");
		passwordFailCount = getSharedPreferences("alarm", 0).getInt("passwordFailCount", 0);
		passwordAttemptCount4Delay = getSharedPreferences("alarm", 0).getInt("passwordAttemptCount4Delay", 0);
		passwordAttemptDate4Delay = getSharedPreferences("alarm", 0).getLong("passwordAttemptDate4Delay", 0);
		TimeLockApplication app = (TimeLockApplication)getApplication();
		//empty
		if (isStringEmpty(storedPassword)) {
			//create password
			Toast.makeText(this, R.string.clock_password_require, Toast.LENGTH_SHORT).show();
			storedPasswordCache = null;
			storedPassword = null;
			startManualUpdating();
		} else {
//			//check if outdate
			storedPasswordCache = storedPassword;
			turnNumberAnimation(true);
			if (System.currentTimeMillis() < passwordAttemptDate4Delay) {
				setStateHacker(true);
			}
		}
		numberSound = app.getSoundIndicator();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		stopUpdateLoop();
		if (isStringEmpty(storedPassword)) {
			storedPassword = "";
		}
		getSharedPreferences("alarm", 0).edit()
			.putBoolean("alarmState", alarmState)
			.putFloat("alarmCurrentValue", alarmCurrentValue)
			.putInt("passwordFailCount", passwordFailCount)
			.putInt("passwordAttemptCount4Delay", passwordAttemptCount4Delay)
			.putLong("passwordAttemptDate4Delay", passwordAttemptDate4Delay)
			.commit();
		save((TimeLockApplication)getApplication(), storedPassword);
		manualUpdating = false;
		turnNumberAnimation(false);
	}
	
	boolean hackerState = false;
	private void changeStateHacker(boolean enable) {
		if (enable != hackerState) {
			setStateHacker(enable);
		}
	}
	private void setStateHacker(boolean enable) {
		hackerState = enable;
		ImageView center = ((ImageView)findViewById(R.id.clockCenterButton));
		if (enable) {
			//TODO update hacker
//			TextView view = ((TextView)findViewById(R.id.hackerBlock));
//			view.setText("");
//			view.setVisibility(View.VISIBLE);
			center.setEnabled(false);
		} else {
//			findViewById(R.id.hackerBlock).setVisibility(View.GONE);
			center.setEnabled(true);
		}
	}
	private void updateTimeHackerIfAny() {
		if (System.currentTimeMillis() < passwordAttemptDate4Delay) {
//			int remain = (int)((passwordAttemptDate4Delay - System.currentTimeMillis()) / 1000 + 1);
//			TextView text = ((TextView)findViewById(R.id.hackerBlock));
//			if (!text.getText().toString().equals(Integer.toString(remain))) {
//				text.setText(Integer.toString(remain));
//			}
		} else {
//			((TextView)findViewById(R.id.hackerBlock)).setText("");
			changeStateHacker(false);
		}
	}
	
	private static boolean isStringEmpty(String password) {
		if (password == null || password.isEmpty() || password.length() <= 0) {
			return true;
		}
		return false;
	}
	int codePass;
	protected void onConfirm() {
		//hacker protection
		if (System.currentTimeMillis() < passwordAttemptDate4Delay) {
			codePass = -1;
			return;
		}
		final int hour = (int)getCurrentInfo().hour;
		final int minute = (int)getCurrentInfo().minute;
		final int second = (int)getCurrentInfo().second;
		String password = generateNew(hour, minute, second);
		//String password = generate((int)getCurrentInfo().hour, (int)getCurrentInfo().minute);
		if (isStringEmpty(storedPasswordCache)) {
			Toast.makeText(this, R.string.clock_password_repeat, Toast.LENGTH_SHORT).show();
			storedPasswordCache = password;
			codePass = 0;
		} else if (isStringEmpty(storedPassword)) {
			if (password.equals(storedPasswordCache)) {
				Toast.makeText(this, R.string.clock_password_success, Toast.LENGTH_SHORT).show();
				//created
				storedPassword = password;
				//go inside
				codePass = 1;
				((TimeLockApplication)getApplication()).setPasscodeCache(hour, minute, second, storedPassword);
			} else {
				storedPasswordCache = null;
				Toast.makeText(this, R.string.clock_password_retry, Toast.LENGTH_SHORT).show();
				codePass = 0;
			}
		} else {
			if (password.equals(storedPassword)) {
				codePass = 1;
				((TimeLockApplication)getApplication()).setPasscodeCache(hour, minute, second, storedPassword);
			} else {
				codePass = -1;
			}
		}
	}
	
	protected void doFinish() {
		changeNext();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	TimeInfo zeroTimeInfo;
	TimeInfo getZeroTimeInfo() {
		if (zeroTimeInfo == null) {
			zeroTimeInfo = new TimeInfo();
			zeroTimeInfo.hour = 0;
			zeroTimeInfo.minute = 0;
			zeroTimeInfo.second = 0;
			zeroTimeInfo.am = true;
			zeroTimeInfo.percent = true;
		}
		return zeroTimeInfo;
	}
	protected boolean manualUpdating = false;
	private void switchManualUpdating() {
		if (manualUpdating) {
			stopManualUpdating();
		} else {
			startManualUpdating();
		}
	}
	private void startManualUpdating() {
		{//TODO glass animates to open
			findViewById(R.id.clockOverlayView).setVisibility(View.GONE);
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			if (metrics.widthPixels < metrics.heightPixels) {
				findViewById(R.id.clockOpenUpView).setVisibility(View.VISIBLE);
			} else {
				findViewById(R.id.clockOpenLeftView).setVisibility(View.VISIBLE);
			}
		}
		manualUpdating = true;
		findViewById(R.id.clockBaseView).setOnTouchListener(clockTouch);
		turnNumberAnimation(false);
		turnAlarmIndicator(false);
		turnSecondIndicator(true);
		changeAction(UpdatePostActionId.RequestZeroTimeAnimation.ordinal());
	}
	private void stopManualUpdating() {
		if (!manualUpdating) return;
		onConfirm();
		//startUpdateTime();
		if (codePass > 0) {
			passwordFailCount = 0;
			passwordAttemptCount4Delay = 0;
			passwordAttemptDate4Delay = 0;
			findViewById(R.id.clockBaseView).setOnTouchListener(null);
			changeAction(UpdatePostActionId.RequestZeroTimeAnimation.ordinal());
			manualUpdating = false;
			{//TODO glass animates to close
				findViewById(R.id.clockOverlayView).setVisibility(View.VISIBLE);
				DisplayMetrics metrics = getResources().getDisplayMetrics();
				if (metrics.widthPixels < metrics.heightPixels) {
					findViewById(R.id.clockOpenUpView).setVisibility(View.GONE);
				} else {
					findViewById(R.id.clockOpenLeftView).setVisibility(View.GONE);
				}
			}
		} else if (codePass == 0) {
			if (isStringEmpty(storedPassword)) {
				changeAction(UpdatePostActionId.RequestZeroTimeAnimation.ordinal());
			} else {
				turnNumberAnimation(true);
				if (alarmState) {
					turnAlarmIndicator(true);
					alarmUpdateValue = alarmCurrentValue;
					turnSecondIndicator(false);
					changeAction(UpdatePostActionId.RequestCurrentAlarmTouchUp.ordinal());
				} else {
					findViewById(R.id.clockBaseView).setOnTouchListener(null);
					changeAction(UpdatePostActionId.RequestUpdateTime.ordinal());
				}
				manualUpdating = false;
				{//TODO glass animates to close
					findViewById(R.id.clockOverlayView).setVisibility(View.VISIBLE);
					DisplayMetrics metrics = getResources().getDisplayMetrics();
					if (metrics.widthPixels < metrics.heightPixels) {
						findViewById(R.id.clockOpenUpView).setVisibility(View.GONE);
					} else {
						findViewById(R.id.clockOpenLeftView).setVisibility(View.GONE);
					}
				}
			}
		} else {
			onPasswordFail();
		}
	}
	private void changeBackUpdateTime() {
		turnNumberAnimation(true);
		if (alarmState) {
			turnAlarmIndicator(true);
			turnSecondIndicator(false);
			alarmUpdateValue = alarmCurrentValue;
			changeAction(UpdatePostActionId.RequestCurrentAlarmTouchUp.ordinal());
		} else {
			findViewById(R.id.clockBaseView).setOnTouchListener(null);
			changeAction(UpdatePostActionId.RequestUpdateTime.ordinal());
		}
		manualUpdating = false;
	}
	protected void postPasswordFail() { }
	private void onPasswordFail() {
		//hacker protection
		if (System.currentTimeMillis() < passwordAttemptDate4Delay) {
			changeStateHacker(true);
			changeBackUpdateTime();
			return;
		}
		//ui
		playMeobuSoundPool(2);//reference TimeLockApplication soundpool
		//logic
		passwordFailCount++;
		passwordAttemptCount4Delay++;
		if (passwordFailCount < 5 ||
				!((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[0]) ||
				!((TimeLockApplication)getApplication()).getSelfDestructionActivated()) {
			if (passwordAttemptCount4Delay < 3) {
				Toast.makeText(this, R.string.clock_password_fail, Toast.LENGTH_SHORT).show();
				changeAction(UpdatePostActionId.RequestZeroTimeAnimation.ordinal());
			} else {
				int second = 0;
				switch (passwordAttemptCount4Delay) {
				case 3:
					second = 60;//1
					break;
				case 4:
					second = 120;//2
					break;
				case 5:
					second = 240;//4
					break;
				default:
					second = 600;//10
					break;
				}
				passwordAttemptDate4Delay = System.currentTimeMillis() + second*1000l;
				String message = String.format(getString(R.string.clock_password_hacker_protection), second);
				new AlertDialog.Builder(this)
					.setTitle(R.string.clock_password_hacker_protection_title)
					.setMessage(message)
					.setNeutralButton(R.string.btn_ok1, null)
					.create().show();
				changeStateHacker(true);
				changeBackUpdateTime();
			}
			postPasswordFail();
			return;
		}
		changeAction(UpdatePostActionId.RequestZeroTimeAnimation.ordinal());
		//show dialog to slow user action
		DialogInterface.OnClickListener okClick = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				changeBackWizard();
			}
		};
		new AlertDialog.Builder(this).setTitle(R.string.clock_password_destruction_title).setMessage(R.string.clock_password_destruction).setNeutralButton(R.string.btn_ok1, okClick).create().show();
		//clean cache values
		passwordFailCount = 0;
		storedPassword = "";
		storedPasswordCache = "";
		save(((TimeLockApplication)getApplication()), storedPassword);
		//async delete database, clear all
		final AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... params) {
				//photos & videos & notes & voices
				TimeLockContentsDataSource ds = null;
				TimeLockFoldersDataSource dsf = null;
				TimeLockNotesDataSource dsn = null;
				TimeLockVoicesDataSource dsv = null;
				try {ds=new TimeLockContentsDataSource(getApplicationContext());ds.open();} catch (Exception e) {}
				try {dsf=new TimeLockFoldersDataSource(getApplicationContext());dsf.open();} catch (Exception e) {}
				try {dsn=new TimeLockNotesDataSource(getApplicationContext());dsn.open();} catch (Exception e) {}
				try {dsv=new TimeLockVoicesDataSource(getApplicationContext());dsv.open();} catch (Exception e) {}
				try {((TimeLockApplication)getApplication()).cleanAll(ds, dsf, dsn, dsv);} catch (Exception e) {}
				try {ds.close();} catch (Exception e) {}
				try {dsf.close();} catch (Exception e) {}
				try {dsn.close();} catch (Exception e) {}
				try {dsv.close();} catch (Exception e) {}
				return 0;
			}
			@Override
			protected void onPostExecute(Integer result) {
			}
		};
		executeVoidVoidInteger(task);
	}
	private void turnSecondIndicator(boolean on) {
		View view = findViewById(R.id.clockSecondView);
		turnIndicator(on, view);
		View viewShadow = findViewById(R.id.clockSecondViewShadow);
		turnIndicator(on, viewShadow);
	}
	private void turnAlarmIndicator(boolean on) {
		View view = findViewById(R.id.clockAlarmView);
		turnIndicator(on, view);
	}
	private void turnIndicator(boolean on, View view) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			turnIndicatorBelow11(on, view);
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			turnIndicatorBelow12(on, view);
		} else {
			turnIndicator12AndOver(on, view);
		}
	}
	private void turnIndicatorBelow11(boolean on, View view) {
		if (on) {
			view.setVisibility(View.INVISIBLE);
		} else {
			view.clearAnimation();
			view.setVisibility(View.GONE);
		}
	}
	private void turnIndicatorBelow12(boolean on, View view) {
		turnIndicatorBelow11(on, view);
	}
	private void turnIndicator12AndOver(boolean on, View view) {
		turnIndicatorBelow11(on, view);
	}
	private float centerPosition = -1, centerPositionY = -1;
	private float getCenterPosition() {
		if (centerPosition <= 0) {
			centerPosition = findViewById(R.id.clockBaseView).getWidth() / 2.0f;
		}
		return centerPosition;
	}
	private float getCenterPositionY() {
		if (centerPositionY <= 0) {
			centerPositionY = findViewById(R.id.clockHourView).getHeight() / 2.0f;
		}
		return centerPositionY;
	}
	float startRotation, startTimeRotation;
//	boolean hourMoving;
	int handMoving;//0 - second, 1 - minute, 2 - hour
	private final OnTouchListener clockTouch = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
				float changeX = event.getX() - getCenterPosition();
				float changeY = event.getY() - getCenterPosition();
				float rotation = getRotation(changeX, changeY);
				startRotation = rotation;
				if (manualUpdating) {
					//check if touch on an indicator
					float hourRotation = ClockData.hourDegree(getCurrentInfo().hour);
					float minuteRotation = ClockData.minuteDegree(getCurrentInfo().minute);
					float secondRotation = ClockData.secondDegree(getCurrentInfo().second);
					float nearHour = Math.min(Math.abs(hourRotation - rotation), Math.abs(hourRotation - rotation + 360));
					float nearMinute = Math.min(Math.abs(minuteRotation - rotation), Math.abs(minuteRotation - rotation + 360));
					float nearSecond = Math.min(Math.abs(secondRotation - rotation), Math.abs(secondRotation - rotation + 360));
					float nearValue = Math.min(nearSecond, Math.min(nearMinute, nearHour));
					if (nearValue <= 15) {
						if (nearValue == nearSecond) {
							handMoving = 0;
							startTimeRotation = secondRotation;
						} else if (nearValue == nearMinute) {
							handMoving = 1;
							startTimeRotation = minuteRotation;
						} else {
							handMoving = 2;
							startTimeRotation = hourRotation;
						}
					} else {
						return false;
					}
//					if (nearHour < nearMinute) {
//						if (nearHour > 15) {
//							return false;
//						}
//						//select hour
//						hourMoving = true;
//						startTimeRotation = hourRotation;
//					} else {
//						if (nearMinute > 15) {
//							return false;
//						}
//						//select minute
//						hourMoving = false;
//						startTimeRotation = minuteRotation;
//						setAlarmNotification(0);//TODO what is this?
//					}
					getCurrentInfo().percent = false;
					getUpdatingInfo().set(getCurrentInfo());
					changeAction(UpdatePostActionId.RequestCurrentTimeMoving.ordinal());
					return true;
				} else if (alarmState) {
					float alarmRotation = ClockData.minuteDegree(alarmCurrentValue);
					float nearAlarm = Math.min(Math.abs(alarmRotation - rotation), Math.abs(alarmRotation - rotation + 360));
					if (nearAlarm > 15) {
						return false;
					}
					turnNumberAnimation(false);
					startTimeRotation = alarmRotation;
					alarmUpdateValue = alarmCurrentValue;
					changeAction(UpdatePostActionId.RequestCurrentAlarmMoving.ordinal());
					return true;
				}
				return false;
			} else if (event.getActionMasked() == MotionEvent.ACTION_MOVE) {
				float changeX = event.getX() - getCenterPosition();
				float changeY = event.getY() - getCenterPosition();
				float rotation = getRotation(changeX, changeY);
				float newRotation = (rotation - startRotation) + startTimeRotation;
				//confirm rotation
				while (newRotation < 0) {
					newRotation += 360;
				}
				while (!(newRotation < 360)) {
					newRotation -= 360;
				}
				if (manualUpdating) {
					//update time
					if (handMoving == 0) {
						getUpdatingInfo().second = ClockData.degreeMinute(newRotation);
					} else if (handMoving == 1) {
						getUpdatingInfo().minute = ClockData.degreeMinute(newRotation);
					} else {
						getUpdatingInfo().hour = ClockData.degreeHour(newRotation);
					}
//					if (hourMoving) {
//						getUpdatingInfo().hour = ClockData.degreeHour(newRotation);
//					} else {
//						getUpdatingInfo().minute = ClockData.degreeMinute(newRotation);
//					}
				} else if (alarmState) {
					alarmUpdateValue = ClockData.degreeMinute(newRotation);;
				}
				//update the view => always auto update
				return true;
			} else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
				float changeX = event.getX() - getCenterPosition();
				float changeY = event.getY() - getCenterPosition();
				float rotation = getRotation(changeX, changeY);
				float newRotation = (rotation - startRotation) + startTimeRotation;
				//confirm rotation
				while (newRotation < 0) {
					newRotation += 360;
				}
				while (!(newRotation < 360)) {
					newRotation -= 360;
				}
				if (manualUpdating) {
					//updateTime
					if (handMoving == 0) {
						getUpdatingInfo().second = ClockData.degreeMinute(newRotation);
					} else if (handMoving == 1) {
						getUpdatingInfo().minute = ClockData.degreeMinute(newRotation);
					} else {
						getUpdatingInfo().hour = ClockData.degreeHour(newRotation);
					}
//					if (hourMoving) {
//						getCurrentInfo().hour = ClockData.degreeHour(newRotation);
//					} else {
//						getCurrentInfo().minute = ClockData.degreeMinute(newRotation);
//					}
					changeAction(UpdatePostActionId.RequestCurrentTimeTouchUp.ordinal());
				} else if (alarmState) {
					alarmCurrentValue = ClockData.degreeMinute(newRotation);
					changeAction(UpdatePostActionId.RequestCurrentAlarmTouchUp.ordinal());
					turnNumberAnimation(true);
				}
				return true;
			}
			return false;
		}
	};
	
	
	private static float getRotation(float x, float y) {
		if (x == 0 || y == 0) {
			if (x == 0) {
				if (y <= 0) {
					return 0;
				}
				return 180;
			}
			if (x >= 0) {
				return 90;
			}
			return 270;
		}
		if (x > 0 && y > 0) {
			return (float)Math.toDegrees(Math.atan(y / x)) + 90.0f;
		}
		if (x > 0 && y < 0) {
			return (float)Math.toDegrees(Math.atan(x / -y));
		}
		if (x < 0 && y > 0) {
			return (float)Math.toDegrees(Math.atan(-x / y)) + 180.0f;
		}
		return (float)Math.toDegrees(Math.atan(-y / -x)) + 270.0f;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	void turnNumberAnimation(boolean on) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			turnNumberAnimationBelow11(on);
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			turnNumberAnimationBelow12(on);
		} else {
			turnNumberAnimation12AndOver(on);
		}
	}
	void turnNumberAnimationBelow11(boolean on) {
//		if (on) {
//			Animation separatorAnim = new AlphaAnimation(1, 0);
//			separatorAnim.setRepeatMode(Animation.REVERSE);
//			separatorAnim.setRepeatCount(Animation.INFINITE);
//			separatorAnim.setDuration(400);
//			findViewById(R.id.clockSeparator).startAnimation(separatorAnim);
//			Animation separatorAnim1 = new AlphaAnimation(1, 0);
//			separatorAnim1.setRepeatMode(Animation.REVERSE);
//			separatorAnim1.setRepeatCount(Animation.INFINITE);
//			separatorAnim1.setDuration(400);
//			findViewById(R.id.clockSeparator1).startAnimation(separatorAnim1);
//		} else {
//			findViewById(R.id.clockSeparator).clearAnimation();
//			findViewById(R.id.clockSeparator1).clearAnimation();
//		}
	}
	void turnNumberAnimationBelow12(boolean on) {
		turnNumberAnimationBelow11(on);
	}
	void turnNumberAnimation12AndOver(boolean on) {
		turnNumberAnimationBelow11(on);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	private void changeAction(int action) {
		this.action = action;
	}
	
	protected boolean stillUpdating;
	protected int action;
	private void startUpdateLoop() {
		stillUpdating = true;
		action = UpdatePostActionId.RequestUpdateTime.ordinal();
		requestUpdateTime();
	}
	private void stopUpdateLoop() {
		stillUpdating = false;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	private void requestUpdateTime() {
		if (stillUpdating) {
			clockUpdate.setAction(action);
			findViewById(R.id.clockBaseView).post(clockUpdate);
		}
	}
	
	private TimeInfo updatingInfo;
	private TimeInfo getUpdatingInfo() {
		if (updatingInfo == null) {
			updatingInfo = new TimeInfo();
		}
		return updatingInfo;
	}
	private TimeInfo latestInfo;
	private TimeInfo getLatestInfo() {
		if (latestInfo == null) {
			latestInfo = new TimeInfo();
		}
		return latestInfo;
	}
	private TimeInfo currentInfo;
	private TimeInfo getCurrentInfo() {
		if (currentInfo == null) {
			currentInfo = new TimeInfo();
		}
		return currentInfo;
	}
	private float alarmCurrentValue;
	private float alarmUpdateValue;
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	enum UpdatePostActionId {
		Nothing,
		RequestUpdateTime,
		RequestZeroTimeAnimation,
		RequestCurrentTimeMoving,
		RequestCurrentTimeTouchUp,
		RequestCurrentAlarmMoving,
		RequestCurrentAlarmTouchUp,
		Finish,
	}
	class ClockUpdateRunnable implements Runnable {
		int action;
		public void setAction(int action) {
			this.action = action;
		}
		@Override
		public void run() {
			TimeInfo infoNew = null;
			TimeInfo infoOld = null;

			updateTimeHackerIfAny();
			switch (UpdatePostActionId.values()[action]) {
			case RequestUpdateTime:
				getLatestInfo().set(Calendar.getInstance(), true);
				infoNew = getLatestInfo();
				updateTime(infoNew, infoOld);
				break;
			case RequestZeroTimeAnimation:
				if (manualUpdating) {
					//do nothing
				} else if (codePass > 0) {
					codePass = -1;
					changeAction(UpdatePostActionId.Finish.ordinal());
				} else {
					//back to time
				}
				infoNew = getZeroTimeInfo();
				infoOld = getCurrentInfo();
				updateTime(infoNew, infoOld);
				break;
			case RequestCurrentTimeMoving:
				infoNew = getUpdatingInfo();
				updateTime(infoNew, infoOld);
				break;
			case RequestCurrentTimeTouchUp:
				infoNew = getUpdatingInfo();
				infoNew.hour = Math.round(infoNew.hour);
				if (infoNew.hour > 11) {
					infoNew.hour -= 12;
				}
				infoNew.minute = Math.round(infoNew.minute);
				if (infoNew.minute > 59) {
					infoNew.minute -= 60;
				}
				infoNew.second = Math.round(infoNew.second);
				if (infoNew.second > 59) {
					infoNew.second -= 60;
				}
				infoOld = getCurrentInfo();
				updateTime(infoNew, infoOld);
				changeAction(UpdatePostActionId.RequestCurrentTimeMoving.ordinal());
				getUpdatingInfo().set(getCurrentInfo());
				break;
			case RequestCurrentAlarmMoving:
				updateAlarm(alarmUpdateValue, -1);
				break;
			case RequestCurrentAlarmTouchUp:
				int alarmUpdateValueRounded = Math.round(alarmUpdateValue);
				if (alarmUpdateValueRounded > 59) {
					alarmUpdateValueRounded -= 60;
				}
				updateAlarm(alarmUpdateValueRounded, alarmCurrentValue);
				changeAction(UpdatePostActionId.RequestUpdateTime.ordinal());
				break;
			case Finish:
				doFinish();
				break;
			default:
				break;
			}
		}
	}
	private final ClockUpdateRunnable clockUpdate = new ClockUpdateRunnable();
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	private void updateAlarmNotification() {
		if (alarmState) {
			long notificationDate = nextAlarmByMinuteIndicator((int)alarmCurrentValue);
			setAlarmNotification(notificationDate);
			//always call because Alarm Service did check if the same value is 
		} else {
			setAlarmNotification(0);
		}
	}
	
	private long nextAlarmByMinuteIndicator(int value) {
		int hour = (int)Math.floor(alarmCurrentValue / 5);
		int minute = (int)(alarmCurrentValue % 5) * 12;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		//not am
		if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
			calendar.set(Calendar.HOUR_OF_DAY, hour + 12);
			//not pm
			if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
				//tomorrow
				calendar.set(Calendar.HOUR_OF_DAY, hour);
				long nextday = calendar.getTimeInMillis() + 86400000l;//= 1000l*60*60*24;
				return nextday;
			}
		}
		return calendar.getTimeInMillis();
	}
	
	void updateAlarm(float alarmNew, float alarmOld) {
		if (alarmOld < 0) {
			//alarm moving
			if (alarmCurrentValue == alarmNew) {
				//do nothing
				onUpdateFinish();
			} else {
				//do update, set value
				doUpdateAlarm(alarmNew, -1);
				alarmCurrentValue = alarmNew;
			}
		} else {
			//alarm animating
			doUpdateAlarm(alarmNew, alarmOld);
			alarmCurrentValue = alarmNew;

			updateAlarmNotification();
		}
		float alarmNewRound = Math.round(alarmNew);
		getUpdatingInfo().am = true;
		getUpdatingInfo().hour = (float)Math.floor(alarmNewRound / 5);
		getUpdatingInfo().minute = (alarmNewRound % 5) * 12;
		getUpdatingInfo().second = 0;
		doUpdateNumbers(getUpdatingInfo());
	}
	void doUpdateAlarm(float alarmNew, float alarmOld) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			doUpdateAlarmBelow11(alarmNew, alarmOld);
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			doUpdateAlarmBelow12(alarmNew, alarmOld);
		} else { //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			doUpdateAlarm12AndOver(alarmNew, alarmOld);
		}
	}
	private void doUpdateAlarmBelow11(float alarmNew, float alarmOld) {
		float minute = ClockData.minuteDegree(alarmNew) + 90;
		float minutePast = minute;
		int duration = 0;
		if (alarmOld >= 0) {
			minutePast = ClockData.minuteDegree(alarmOld) + 90;
			duration = 300;
		}
		Animation minuteAnim = createAnimation(minutePast, minute, getCenterPosition(), getCenterPositionY(), duration);
		minuteAnim.setAnimationListener(animListener);
		View alarmView = findViewById(R.id.clockAlarmView);
		if (alarmView.getVisibility() != View.GONE) {
			alarmView.startAnimation(minuteAnim);
		}
	}
	private void doUpdateAlarmBelow12(float alarmNew, float alarmOld) {
		doUpdateAlarmBelow11(alarmNew, alarmOld);
	}
	private void doUpdateAlarm12AndOver(float alarmNew, float alarmOld) {
		doUpdateAlarmBelow11(alarmNew, alarmOld);
	}
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	//update time immediately, like second tick-tak-tick-tak
	//if (infoOld) scroll time to, from infoOld, scroll to infoNew
	void updateTime(TimeInfo infoNew, TimeInfo infoOld) {
		if (infoNew == null) return;
		if (infoOld == null) {
			if (getCurrentInfo().equals(infoNew)) {
				//do nothing
				onUpdateFinish();
			} else {
				//do update, set value
				doUpdateTimeView(infoNew, null);
				getCurrentInfo().set(infoNew);
			}
		} else {
			if (getCurrentInfo().equals(infoNew) && infoNew.equals(infoOld)) {
				//do nothing
				onUpdateFinish();
			} else {
				//do anim, set value
				doUpdateTimeView(infoNew, infoOld);
				getCurrentInfo().set(infoNew);
			}
		}
	}
	
	void doUpdateTimeView(TimeInfo infoNew, TimeInfo infoOld) {
		if (infoNew == null) return;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			doUpdateTimeViewBelow11(infoNew, infoOld);
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			doUpdateTimeViewBelow12(infoNew, infoOld);
		} else {
			doUpdateTimeView12AndOver(infoNew, infoOld);
		}
		doUpdateNumbers(infoNew);
	}
	
	void onUpdateFinish() {
		requestUpdateTime();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	void doUpdateNumbers(TimeInfo infoNew) {
		int hour = Math.round(infoNew.hour) + ((infoNew.am)?0:12);
		if (infoNew.am) {
			if (hour > 11) {
				hour -= 12;
			}
		}
		int minute = Math.round(infoNew.minute);
		if (minute > 59) {
			minute -= 60;
		}
		int second = Math.round(infoNew.second);
		if (second > 59) {
			second -= 60;
		}
		setNumbers(hour/10, hour%10, minute/10, minute%10, second/10, second%10);
	}
	private int numberHour1 = -1, numberHour2 = -1, numberMin1 = -1, numberMin2 = -1, numberSec1 = -1, numberSec2 = -1;
	private boolean numberSound = false;
	void setNumbers(int hour1, int hour2, int min1, int min2, int sec1, int sec2) {
		boolean changed = false;
		if (hour1 != this.numberHour1) {
			setNumber((ImageView)findViewById(R.id.clockHour0), hour1);
			changed = true;
			this.numberHour1 = hour1;
		}
		if (hour2 != this.numberHour2) {
			setNumber((ImageView)findViewById(R.id.clockHour1), hour2);
			changed = true;
			this.numberHour2 = hour2;
		}
		if (min1 != this.numberMin1) {
			setNumber((ImageView)findViewById(R.id.clockMinute0), min1);
			changed = true;
			this.numberMin1 = min1;
		}
		if (min2 != this.numberMin2) {
			setNumber((ImageView)findViewById(R.id.clockMinute1), min2);
			changed = true;
			this.numberMin2 = min2;
		}
		if (sec1 != this.numberSec1) {
			setNumber((ImageView)findViewById(R.id.clockSecond0), sec1);
			changed = true;
			this.numberSec1 = sec1;
		}
		if (sec2 != this.numberSec2) {
			setNumber((ImageView)findViewById(R.id.clockSecond1), sec2);
			changed = true;
			this.numberSec2 = sec2;
		}
		if (changed && numberSound && manualUpdating && action == UpdatePostActionId.RequestCurrentTimeMoving.ordinal()) {
			playMeobuSoundPool(1);//reference TimeLockApplication soundpool
		}
	}
	
	static final int[] numberResources = {
		R.drawable.num_0,
		R.drawable.num_1,
		R.drawable.num_2,
		R.drawable.num_3,
		R.drawable.num_4,
		R.drawable.num_5,
		R.drawable.num_6,
		R.drawable.num_7,
		R.drawable.num_8,
		R.drawable.num_9 };
	Drawable[] numberDrawables;
	Drawable[] getNumberDrawables() {
		if (numberDrawables == null) {
			numberDrawables = new Drawable[numberResources.length];
			for (int i=0; i<numberResources.length; i++) {
				numberDrawables[i] = getResources().getDrawable(numberResources[i]);
			}
		}
		return numberDrawables;
	}
	void setNumber(ImageView view, int number) {
		view.setImageDrawable(getNumberDrawables()[number]);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	void doUpdateTimeViewBelow11(TimeInfo infoNew, TimeInfo infoOld) {
		if (infoNew == null) return;
		float second = ClockData.secondDegree(infoNew.second) + 90;
		float minute = ClockData.minuteDegree(infoNew.minute) + 90;
		float hour = ClockData.hourDegree(infoNew.hour) + 90;
		if (infoNew.percent) {
			minute = ClockData.minuteDegree(infoNew.minute, infoNew.second) + 90;
			hour = ClockData.hourDegree(infoNew.hour, infoNew.minute) + 90;
		}
		float secondPast = second;
		float minutePast = minute;
		float hourPast = hour;
		int duration = 0;
		if (infoOld != null && !infoOld.equals(infoNew)) {
			secondPast = infoOld.second * ClockData.DEGREE_PER_SECOND + 90;
			minutePast = infoOld.minute * ClockData.DEGREE_PER_SECOND + 90;
			hourPast = infoOld.hour * 5 * ClockData.DEGREE_PER_SECOND + 90;
			if (infoOld.percent) {
				minutePast += (infoOld.second / 60.0f) * ClockData.DEGREE_PER_SECOND;
				hourPast += (infoOld.minute / 60.0f) * 5 * ClockData.DEGREE_PER_SECOND;
			}
			duration = 300;
		}
		Animation secondAnim = createAnimation(secondPast, second, getCenterPosition(), getCenterPositionY(), duration);
		Animation secondAnimShadow = createAnimation(secondPast, second, getCenterPosition(), getCenterPositionY(), duration);
		Animation minuteAnim = createAnimation(minutePast, minute, getCenterPosition(), getCenterPositionY(), duration);
		Animation minuteAnimShadow = createAnimation(minutePast, minute, getCenterPosition(), getCenterPositionY(), duration);
		Animation hourAnim = createAnimation(hourPast, hour, getCenterPosition(), getCenterPositionY(), duration);
		Animation hourAnimShadow = createAnimation(hourPast, hour, getCenterPosition(), getCenterPositionY(), duration);
		hourAnim.setAnimationListener(animListener);
		View secondView = findViewById(R.id.clockSecondView);
		if (secondView.getVisibility() != View.GONE) {
			secondView.startAnimation(secondAnim);
		}
		View secondViewShadow = findViewById(R.id.clockSecondViewShadow);
		if (secondViewShadow.getVisibility() != View.GONE) {
			secondViewShadow.startAnimation(secondAnimShadow);
		}
		findViewById(R.id.clockMinuteView).startAnimation(minuteAnim);
		findViewById(R.id.clockMinuteViewShadow).startAnimation(minuteAnimShadow);
		findViewById(R.id.clockHourView).startAnimation(hourAnim);
		findViewById(R.id.clockHourViewShadow).startAnimation(hourAnimShadow);
	}
	private final AnimationListener animListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation anim) { }
		@Override
		public void onAnimationRepeat(Animation anim) { }
		@Override
		public void onAnimationEnd(Animation anim) {
			onUpdateFinish();
		}
	};
	private Animation createAnimation(float from, float to, float pivotX, float pivotY, int duration) {
		if (from - to > 360 + to - from) {
			from -= 360;
		}
		return doCreateAnimation(from, to, pivotX, pivotY, duration);
	}
	private Animation doCreateAnimation(float from, float to, float pivotX, float pivotY, int duration) {
		Animation anim = new RotateAnimation(from, to, pivotX, pivotY);
		anim.setFillAfter(true);
		anim.setDuration(duration);
		return anim;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	void doUpdateTimeViewBelow12(TimeInfo infoNew, TimeInfo infoOld) {
		doUpdateTimeViewBelow11(infoNew, infoOld);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	void doUpdateTimeView12AndOver(TimeInfo infoNew, TimeInfo infoOld) {
		doUpdateTimeViewBelow11(infoNew, infoOld);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
    public static void setSize(View view, float width, float height) {
    	ViewGroup.LayoutParams layout = view.getLayoutParams();
    	layout.width = (int)width;
    	layout.height = (int)height;
    	view.setLayoutParams(layout);
    }
    public static void setPadding(View view, float left, float top, float right, float bottom) {
    	view.setPadding((int)left, (int)top, (int)right, (int)bottom);
    }
    public static void setMargin(View view, float top, float left, float bottom, float right) {
    	ViewGroup.MarginLayoutParams layout = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
    	layout.topMargin = (int)top;
    	layout.leftMargin = (int)left;
    	layout.bottomMargin = (int)bottom;
    	layout.rightMargin = (int)right;
    	view.setLayoutParams(layout);
    }
    
    public static void updateViews1(Activity activity) {

		DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
		int shadowSize = 0;
		if (metrics.widthPixels < metrics.heightPixels) {
			shadowSize = metrics.widthPixels;
		} else {
	    	int topBarHeight = 0;
			int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
			if (resourceId > 0) {
				topBarHeight = activity.getResources().getDimensionPixelSize(resourceId);
			}
			shadowSize = metrics.heightPixels - topBarHeight;
		}
		
		float ratio = shadowSize * 1.0f / 593;
		float shadowPadding = 5*ratio;
		
		setSize(activity.findViewById(R.id.clockShadowView), shadowSize, shadowSize);
		setSize(activity.findViewById(R.id.clockBaseView), 524*ratio, 524*ratio);
		setSize(activity.findViewById(R.id.clockOverlayView), 524*ratio, 524*ratio);
		setSize(activity.findViewById(R.id.clockOpenUpView), 524*ratio, 185*ratio);
		setMargin(activity.findViewById(R.id.clockOpenUpView), 0, 0, -20*ratio, 0);
		setSize(activity.findViewById(R.id.clockOpenLeftView), 185*ratio, 524*ratio);
		setMargin(activity.findViewById(R.id.clockOpenLeftView), 0, 0, 0, -20*ratio);
		setSize(activity.findViewById(R.id.clockCenter), 22*ratio, 22*ratio);//clockCenter/clockCenterButton, 22,
		setSize(activity.findViewById(R.id.clockCenterButton), 42*ratio, 42*ratio);
		setPadding(activity.findViewById(R.id.clockCenterButton), 10*ratio, 10*ratio, 10*ratio, 10*ratio);
		setSize(activity.findViewById(R.id.clockNumberContainer), 260*ratio, 100*ratio);//clockNumberContainer, 260, 100, top 14,
		setMargin(activity.findViewById(R.id.clockNumberContainer), 14*ratio, 0, 0, 0);
		setMargin(activity.findViewById(R.id.clockNumbers), 4*ratio, 0, 0, 0);//clockNumbers, top 4,
		setSize(activity.findViewById(R.id.clockHour0), 63*ratio, 88*ratio);//clock?X, 63, 88, right/leftright -17,
		setSize(activity.findViewById(R.id.clockHour1), 63*ratio, 88*ratio);
		setSize(activity.findViewById(R.id.clockMinute0), 63*ratio, 88*ratio);
		setSize(activity.findViewById(R.id.clockMinute1), 63*ratio, 88*ratio);
		setSize(activity.findViewById(R.id.clockSecond0), 63*ratio, 88*ratio);
		setSize(activity.findViewById(R.id.clockSecond1), 63*ratio, 88*ratio);
		setMargin(activity.findViewById(R.id.clockHour0), 0, 0, 0, -17*ratio);
		setMargin(activity.findViewById(R.id.clockHour1), 0, -17*ratio, 0, -17*ratio);
		setMargin(activity.findViewById(R.id.clockMinute0), 0, -17*ratio, 0, -17*ratio);
		setMargin(activity.findViewById(R.id.clockMinute1), 0, -17*ratio, 0, -17*ratio);
		setMargin(activity.findViewById(R.id.clockSecond0), 0, -17*ratio, 0, -17*ratio);
		setMargin(activity.findViewById(R.id.clockSecond1), 0, -17*ratio, 0, 0);
		setSize(activity.findViewById(R.id.clockSeparator), 8*ratio, 40*ratio);//clockSeparator, 8x40,
		setSize(activity.findViewById(R.id.clockNumberHighlight), 237*ratio, 48*ratio);//clockNumberHighlight, 237, 48, top 5,
		setMargin(activity.findViewById(R.id.clockNumberHighlight), 10*ratio, 0, 0, 0);
		setSize(activity.findViewById(R.id.clockHourView), 524*ratio, 30*ratio);//clock?View, 524, 30,
		setSize(activity.findViewById(R.id.clockHourViewShadow), 524*ratio, 30*ratio);
		setMargin(activity.findViewById(R.id.clockHourViewShadow), shadowPadding , shadowPadding , 0, 0);
		setSize(activity.findViewById(R.id.clockMinuteView), 524*ratio, 30*ratio);
		setSize(activity.findViewById(R.id.clockMinuteViewShadow), 524*ratio, 30*ratio);
		setMargin(activity.findViewById(R.id.clockMinuteViewShadow), shadowPadding , shadowPadding , 0, 0);
		setSize(activity.findViewById(R.id.clockSecondView), 524*ratio, 30*ratio);
		setSize(activity.findViewById(R.id.clockSecondViewShadow), 524*ratio, 30*ratio);
		setMargin(activity.findViewById(R.id.clockSecondViewShadow), shadowPadding , shadowPadding , 0, 0);
		setSize(activity.findViewById(R.id.clockAlarmView), 524*ratio, 30*ratio);
		
    }

	public static void updateViews(Activity activity) {
		int topBarHeight = 0;
		//android.R.dimen.
		int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			topBarHeight = activity.getResources().getDimensionPixelSize(resourceId);
		}
		DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
		float realDensity = 0;
		if (metrics.widthPixels < metrics.heightPixels) {
			realDensity = metrics.widthPixels * 1.0f / metrics.densityDpi;
		} else {
			realDensity = metrics.heightPixels * 1.0f / metrics.densityDpi;
		}
		final float totalPadding = (float)Math.floor(10 * realDensity);//dpi
		setPadding(activity.findViewById(R.id.clockPage), totalPadding, totalPadding, totalPadding, totalPadding);
		
		float clockBaseWidth = 0;
		if (metrics.widthPixels < metrics.heightPixels) {
			//portrait
			clockBaseWidth = metrics.widthPixels - totalPadding*2;
		} else {
			//landscape
			clockBaseWidth = metrics.heightPixels - totalPadding*2 - topBarHeight;
		}
		float ratio = clockBaseWidth / 640.0f;
		setSize(activity.findViewById(R.id.clockCenter), 90*ratio, 90*ratio);
		setSize(activity.findViewById(R.id.clockCenterButton), 90*ratio, 90*ratio);
		setMargin(activity.findViewById(R.id.clockNumbers), 20*ratio, 0, 0, 0);
		final float numberWidth = 47*ratio;
		final float numberHeight = 63*ratio;
		setSize(activity.findViewById(R.id.clockSeparator), 24*ratio, 70*ratio);
		setSize(activity.findViewById(R.id.clockSeparator1), 24*ratio, 70*ratio);
		setSize(activity.findViewById(R.id.clockHour0), numberWidth, numberHeight);
		setSize(activity.findViewById(R.id.clockHour1), numberWidth, numberHeight);
		setSize(activity.findViewById(R.id.clockMinute0), numberWidth, numberHeight);
		setSize(activity.findViewById(R.id.clockMinute1), numberWidth, numberHeight);
		setSize(activity.findViewById(R.id.clockSecond0), numberWidth, numberHeight);
		setSize(activity.findViewById(R.id.clockSecond1), numberWidth, numberHeight);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	static class TimeInfo {
		//public int hour, minute, second;
		public float hour, minute, second;
		public boolean am;
		public boolean percent;
		void set(TimeInfo info) {
			hour = info.hour;
			minute = info.minute;
			second = info.second;
			am = info.am;
			percent = info.percent;
		}
		void set(int hour, int minute, int second, boolean am, boolean percent) {
			this.hour = hour;
			this.minute = minute;
			this.second = second;
			this.am = am;
			this.percent = percent;
		}
		void set(Calendar calendar, boolean percent) {
			this.hour = calendar.get(Calendar.HOUR);
			this.minute = calendar.get(Calendar.MINUTE);
			this.second = calendar.get(Calendar.SECOND);
			this.am = calendar.get(Calendar.AM_PM) == Calendar.AM;
			this.percent = percent;
		}
		static TimeInfo create(int hour, int minute, int second, boolean am, boolean percent) {
			TimeInfo ti = new TimeInfo();
			ti.set(hour, minute, second, am, percent);
			return ti;
		}
		static TimeInfo create(Calendar calendar, boolean percent) {
			TimeInfo ti = new TimeInfo();
			ti.set(calendar, percent);
			return ti;
		}
		boolean equals(TimeInfo object) {
			if (hour == object.hour && minute == object.minute && second == object.second
					&& am == object.am && percent == object.percent) {
				return true;
			}
			return false;
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
}
