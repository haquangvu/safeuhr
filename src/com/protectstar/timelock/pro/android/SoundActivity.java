package com.protectstar.timelock.pro.android;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.protectstar.safeuhr.android.R;

public class SoundActivity extends TimeLockActivity {

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sound);
		setupTouchable();
	}
	
	boolean[] items;
	@Override
	protected void onResume() {
		super.onResume();
		items = new boolean[itemids.length];
		TimeLockApplication app = (TimeLockApplication)getApplication();
		items[0] = app.getSoundIndicator();
		items[1] = app.getSoundSafe();
		items[2] = app.getSoundDeletion();
		items[3] = app.getSoundDestruction();
		updateItems(items);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	private final int[] itemids = {R.id.soundIndicator, R.id.soundSafe, R.id.soundDeletion, R.id.soundDestruction};
	private void setupTouchable() {
		for (int i=0; i<itemids.length; i++) {
			View view = findViewById(itemids[i]);
			view.setTag(i);
			view.setOnClickListener(itemClick);
		}
	}
	private final View.OnClickListener itemClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			int index = getViewTag(v);
			SoundActivity.this.onClick(index);
		}
	};
	
	//----------//----------//----------//----------//----------//
	
	Drawable on, off;
	private Drawable getOnDrawable() {
		if (on == null) {
			on = getResources().getDrawable(R.drawable.switch_on);
		}
		return on;
	}
	private Drawable getOffDrawable() {
		if (off == null) {
			off = getResources().getDrawable(R.drawable.switch_off);
		}
		return off;
	}
	private void updateItems(boolean[] items) {
		for (int i=0; i<itemids.length; i++) {
			updateItem(i, items[i]);
		}
	}
	private void updateItem(int index, boolean value) {
		TextView view = (TextView)findViewById(itemids[index]);
		try {
			if (items[index]) {
				view.setCompoundDrawablesWithIntrinsicBounds(null, null, getOnDrawable(), null);
			} else {
				view.setCompoundDrawablesWithIntrinsicBounds(null, null, getOffDrawable(), null);
			}
		} catch (Exception e) { }
	}
	
	private void onClick(int index) {
		items[index] = !items[index];
		TimeLockApplication app = (TimeLockApplication)getApplication();
		switch (index) {
		case 0: app.setSoundIndicator(items[index]); break;
		case 1: app.setSoundSafe(items[index]); break;
		case 2: app.setSoundDeletion(items[index]); break;
		case 3: 
		default:app.setSoundDestruction(items[index]); break;
		}
		updateItem(index, items[index]);
	}
	
	//----------//----------//----------//----------//----------//
	

}
