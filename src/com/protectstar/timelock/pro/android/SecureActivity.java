package com.protectstar.timelock.pro.android;

import meobu.android.base.MeobuEncryption;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.protectstar.safeuhr.android.R;

public class SecureActivity extends TimeLockActivity {

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}
	
	//----------//----------//----------//----------//----------//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_securedfolder);
		setupClickable();
	}
	
	boolean visible, password;
	@Override
	protected void onResume() {
		super.onResume();
		TimeLockApplication app = (TimeLockApplication)getApplication();
		visible = app.getSecuredFolderVisible();
		password = app.getSecuredFolderPassword();
		updateViews(visible, password);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		TimeLockApplication app = (TimeLockApplication)getApplication();
		if (visible != app.getSecuredFolderVisible()) {
			app.setSecuredFolderVisible(visible);
			app.setFeaturedFolderVisibility4Views(TimeLockApplication.SECURED_FOLDER_ID, visible);
		}
		if (password != app.getSecuredFolderPassword()) {
			app.setSecuredFolderPassword(password);
		}
	}
	
	//----------//----------//----------//----------//----------//
	
	private void setupClickable() {
		findViewById(R.id.securedFolderVisible).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				visible = !visible;
				((TextView)findViewById(R.id.securedFolderVisible)).setCompoundDrawablesWithIntrinsicBounds(null, null, visible?getActivatedDrawable():getUnactivatedDrawable(), null);
			}
		});
		findViewById(R.id.securedFolderPassword).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				password = !password;
				((TextView)findViewById(R.id.securedFolderPassword)).setCompoundDrawablesWithIntrinsicBounds(null, null, password?getActivatedDrawable():getUnactivatedDrawable(), null);
				if (password) {
					setPasswordRaw(getApplicationContext(), "");
					doChangePassword();
				}
			}
		});
		findViewById(R.id.securedFolderChange).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (password) {
					doChangePassword();
				}
			}
		});
	}
	private void doChangePassword() {
		final View view = getLayoutInflater().inflate(R.layout.activity_securedfolder_changepass, null);
		if (!hasPassword(getApplicationContext())) {
			view.findViewById(R.id.changepassOldPassword).setVisibility(View.GONE);
		}
		final AlertDialog alert = new AlertDialog.Builder(SecureActivity.this)
				.setTitle(R.string.secured_folder_change_title)
				.setView(view)
				.setNegativeButton(R.string.changecode_button_cancel, null)
				.setPositiveButton(R.string.btn_ok1, null)
				.create();
		final View.OnClickListener onClick = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//check old pass
				if (hasPassword(getApplicationContext())) {
					String input = ((EditText)view.findViewById(R.id.changepassOldPassword)).getText().toString();
					String inputPassword = genPassword(input);
					if (inputPassword == null || inputPassword.length() <= 0
							|| !inputPassword.equals(getPassword(getApplicationContext()))) {
						//wrong pass
						Toast.makeText(SecureActivity.this, R.string.secured_folder_change_oldpass_wrong, Toast.LENGTH_SHORT).show();
						return;
					}
				}
				//check new pass password
				String input1 = ((EditText)view.findViewById(R.id.changepassNewPassword)).getText().toString();
				if (input1 == null || input1.length() <= 0) {
					Toast.makeText(SecureActivity.this, R.string.secured_folder_change_newpass_wrong, Toast.LENGTH_SHORT).show();
					return;
				}
				String input2 = ((EditText)view.findViewById(R.id.changepassNewPasswordRepeat)).getText().toString();
				if (!input2.equals(input1)) {
					Log.d("meobudebug", "input1:[" + input1 + "]     input2:[" + input2 + "]");
					Toast.makeText(SecureActivity.this, R.string.secured_folder_change_repeat_wrong, Toast.LENGTH_SHORT).show();
					return;
				}
				//save
				setPassword(getApplicationContext(), input1);
				Toast.makeText(SecureActivity.this, R.string.secured_folder_change_created, Toast.LENGTH_SHORT).show();
				alert.dismiss();
			}
		};
		alert.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				((AlertDialog)dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(onClick);
			}
		});
		alert.show();
	}
	
	public static final String genPassword(String input) {
		try {
			return MeobuEncryption.SHA256(input);
		} catch (Exception e) {}
		return "";
	}
	public static boolean compare(Context applicationContext, String input) {
		if (input == null) return false;
		String inputPassword = genPassword(input);
		return inputPassword.equals(getPassword(applicationContext));
	}
	public static boolean hasPassword(Context applicationContext) {
		String passcode = getPassword(applicationContext);
		if (passcode == null || passcode.length() <= 0) {
			return false;
		}
		return true;
	}
	public static String getPassword(Context applicationContext) {
		return applicationContext.getSharedPreferences("securedfolder", 0).getString("passcode", "");
	}
	private static void setPassword(Context applicationContext, String password) {
		setPasswordRaw(applicationContext, genPassword(password));
	}
	private static void setPasswordRaw(Context applicationContext, String password) {
		applicationContext.getSharedPreferences("securedfolder", 0).edit().putString("passcode", password).commit();
	}

	private Drawable activatedDrawable, unactivatedDrawable;
	private Drawable getActivatedDrawable() {
		if (activatedDrawable == null) {
			activatedDrawable = getResources().getDrawable(R.drawable.switch_on);
		}
		return activatedDrawable;
	}
	private Drawable getUnactivatedDrawable() {
		if (unactivatedDrawable == null) {
			unactivatedDrawable = getResources().getDrawable(R.drawable.switch_off);
		}
		return unactivatedDrawable;
	}
	private void updateViews(boolean visible, boolean password) {
		((TextView)findViewById(R.id.securedFolderVisible)).setCompoundDrawablesWithIntrinsicBounds(null, null, visible?getActivatedDrawable():getUnactivatedDrawable(), null);
		((TextView)findViewById(R.id.securedFolderPassword)).setCompoundDrawablesWithIntrinsicBounds(null, null, password?getActivatedDrawable():getUnactivatedDrawable(), null);
	}

}
