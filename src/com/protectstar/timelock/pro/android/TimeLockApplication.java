package com.protectstar.timelock.pro.android;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;

import meobu.android.base.MeobuApplication;
import meobu.android.base.MeobuEncryption;
import meobu.android.base.MeobuListViewData;
import meobu.android.base.MeobuSettingsDataSource;
import meobu.android.base.MeobuSettingsDataSource.MeobuSettingsContent;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.TimeLockContent;
import com.protectstar.timelock.pro.android.data.TimeLockContent4View;
import com.protectstar.timelock.pro.android.data.TimeLockContentsDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockFolder;
import com.protectstar.timelock.pro.android.data.TimeLockFolder4View;
import com.protectstar.timelock.pro.android.data.TimeLockFoldersDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockImageDownloader;
import com.protectstar.timelock.pro.android.data.TimeLockNode;
import com.protectstar.timelock.pro.android.data.TimeLockNotesDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockSettingDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockVoice;
import com.protectstar.timelock.pro.android.data.TimeLockVoicesDataSource;

import android.content.Context;
import android.util.Log;

public class TimeLockApplication extends MeobuApplication {

	static TimeLockApplication instance;
	static TimeLockApplication getTimeLockApplication() {
		return instance;
	}
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}
	
	//----------//----------//----------//----------//----------//
	
	Hashtable<String, MeobuSettingsContent> settings;
	@Override protected boolean isSupportMeobuSettings() { return true; }
	@Override protected MeobuSettingsDataSource newMeobuSettingsDataSource() { return new TimeLockSettingDataSource(this); }
	@Override protected Hashtable<String, MeobuSettingsContent> getMeobuSettings() { return settings; }
	@Override protected void setMeobuSettings(Hashtable<String, MeobuSettingsContent> settings) { this.settings = settings; demoMeobuSettings(); }
	protected void demoMeobuSettings() {
		setMeobuSettings(TimeLockSettingDataSource.keys[1], true);//because of wrong implementation, keep this feature always on
		//TODO demo iap
		setMeobuSettings(TimeLockSettingDataSource.keys[0], true);//self destruction
//		Log.d("meobudebug", "featued: " + getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]) + " " + getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]));
		setMeobuSettings(TimeLockSettingDataSource.keys[2], true);//secured folder
		setMeobuSettings(TimeLockSettingDataSource.keys[4], true);//spy camera
//		checkPurchaseDate();
		setMeobuSettings(TimeLockSettingDataSource.keys[3], true);//note text
		setMeobuSettings(TimeLockSettingDataSource.keys[6], true);//voice memo
	}
	
	//----------//----------//----------//----------//----------//
	
	public boolean getSelfDestructionActivated() {
		return getSharedPreferences("setting", 0).getBoolean("self_destruction_activate", false);
	}
//	public int getSelfDestructionMethod() {
//		return getSharedPreferences("setting", 0).getInt("self_destruction_method", 0);
//	}
	public boolean getSoundIndicator() {
		return getSharedPreferences("setting", 0).getBoolean("sound_indicator_activate", true);
	}
	public boolean getSoundSafe() {
		return getSharedPreferences("setting", 0).getBoolean("sound_safe_activate", true);
	}
	public boolean getSoundDeletion() {
		return getSharedPreferences("setting", 0).getBoolean("sound_deletion_activate", true);
	}
	public boolean getSoundDestruction() {
		return getSharedPreferences("setting", 0).getBoolean("sound_destruction_activate", true);
	}
	public void setSelfDestructionMethod(int method) {
		getSharedPreferences("setting", 0).edit().putInt("self_destruction_method", method).commit();
	}
	public void setSelfDestructionActivated(boolean activated) {
		getSharedPreferences("setting", 0).edit().putBoolean("self_destruction_activate", activated).commit();
	}
	public void setSoundIndicator(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("sound_indicator_activate", value).commit();
	}
	public void setSoundSafe(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("sound_safe_activate", value).commit();
	}
	public void setSoundDeletion(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("sound_deletion_activate", value).commit();
	}
	public void setSoundDestruction(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("sound_destruction_activate", value).commit();
	}
	public void setLockOptionIndex(int index) {
		getSharedPreferences("setting", 0).edit().putInt("lock_option_index", index).commit();
	}
	public int getLockOptionIndex() {
		return getSharedPreferences("setting", 0).getInt("lock_option_index", 4);
	}
	public boolean getSecuredFolderVisible() {
		return getSharedPreferences("setting", 0).getBoolean("secured_folder_visible", true);
	}
	public void setSecuredFolderVisible(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("secured_folder_visible", value).commit();
	}
	public boolean getSecuredFolderPassword() {
		return getSharedPreferences("setting", 0).getBoolean("secured_folder_password", false);
	}
	public void setSecuredFolderPassword(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("secured_folder_password", value).commit();
	}
	public boolean getSpyCameraActivated() {
		return getSharedPreferences("setting", 0).getBoolean("spy_camera_activated", true);
	}
	public void setSpyCameraActivated(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("spy_camera_activated", value).commit();
	}
	public boolean getNotesFolderVisible() {
		return getSharedPreferences("setting", 0).getBoolean("notes_folder_visible", true);
	}
	public void setNotesFolderVisible(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("notes_folder_visible", value).commit();
	}
	public boolean getExternalStoreEnable() {
		return getSharedPreferences("setting", 0).getBoolean("store_external_enable", false);
	}
	public void setExternalStoreEnable(boolean value) {
		getSharedPreferences("setting", 0).edit().putBoolean("store_external_enable", value).commit();
	}
	
	//----------//----------//----------//----------//----------//
	
	protected void checkPurchaseDate() {//TODO not use
		long dateSecurity = getSharedPreferences("setting", 0).getLong("security_purchase_date", 0);
		long dateNotes = getSharedPreferences("setting", 0).getLong("note_purchase_date", 0);
		if (dateSecurity <= 0) {
			setMeobuSettings(TimeLockSettingDataSource.keys[2], false);
			setMeobuSettings(TimeLockSettingDataSource.keys[4], false);
			saveMeobuSettings();
		}
		if (dateNotes <= 0) {
			setMeobuSettings(TimeLockSettingDataSource.keys[3], false);
			saveMeobuSettings();
		}
	}
	
	public void setSecurityPackagePurchased() {
		setMeobuSettings(TimeLockSettingDataSource.keys[2], true);
		setMeobuSettings(TimeLockSettingDataSource.keys[4], true);
		Log.d("meobudebug", "settings: " + getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]) + " " + getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]));
		saveMeobuSettings();
		getSharedPreferences("setting", 0).edit().putLong("security_purchase_date", System.currentTimeMillis()).commit();
	}
	
	public void setNotesPackagePurchased() {
		setMeobuSettings(TimeLockSettingDataSource.keys[3], true);
		Log.d("meobudebug", "settings: " + getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[3]));
		saveMeobuSettings();
		getSharedPreferences("setting", 0).edit().putLong("note_purchase_date", System.currentTimeMillis()).commit();
	}
	
	//----------//----------//----------//----------//----------//
	
	private static String cachePasscode;
	private static int cacheHour = -2, cacheMinute = -2, cacheSecond = -2;
	public static void buildPasscodeNCache(int hour, int minute, int second) {
		if (cacheHour != hour || cacheMinute != minute || cacheSecond != second) {
			cacheHour = hour;
			cacheMinute = minute;
			cacheSecond = second;
			cachePasscode = buildPasscode(hour, minute, second);
		}
	}
	public static String buildPasscode(int hour, int minute, int second) {
		String key = String.format("%02d:%02d:%02d", hour, minute, second);
		String passcode = null;
		try {
			String sha2 = MeobuEncryption.SHA256(MeobuEncryption.SHA256(key));
			passcode = sha2.substring(0, 32);
		} catch (Exception e) { }
		return passcode;
	}
	public static String cachePasscode() {
		return cachePasscode;
	}
	public static void setCachePasscodeFromRestore(String restored) {
		cachePasscode = restored;
	}
	
	//----------//----------//----------//----------//----------//
	
	int cachePasscodeHour, cachePasscodeMinute, cachePasscodeSecond;
	String cachePassword;
	public int getCachePasscodeHour() {
		return cachePasscodeHour;
	}
	public int getCachePasscodeMinute() {
		return cachePasscodeMinute;
	}
	public int getCachePasscodeSecond() {
		return cachePasscodeSecond;
	}
	public String getCachePassword() {
		return cachePassword;
	}
	public void setPasscodeCache(int hour, int minute, int second, String password) {
		cachePasscodeHour = hour;
		cachePasscodeMinute = minute;
		cachePasscodeSecond = second;
		cachePassword = password;
		buildPasscodeNCache(hour, minute, second);
	}
	public void setNewPasscode(int hour, int minute, int second) {
		String password = ClockActivity6.generateNew(hour, minute, second);
		ClockActivity6.save(this, password);
		setPasscodeCache(hour, minute, second, password);
	}
	public void clearCachePasscode() {
		cachePasscodeHour = cachePasscodeMinute = cachePasscodeSecond = -1;
		cachePassword = null;
		buildPasscodeNCache(-1, -1, -1);
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	public boolean supportMeobuSoundPool() {
		return true;
	}
	private final int[] soundResources = {
			//R.raw.alarm,
			R.raw.click,//1
			R.raw.wrongpass,//2
			R.raw.selfdestruction,//3
			R.raw.open,//4
			R.raw.delete,//5
			R.raw.close};//6
	@Override
	public int[] getSoundResourceId() {
		return soundResources;
	}
	
	@Override
	public boolean supportMeobuImageLoader() {
		return true;
	}
	@Override
	protected boolean supportImageLoaderCustomDownloader() {
		return true;
	}
	@Override
	protected boolean supportImageLoaderCacheOnDisc() {
		return true;
	}
	@Override
	protected BaseImageDownloader createCustomDownloader() {
		return new TimeLockImageDownloader(getApplicationContext());
	}
	
	//----------//----------//----------//----------//----------//
	
	public boolean shouldGuide() {
		return getSharedPreferences("helloworld", 0).getBoolean("guide", true);
	}
	public void offGuide() {
		getSharedPreferences("helloworld", 0).edit().putBoolean("guide", false).commit();
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected boolean supportMeobuLanguage() {
		return true;
	}

	@Override
	public Locale getLocaleWithLanguageIndex(int index) {
		switch (index) {
		case 1:	return Locale.ENGLISH;//English is represented by en
		case 2:	return new Locale("es");//Spanish is represented by es
		case 3:	return Locale.FRENCH;//French is represented by fr
		case 4:	return Locale.GERMAN;//German is represented by de
		case 5: return new Locale("pt", "BR");//Portuguese is represented by pt_rBR
		case 6: return Locale.ITALIAN;//Italy is represented by it
		default:	return null;
		}
	}
	
	//----------//----------//----------//----------//----------//
	
	public void refreshViewData4Language() {
		Enumeration<TimeLockNode> elements = getHashNode().elements();
		while (elements.hasMoreElements()) {
			TimeLockNode node = elements.nextElement();
			if (node != null && node.getListViewData() != null) {
				for (int i=0; i<node.getListViewData().size(); i++) {
					ViewData viewData = node.getListViewData().get(i);
					if (viewData instanceof HeaderViewData) {
						((HeaderViewData)viewData).clearTimeCache();
					}
				}
			}
		}
//		for (int i=0; i<getListViewData().size(); i++) {
//			ViewData viewData = getListViewData().get(i);
//			if (viewData instanceof HeaderViewData) {
//				((HeaderViewData)viewData).clearTimeCache();
//			}
//		}
	}
	
	public void cleanAll(TimeLockContentsDataSource ds, TimeLockFoldersDataSource dsf, TimeLockNotesDataSource dsn, TimeLockVoicesDataSource dsv) {
		//clean cache
//		listData = null;
//		listViewData = null;
//		hashData = null;
		hashNode = null;
		//clean db
		try {
			dsf.removeAll();
		} catch (Exception e) {}
		try {
			ArrayList<TimeLockContent> list = ds.retrieve(0, Integer.MAX_VALUE);
			int count = ds.removeAll();
			if (count != list.size()) {
				Log.e("meobuerror", "TimeLockApplication.cleanAll(TimeLockContentsDataSource)#Cleaned not clear! " + count + "/" + list.size());
			}
			for (int i=0; i<list.size(); i++) {
				TimeLockContent content = list.get(i);
				try {new File(content.getDataPath()).delete();} catch (Exception e) {}
				try {new File(content.getDataPathDecrypted()).delete();} catch (Exception e) {}
				try {new File(content.getDataThumbnail()).delete();} catch (Exception e) {}
			}
		} catch (Exception e) {}
		try {
			dsn.removeAll();
		} catch (Exception e) {}
		try {
			ArrayList<TimeLockVoice> list = dsv.retrieve(0, Integer.MAX_VALUE);
			int count = dsv.removeAll();
			if (count != list.size()) {
				Log.e("meobuerror", "TimeLockApplication.cleanAll(TimeLockVoicesDataSource)#Cleaned not clear! " + count + "/" + list.size());
			}
			for (int i=0; i<list.size(); i++) {
				TimeLockVoice content = list.get(i);
				try {new File(content.getPath()).delete();} catch (Exception e) {}
				try {new File(content.getDecryptedPath()).delete();} catch (Exception e) {}
			}
		} catch (Exception e) { }
	}
	
	//when a photo is selected
	//store photo & create thumbnail to "secret place" (such as: data folder)
	
	//create data with 2 files
	//insert database
	//add to hash
	//insert to list
	//update list view data
	
	private static void addData(String paths[], int type, TimeLockContentsDataSource ds, long date, int folder,
			ArrayList<ViewData> listViewData, Hashtable<Integer, Data> hashData, ArrayList<Data> listData) {
		//create data from path & type
		Data data = createDataFromPaths(paths, type, date);
		TimeLockContent content = null;
		if (!(data instanceof TimeLockContent)) {
			Log.e("meobuerror", "###Storing data FAIL, unsupported class.");
			return;
		}
		//content from data
		content = (TimeLockContent)data;
		content.setFolder(folder);
		//insert database
		ds.put(content);
		//add hash
		if (hashData != null) {
			hashData.put(data.getDataId(), data);
		}
		//insert to list
		if (listData != null) {
			//search from top
			if (listData.isEmpty()) {
				listData.add(content);
			} else {
				int index = 0;
				while (index < listData.size() && listData.get(index).getDataTime() > content.getDataTime()) {
					index++;
				}
				if (index >= listData.size()) {
					listData.add(content);
				} else {
					listData.add(index, content);
				}
			}
			//listData.add(0, data);//always top
		}
		//update list view data
		if (listViewData != null) {
			insertListViewDataSort(data, listViewData);
		}
//		updateListViewDataUse();
	}
	
	public void renameFolder(String name, int folderid, int parent, TimeLockFoldersDataSource dsf) {
		try {
			dsf.update(name, folderid);
			getHashNode().get(folderid).setName(name);
			for (TimeLockFolder folder : getHashNode().get(parent).getFolders()) {
				if (folder != null && folder.getId() == folderid) {
					folder.setName(name);
				}
			}
		} catch (Exception e) { }
	}

	boolean adding = false;
	public void addFolder(String name, int parent, TimeLockFoldersDataSource dsf) {
		if (adding) return;
		adding = true;
		TimeLockFolder folder = new TimeLockFolder();
		folder.setFolder(parent);
		folder.setName(name);
		dsf.put(folder);
		//put parent
		getHashNode().get(parent).getFolders().add(folder);
		//put hash
		TimeLockNode newNode = TimeLockNode.copy(folder);
		getHashNode().put(newNode.getId(), newNode);
		//add view to parents
		addFolderToViews(newNode);
//		ArrayList<ViewData> views = getHashNode().get(parent).getListViewData();
//		if (views.size() <= 1) {
//			//empty here
//			views.clear();
//			addHeaderFolder(0, folder, views);
//		} else if (!(views.get(0) instanceof FolderHeaderViewData)) {
//			//no folder here
//			addHeaderFolder(0, folder, views);
//		} else {
//			//there's a folder
//			//find last
//			int index = 1;
//			while (index < views.size() && views.get(index) instanceof FoldersViewData) {
//				index++;
//			}
//			FoldersViewData lastViewFolder = null;
//			int lastViewFolderIndex = 0;
//			if (index >= views.size()) {
//				//add to last view
//				lastViewFolderIndex = views.size() - 1;
//			} else {
//				//add to previous index
//				lastViewFolderIndex = index - 1;
//			}
//			lastViewFolder = (FoldersViewData)views.get(lastViewFolderIndex);
//			//add to last
//			TimeLockFolder4View folderItem = new TimeLockFolder4View(folder);
//			if (lastViewFolder.getCount() >= 4) {
//				//new folder data
//				FoldersViewData newFolderData = new FoldersViewData();
//				newFolderData.add(folderItem);
//				views.add(lastViewFolderIndex + 1, newFolderData);
//				lastViewFolder = newFolderData;
//				lastViewFolderIndex++;
//			} else {
//				lastViewFolder.add(folderItem);
//			}
//		}
		adding = false;
	}
	private void addFolderToViews(int folderid) {
		if (getHashNode().containsKey(folderid)) {
			addFolderToViews(getHashNode().get(folderid));
		}
	}
	private void addFolderToViews(TimeLockNode folder) {
		if (!getHashNode().containsKey(folder.getFolder())) {
			return;
		}
		TimeLockNode parentNode = getHashNode().get(folder.getFolder());
		parentNode.updatedDate = System.currentTimeMillis();
		ArrayList<ViewData> views = parentNode.getListViewData();
		if (views == null) return;
		if (views.size() <= 1) {
			//empty here
			views.clear();
			addHeaderFolder(0, folder, views);
		} else if (!(views.get(0) instanceof FolderHeaderViewData)) {
			//no folder here
			addHeaderFolder(0, folder, views);
		} else {
			//there's a folder
			//find last
			int index = 1;
			while (index < views.size() && views.get(index) instanceof FoldersViewData) {
				index++;
			}
			FoldersViewData lastViewFolder = null;
			int lastViewFolderIndex = 0;
			if (index >= views.size()) {
				//add to last view
				lastViewFolderIndex = views.size() - 1;
			} else {
				//add to previous index
				lastViewFolderIndex = index - 1;
			}
			lastViewFolder = (FoldersViewData)views.get(lastViewFolderIndex);
			//add to last
			TimeLockFolder4View folderItem = new TimeLockFolder4View(folder);
			if (lastViewFolder.getCount() >= 4) {
				//new folder data
				FoldersViewData newFolderData = new FoldersViewData();
				newFolderData.add(folderItem);
				views.add(lastViewFolderIndex + 1, newFolderData);
				lastViewFolder = newFolderData;
				lastViewFolderIndex++;
			} else {
				lastViewFolder.add(folderItem);
			}
		}
		adding = false;
	}
//	public void addData(String paths[], int type, TimeLockContentsDataSource ds, long date) {
//		addData(paths, type, ds, date, 0);
//	}
	public void addData(String paths[], int type, TimeLockContentsDataSource ds, TimeLockFoldersDataSource dsf, long date, int folder) {
		if (!getHashNode().containsKey(folder)) {
			loadData(ds, dsf);
		}
		TimeLockNode node = getHashNode().get(folder);
//		Hashtable<Integer, Data> hashData = node.getHashContents();
//		boolean shouldResetHashData = false;
//		if (hashData == null) {
//			hashData = new Hashtable<Integer, Data>();
//			shouldResetHashData = true;
//		}
//		ArrayList<Data> listData = node.getContents();
//		boolean shouldResetListData = false;
//		if (listData == null) {
//			listData = new ArrayList<Data>();
//			shouldResetListData = true;
//		}
//		ArrayList<ViewData> listViewData = node.getListViewData();
//		boolean shouldResetListViewData = false;
//		if (listViewData == null) {
//			listViewData = new ArrayList<ViewData>();
//			shouldResetListViewData = true;
//		}
		node.updatedDate = System.currentTimeMillis();
		addData(paths, type, ds, date, folder, node.getListViewData(), node.getHashContents(), node.getContents());
//		if (shouldResetHashData) {
//			node.hashData = hashData;
//		}
//		if (shouldResetListData) {
//			this.listData = listData;
//		}
//		if (shouldResetListViewData) {
//			node.setListViewData(listViewData);
//		}
//		//create data from path & type
//		Data data = createDataFromPaths(paths, type, date);
//		TimeLockContent content = null;
//		if (!(data instanceof TimeLockContent)) {
//			Log.e("meobuerror", "###Storing data FAIL, unsupported class.");
//			return;
//		}
//		//content from data
//		content = (TimeLockContent)data;
//		//insert database
//		ds.put(content);
//		//add hash
//		getHashData().put(data.getDataId(), data);
//		//insert to list
//		getListData().add(0, data);//always top
//		//update list view data
//		insertListViewDataSort(data, listViewData);
////		updateListViewDataUse();
	}
	
	private static Data createDataFromPaths(String[] paths, int type, long date) {
		TimeLockContent content = new TimeLockContent();
		content.setDate(date);
		content.setPath(paths[0]);
		content.setThumbnail(paths[1]);
		content.setType(type);
		return content;
	}
	private static ItemViewData createItemViewDataFromData(Data data) {
		return new TimeLockContent4View(data);
	}
	private static int findGroupHeader(Data data, ArrayList<ViewData> listViewData) {
		return findGroupHeader(data, listViewData, false);
	}
	private static int findGroupHeader(Data data, ArrayList<ViewData> listViewData, boolean useNearest) {
		int startGroup = -1;
		for (int i=0; i<listViewData.size(); i++) {
			if (listViewData.get(i) instanceof HeaderViewData) {
				ContentViewData cvd = (ContentViewData)listViewData.get(i+1);
				int result = compareDay(cvd.getItems().get(0).getViewDate(), data.getDataTime());
				if (result == 0) {
					//correct
					startGroup = i;
					break;
				} else if (result < 0 && useNearest) {
					startGroup = i;
					break;
				}
			}
		}
		return startGroup;
	}
	private static void removeListViewData(Data data, ArrayList<ViewData> listViewData) {
		if (listViewData == null || listViewData.size() < 2) {
			//list data null
			//list data empty or contain only empty view data
			return;
		}
		//find the day
		int startGroup = findGroupHeader(data, listViewData);
//		for (int i=0; i<listViewData.size(); i++) {
//			if (listViewData.get(i) instanceof HeaderViewData) {
//				ContentViewData cvd = (ContentViewData)listViewData.get(i+1);
//				if (sameDay(data.getDataTime(), cvd.getItems().get(i).getViewDate())) {
//					startGroup = i;
//				}
//			}
//		}
		if (startGroup < 0) {
			//Log.d("meobudebug", "removeListViewData   Cannot file the date group of given item: " + data.getDataId());
			return;
		}
		//find index & item view data
		int startItem = -1;
		int itemIndex = -1;
		for (int i=startGroup + 1; i<listViewData.size(); i++) {
			ViewData vd = listViewData.get(i);
			if (vd instanceof HeaderViewData) {
				break;
			}
			ContentViewData cvd = (ContentViewData)vd;
			for (int j=0; j<cvd.getCount(); j++) {
				if (cvd.getItems().get(j).getDataId() == data.getDataId()) {
					startItem = i;
					itemIndex = j;
					break;
				}
			}
			if (startItem > 0 && itemIndex >= 0) {
				break;
			}
		}
		if (startItem < 0 || itemIndex < 0) {
			//Log.d("meobudebug", "removeListViewData   Cannot find the item line or item index of given item: " + data.getDataId());
			return;
		}
		//remove & re-arrange
		int previousLineIndex = startItem;
		ArrayList<ItemViewData> previousLineItems = ((ContentViewData)listViewData.get(startItem)).getItems();
		previousLineItems.remove(itemIndex);
		for (int i=startItem+1; i<listViewData.size(); i++) {
			ViewData vd = listViewData.get(i);
			if (vd instanceof HeaderViewData) {
				//done
				break;
			}
			ContentViewData line = (ContentViewData)vd;
			//@condition: items has data, for sure this time 
			ItemViewData ivd = line.getItems().remove(0);
			previousLineItems.add(ivd);
			previousLineItems = line.getItems();
			previousLineIndex = i;
		}
		if (previousLineItems.isEmpty()) {
			//1 line empty
			//remove that lineout
			listViewData.remove(previousLineIndex);
			
			if (listViewData.size() < 2) {
				//no item here, only the last header
				//clear & add empty
				listViewData.clear();
				listViewData.add(new EmptyViewData());
			} else if (previousLineIndex == startGroup + 1) {
				//last line of this group is removed
				//remove group header
				listViewData.remove(startGroup);
			}
		}
	}
	private static void insertListViewDataSort(Data data, ArrayList<ViewData> listViewData) {
		if (listViewData == null) {
			//cannot do any thing
		} else if (listViewData.size() <= 1) {
			//just empty view
			//clear & add group
			listViewData.clear();
			addHeaderContent(data, listViewData);
		} else {
			//find group
			int startGroup = -1;
			int nextGroup = -1;
			for (int i=0; i<listViewData.size(); i++) {
				if (listViewData.get(i) instanceof HeaderViewData) {
					ContentViewData cvd = (ContentViewData)listViewData.get(i+1);
					int result = compareDay(cvd.getItems().get(0).getViewDate(), data.getDataTime());
					if (result == 0) {
						//correct
						startGroup = i;
						break;
					} else if (result < 0) {
						nextGroup = i;
						break;
					}
				}
			}
			if (startGroup < 0) {
				//not exist same day
				if (nextGroup < 0) {
					//not exist older group
					//create last group
					addHeaderContent(-1, data, listViewData);//add to last
				} else {
					//exist older group
					//create new group above
					addHeaderContent(nextGroup, data, listViewData);//add to above older group
				}
			} else {
				//found same day group
				//find startItem, indexItem
				int startItem = -1, indexItem = -1;
				int nextItem = -1;
				//boolean previousNewer = true;
				for (int i=startGroup+1; i<listViewData.size(); i++) {
					ViewData vd = listViewData.get(i);
					if (vd instanceof HeaderViewData) {
						//out of group
						nextItem = i;
						break;
					}
					ContentViewData cvd = (ContentViewData)vd;
					if (cvd.getItems().get(0).getViewDate() <= data.getDataTime()) {
						//data is newer
						//add at 0
						startItem = i;
						indexItem = 0;
						break;
					}
					for (int j=1; j<cvd.getItems().size(); j++) {
						if (cvd.getItems().get(j).getViewDate() <= data.getDataTime()) {
							//data is newer
							//add here
							startItem = i;
							indexItem = j;
							break;
						}
					}
					if (startItem < 0 || indexItem < 0) {
						if (cvd.getItems().size() < 4) {
							//not full item
							//add here
							startItem = i;
							indexItem = cvd.getItems().size();
							break;
						}
						//contain 4 and the last still newer
						//go next line
					} else {
						//did found slot
						break;
					}
				}
				//check result
				if (startItem < 0 || indexItem < 0) {
					//did not found, last line contains 4 and still newer
					//add new Content View Data and DONE
					ItemViewData outItem = createItemViewDataFromData(data);
					ContentViewData newContentData = new ContentViewData();
					newContentData.add(outItem);
					if (nextItem < 0) {
						listViewData.add(newContentData);
					} else {
						listViewData.add(nextItem, newContentData);
					}
				} else {
					//the position is in the middle of crowd
					//add, pop, add
					ItemViewData outItem = createItemViewDataFromData(data);
//					ArrayList<ItemViewData> itemList = ((ContentViewData)listViewData.get(startItem)).getItems();
//					itemList.add(indexItem, outItem);
//					if (itemList.size() > 4) {
//						outItem = itemList.remove(4);
//					} else {
//						outItem = null;
//					}
					int i = startItem;
					while (outItem != null && i < listViewData.size() && listViewData.get(i) instanceof ContentViewData) {
						ContentViewData cvd = (ContentViewData)listViewData.get(i);
						outItem = cvd.insert(indexItem, outItem);
						indexItem = 0;
						i++;
					}
					if (outItem == null) {
						//added to last group
					} else if (i >= listViewData.size()) {
						//out of the only 1 group
						//did process above: //did not found, last line contains 4 and still newer
						ContentViewData newContentData = new ContentViewData();
						newContentData.add(outItem);
						listViewData.add(newContentData);
					} else {
						//out of a group which is above another group
						//did process above: //did not found, last line contains 4 and still newer
						ContentViewData newContentData = new ContentViewData();
						newContentData.add(outItem);
						listViewData.add(i, newContentData);
					}
				}
			}
			
//				//same day with previous
//				ItemViewData outItem = createItemViewDataFromData(data);
//				int i = 1;
//				while (outItem != null && i < listViewData.size() && listViewData.get(i) instanceof ContentViewData) {
//					ContentViewData contentViewData = (ContentViewData)listViewData.get(i);
//					outItem = contentViewData.insert0(outItem);
//					i++;
//				}
//				if (outItem == null) {
//					//added to previous
//				} else if (i >= listViewData.size()) {
//					//1 group only, add more
//					ContentViewData newContentData = new ContentViewData();
//					newContentData.add(outItem);
//					listViewData.add(newContentData);
//				} else {
//					//2 or more group, is at index of header of 2nd group
//					ContentViewData newContentData = new ContentViewData();
//					newContentData.add(outItem);
//					listViewData.add(i, newContentData);
//				}
		}
	}
//	private static void insertTopListViewData(Data data, ArrayList<ViewData> listViewData) {
//		if (listViewData == null) {
//			//cannot do any thing
//		} else if (listViewData.isEmpty() || listViewData.size() == 1) {
//			listViewData.clear();
//			addHeaderContent(data, listViewData);
//		} else {
//			long latestDate = getLatestDate(listViewData);
//			if (!sameDay(data.getDataTime(), latestDate)) {
//				//different day, just create new header & add at first
//				addHeaderContent(0, data, listViewData);
//			} else {
//				//same day with previous
//				ItemViewData outItem = createItemViewDataFromData(data);
//				int i = 1;
//				while (outItem != null && i < listViewData.size() && listViewData.get(i) instanceof ContentViewData) {
//					ContentViewData contentViewData = (ContentViewData)listViewData.get(i);
//					outItem = contentViewData.insert0(outItem);
//					i++;
//				}
//				if (outItem == null) {
//					//added to previous
//				} else if (i >= listViewData.size()) {
//					//1 group only, add more
//					ContentViewData newContentData = new ContentViewData();
//					newContentData.add(outItem);
//					listViewData.add(newContentData);
//				} else {
//					//2 or more group, is at index of header of 2nd group
//					ContentViewData newContentData = new ContentViewData();
//					newContentData.add(outItem);
//					listViewData.add(i, newContentData);
//				}
//			}
//		}
//	}
//	private static long getLatestDate(ArrayList<ViewData> list) {
//		ViewData vd = list.get(1);
//		ItemViewData ivd = vd.getItems().get(0);
//		return ivd.getViewDate();
//	}
//	private static FoldersViewData addHeaderFolder(TimeLockFolder folder, ArrayList<ViewData> list) {
//		return addHeaderFolder(-1, folder, list);
//	}
	private static FoldersViewData addHeaderFolder(int index, TimeLockFolder folder, ArrayList<ViewData> list) {
		//group header
		FolderHeaderViewData newHeaderData = new FolderHeaderViewData();
		//group folder
		FoldersViewData newFolderData = new FoldersViewData();
		TimeLockFolder4View folderItem = new TimeLockFolder4View(folder);
		newFolderData.add(folderItem);
		if (index < 0 || index > list.size()) {
			list.add(newHeaderData);
			list.add(newFolderData);
		} else {
			list.add(index, newFolderData);
			list.add(index, newHeaderData);
		}
		return newFolderData;
	}
	private static ContentViewData addHeaderContent(Data data, ArrayList<ViewData> list) {
		return addHeaderContent(-1, data, list);
	}
	private static ContentViewData addHeaderContent(int index, Data data, ArrayList<ViewData> list) {
		//add group header
		HeaderViewData newHeaderData = new HeaderViewData(data.getDataTime());
		//add group content
		ContentViewData newContentData = new ContentViewData();
		TimeLockContent4View contentItem = new TimeLockContent4View(data);
		newContentData.add(contentItem);
		if (index < 0 || index > list.size()) {
			list.add(newHeaderData);
			list.add(newContentData);
		} else {
			list.add(index, newContentData);
			list.add(index, newHeaderData);
		}
		return newContentData;
	}
	public void setFeaturedFolderVisibility4Views(int folderid, boolean visible) {
		if (visible) {
			//add
			addFolderToViews(folderid);
		} else {
			//remove
			removeFolderFromViews(folderid);
		}
	}
//	public void setSecuredFolderVisibility4Views(boolean visible) {
//		setFeaturedFolderVisibility4Views(SECURED_FOLDER_ID, visible);
//	}
	public void removeFolder(int folderid, TimeLockFoldersDataSource dsf, TimeLockContentsDataSource ds) {
		Log.d("meobudebug", "removeFolder: " + folderid);
		if (folderid <= 0 || !getHashNode().containsKey(folderid)) return;
//				|| getHashNode().get(folderid).getListViewData() == null
//				|| !(getHashNode().get(folderid).getListViewData().get(0) instanceof FolderHeaderViewData)) return;
		//make list 
		ArrayList<Data> data = new ArrayList<Data>();
		ArrayList<TimeLockNode> nodes = new ArrayList<TimeLockNode>();
		nodes.add(getHashNode().get(folderid));
		int walker = 0;
		while (walker < nodes.size()) {
			TimeLockNode node = nodes.get(walker);
			//fill folder
			for (TimeLockFolder folder : node.getFolders()) {
				if (getHashNode().containsKey(folder.getId())) {
					//add to queue
					TimeLockNode n = getHashNode().get(folder.getId());
					boolean already = false;
					for (TimeLockNode cn : nodes) {
						if (cn.getId() == n.getId()) {
							already = true;
							break;
						}
					}
					if (!already) {
						nodes.add(n);
					} else {
						Log.d("meobuerror", "not linux, shouldn't come here!");
					}
				}
			}
			//fill content
			data.addAll(node.getContents());
			walker++;
		}
		Log.d("meobudebug", "folders:" + nodes.size() + "     contents:" + data.size());
		//delete
		ArrayList<TimeLockContent> contents = new ArrayList<TimeLockContent>();
		for (Data item : data) {
			if (item instanceof TimeLockContent) {
				contents.add((TimeLockContent)item);
			}
		}
		ds.remove(contents);//databse
		for (TimeLockContent content : contents) {//files
			try { new File(content.getDataPath()).delete(); } catch (Exception e) {}
			try { new File(content.getDataPathDecrypted()).delete(); } catch (Exception e) {}
			try { new File(content.getDataThumbnail()).delete(); } catch (Exception e) {}
		}
		ArrayList<TimeLockFolder> folders = new ArrayList<TimeLockFolder>(nodes);
		dsf.remove(folders);//database
		//remove this one from current view (the parent of given folder)
		removeFolderFromViews(folderid);
//		ArrayList<ViewData> views = getHashNode().get((getHashNode().get(folderid).getFolder())).getListViewData();
//		if (views == null || !(views.get(0) instanceof FolderHeaderViewData)) return;
//		FoldersViewData missing = null;
//		int i;
//		for (i=1; i<views.size() && views.get(i) instanceof FoldersViewData; i++) {
//			FoldersViewData view = (FoldersViewData)views.get(i);
//			if (missing == null) {
//				//find the line
//				for (int j=0; j<view.getFolderItems().size(); j++) {
//					if (view.getFolderItems().get(j).getDataId() == folderid) {
//						missing = view;
//						missing.items.remove(j);
//						break;
//					}
//				}
//			} else {
//				missing.add(view.items.remove(0));
//				missing = view;
//			}
//		}
//		if (missing == null) {
//		} else if (missing.getCount() <= 0) {
//			if (i == 2) {
//				//only one folder is deleted
//				views.remove(1);
//				views.remove(0);
//			} else {
//				views.remove(i-1);
//			}
//		}
	}
	private void removeFolderFromViews(int folderid) {
		if (!getHashNode().containsKey(folderid) || !getHashNode().containsKey(getHashNode().get(folderid).getFolder())) return;
		TimeLockNode parentNode = getHashNode().get((getHashNode().get(folderid).getFolder()));
		parentNode.updatedDate = System.currentTimeMillis();
		ArrayList<ViewData> views = parentNode.getListViewData();
		if (views == null || !(views.get(0) instanceof FolderHeaderViewData)) return;
		FoldersViewData missing = null;
		int i;
		for (i=1; i<views.size() && views.get(i) instanceof FoldersViewData; i++) {
			FoldersViewData view = (FoldersViewData)views.get(i);
			if (missing == null) {
				//find the line
				for (int j=0; j<view.getFolderItems().size(); j++) {
					if (view.getFolderItems().get(j).getDataId() == folderid) {
						missing = view;
						missing.items.remove(j);
						break;
					}
				}
			} else {
				missing.add(view.items.remove(0));
				missing = view;
			}
		}
		if (missing == null) {
		} else if (missing.getCount() <= 0) {
			if (i == 2) {
				//only one folder is deleted
				views.remove(1);
				views.remove(0);
			} else {
				views.remove(i-1);
			}
		}
	}
//	public void removeData(Data data, TimeLockContentsDataSource ds) {
//		removeData(data, ds, 0);
//	}
	public void removeData(Data data, TimeLockContentsDataSource ds, int folder) {
		if (!(data instanceof TimeLockContent)) {
			Log.e("meobuerror", "###Delete data FAIL, unsupported class.");
			return;
		}
		if (!getHashNode().containsKey(folder)) return;
		TimeLockNode node = getHashNode().get(folder);
		shouldUpdate = true;
		//delete file
		new File(data.getDataPath()).delete();
		new File(data.getDataThumbnail()).delete();
		//remove data base
		ds.remove((TimeLockContent)data);
		//remove list index
		node.getContents().remove(data);
		//remove hash index
		node.getHashContents().remove(data.getDataId());
		//remove view data
		node.updatedDate = System.currentTimeMillis();
		removeListViewData(data, node.getListViewData());
	}

	//----------//----------//----------//----------//----------//
	
	private Hashtable<Integer, TimeLockNode> hashNode;
	public Hashtable<Integer, TimeLockNode> getHashNodeForCodeActivityOnly() {
		return getHashNode();
	}
	public Hashtable<Integer, TimeLockNode> getHashNodeForSafeActivitySavingBackOnly() {
		return getHashNode();
	}
	public Hashtable<Integer, TimeLockNode> getHashNodeForSafeActivityFolderName() {
		return getHashNode();
	}
	private Hashtable<Integer, TimeLockNode> getHashNode() {
		if (hashNode == null) {
			hashNode = new Hashtable<Integer, TimeLockNode>();
		}
		return hashNode;
	}
//	private Hashtable<Integer, Data> hashData;
//	private Hashtable<Integer, Data> getHashData() {
//		if (hashData == null) {
//			hashData = new Hashtable<Integer, Data>();
//		}
//		return hashData;
//	}
//	private ArrayList<Data> listData;
//	public ArrayList<Data> getListData() {
//		if (listData == null) {
//			listData = new ArrayList<Data>();
//		}
//		return listData;
//	}
	public void cleanCache() {
		ImageLoader.getInstance().clearDiscCache();
		Enumeration<TimeLockNode> elements = getHashNode().elements();
		while (elements.hasMoreElements()) {
			TimeLockNode node = elements.nextElement();
			for (Data data : node.getContents()) {
				if (data.getDataPathDecrypted() != null) {
					new File(data.getDataPathDecrypted()).delete();
					data.setDataPathDecrypted(null);
				}
			}
		}
//		ArrayList<Data> data = getListData();
//		if (data == null || data.isEmpty() || data.size() <= 0) {
//			return;
//		}
//		for (int i=0; i<data.size(); i++) {
//			Data item = data.get(i);
//			if (item.getDataPathDecrypted() != null) {
//				//clean cache file
//				new File(item.getDataPathDecrypted()).delete();
//				item.setDataPathDecrypted(null);
//			}
//		}
		hashNode = null;
//		listData = null;
//		listViewData = null;
//		hashData = null;
	}
	
	public static interface Data {
		public int getDataId();
		public int getDataType();
		public long getDataTime();
		public String getDataPath();
		public String getDataThumbnail();
		public String getDataPathDecrypted();
		public void setDataPathDecrypted(String path);
	}

	//private ArrayList<ViewData> listViewData;
	//private ArrayList<ViewData> getListViewData() {
	//	return listViewData;
	//}
	public TimeLockNode getNode(int folderid) {
		if (getHashNode().containsKey(folderid)) {
			return getHashNode().get(folderid);
		}
		return null;
	}
//	public ArrayList<ViewData> getListViewData(int folderid) {
//		if (getHashNode().containsKey(folderid)) {
//			return getHashNode().get(folderid).getListViewData();
//		}
//		return null;
//	}
	public ArrayList<Data> getListData(int folderid) {
		if (getHashNode().containsKey(folderid)) {
			return getHashNode().get(folderid).getContents();
		}
		return null;
	}
	
//	public boolean isDataUpdated() {
//		if (getListData().isEmpty() && listViewData == null) {
//			return true;
//		}
//		return false;
//	}
	private boolean shouldUpdate = true;
	private void loadData(TimeLockContentsDataSource dsc, TimeLockFoldersDataSource dsf) {
		ArrayList<TimeLockFolder> folders = null;
		if (dsf != null) {
			folders = dsf.retrieve(0, Integer.MAX_VALUE);
		} else {
			folders = new ArrayList<TimeLockFolder>();
		}
		ArrayList<TimeLockContent> contents = null;
		if (dsc != null) {
			contents = dsc.retrieve(0, Integer.MAX_VALUE);
		} else {
			contents = new ArrayList<TimeLockContent>();
		}
//		boolean secure = getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]);
//		boolean note = getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[3]);
//		boolean spy = getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]);
		loadData(folders, contents, getHashNode(), this);//, secure, note, spy);
	}
	public static final int SECURED_FOLDER_ID = Integer.MAX_VALUE;
	public static final int NOTE_FOLDER_ID = Integer.MAX_VALUE - 1;
	public static final int VOICE_FOLDER_ID = Integer.MAX_VALUE - 2;
	public static final int SPY_FOLDER_ID = Integer.MAX_VALUE - 5;
	private static TimeLockNode createNodeSecured(Context context) {
		TimeLockNode nodeSecured = new TimeLockNode();
		nodeSecured.setId(SECURED_FOLDER_ID);
		nodeSecured.setName(context.getString(R.string.safe_folder_secured_title));
		nodeSecured.setFolder(0);
		return nodeSecured;
	}
	private static void predefineFolders(Hashtable<Integer, TimeLockNode> nodes, Context context) {//, boolean secure, boolean note, boolean spy) {
		TimeLockNode node0 = new TimeLockNode();
		node0.setId(0);
		node0.setName("root");
		node0.setFolder(0);
		nodes.put(node0.getId(), node0);
		//TODO next: secured folder, spy folder, note folder
		//folder id from Integer.MAX_VALUE;
		{
			TimeLockNode nodeSecured = createNodeSecured(context);
			nodes.put(nodeSecured.getId(), nodeSecured);
			node0.getFolders().add(nodeSecured);
		}
		{
			TimeLockNode nodeNote = new TimeLockNode();
			nodeNote.setId(NOTE_FOLDER_ID);
			nodeNote.setName(context.getString(R.string.note_screen_title));
			nodeNote.setFolder(0);
			nodes.put(nodeNote.getId(), nodeNote);
			node0.getFolders().add(nodeNote);
		}
		{
			TimeLockNode nodeSpy = new TimeLockNode();
			nodeSpy.setId(SPY_FOLDER_ID);
			nodeSpy.setName(context.getString(R.string.safe_folder_spy_title));
			nodeSpy.setFolder(0);
			nodes.put(nodeSpy.getId(), nodeSpy);
			node0.getFolders().add(nodeSpy);
		}
		{
			TimeLockNode nodeVoice = new TimeLockNode();
			nodeVoice.setId(VOICE_FOLDER_ID);
			nodeVoice.setName(context.getString(R.string.folder_voice_title));
			nodeVoice.setFolder(0);
			nodes.put(nodeVoice.getId(), nodeVoice);
			node0.getFolders().add(nodeVoice);
		}
	}
	private static void addToListSorter(ArrayList<Data> data, Data item) {
		//copy to addData(..., use search from top, since when add new, the new object mostly newer than others
//		//search from top
//		int index = 0;
//		while (index < getListData().size() && getListData().get(index).getDataTime() > content.getDataTime()) {
//			index++;
//		}
//		getListData().add(index+1, content);
		//search from bottom
		int index = data.size() - 1;
		while (index >= 0 && data.get(index).getDataTime() < item.getDataTime()) {
			index--;
		}
		data.add(index+1, item);
	}
	private static void loadData(ArrayList<TimeLockFolder> folders, ArrayList<TimeLockContent> contents,
			Hashtable<Integer, TimeLockNode> nodes, Context context) {//, boolean secure, boolean note, boolean spy) {//, Hashtable<Integer, Data> hashData, ArrayList<Data> data) {
		//FOLDERs
		predefineFolders(nodes, context);//, secure, note, spy);
		//Hashtable<Integer, TimeLockNode> nodes = getHashNode();
		for (TimeLockFolder folder : folders) {
			//add myself
			if (!nodes.containsKey(folder.getId())) {
				TimeLockNode node = TimeLockNode.copy(folder);
				nodes.put(node.getId(), node);
			} else {
				TimeLockNode node = nodes.get(folder.getId());
				node.setFolder(folder.getFolder());
				node.setName(folder.getName());
			}
			//add to parent
			if (!nodes.containsKey(folder.getFolder())) {
				//no parent here
				TimeLockNode node = new TimeLockNode();
				node.setId(folder.getFolder());
				nodes.put(node.getId(), node);
			}
			nodes.get(folder.getFolder()).getFolders().add(folder);
		}
		//CONTENTS
		for (TimeLockContent content : contents) {
			//hashData.put(content.getId(), content);
			//addToListSorter(data, content);
			//add to folder
			TimeLockNode folder = null;
			if (nodes.containsKey(content.getFolder())) {
				folder = nodes.get(content.getFolder());
			} else {
				Log.d("meobudebug", "shouldn't come here. No problem, some items inside [secured folder]");
				folder = new TimeLockNode();
				folder.setId(content.getFolder());
				nodes.put(folder.getId(), folder);
			}
			folder.getHashContents().put(content.getId(), content);
			addToListSorter(folder.getContents(), content);
		}
	}
//	public boolean updateData(TimeLockContentsDataSource ds) {
//		return updateData(ds, null);
//	}
//	public boolean updateData(TimeLockContentsDataSource ds, TimeLockFoldersDataSource fds) {
//		return updateData(ds, fds, 0);
//	}
	private static void generateViewData(TimeLockNode node, boolean folder, boolean securedFolderVisible, boolean spyFolderVisible, boolean noteFolderVisible, boolean voiceFolderVisible) {
		ArrayList<ViewData> listViewData = new ArrayList<ViewData>();
		//getListData()
		long date = 0;
		ContentViewData current = null;
		for (int i=0; i<node.getContents().size(); i++) {
			Data data = node.getContents().get(i);
			//new group found
			if (listViewData.isEmpty() || !sameDay(data.getDataTime(), date)) {
				date = data.getDataTime();
				current = addHeaderContent(data, listViewData);
			} else {
				//add object 
				TimeLockContent4View contentItem = new TimeLockContent4View(data);
				if (current.getCount() >= 4) {
					//new content data
					ContentViewData newContentData = new ContentViewData();
					newContentData.add(contentItem);
					listViewData.add(newContentData);
					current = newContentData;
				} else {
					current.add(contentItem);
				}
			}
		}
		if (folder) {
			//Log.d("meobudebug", "ye, test folder: " + node.getFolders().size());
			ArrayList<TimeLockFolder> getFolders = null;
			if (securedFolderVisible && spyFolderVisible && noteFolderVisible) {
				getFolders = node.getFolders();
			} else {
				getFolders = new ArrayList<TimeLockFolder>(node.getFolders());
				//remove secure
				for (int i=0; i<getFolders.size(); i++) {
					//TODO if add new predefine folder
					if ((getFolders.get(i).getId() == SECURED_FOLDER_ID && !securedFolderVisible) ||
							(getFolders.get(i).getId() == SPY_FOLDER_ID && !spyFolderVisible) ||
							(getFolders.get(i).getId() == NOTE_FOLDER_ID && !noteFolderVisible) ||
							(getFolders.get(i).getId() == VOICE_FOLDER_ID && !voiceFolderVisible)) {
						getFolders.remove(i);
						i--;
						continue;
					}
				}
			}
			if (!getFolders.isEmpty()) { 
				FoldersViewData currentFolder = addHeaderFolder(0, getFolders.get(0), listViewData);
				int currentFolderIndex = 1;
				for (int i=1; i<getFolders.size(); i++) {
					//add object
					TimeLockFolder4View folderItem = new TimeLockFolder4View(getFolders.get(i));
					if (currentFolder.getCount() >= 4) {
						//new folder data
						FoldersViewData newFolderData = new FoldersViewData();
						newFolderData.add(folderItem);
						listViewData.add(currentFolderIndex + 1, newFolderData);
						currentFolder = newFolderData;
						currentFolderIndex++;
					} else {
						currentFolder.add(folderItem);
					}
				}
			}
		}
		if (listViewData.isEmpty()) {
			listViewData.add(new EmptyViewData());
		}
		node.setListViewData(listViewData);
	}
	public boolean updateData(TimeLockContentsDataSource ds, TimeLockFoldersDataSource fds, int folderid) {
		boolean result = shouldUpdate;shouldUpdate = false;
		//if not load, load all
		if (getHashNode().isEmpty()) {
			loadData(ds, fds);
//			ArrayList<TimeLockContent> contents = ds.retrieve(0, Integer.MAX_VALUE);
//			for (int i=0; i<contents.size(); i++) {
//				TimeLockContent content = contents.get(i);
//				//add to hash by id
//				getHashData().put(content.getId(), content);
//				//add to list by date
//				//copy to addData(..., use search from top, since when add new, the new object mostly newer than others
////				//search from top
////				int index = 0;
////				while (index < getListData().size() && getListData().get(index).getDataTime() > content.getDataTime()) {
////					index++;
////				}
////				getListData().add(index+1, content);
//				//search from bottom
//				int index = getListData().size() - 1;
//				while (index >= 0 && getListData().get(index).getDataTime() < content.getDataTime()) {
//					index--;
//				}
//				getListData().add(index+1, content);
//			}
//			//debug only
////			for (int i=0; i<getListData().size(); i++) {
////				if (i+1 < getListData().size()) {
////					if (getListData().get(i).getDataTime() > getListData().get(i+1).getDataTime()) {
////						Log.d("meobudebug", "item " + i + ": " + getListData().get(i).getDataTime());
////					} else {
////						Log.d("meobudebug", "item " + i + ": " + getListData().get(i).getDataTime() + " NOOOOO");
////					}
////				}
////			}
		}
		if (getHashNode().containsKey(folderid)) {
			TimeLockNode node = getHashNode().get(folderid);
			if (node.getListViewData() == null) {
				node.updatedDate = System.currentTimeMillis();
				result = true;
//				boolean securedVisible = getSecuredFolderVisible() && (folderid == 0);
				boolean secure = getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[2]) && (folderid == 0) && getSecuredFolderVisible() ;
				boolean note = getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[3]) && (folderid == 0) && getNotesFolderVisible();
				boolean spy = getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]) && (folderid == 0);
				boolean voice = getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[6]) && (folderid == 0);
				generateViewData(node, getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[1]), secure, spy, note, voice);
			}
		} else {
			Log.d("meobudebug", "shouldn't come here. don't know why");
		}
//		//if not convert, convert all
//		if (listViewData == null) {
//			//convert to use addData only //# no need, because this function works faster
//			result = true;
//			ArrayList<ViewData> listViewData = new ArrayList<ViewData>();
//			//getListData()
//			long date = 0;
//			ContentViewData current = null;
//			for (int i=0; i<getListData().size(); i++) {
////			for (int k=getListData().size()-1; k>=0; k--) {
//				Data data = getListData().get(i);
////				if (listViewData.isEmpty()) {
////					date = data.getDataTime();
////					current = addHeaderContent(data, listViewData);
////				}
//				//new group found
//				if (listViewData.isEmpty() || !sameDay(data.getDataTime(), date)) {
//					date = data.getDataTime();
//					current = addHeaderContent(data, listViewData);
//				} else {
//					//add object 
//					TimeLockContent4View contentItem = new TimeLockContent4View(data);
//					if (current.getCount() >= 4) {
//						//new content data
//						ContentViewData newContentData = new ContentViewData();
//						newContentData.add(contentItem);
//						listViewData.add(newContentData);
//						current = newContentData;
//					} else {
//						current.add(contentItem);
//					}
//				}
//			}
//			if (listViewData.isEmpty()) {
//				listViewData.add(new EmptyViewData());
//			}
//			this.listViewData = listViewData;
//		}
////		updateListViewDataUse();
		return result;
	}
//	private ArrayList<ViewData> listViewDataUse;
//	public ArrayList<ViewData> getListViewDataUse() {
//		return listViewDataUse;
//	}
//	private void updateListViewDataUse() {
//		if (listViewDataUse == null) {
//			ArrayList<ViewData> useList = new ArrayList<ViewData>(listViewData);
//			for (int i=useList.size(); i<5; i++) {
//				useList.add(emptyContentViewData());
//			}
//			listViewDataUse = useList;
//		} else {
//			listViewDataUse.clear();
//			listViewDataUse.addAll(listViewData);
//			for (int i=listViewDataUse.size(); i<5; i++) {
//				listViewDataUse.add(emptyContentViewData());
//			}
//		}
//	}
	private static int compareDay(long param1, long param2) {
		final long param1t = param1 / 86400000l;
		final long param2t = param2 / 86400000l;
		if (param1t == param2t) {
			return 0;
		}
		if (param1t < param2t) {
			return -1;
		}
		return 1;
	}
	private static boolean sameDay(long param1, long param2) {
		if (param1 / 86400000l == param2 / 86400000l) {
			return true;
		}
		return false;
	}
	private static String formatHeader(long date) {
		SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
		return format.format(new Date(date));
	}
	
	//----------//----------//----------//----------//----------//
	
	public static interface ItemViewData {
		enum ItemViewDataType {
			Photo,
			Video,
		}
		public int getDataId();
		public int getViewType();
		public String getViewThumbnailPath();
		public long getViewDate();
	}
	public static interface FolderItemViewData {
		public int getDataId();
		public int getFolderViewType();
	}
	public static interface FolderItemViewData2 extends FolderItemViewData {
		public String getFolderName();
	}
//	public static class SimpleItemViewData implements ItemViewData {
//		private int id, type;
//		private BitmapDrawable thumbnail;
//		public SimpleItemViewData(int id, int type, BitmapDrawable thumbnail, String path) {
//			this.id = id;
//			this.type = type;
//			this.thumbnail = thumbnail;
//		}
//		@Override
//		public int getDataId() { return id; }
//		@Override
//		public int getViewType() { return type; }
//		@Override
//		public BitmapDrawable getViewThumbnail() { return thumbnail; }
//		@Override
//		public String getViewThumbnailPath() { return null; }
//	}
	
	public static interface ViewData extends MeobuListViewData {
		enum ViewDataType {
			GroupEmpty,
			GroupHeader,
			GroupContent,
			GroupFolderHeader,
			GroupFolders,
		}
		public String getTime();
		public int getCount();
		public ArrayList<ItemViewData> getItems();
	}
	public static class EmptyViewData implements ViewData {
		@Override
		public int getType() { return ViewDataType.GroupEmpty.ordinal(); }
		@Override @Deprecated
		public String getTime() { return null; }
		@Override @Deprecated
		public int getCount() { return 0; }
		@Override @Deprecated
		public ArrayList<ItemViewData> getItems() { return null; }
	}
	public static class FolderHeaderViewData implements ViewData {
		@Override
		public int getType() { return ViewDataType.GroupFolderHeader.ordinal(); }
		@Override @Deprecated
		public String getTime() { return null; }
		@Override @Deprecated
		public int getCount() { return 0; }
		@Override @Deprecated
		public ArrayList<ItemViewData> getItems() { return null; }
	}
	public static class FoldersViewData implements ViewData {
		@Override
		public int getType() { return ViewDataType.GroupFolders.ordinal(); }
		@Override @Deprecated
		public String getTime() { return null; }
		@Override
		public int getCount() { return items.size(); }
		@Override @Deprecated
		public ArrayList<ItemViewData> getItems() { return null; }

		public ArrayList<FolderItemViewData> getFolderItems() { return items; }
		ArrayList<FolderItemViewData> items = new ArrayList<FolderItemViewData>();
		public void add(FolderItemViewData item) {
			items.add(item);
		}
		public FolderItemViewData insert(int index, FolderItemViewData item) {
			items.add(index, item);
			if (items.size() <= 4) {
				return null;
			}
			FolderItemViewData out = items.remove(items.size() - 1);
			return out;
		}
		public FolderItemViewData insert0(FolderItemViewData item) {
			return insert(0, item);
		}
	}
	public static class HeaderViewData implements ViewData {
		public HeaderViewData(long time) {
			this.time = time;
		}
		long time;
		String timeString;
		public void clearTimeCache() {
			timeString = null;
		}
		@Override
		public int getType() { return ViewDataType.GroupHeader.ordinal(); }
		@Override
		public String getTime() {
			if (timeString == null) {
				timeString = formatHeader(time);
			}
			return timeString;
		}
		@Override
		public int getCount() { return 0; }
		@Override
		public ArrayList<ItemViewData> getItems() { return null; }
	}
	static ContentViewData empty;
	//use for empty shelf on the view only
	public static ContentViewData emptyContentViewData() {
		if (empty == null) {
			empty = new ContentViewData();
		}
		return empty;
	}
	public static class ContentViewData implements ViewData {
		public ContentViewData() { }
		@Override
		public int getType() { return ViewDataType.GroupContent.ordinal(); }
		@Override
		public String getTime() { return null; }
		@Override
		public int getCount() { return items.size(); }
		@Override
		public ArrayList<ItemViewData> getItems() { return items; }
		ArrayList<ItemViewData> items = new ArrayList<ItemViewData>();
		public void add(ItemViewData item) {
			items.add(item);
		}
		public ItemViewData insert(int index, ItemViewData item) {
			items.add(index, item);
			if (items.size() <= 4) {
				return null;
			}
			ItemViewData out = items.remove(items.size() - 1);
			return out;
		}
		public ItemViewData insert0(ItemViewData item) {
			return insert(0, item);
//			items.add(0, item);
//			if (items.size() <= 4) {
//				return null;
//			}
//			ItemViewData out = items.remove(items.size() - 1);
//			return out;
		}
	}
	
	//----------//----------//----------//----------//----------//
}
