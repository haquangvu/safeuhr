package com.protectstar.timelock.pro.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * no use
 * @author meobu
 *
 */
public class ScreenOffReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("meobudebug", "ScreenOffReceiver.onReceive: " + intent);
	}
	
}
