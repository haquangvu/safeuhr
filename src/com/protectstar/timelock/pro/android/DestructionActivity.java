package com.protectstar.timelock.pro.android;

import com.protectstar.safeuhr.android.R;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DestructionActivity extends TimeLockActivity {

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selfdestruction);
		setupClickable();
	}
	
	boolean activated;
	int method;
	@Override
	protected void onResume() {
		super.onResume();
		TimeLockApplication app = (TimeLockApplication)getApplication();
		activated = app.getSelfDestructionActivated();
		//activated = app.getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[0]);
		//method = app.getSelfDestructionMethod();
		//Log.d("meobudebug", "activated: " + activated + "   method: " + method);
		updateViews(activated, method);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	private final int[] method_viewid = {R.id.destructMethod0, R.id.destructMethod1};
	private void setupClickable() {
		findViewById(R.id.destructionActivated).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				activated = !activated;
				((TimeLockApplication)getApplication()).setSelfDestructionActivated(activated);
				updateActivated(activated);
			}
		});
		final View.OnClickListener methodClick = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				int index = getViewTag(view);
				method = index;
				//Log.d("meobudebug", "change method: " + method);
				((TimeLockApplication)getApplication()).setSelfDestructionMethod(method);
				updateMethod(method);
			}
		};
		for (int i=0; i<method_viewid.length; i++) {
			View view = findViewById(method_viewid[i]);
			view.setTag(i);
			view.setOnClickListener(methodClick);
		}
	}
	
	//----------//----------//----------//----------//----------//
	
	private void updateViews(boolean activated, int method) {
		updateActivated(activated);
		updateMethod(method);
	}
	
	private Drawable activatedDrawable, unactivatedDrawable, checkDrawable;
	private Drawable getActivatedDrawable() {
		if (activatedDrawable == null) {
			activatedDrawable = getResources().getDrawable(R.drawable.switch_on_big);
		}
		return activatedDrawable;
	}
	private Drawable getUnactivatedDrawable() {
		if (unactivatedDrawable == null) {
			unactivatedDrawable = getResources().getDrawable(R.drawable.switch_off_big);
		}
		return unactivatedDrawable;
	}
	private void updateActivated(boolean activated) {
		ImageView view = (ImageView)findViewById(R.id.destructionActivated);
		if (activated) {
			view.setImageDrawable(getActivatedDrawable());
		} else {
			view.setImageDrawable(getUnactivatedDrawable());
		}
	}
	
	private Drawable getCheckDrawable() {
		if (checkDrawable == null) {
			checkDrawable = getResources().getDrawable(R.drawable.checkmark);
		}
		return checkDrawable;
	}
	private void updateMethod(int method) {
		for (int i=0; i<method_viewid.length; i++) {
			TextView view = (TextView)findViewById(method_viewid[i]);
			if (method == i) {
				view.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckDrawable(), null);
			} else {
				view.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
			}
		}
	}
	
	//----------//----------//----------//----------//----------//
	
}
