package com.protectstar.timelock.pro.android;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.protectstar.safeuhr.android.R;

public class LockActivity extends TimeLockActivity {
	
	//----------//----------//----------//----------//----------//
		
	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lock);
		setupCheck();
	}

	@Override
	protected void onResume() {
		super.onResume();
		resetCheck();
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	public static final int[] option_ids = { R.id.lockOption1,
			R.id.lockOption2, R.id.lockOption3,
			R.id.lockOption4, R.id.lockOption5 };
	
	private void setupCheck() {
		for (int i = 0; i < option_ids.length; i++) {
			View view = findViewById(option_ids[i]);
			view.setTag(i);
			view.setOnClickListener(onLineClickListener);
		}
	}
	
	private void resetCheck() {
		for (int i = 0; i < option_ids.length; i++) {
			TextView view = (TextView) findViewById(option_ids[i]);
			if (i == getLockIndex()) {
				view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.radio_on, 0);
			} else {
				view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.radio_off, 0);
			}
		}
	}

	final View.OnClickListener onLineClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			int index = getViewTag(v);
			setLockIndex(index);
			resetCheck();
		}
	};
	
	private int getLockIndex() {
		return ((TimeLockApplication)getApplication()).getLockOptionIndex();
	}
	
	private void setLockIndex(int selectedIndex) {
		((TimeLockApplication)getApplication()).setLockOptionIndex(selectedIndex);
	}
	
	//----------//----------//----------//----------//----------//
	
}
