package com.protectstar.timelock.pro.android.data;

import java.util.ArrayList;

import meobu.android.base.MeobuSQLiteDataSource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

public class TimeLockContentsDataSource implements MeobuSQLiteDataSource {

	// ----------//----------//----------//----------//----------//

	SQLiteOpenHelper helper;
	public TimeLockContentsDataSource(Context context) {
		helper = new TimeLockSQLiteOpenHelper(context);
	}
	
	@Override
	public void open() {
		//Log.d("meobudebug", "did open!!!");
	}
	
	@Override
	public void close() {
		helper.close();
	}

	// ----------//----------//----------//----------//----------//

	public ArrayList<TimeLockContent> retrieve(int from, int size) {
		ArrayList<TimeLockContent> contents = new ArrayList<TimeLockContent>();
		SQLiteDatabase db = helper.getReadableDatabase();

		//SQLiteOpenHelper helper = new TimeLockSQLiteOpenHelper(context);
		//SQLiteDatabase db = helper.getReadableDatabase();
		String query = SQLiteQueryBuilder.buildQueryString(false,
				TimeLockSQLiteOpenHelper.TABLE_CONTENTS,
				TimeLockContent.CONTENT_KEYS, null, null, null,
				TimeLockContent.CONTENT_PRIMARY + " desc", from + ", " + size);
		//Log.d("meobudebug", "retrieve query:\n" + query);
		Cursor cursor = db.rawQuery(query, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			TimeLockContent content = TimeLockContent.fromCursor(cursor);
			contents.add(content);
			cursor.moveToNext();
		}
		//Log.d("meobudebug", "retrieve count: " + contents.size());
		db.close();
		return contents;
	}
	
	public int put(TimeLockContent content) {
		SQLiteDatabase db = helper.getWritableDatabase();
		
		ContentValues values = content.toContentValues();
		long id = db.insert(TimeLockSQLiteOpenHelper.TABLE_CONTENTS, null, values);
		if (id >= 0) {
			content.setId((int)id);
		} else {
			Log.e("meobuerror", "###insert record FAILED, don't know why");
		}
		db.close();
		return (int)id;
	}
	
	public int remove(TimeLockContent content) {
		SQLiteDatabase db = helper.getWritableDatabase();
		
		String whereClause = TimeLockContent.CONTENT_PRIMARY + "=" + content.getId();
		String[] whereArgs = null;
		int result = db.delete(TimeLockSQLiteOpenHelper.TABLE_CONTENTS, whereClause, whereArgs);
		if (result <= 0) {
			Log.e("meobuerror", "###delete record FAILED, don't know why? id:" + content.getId());
			//Log.d("meobudebug", "id = " + content.getId());
		}
		db.close();
		return result;
	}
	
	public int removeAll() {
		SQLiteDatabase db = helper.getWritableDatabase();
		int count = db.delete(TimeLockSQLiteOpenHelper.TABLE_CONTENTS, null, null);
		db.close();
		return count;
	}
	
	public int remove(ArrayList<TimeLockContent> contents) {
		SQLiteDatabase db = helper.getWritableDatabase();
		int result = 0;
		for (TimeLockContent content : contents) {
			String whereClause = TimeLockContent.CONTENT_PRIMARY + "=" + content.getId();
			String[] whereArgs = null;
			result += db.delete(TimeLockSQLiteOpenHelper.TABLE_CONTENTS, whereClause, whereArgs);
			if (result <= 0) {
				Log.e("meobuerror", "###delete record FAILED, don't know why? id:" + content.getId());
			}
		}
		db.close();
		return result;
	}
	public SQLiteDatabase doOpen() {
		return helper.getWritableDatabase();
	}
	public void doClose(SQLiteDatabase db) {
		db.close();
	}
	public int doRemove(int id, SQLiteDatabase db) {
		String whereClause = TimeLockContent.CONTENT_PRIMARY + "=" + id;
		String[] whereArgs = null;
		int result = db.delete(TimeLockSQLiteOpenHelper.TABLE_CONTENTS, whereClause, whereArgs);
		if (result <= 0) {
			Log.e("meobuerror", "###delete record FAILED, don't know why? id:" + id);
		}
		return result;
	}

	// ----------//----------//----------//----------//----------//

}