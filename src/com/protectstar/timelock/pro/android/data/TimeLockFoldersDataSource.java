package com.protectstar.timelock.pro.android.data;

import java.util.ArrayList;

import meobu.android.base.MeobuSQLiteDataSource;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

public class TimeLockFoldersDataSource implements MeobuSQLiteDataSource {

	// ----------//----------//----------//----------//----------//

	SQLiteOpenHelper helper;
	public TimeLockFoldersDataSource(Context context) {
		helper = new TimeLockSQLiteOpenHelper(context);
	}
	
	@Override
	public void open() {
		//Log.d("meobudebug", "did open!!!");
	}
	
	@Override
	public void close() {
		helper.close();
	}

	// ----------//----------//----------//----------//----------//

	public ArrayList<TimeLockFolder> retrieve(int from, int size) {
		ArrayList<TimeLockFolder> folders = new ArrayList<TimeLockFolder>();
		SQLiteDatabase db = helper.getReadableDatabase();

		//SQLiteOpenHelper helper = new TimeLockSQLiteOpenHelper(context);
		//SQLiteDatabase db = helper.getReadableDatabase();
		String query = SQLiteQueryBuilder.buildQueryString(false,
				TimeLockSQLiteOpenHelper.TABLE_FOLDERS,
				TimeLockFolder.FOLDER_KEYS, null, null, null,
				TimeLockFolder.FOLDER_PRIMARY + " asc", from + ", " + size);
		//Log.d("meobudebug", "retrieve query:\n" + query);
		Cursor cursor = db.rawQuery(query, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			TimeLockFolder folder = TimeLockFolder.fromCursor(cursor);
			folders.add(folder);
			cursor.moveToNext();
		}
		//Log.d("meobudebug", "retrieve count: " + folders.size());
		db.close();
		return folders;
	}
	
	public int put(TimeLockFolder folder) {
		SQLiteDatabase db = helper.getWritableDatabase();
		
		ContentValues values = folder.toContentValues();
		long id = db.insert(TimeLockSQLiteOpenHelper.TABLE_FOLDERS, null, values);
		if (id >= 0) {
			folder.setId((int)id);
		} else {
			Log.e("meobuerror", "###insert record FAILED, don't know why");
		}
		db.close();
		return (int)id;
	}
	
	public int update(String name, int id) {
		SQLiteDatabase db = helper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(TimeLockSQLiteOpenHelper.COLUMN_NAME, name);
		int count = db.update(TimeLockSQLiteOpenHelper.TABLE_FOLDERS, values, TimeLockSQLiteOpenHelper.COLUMN_ID + "=" + id, null);
		
		db.close();
		return count;
	}
	
	public int remove(TimeLockFolder folder) {
		SQLiteDatabase db = helper.getWritableDatabase();
		
		String whereClause = TimeLockFolder.FOLDER_PRIMARY + "=" + folder.getId();
		String[] whereArgs = null;
		int result = db.delete(TimeLockSQLiteOpenHelper.TABLE_FOLDERS, whereClause, whereArgs);
		if (result <= 0) {
			Log.e("meobuerror", "###delete record FAILED, don't know why");
			//Log.d("meobudebug", "id = " + folder.getId());
		}
		db.close();
		return result;
	}
	
	public int remove(ArrayList<TimeLockFolder> folders) {
		SQLiteDatabase db = helper.getWritableDatabase();
		int result = 0;
		for (TimeLockFolder folder : folders) {
			String whereClause = TimeLockFolder.FOLDER_PRIMARY + "=" + folder.getId();
			String[] whereArgs = null;
			result += db.delete(TimeLockSQLiteOpenHelper.TABLE_FOLDERS, whereClause, whereArgs);
			if (result <= 0) {
				Log.e("meobuerror", "###delete record FAILED, don't know why? id " + folder.getId());
			}
		}
		db.close();
		return result;
	}
	
	public int removeAll() {
		SQLiteDatabase db = helper.getWritableDatabase();
		int count = db.delete(TimeLockSQLiteOpenHelper.TABLE_FOLDERS, null, null);
		db.close();
		return count;
	}

	// ----------//----------//----------//----------//----------//

}