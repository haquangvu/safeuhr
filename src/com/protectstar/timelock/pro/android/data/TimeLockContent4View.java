package com.protectstar.timelock.pro.android.data;

import com.protectstar.timelock.pro.android.TimeLockApplication.Data;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData;

public class TimeLockContent4View implements ItemViewData {

	public Data content;
	public TimeLockContent4View(Data content) {
		this.content = content;
	}
	@Override
	public int getDataId() {
		return content.getDataId();
	}
	@Override
	public int getViewType() {
		return content.getDataType();
	}
	@Override
	public String getViewThumbnailPath() {
		return content.getDataThumbnail();
	}
	@Override
	public long getViewDate() {
		return content.getDataTime();
	}
}
