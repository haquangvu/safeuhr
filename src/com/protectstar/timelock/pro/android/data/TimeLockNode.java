package com.protectstar.timelock.pro.android.data;

import java.util.ArrayList;
import java.util.Hashtable;

import com.protectstar.timelock.pro.android.TimeLockApplication.Data;
import com.protectstar.timelock.pro.android.TimeLockApplication.ViewData;

public class TimeLockNode extends TimeLockFolder {
	
	public long updatedDate;
	
	private Hashtable<Integer, Data> hashcontents;
	public Hashtable<Integer, Data> getHashContents() {
		if (hashcontents == null) {
			hashcontents = new Hashtable<Integer, Data>();
		}
		return hashcontents;
	}
	private ArrayList<Data> contents;
	public ArrayList<Data> getContents() {
		if (contents == null) {
			contents = new ArrayList<Data>();
		}
		return contents;
	}
	private ArrayList<ViewData> listViewData;
	public ArrayList<ViewData> getListViewData() {
		return listViewData;
	}
	public void setListViewData(ArrayList<ViewData> listViewData) {
		this.listViewData = listViewData;
	}
	private ArrayList<TimeLockFolder> folders;
	public ArrayList<TimeLockFolder> getFolders() {
		if (folders == null) {
			folders = new ArrayList<TimeLockFolder>();
		}
		return folders;
	}
	
	public static TimeLockNode copy(TimeLockFolder folder) {
		TimeLockNode node = new TimeLockNode();
		node.setId(folder.getId());
		node.setName(folder.getName());
		node.setFolder(folder.getFolder());
		return node;
	}
	
}
