package com.protectstar.timelock.pro.android.data;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import meobu.android.base.MeobuEncryption;
import meobu.android.base.MeobuFileTools;

import com.protectstar.timelock.pro.android.TimeLockApplication;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData;
import com.protectstar.timelock.pro.android.data.service.BackgroundActionStatusUpdate;

import common.view.OneByOnePercentAsyncTask;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;

public class TimeLockMediaFileLayer {
	
	public static File getDataFolder(Context context, boolean external) {
		if (!external) {
			return context.getApplicationContext().getFilesDir();
		}
		return context.getApplicationContext().getExternalFilesDir(null);
	}

	public static int THUMBNAIL_SIZE = 100;
	
	private static Bitmap lessResolutionNew(String filePath, int width, int height, BackgroundActionStatusUpdate statis, PrintStream log) {
		int reqHeight=width;
		int reqWidth=height;
		if (log != null) try { log.println("[42111]"); } catch (Exception e) { }
		BitmapFactory.Options options = new BitmapFactory.Options();   
        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
		if (log != null) try { log.println("[42112]"); } catch (Exception e) { }
        BitmapFactory.decodeFile(filePath, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;        
		if (log != null) try { log.println("[42113]"); } catch (Exception e) { }
        return BitmapFactory.decodeFile(filePath, options); 
	}
	private static Bitmap lessResolution (String filePath, int width, int height, OneByOnePercentAsyncTask<?, ?> task) {
		int reqHeight=width;
		int reqWidth=height;
		task.doPublishLog(42111);
		BitmapFactory.Options options = new BitmapFactory.Options();   
        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
		task.doPublishLog(42112);
        BitmapFactory.decodeFile(filePath, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;        
		task.doPublishLog(42113);
        return BitmapFactory.decodeFile(filePath, options); 
    }
//	private static Bitmap lessResolution(InputStream fileDescriptor, int width, int height) {
//		int reqHeight=width;
//		int reqWidth=height;
//		BitmapFactory.Options options = new BitmapFactory.Options();   
//        // First decode with inJustDecodeBounds=true to check dimensions
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeStream(fileDescriptor, null, options);
//        // Calculate inSampleSize
//        try {
//        	fileDescriptor.reset();
//        } catch (Exception e) { }
//        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
//        // Decode bitmap with inSampleSize set
//        options.inJustDecodeBounds = false;        
//        return BitmapFactory.decodeStream(fileDescriptor, null, options); 
//    }
	private static Bitmap lessResolutionNew(FileDescriptor fileDescriptor, int width, int height, BackgroundActionStatusUpdate status, PrintStream log) {
		int reqHeight=width;
		int reqWidth=height;
		if (log != null) try { log.println("[42111]"); } catch (Exception e) { }
		BitmapFactory.Options options = new BitmapFactory.Options();   
        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
		if (log != null) try { log.println("[42112]"); } catch (Exception e) { }
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;        
		if (log != null) try { log.println("[42113]"); } catch (Exception e) { }
        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options); 
	}
	private static Bitmap lessResolution(FileDescriptor fileDescriptor, int width, int height, OneByOnePercentAsyncTask<?, ?> task) {
		int reqHeight=width;
		int reqWidth=height;
		task.doPublishLog(42111);
		BitmapFactory.Options options = new BitmapFactory.Options();   
        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
		task.doPublishLog(42112);
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;        
		task.doPublishLog(4213);
        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options); 
    }
	private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	    if (height > reqHeight && width > reqWidth) {
	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);
	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	    return inSampleSize;
    }
	private static Bitmap createThumbnailNew(FileDescriptor path, int type, BackgroundActionStatusUpdate status, PrintStream log) {
		Bitmap thumb = null;
		if (type == ItemViewData.ItemViewDataType.Photo.ordinal()) {
			if (log != null) try { log.println("[4211]"); } catch (Exception e) { }
			Bitmap source = lessResolutionNew(path, THUMBNAIL_SIZE, THUMBNAIL_SIZE, status, log);
			if (log != null) try { log.println("[4212]"); } catch (Exception e) { }
			thumb = createThumbnail(source);
		} else {
			MediaMetadataRetriever retriever = new MediaMetadataRetriever();
			retriever.setDataSource(path);
			Bitmap thumbtemp = retriever.getFrameAtTime(-1);
			if (THUMBNAIL_SIZE <= 384) {
				thumb = ThumbnailUtils.extractThumbnail(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
			} else {
				//scale to square
				thumb = Bitmap.createScaledBitmap(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, true);
				thumbtemp.recycle();
			}
		}
		return thumb;
	}
	private static Bitmap createThumbnail(FileDescriptor path, int type, OneByOnePercentAsyncTask<?, ?> task) {
		Bitmap thumb = null;
		if (type == ItemViewData.ItemViewDataType.Photo.ordinal()) {
			task.doPublishLog(4211);
			Bitmap source = lessResolution(path, THUMBNAIL_SIZE, THUMBNAIL_SIZE, task);
			task.doPublishLog(4212);
			thumb = createThumbnail(source);
		} else {
//			if (THUMBNAIL_SIZE <= 96) {
//				thumb = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
//			} else {
				//Bitmap thumbtemp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
				MediaMetadataRetriever retriever = new MediaMetadataRetriever();
				retriever.setDataSource(path);
				Bitmap thumbtemp = retriever.getFrameAtTime(-1);
				if (THUMBNAIL_SIZE <= 384) {
					thumb = ThumbnailUtils.extractThumbnail(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
				} else {
					//scale to square
					thumb = Bitmap.createScaledBitmap(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, true);
					thumbtemp.recycle();
				}
//			}
		}
		return thumb;
	}
//	private static Bitmap createThumbnail(InputStream path, int type) {
//		Bitmap thumb = null;
//		if (type == ItemViewData.ItemViewDataType.Photo.ordinal()) {
//			Bitmap source = lessResolution(path, THUMBNAIL_SIZE, THUMBNAIL_SIZE);
//			thumb = createThumbnail(source);
//		} else {
////			if (THUMBNAIL_SIZE <= 96) {
////				thumb = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
////			} else {
//				//Bitmap thumbtemp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
//				MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//				retriever.setDataSource(path);
//				Bitmap thumbtemp = retriever.getFrameAtTime(-1);
//				if (THUMBNAIL_SIZE <= 384) {
//					thumb = ThumbnailUtils.extractThumbnail(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
//				} else {
//					//scale to square
//					thumb = Bitmap.createScaledBitmap(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, true);
//					thumbtemp.recycle();
//				}
////			}
//		}
//		return thumb;
//	}
	private static Bitmap createThumbnailNew(String path, int type, BackgroundActionStatusUpdate status, PrintStream log) {
		Bitmap thumb = null;
		if (type == ItemViewData.ItemViewDataType.Photo.ordinal()) {
			if (log != null) try { log.println("[4211]"); } catch (Exception e) { }
			Bitmap source = lessResolutionNew(path, THUMBNAIL_SIZE, THUMBNAIL_SIZE, status, log);
			if (log != null) try { log.println("[4212]"); } catch (Exception e) { }
			thumb = createThumbnail(source);
		} else {
			if (THUMBNAIL_SIZE <= 96) {
				thumb = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
			} else {
				Bitmap thumbtemp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
				if (THUMBNAIL_SIZE <= 384) {
					thumb = ThumbnailUtils.extractThumbnail(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
				} else {
					//scale to square
					thumb = Bitmap.createScaledBitmap(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, true);
					thumbtemp.recycle();
				}
			}
		}
		return thumb;
	}
	private static Bitmap createThumbnail(String path, int type, OneByOnePercentAsyncTask<?, ?> task) {
		Bitmap thumb = null;
		if (type == ItemViewData.ItemViewDataType.Photo.ordinal()) {
			task.doPublishLog(4211);
			Bitmap source = lessResolution(path, THUMBNAIL_SIZE, THUMBNAIL_SIZE, task);
			task.doPublishLog(4212);
			thumb = createThumbnail(source);
//			if (source.getWidth() >= THUMBNAIL_SIZE && source.getHeight() >= THUMBNAIL_SIZE) {
//				thumb = ThumbnailUtils.extractThumbnail(source, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
//			} else {
//				Bitmap thumbtemp = ThumbnailUtils.extractThumbnail(source, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
//				thumb = Bitmap.createScaledBitmap(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, true);
//				thumbtemp.recycle();
//			}
		} else {
			if (THUMBNAIL_SIZE <= 96) {
				thumb = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
			} else {
				Bitmap thumbtemp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
				if (THUMBNAIL_SIZE <= 384) {
					thumb = ThumbnailUtils.extractThumbnail(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
				} else {
					//scale to square
					thumb = Bitmap.createScaledBitmap(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, true);
					thumbtemp.recycle();
				}
			}
		}
		return thumb;
	}
	private static Bitmap createThumbnail(Bitmap source) {
		Bitmap thumb = null;
		if (source.getWidth() >= THUMBNAIL_SIZE && source.getHeight() >= THUMBNAIL_SIZE) {
			thumb = ThumbnailUtils.extractThumbnail(source, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
		} else {
			Bitmap thumbtemp = ThumbnailUtils.extractThumbnail(source, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
			thumb = Bitmap.createScaledBitmap(thumbtemp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, true);
			thumbtemp.recycle();
		}
		return thumb;
	}
	private static void bitmap2file(Bitmap bitmap, String filePath) {
	    try {
	        File file = new File(filePath);
	        FileOutputStream fOut = new FileOutputStream(file);
	        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
	        fOut.flush();
	        fOut.close();
        } catch (Exception e) { }
	}
	
	public static String[] generateSecretPaths(Context context, boolean external) {
		String[] paths = new String[2];
		String name = Long.toString(System.currentTimeMillis());
		String nameThumb = name + "3";
		paths[0] = new File(getDataFolder(context, external), name).getAbsolutePath();
		paths[1] = new File(context.getApplicationContext().getFilesDir(), nameThumb).getAbsolutePath();
		return paths;
	}

	/**
	 * this function is called when we received a media
	 * call this one the duplicate, thumbnail the media
	 * 
	 * @param media the media uri from folder library
	 * @param type type of media, photo, or video, only 1 of 2 at this time
	 * @return a list of string, included path of full media file and path of thumbnail file
	 */
//	public static String[] storeMedia(String source, int type, String[] paths) {
//		String[] result = new String[2];
//		result[1] = paths[1];
//		//copy full media to secret
//		String fullMediaNameInSecretFolderPath = paths[0];
//		result[0] = fullMediaNameInSecretFolderPath;
//		copyFile(new File(source), new File(fullMediaNameInSecretFolderPath));
//		//create thumbnail and store to secretFolderPath
//		String thumbnailMediaNameInSecretFolderPath = paths[1];
//		result[1] = thumbnailMediaNameInSecretFolderPath;
//		createNstoreThumbnail(source, type, thumbnailMediaNameInSecretFolderPath);
//		//return full media path, thumbnail media path
//		return result;
//	}
	public static void storeMedia1New_Encrypt(File source, int type, String[] paths, String passcode, BackgroundActionStatusUpdate status, PrintStream log) {
		//copy full media to secret
		String fullMediaNameInSecretFolderPath = paths[0];
		if (log != null) try { log.println("[41]"); } catch (Exception e) { }
		MeobuFileTools.copyEncryptNew(source, new File(fullMediaNameInSecretFolderPath), passcode, status, log);
	}
	public static void storeMedia1New_Thumbnail(File source, int type, String[] paths, BackgroundActionStatusUpdate status, PrintStream log) {
		//create thumbnail and store to secretFolderPath
		String thumbnailMediaNameInSecretFolderPath = paths[1];
		if (log != null) try { log.println("[42]"); } catch (Exception e) { }
		createNstoreThumbnailNew(source.getPath(), type, thumbnailMediaNameInSecretFolderPath, status, log);
	}
	public static String[] storeMedia1(File source, int type, String[] paths, OneByOnePercentAsyncTask<?, ?> task) {
		String[] result = new String[2];
		result[1] = paths[1];
		//copy full media to secret
		String fullMediaNameInSecretFolderPath = paths[0];
		result[0] = fullMediaNameInSecretFolderPath;
		//copyFile1(source, new File(fullMediaNameInSecretFolderPath));
		task.doPublishLog(41);
		MeobuFileTools.copyEncrypt(source, new File(fullMediaNameInSecretFolderPath), TimeLockApplication.cachePasscode(), task);
		//create thumbnail and store to secretFolderPath
		String thumbnailMediaNameInSecretFolderPath = paths[1];
		result[1] = thumbnailMediaNameInSecretFolderPath;
		task.doPublishLog(42);
		createNstoreThumbnail(source.getPath(), type, thumbnailMediaNameInSecretFolderPath, task);
		//return full media path, thumbnail media path
		return result;
	}
//	public static String[] storeMedia(InputStream source, int type, String[] paths) {
//		String[] result = new String[2];
//		result[1] = paths[1];
//		//copy full media to secret
//		String fullMediaNameInSecretFolderPath = paths[0];
//		result[0] = fullMediaNameInSecretFolderPath;
//		copyFile1(source, new File(fullMediaNameInSecretFolderPath));
//		//create thumbnail and store to secretFolderPath
//		String thumbnailMediaNameInSecretFolderPath = paths[1];
//		result[1] = thumbnailMediaNameInSecretFolderPath;
//		createNstoreThumbnail(fullMediaNameInSecretFolderPath, type, thumbnailMediaNameInSecretFolderPath);
//		//return full media path, thumbnail media path
//		return result;
//	}
	public static void storeMedia2New_Encrypt(FileDescriptor sourceFD, FileInputStream sourceStream,
			int type, String[] paths, String passcode, BackgroundActionStatusUpdate status, PrintStream log) {
		//copy full media to secret
		String fullMediaNameInSecretFolderPath = paths[0];
		if (log != null) try { log.println("[41]"); } catch (Exception e) { }
		MeobuFileTools.copyEncryptNew(sourceStream, new File(fullMediaNameInSecretFolderPath), passcode, status, log);
		try { sourceStream.reset(); } catch (Exception e) { }
	}
	public static void storeMedia2New_Thumbnail(FileDescriptor sourceFD, FileInputStream sourceStream,
			int type, String[] paths, BackgroundActionStatusUpdate status, PrintStream log) {
		//create thumbnail and store to secretFolderPath
		String thumbnailMediaNameInSecretFolderPath = paths[1];
		if (log != null) try { log.println("[42]"); } catch (Exception e) { }
		createNstoreThumbnailNew(sourceFD, type, thumbnailMediaNameInSecretFolderPath, status, log);
	}
	
	public static String[] storeMedia2(FileDescriptor sourceFD, FileInputStream sourceStream, int type, String[] paths, OneByOnePercentAsyncTask<?, ?> task) {
		String[] result = new String[2];
		result[1] = paths[1];
		//copy full media to secret
		String fullMediaNameInSecretFolderPath = paths[0];
		result[0] = fullMediaNameInSecretFolderPath;
		//copyFile1(sourceStream, new File(fullMediaNameInSecretFolderPath));
		task.doPublishLog(41);
		MeobuFileTools.copyEncrypt(sourceStream, new File(fullMediaNameInSecretFolderPath), TimeLockApplication.cachePasscode(), task);
		try { sourceStream.reset(); } catch (Exception e) { }
		//create thumbnail and store to secretFolderPath
		String thumbnailMediaNameInSecretFolderPath = paths[1];
		result[1] = thumbnailMediaNameInSecretFolderPath;
		task.doPublishLog(42);
		createNstoreThumbnail(sourceFD, type, thumbnailMediaNameInSecretFolderPath, task);
		//return full media path, thumbnail media path
		return result;
	}
//	public static String[] storeMedia1(FileDescriptor source, int type, String[] paths) {
//		String[] result = new String[2];
//		result[1] = paths[1];
//		//copy full media to secret
//		String fullMediaNameInSecretFolderPath = paths[0];
//		result[0] = fullMediaNameInSecretFolderPath;
//		//test
//		FileInputStream fis = new FileInputStream(source);
//		copyFile1(fis, new File(fullMediaNameInSecretFolderPath));
//		try { fis.reset(); } catch (Exception e) { }
//		//copyFile1(source, new File(fullMediaNameInSecretFolderPath));
////		copyFile(source, new File(fullMediaNameInSecretFolderPath));
//		//create thumbnail and store to secretFolderPath
//		String thumbnailMediaNameInSecretFolderPath = paths[1];
//		result[1] = thumbnailMediaNameInSecretFolderPath;
//		createNstoreThumbnail(source, type, thumbnailMediaNameInSecretFolderPath);
//		//return full media path, thumbnail media path
//		
//		try { fis.close(); } catch (Exception e) { }
//		return result;
//	}
//	public static String[] storeMedia(Bitmap source, int type, String[] paths) {
//		String[] result = new String[2];
//		result[1] = paths[1];
//		//copy full media to secret
//		String fullMediaNameInSecretFolderPath = paths[0];
//		result[0] = fullMediaNameInSecretFolderPath;
//		//copyFile(source, new File(fullMediaNameInSecretFolderPath));
//		bitmap2file(source, fullMediaNameInSecretFolderPath);
//		//create thumbnail and store to secretFolderPath
//		String thumbnailMediaNameInSecretFolderPath = paths[1];
//		result[1] = thumbnailMediaNameInSecretFolderPath;
//		createNstoreThumbnail(source, thumbnailMediaNameInSecretFolderPath);
//		//return full media path, thumbnail media path
//		return result;
//	}
//	private static void createNstoreThumbnail(InputStream source, int type, String path) {
//		Bitmap thumb = createThumbnail(source, type);
//		bitmap2file(thumb, path);
//		thumb.recycle();
//	}
	private static void createNstoreThumbnailNew(FileDescriptor source, int type, String path, BackgroundActionStatusUpdate status, PrintStream log) {
		if (log != null) try { log.println("[421]"); } catch (Exception e) { }
		Bitmap thumb = createThumbnailNew(source, type, status, log);
		if (log != null) try { log.println("[422]"); } catch (Exception e) { }
		bitmap2file(thumb, path);
		if (log != null) try { log.println("[423]"); } catch (Exception e) { }
		thumb.recycle();
	}
	private static void createNstoreThumbnail(FileDescriptor source, int type, String path, OneByOnePercentAsyncTask<?, ?> task) {
		task.doPublishLog(421);
		Bitmap thumb = createThumbnail(source, type, task);
		task.doPublishLog(422);
		bitmap2file(thumb, path);
		task.doPublishLog(423);
		thumb.recycle();
	}
	private static void createNstoreThumbnailNew(String source, int type, String path, BackgroundActionStatusUpdate status, PrintStream log) {
		if (log != null) try { log.println("[421]"); } catch (Exception e) { }
		Bitmap thumb = createThumbnailNew(source, type, status, log);
		if (log != null) try { log.println("[422]"); } catch (Exception e) { }
		bitmap2file(thumb, path);
		if (log != null) try { log.println("[423]"); } catch (Exception e) { }
		thumb.recycle();
	}
	private static void createNstoreThumbnail(String source, int type, String path, OneByOnePercentAsyncTask<?, ?> task) {
		task.doPublishLog(421);
		Bitmap thumb = createThumbnail(source, type, task);
		task.doPublishLog(422);
		bitmap2file(thumb, path);
		task.doPublishLog(423);
		thumb.recycle();
	}
//	private static void createNstoreThumbnail(Bitmap source, String path) {
//		Bitmap thumb = createThumbnail(source);
//		bitmap2file(thumb, path);
//		thumb.recycle();
//	}
	public static void copeFile2(File from, String password, File to) {
		try {
			copyFile2(new FileInputStream(from), password, to);
		} catch (Exception e) { }
	}
	private static void copyFile2(InputStream from, String password, File to) {
		if (from == null) return;
		InputStream cis = null;
		OutputStream fos = null;
		OutputStream cos = null;
		OutputStream bos = null;
		try {
			cis = MeobuEncryption.inStreamDecryptedAES(cis, password);
			fos = new FileOutputStream(to);
			cos = MeobuEncryption.outStreamEncryptedAES(fos, TimeLockApplication.cachePasscode());
			bos = new BufferedOutputStream(cos);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int len = 0;
			while ((len = cis.read(buffer)) != -1) {
			    bos.write(buffer, 0, len);
			}
			bos.flush();
		} catch (Exception e) {
			Log.e("meobuerror", "TimeLockMediaFileLayer.copyFile1(InputStream, File)###Copy file failed.");
		} finally {
			if (fos != null) {
				try {fos.close();} catch (Exception e) {}
			}
			if (cos != null) {
				try {cos.close();} catch (Exception e) {}
			}
			if (bos != null) {
				try {bos.close();} catch (Exception e) {}
			}
			if (from != null) {
				try {from.close();} catch (Exception e) {}
			}
			if (cis != null) {
				try {cis.close();} catch (Exception e) {}
			}
		}
	}
//	private static void copyFile1(InputStream from, File to) {
//		if (from == null) return;
//		OutputStream fos = null;
//		OutputStream cos = null;
//		try {
//			fos = new FileOutputStream(to);
//			cos = MeobuEncryption.outStreamEncryptedAES(fos, TimeLockApplication.cachePasscode());
//			//bos = new BufferedOutputStream(cos);
//			int bufferSize = 1024;
//			byte[] buffer = new byte[bufferSize];
//			int len = 0;
//			while ((len = from.read(buffer)) != -1) {
//			    cos.write(buffer, 0, len);
//			}
//			cos.flush();
//		} catch (Exception e) {
//			Log.e("meobuerror", "TimeLockMediaFileLayer.copyFile1(InputStream, File)###Copy file failed. " + e.getMessage());
//			e.printStackTrace();
//		} finally {
//			if (cos != null) {
//				try {cos.close();} catch (Exception e) {}
//			}
//			if (fos != null) {
//				try {fos.close();} catch (Exception e) {}
//			}
//		}
//	}
//	private static void copyFile1(File from, File to) {
//		FileInputStream fis = null;
//		try {
//			fis = new FileInputStream(from);
//			copyFile1(fis, to);
//		} catch (Exception e) {
//			Log.e("meobuerror", "TimeLockMediaFileLayer.copyFile1(File, File)###Copy file failed. " + e.getMessage());
//			e.printStackTrace();
//		} finally {
//			if (fis != null) {
//				try { fis.close(); } catch (Exception e) { }
//			}
//		}
//	}
	
	/**
	 * this function is called when we cannot insert record to database
	 * call this one to delete the stored media in our secret folder
	 * 
	 * @param paths the list of secret folder path, name of full media file, name of thumbnail file (from storeMedia function)
	 */
	public static void deleteMedia(String[] paths) {
		File fullMediaFile = new File(paths[0]);
		fullMediaFile.delete();
		File thumbnailFile = new File(paths[1]);
		thumbnailFile.delete();
	}
//	private static void copyFile1(InputStream from, File to) {
//	if (from == null) return;
//	OutputStream fos = null;
//	OutputStream cos = null;
//	try {
//		fos = new FileOutputStream(to);
//		cos = MeobuEncryption.outStreamEncryptedAES(fos, TimeLockApplication.cachePasscode());
//		//bos = new BufferedOutputStream(cos);
//		int bufferSize = 1024;
//		byte[] buffer = new byte[bufferSize];
//		int len = 0;
//		while ((len = from.read(buffer)) != -1) {
//		    cos.write(buffer, 0, len);
//		}
//		cos.flush();
//	} catch (Exception e) {
//		Log.e("meobuerror", "TimeLockMediaFileLayer.copyFile1(InputStream, File)###Copy file failed. " + e.getMessage());
//		e.printStackTrace();
//	} finally {
//		if (cos != null) {
//			try {cos.close();} catch (Exception e) {}
//		}
//		if (fos != null) {
//			try {fos.close();} catch (Exception e) {}
//		}
//	}
//}
	
}
