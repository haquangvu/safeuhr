package com.protectstar.timelock.pro.android.data.service;

import org.json.JSONObject;

import android.content.Intent;

public class BackgroundActionChangeCodeConvert extends BackgroundActionImpl {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	//data & only ONE set function
	protected final int action = BackgroundService.ACTION_CHANGECODE;
	protected String oldPasscode, newPasscode, logPath;
	public int errorCount, hour, minute, second;
	public static final String OLD_PASSCODE_KEY = "old_passcode";
	public static final String NEW_PASSCODE_KEY = "new_passcode";
	public static final String LOGPATH_KEY = "log_path";
	public static final String ERRORCOUNT_KEY = "error_count";
	public static final String HOUR_KEY = "hour_hand";
	public static final String MINUTE_KEY = "minute_hand";
	public static final String SECOND_KEY = "second_hand";
	
	public void set(String oldPasscode, String newPasscode, int hour, int minute, int second) {
		this.oldPasscode = oldPasscode;
		this.newPasscode = newPasscode;
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	public void put2Intent(Intent intent) {
		//don't need status
		intent.putExtra(BackgroundService.ACTION_KEY, action);
		intent.putExtra(NEW_PASSCODE_KEY, newPasscode);
		intent.putExtra(OLD_PASSCODE_KEY, oldPasscode);
		intent.putExtra(LOGPATH_KEY, logPath);
		intent.putExtra(ERRORCOUNT_KEY, errorCount);
		intent.putExtra(HOUR_KEY, hour);
		intent.putExtra(MINUTE_KEY, minute);
		intent.putExtra(SECOND_KEY, second);
	}

	@Override
	public void set(Intent intent) {
		super.set(intent);
		//set status().listTotal
		newPasscode = intent.getStringExtra(NEW_PASSCODE_KEY);
		if (newPasscode == null) newPasscode = "";
		oldPasscode = intent.getStringExtra(OLD_PASSCODE_KEY);
		if (oldPasscode == null) oldPasscode = "";
		logPath = intent.getStringExtra(LOGPATH_KEY);
		if (logPath == null) logPath = "";
		errorCount = intent.getIntExtra(ERRORCOUNT_KEY, 0);
		hour = intent.getIntExtra(HOUR_KEY, 0);
		minute = intent.getIntExtra(MINUTE_KEY, 0);
		second = intent.getIntExtra(SECOND_KEY, 0);
		status().listTotal = -1;
	}

	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	protected void set2JSON(JSONObject json) {
		//status is processed, just ignore it
		try { json.put(BackgroundService.ACTION_KEY, action); } catch (Exception e) { }
		try { json.put(NEW_PASSCODE_KEY, newPasscode); } catch (Exception e) { }
		try { json.put(OLD_PASSCODE_KEY, oldPasscode); } catch (Exception e) { }
		try { json.put(LOGPATH_KEY, logPath); } catch (Exception e) { }
		try { json.put(ERRORCOUNT_KEY, errorCount); } catch (Exception e) { }
		try { json.put(HOUR_KEY, hour); } catch (Exception e) { }
		try { json.put(MINUTE_KEY, minute); } catch (Exception e) { }
		try { json.put(SECOND_KEY, second); } catch (Exception e) { }
	}
	
	@Override
	public void set(JSONObject json) {
		super.set(json);
		//set status().listTotal
		try { newPasscode = json.getString(NEW_PASSCODE_KEY); } catch (Exception e) { }
		if (newPasscode == null) newPasscode = "";
		try { oldPasscode = json.getString(OLD_PASSCODE_KEY); } catch (Exception e) { }
		if (oldPasscode == null) oldPasscode = "";
		try { logPath = json.getString(LOGPATH_KEY); } catch (Exception e) { }
		if (logPath == null) logPath = "";
		try { errorCount = json.getInt(ERRORCOUNT_KEY); } catch (Exception e) { }
		try { hour = json.getInt(HOUR_KEY); } catch (Exception e) { }
		try { minute = json.getInt(MINUTE_KEY); } catch (Exception e) { }
		try { second = json.getInt(SECOND_KEY); } catch (Exception e) { }
		status().listTotal = -1;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	protected void doRunOne(int index) {
		// TODO Auto-generated method stub
		
	}

}
