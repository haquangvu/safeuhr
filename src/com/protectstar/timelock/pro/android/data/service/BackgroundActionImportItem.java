package com.protectstar.timelock.pro.android.data.service;

import java.io.File;

import org.json.JSONObject;

import meobu.android.base.MeobuGalleryHandler.MeobuGalleryItem;

import android.net.Uri;

public class BackgroundActionImportItem {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static final String TYPE_KEY = "ii_type";
	public static final String PATH_KEY = "ii_path";
	public static final String DATE_KEY = "ii_date";
	public static final String ID_KEY = "ii_id";
	public int type;//item type: PHOTO, VIDEO //use for: import file, import uri, import MeobuGalleryItem
	public String path;//use for: import file, import uri, import MeobuGalleryItem
	public long date, id;//use for: import MeobuGalleryItem
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public int set(Object obj, int type) {
		this.type = type;
		int fileType = 0;
		if (obj instanceof File) {
			setFile((File)obj);
			fileType = BackgroundActionImport.ACTION_SUB_IMPORT_FILE;
		} else if (obj instanceof Uri) {
			setUri((Uri)obj);
			fileType = BackgroundActionImport.ACTION_SUB_IMPORT_URI;
		} else if (obj instanceof MeobuGalleryItem) {
			setGallery((MeobuGalleryItem)obj);
			fileType = BackgroundActionImport.ACTION_SUB_IMPORT_GALLERY;
		} else if (obj instanceof String) {
			path = (String)obj;
			fileType = BackgroundActionImport.ACTION_SUB_IMPORT_UNKNOWN;
		}
		return fileType;
	}
	
	public void setFile(File file) {
		this.path = file.getAbsolutePath();
	}
	
	public void setUri(Uri uri) {
		this.path = uri.toString();
	}
	
	public void setGallery(MeobuGalleryItem item) {
		this.id = item.id;
		this.path = item.path;
		this.date = item.date;
	}
	
	public void setJSONString(String input) {
		try {
			JSONObject json = new JSONObject(input);
			setJSON(json);
		} catch (Exception e) { }
	}
	
	public void setJSON(JSONObject json) {
		try { type = json.getInt(TYPE_KEY); } catch (Exception e) { }
		try { path = json.getString(PATH_KEY); } catch (Exception e) { }
		try { date = json.getLong(DATE_KEY); } catch (Exception e) { }
		try { id = json.getLong(ID_KEY); } catch (Exception e) { }
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public String toString() {
		return toJSONString();
	}
	
	public String toJSONString() {
		return toJSON().toString();
	}
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try { json.put(TYPE_KEY, type); } catch (Exception e) { }
		try { json.put(PATH_KEY, path); } catch (Exception e) { }
		try { json.put(DATE_KEY, date); } catch (Exception e) { }
		try { json.put(ID_KEY, id); } catch (Exception e) { }
		return json;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
}