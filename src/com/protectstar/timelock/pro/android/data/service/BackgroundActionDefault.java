package com.protectstar.timelock.pro.android.data.service;

import org.json.JSONObject;

import android.content.Intent;

public class BackgroundActionDefault extends BackgroundActionImpl {
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	public void put2Intent(Intent intent) {
		//don't need status
	}
	
	@Override
	public void set(Intent intent) {
		super.set(intent);
		//set status().listTotal
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	protected void set2JSON(JSONObject json) {
		//status is processed, just ignore it
	}
	
	@Override
	public void set(JSONObject json) {
		super.set(json);
		//set status().listTotal
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	protected void doRunOne(int index) {
		
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

}
