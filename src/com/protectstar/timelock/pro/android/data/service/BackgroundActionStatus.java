package com.protectstar.timelock.pro.android.data.service;

import org.json.JSONObject;

import android.content.Intent;

public class BackgroundActionStatus {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static BackgroundActionStatus from(Intent intent) {
		BackgroundActionStatus status = new BackgroundActionStatus();
		status.set(intent);
		return status;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public int listTotal = 0, listCurrent = 0, itemTotal = 0, itemCurrent = 0;
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public void fromBeginning() {
		listCurrent = 0;
		itemTotal = 0;
		itemCurrent = 0;
	}
	
	public boolean finished() {
		return listTotal >= 0 && listCurrent > listTotal;
	}
	
	public boolean over() {
		return listTotal >= 0 && listCurrent > listTotal + 1;
	}
	
	public void next() {
		listCurrent++;
		itemTotal = itemCurrent = 0;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public void set(JSONObject json) {
		try {
			if (json.has("list_current")) {
				listCurrent = json.getInt("list_current");
			}
		} catch (Exception e) {
			listCurrent = 0;
		}
		itemTotal = itemCurrent = 0;
	}
	
	public void set(Intent intent) {
		listTotal = intent.getIntExtra("listTotal", 0);
		listCurrent = intent.getIntExtra("listCurrent", 0);
		itemTotal = intent.getIntExtra("itemTotal", 0);
		itemCurrent = intent.getIntExtra("itemCurrent", 0);
	}
	
	public void get(BackgroundActionStatus status) {
		status.listTotal = listTotal;
		status.listCurrent = listCurrent;
		status.itemTotal = itemTotal;
		status.itemCurrent = itemCurrent;
	}
	
	public void get(Intent intent) {
		intent.putExtra("listTotal", listTotal);
		intent.putExtra("listCurrent", listCurrent);
		intent.putExtra("itemTotal", itemTotal);
		intent.putExtra("itemCurrent", itemCurrent);
	}
	
	public void set2JSON(JSONObject json) {
		try {
			json.put("list_current", listCurrent);
		} catch (Exception e) { }
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
}
