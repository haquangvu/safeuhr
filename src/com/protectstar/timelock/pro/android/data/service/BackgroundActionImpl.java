package com.protectstar.timelock.pro.android.data.service;

import org.json.JSONObject;

import android.content.Intent;

import com.protectstar.timelock.pro.android.data.service.BackgroundService.BackgroundActionInterface;

public abstract class BackgroundActionImpl implements BackgroundActionInterface {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	//always runnable from the beginning, won't run anymore after stop
	protected boolean running = true;
	
	@Override
	public void stop() {
		running = false;
	}

	@Override
	public boolean isFinish() {
		return status().finished();
	}
	
	@Override
	public void runOne() {
		if (running) {
			status().next();//process at index-1
			if (!status().finished()) {
				doRunOne(status().listCurrent-1);
			}
		}
	}
	
	protected abstract void doRunOne(int index);
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	//<listTotal> files to import, is working at index <listCurrent-1>, total size to process is <itemTotal> MB, processed at <itemCurrent>th MB
	//protected int listTotal, listCurrent, itemTotal, itemCurrent;
	protected BackgroundActionStatus status;
	protected final BackgroundActionStatus status() {
		if (status == null) {
			status = new BackgroundActionStatus();
		}
		return status;
	}
	
	@Override
	public void updateStatus(BackgroundActionStatus status) {
		status().get(status);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	//must update listTotal & call super
	@Override
	public void set(Intent intent) {
		status().fromBeginning();
	}
	
	//must call super
	@Override
	public void set(JSONObject json) {
		status().set(json);
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		set2JSON(json);
		status().set2JSON(json);
		return json;
	}
	
	protected abstract void set2JSON(JSONObject json);
	
	//----------//----------//----------//----------//----------//----------//----------//
	
}
