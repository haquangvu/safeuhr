package com.protectstar.timelock.pro.android.data.service;

public interface BackgroundActionStatusUpdate {
	public void update(int itemTotal, int itemCurrent);
}
