package com.protectstar.timelock.pro.android.data.service;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;

import meobu.android.base.MeobuFileTools;
import meobu.android.base.MeobuGalleryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;

import com.protectstar.timelock.pro.android.TimeLockApplication;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData.ItemViewDataType;
import com.protectstar.timelock.pro.android.data.TimeLockContentsDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockFoldersDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockMediaFileLayer;

public class BackgroundActionImport extends BackgroundActionImportConvert implements BackgroundActionStatusUpdate {
	
	final Context context;
	public BackgroundActionImport(Context context) {
		this.context = context;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	PrintStream log = null;
	protected PrintStream log() {
		if (log == null) {
			File logFile = null;
			if (logPath == null || logPath.length() <= 0) {
				logFile = MeobuFileTools.tempLog(context);
				logPath = logFile.getAbsolutePath();
			} else {
				logFile = new File(logPath);
			}
			try {
				log = new PrintStream(logFile);
			} catch (Exception e) { }
		}
		return log;
	}

	@Override
	protected void doRunOne(int index) {
		BackgroundActionImportItem item = items().get(index);
		log().println("##########Item."+index+"##########" + item.path);
		Context context = this.context;
		boolean external = this.external;
		int folder = this.folder;
		TimeLockContentsDataSource db = getDataSource();
		TimeLockFoldersDataSource dbf = getDataSourceFolder();
		String passcode = this.passcode;
		switch (actionSub) {
		case ACTION_SUB_IMPORT_GALLERY:
			doRunGallery(item, context, external, folder, db, dbf, passcode);
			break;
		case ACTION_SUB_IMPORT_FILE:
			doRunFile(item, context, external, folder, db, dbf, passcode);
			break;
		case ACTION_SUB_IMPORT_URI:
			doRunUri(item, context, external, folder, db, dbf, passcode);
			break;
		default:
			break;
		}
	}
	
	@Override
	public void update(int itemTotal, int itemCurrent) {
		status().itemTotal = itemTotal;
		status().itemCurrent = itemCurrent;
	}
	
	protected void doRunGallery(BackgroundActionImportItem item, Context context,
			boolean external, int folder, TimeLockContentsDataSource db, TimeLockFoldersDataSource dbf, String passcode) {
		String[] paths = TimeLockMediaFileLayer.generateSecretPaths(context, external);
		File file = new File(item.path);
		try {
			TimeLockMediaFileLayer.storeMedia1New_Encrypt(file, type, paths, passcode, this, log());
			try {
				TimeLockMediaFileLayer.storeMedia1New_Thumbnail(file, type, paths, this, log());
			} catch (Exception e) { }
			((TimeLockApplication)context.getApplicationContext()).addData(paths, type, db, dbf, item.date, folder);
			
			if (delete) {
				try {
					Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, item.id);
				    context.getContentResolver().delete(deleteUri, null, null);
				} catch (Exception e) {}
				try {file.delete();} catch (Exception e) {}
			}

		} catch (Exception e) {
			errorCount++;
			e.printStackTrace(log());
			try {new File(paths[0]).delete();} catch (Exception ee) {}
			try {new File(paths[1]).delete();} catch (Exception ee) {}
		}
	}
	
	protected void doRunFile(BackgroundActionImportItem item, Context context,
			boolean external, int folder, TimeLockContentsDataSource db, TimeLockFoldersDataSource dbf, String passcode) {
		String[] paths = TimeLockMediaFileLayer.generateSecretPaths(context, external);
		File file = new File(item.path);
		try {
			TimeLockMediaFileLayer.storeMedia1New_Encrypt(file, type, paths, passcode, this, log());
			try {
				TimeLockMediaFileLayer.storeMedia1New_Thumbnail(file, type, paths, this, log());
			} catch (Exception e) { }
			((TimeLockApplication)context.getApplicationContext()).addData(paths, type, db, dbf, file.lastModified(), folder);
			
			if (delete) {
				try {file.delete();} catch (Exception e) {}
			}
			
		} catch (Exception e) {
			errorCount++;
			e.printStackTrace(log());
			try {new File(paths[0]).delete();} catch (Exception ee) {}
			try {new File(paths[1]).delete();} catch (Exception ee) {}
		}
	}
	
	protected void doRunUri(BackgroundActionImportItem item, Context context,
			boolean external, int folder, TimeLockContentsDataSource db, TimeLockFoldersDataSource dbf, String passcode) {
		Uri mediaUri = null;
		try { mediaUri = Uri.parse(item.path); } catch (Exception e) { }
		
		String[] paths = TimeLockMediaFileLayer.generateSecretPaths(context, external);
		
		long date = 0;
		boolean success = false;
		int fileNotFound = 0;
		{//first try, 1.0.7 code
			ParcelFileDescriptor pfd = null;
			FileDescriptor fd = null;
			FileInputStream fis = null;
			try {
				pfd = context.getContentResolver().openFileDescriptor(mediaUri, "r");
				fd = pfd.getFileDescriptor();
				fis = new FileInputStream(fd);
				TimeLockMediaFileLayer.storeMedia2New_Encrypt(fd, fis, type, paths, passcode, this, log());
				try {
					TimeLockMediaFileLayer.storeMedia2New_Thumbnail(fd, fis, type, paths, this, log());
				} catch (Exception e) { }
				success = true;
			} catch (FileNotFoundException e) {
				fileNotFound++;
				log().println("###1st try###");
				e.printStackTrace(log());
			} catch (Exception e) {
				log().println("###1st try###");
				e.printStackTrace(log());
			} finally {
				fd = null;
				if (fis != null) { try { fis.close(); } catch (Exception e) { } fis = null; }
				if (pfd != null) { try { pfd.close(); } catch (Exception e) { } pfd = null; }
			}
			if (!success) {
				//clean
				try {new File(paths[0]).delete();} catch (Exception ee) {}
				try {new File(paths[1]).delete();} catch (Exception ee) {}
			}
		}
		
		if (!success) {//second try, 1.0.8 code, file
			if (mediaUri != null && mediaUri.getScheme().contains(ContentResolver.SCHEME_FILE)) {
				try {
					File file = new File(mediaUri.getPath());
					if (file.exists()) {
						//messageItems.append(4).append(' ');
						TimeLockMediaFileLayer.storeMedia1New_Encrypt(file, type, paths, passcode, this, log());
						try {
							TimeLockMediaFileLayer.storeMedia1New_Thumbnail(file, type, paths, this, log());
						} catch (Exception e) { }
						date = file.lastModified();
						success = true;
					}
				} catch (Exception e) {
					log().println("###2nd try###");
					e.printStackTrace(log());
				}
				if (!success) {
					//clean
					try {new File(paths[0]).delete();} catch (Exception ee) {}
					try {new File(paths[1]).delete();} catch (Exception ee) {}
				}
			}
		}
		
		if (!success) {//third try, 1.0.9 code, with new openAssetFileDescriptor
			AssetFileDescriptor pfd = null;
			FileDescriptor fd = null;
			FileInputStream fis = null;
			try {
				pfd = context.getContentResolver().openAssetFileDescriptor(mediaUri, "r");
				fd = pfd.getFileDescriptor();
				fis = new FileInputStream(fd);
				TimeLockMediaFileLayer.storeMedia2New_Encrypt(fd, fis, type, paths, passcode, this, log());
				try {
					TimeLockMediaFileLayer.storeMedia2New_Thumbnail(fd, fis, type, paths, this, log());
				} catch (Exception e) { }
				success = true;
			} catch (FileNotFoundException e) {
				fileNotFound++;
				log().println("###3rd try###");
				e.printStackTrace(log());
			} catch (Exception e) {
			} finally {
				fd = null;
				if (fis != null) { try { fis.close(); } catch (Exception e) { } fis = null; }
				if (pfd != null) { try { pfd.close(); } catch (Exception e) { } pfd = null; }
			}
			if (!success) {
				//clean
				try {new File(paths[0]).delete();} catch (Exception ee) {}
				try {new File(paths[1]).delete();} catch (Exception ee) {}
			}
		}
		
		if (!success) {//forth try, 1.0.9 code, use openInputStream
			InputStream is = null;
			try {
				is = context.getContentResolver().openInputStream(mediaUri);
				File tempFile = File.createTempFile("mbu", null);
				MeobuFileTools.copy(is, tempFile);
				TimeLockMediaFileLayer.storeMedia1New_Encrypt(tempFile, type, paths, passcode, this, log());
				try {
					TimeLockMediaFileLayer.storeMedia1New_Thumbnail(tempFile, type, paths, this, log());
				} catch (Exception e) { }
				success = true;
			} catch (FileNotFoundException e) {
				fileNotFound++;
				log().println("###4th try###");
				e.printStackTrace(log());
			} catch (Exception e) {
				log().println("###4th try###");
				e.printStackTrace(log());
			} finally {
				if (is != null) try {is.close();} catch(Exception e) {}
			}
			if (!success) {
				//clean
				try {new File(paths[0]).delete();} catch (Exception ee) {}
				try {new File(paths[1]).delete();} catch (Exception ee) {}
			}
		}
		
		if (!success) {//fifth try, 1.0.9 code, use XXXFileDescriptor to temp file
			if (fileNotFound < 3) {
				InputStream is = null;
				ParcelFileDescriptor pfd = null;
				AssetFileDescriptor afd = null;
				{//first try to open inputstream 1 time
					try {
						pfd = context.getContentResolver().openFileDescriptor(mediaUri, "r");
						is = new FileInputStream(pfd.getFileDescriptor());
					} catch (Exception e) {
						log().println("###5th.1 try###");
						e.printStackTrace(log());
						if (pfd != null) try {pfd.close();} catch (Exception ee) {}
						if (is != null) try {is.close();} catch(Exception ee) {}
						is = null;
					}
				}
				if (is == null) {//second try to open inputstream 1 time
					try {
						afd = context.getContentResolver().openAssetFileDescriptor(mediaUri, "r");
						is = new FileInputStream(afd.getFileDescriptor());
					} catch (Exception e) {
						log().println("###5th.2 try###");
						e.printStackTrace(log());
						if (afd != null) try {afd.close();} catch (Exception ee) {}
						if (is != null) try {is.close();} catch(Exception ee) {}
						is = null;
					}
				}
				if (is != null) {
					try {
						File tempFile = File.createTempFile("mbu", null);
						MeobuFileTools.copy(is, tempFile);
						TimeLockMediaFileLayer.storeMedia1New_Encrypt(tempFile, type, paths, passcode, this, log());
						try {
							TimeLockMediaFileLayer.storeMedia1New_Thumbnail(tempFile, type, paths, this, log());
						} catch (Exception e) { }
						success = true;
					} catch (Exception e) {
						log().println("###5th.3 try###");
						e.printStackTrace(log());
					}
					if (!success) {
						//clean
						try {new File(paths[0]).delete();} catch (Exception ee) {}
						try {new File(paths[1]).delete();} catch (Exception ee) {}
					}
				}
				if (is != null) try {is.close();} catch(Exception ee) {}
				if (pfd != null) try {pfd.close();} catch (Exception ee) {}
				if (afd != null) try {afd.close();} catch (Exception ee) {}
			}
		}
		
		if (success) {
			if (date == 0) {
				try {
					date = MeobuGalleryHandler.queryMediaDate(context, mediaUri);
				} catch (Exception e) {
					date = System.currentTimeMillis();
				}
			}
			if (date == 0) {
				date = System.currentTimeMillis();
			}
			try {
				((TimeLockApplication)context.getApplicationContext()).addData(paths, type, db, dbf, date, folder);
				
				if (delete) {
					if (mediaUri == null) {
					} else if (mediaUri.getScheme().contains("file")) {
						new File(mediaUri.getPath()).delete();
					} else {
						long id = -1;
						try { id = ContentUris.parseId(mediaUri); } catch (Exception e) {}
						if (id < 0) {//from somewhere
							try { context.getContentResolver().delete(mediaUri, null, null); } catch (Exception e) { }
						} else {
							Uri deleteUri = null;
							if (type == ItemViewDataType.Photo.ordinal()) {
								deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
							} else {
								deleteUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id);
							}
						    context.getContentResolver().delete(deleteUri, null, null);
						}
					}
				}

			} catch (Exception e) { }
		} else {
			errorCount++;
			if (mediaUri != null && mediaUri.toString().contains("com.google.android.apps")) {
				errorCount += 2508;
			}
			if (fileNotFound > 1) {
				errorCount += 5016;
			}
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------// TODO fill data here
	
	TimeLockContentsDataSource datasource;
	private TimeLockContentsDataSource getDataSource() {
		if (datasource == null) {
			datasource = new TimeLockContentsDataSource(context);
		}
		return datasource;
	}
	
	TimeLockFoldersDataSource datasourcefolder;
	private TimeLockFoldersDataSource getDataSourceFolder() {
		if (datasourcefolder == null) {
			datasourcefolder = new TimeLockFoldersDataSource(context);
		}
		return datasourcefolder;
	}
	
}
