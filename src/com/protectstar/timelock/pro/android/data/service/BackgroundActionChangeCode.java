package com.protectstar.timelock.pro.android.data.service;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;

import meobu.android.base.MeobuFileTools;

import android.content.Context;

import com.protectstar.timelock.pro.android.data.TimeLockVoice;
import com.protectstar.timelock.pro.android.data.TimeLockVoicesDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockContent;
import com.protectstar.timelock.pro.android.data.TimeLockContentsDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockMediaFileLayer;

public class BackgroundActionChangeCode extends BackgroundActionChangeCodeConvert implements BackgroundActionStatusUpdate {
	
	private final Context context;
	public BackgroundActionChangeCode(Context context) {
		this.context = context;
	}
	
	ArrayList<TimeLockContent> items;
	ArrayList<TimeLockContent> items() {
		if (items == null) {
			items = getDataSource().retrieve(0, Integer.MAX_VALUE);
		}
		return items;
	}
	
	ArrayList<TimeLockVoice> voices;
	ArrayList<TimeLockVoice> voices() {
		if (voices == null) {
			voices = getDataSourceVoice().retrieve(0, Integer.MAX_VALUE);
		}
		return voices;
	}
	
	PrintStream log = null;
	protected PrintStream log() {
		if (log == null) {
			File logFile = null;
			if (logPath == null || logPath.length() <= 0) {
				logFile = MeobuFileTools.tempLog(context);
				logPath = logFile.getAbsolutePath();
			} else {
				logFile = new File(logPath);
			}
			try {
				log = new PrintStream(logFile);
			} catch (Exception e) { }
		}
		return log;
	}
	
	@Override
	protected void doRunOne(int index) {
		status().listTotal = items().size() + voices().size();
		if (index >= items().size()) {
			if (index - items().size() >= voices().size()) {
				//tada
			} else {
				doRunOne(voices().get(index - items().size()));
			}
		} else {
			doRunOne(items().get(index));
		}
	}
	private void doRunOne(TimeLockVoice item) {
		String[] tempPaths = TimeLockMediaFileLayer.generateSecretPaths(context, false);
		File tempFile = new File(tempPaths[0]);
		MeobuFileTools.copyDecryptNew(item.getPath(), tempFile, oldPasscode, this, log());
		MeobuFileTools.copyEncryptNew(tempFile, new File(item.getPath()), newPasscode, this, log());
		try { tempFile.delete(); } catch (Exception e) { }
	}
	private void doRunOne(TimeLockContent item) {
		String[] tempPaths = TimeLockMediaFileLayer.generateSecretPaths(context, false);
		File tempFile = new File(tempPaths[0]);
		MeobuFileTools.copyDecryptNew(item.getDataPath(), tempFile, oldPasscode, this, log());
		MeobuFileTools.copyEncryptNew(tempFile, new File(item.getDataPath()), newPasscode, this, log());
		try { tempFile.delete(); } catch (Exception e) { }
	}
	
	TimeLockContentsDataSource datasource;
	private TimeLockContentsDataSource getDataSource() {
		if (datasource == null) {
			datasource = new TimeLockContentsDataSource(context);
		}
		return datasource;
	}
	
	TimeLockVoicesDataSource datasourceVoice;
	private TimeLockVoicesDataSource getDataSourceVoice() {
		if (datasourceVoice == null) {
			datasourceVoice = new TimeLockVoicesDataSource(context);
		}
		return datasourceVoice;
	}

	@Override
	public void update(int itemTotal, int itemCurrent) {
		status().itemTotal = itemTotal;
		status().itemCurrent = itemCurrent;
	}
	
}
