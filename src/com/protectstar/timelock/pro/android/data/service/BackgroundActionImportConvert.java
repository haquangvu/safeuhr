package com.protectstar.timelock.pro.android.data.service;

import java.io.File;
import java.util.ArrayList;

import meobu.android.base.MeobuGalleryHandler.MeobuGalleryItem;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;

public class BackgroundActionImportConvert extends BackgroundActionImpl {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	protected final int action = BackgroundService.ACTION_IMPORT;
	public int actionSub;
	public int type;
	protected int folder;
	protected boolean external;
	protected String passcode;
	public boolean delete;
	public String logPath;
	public int errorCount;
	public ArrayList<BackgroundActionImportItem> items;
	public static final String ACTION_SUB_KEY = "i_action_sub";
	public static final String TYPE_KEY = "i_type";
	public static final String FOLDER_KEY = "i_folder";
	public static final String EXTERNAL_KEY = "i_external";
	public static final String PASSCODE_KEY = "i_passcode";
	public static final String DELETE_KEY = "i_delete";
	public static final String LOGPATH_KEY = "i_logpath";
	public static final String ERRORCOUNT_KEY = "i_errorcount";
	public static final String ITEMS_KEY = "i_items";
	public static final int ACTION_SUB_IMPORT_UNKNOWN = -1;
	public static final int ACTION_SUB_IMPORT_FILE = 0;
	public static final int ACTION_SUB_IMPORT_URI = 1;
	public static final int ACTION_SUB_IMPORT_GALLERY = 2;
	protected ArrayList<BackgroundActionImportItem> items() {
		if (items == null) {
			items = new ArrayList<BackgroundActionImportItem>();
		}
		return items;
	}
	
	public void set(Object[] objs, int type, int folder, boolean external, String passcode, boolean delete) {
		this.type = type;
		if (objs[0] instanceof File) {
			actionSub = ACTION_SUB_IMPORT_FILE;
		} else if (objs[0] instanceof Uri) {
			actionSub = ACTION_SUB_IMPORT_URI;
		} else if (objs[0] instanceof MeobuGalleryItem) {
			actionSub = ACTION_SUB_IMPORT_GALLERY;
		}
		this.folder = folder;
		this.external = external;
		this.passcode = passcode;
		this.delete = delete;
		items().clear();
		for (int i=0; i<objs.length; i++) {
			BackgroundActionImportItem item = new BackgroundActionImportItem();
			actionSub = item.set(objs[i], type);
			items().add(item);
		}
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	public void put2Intent(Intent intent) {
		//don't need status
		intent.putExtra(BackgroundService.ACTION_KEY, action);
		intent.putExtra(ACTION_SUB_KEY, actionSub);
		intent.putExtra(TYPE_KEY, type);
		intent.putExtra(FOLDER_KEY, folder);
		intent.putExtra(EXTERNAL_KEY, external);
		if (passcode == null) passcode = "";
		intent.putExtra(PASSCODE_KEY, passcode);
		intent.putExtra(DELETE_KEY, delete);
		if (logPath == null) logPath = "";
		intent.putExtra(LOGPATH_KEY, logPath);
		intent.putExtra(ERRORCOUNT_KEY, errorCount);
		String[] itemsIntent = new String[items().size()];
		for (int i=0; i<items().size(); i++) {
			itemsIntent[i] = items().get(i).toJSONString();
		}
		intent.putExtra(ITEMS_KEY, itemsIntent);
	}
	
	@Override
	public void set(Intent intent) {
		super.set(intent);
		//set status().listTotal
		actionSub = intent.getIntExtra(ACTION_SUB_KEY, 0);
		type = intent.getIntExtra(TYPE_KEY, 0);
		folder = intent.getIntExtra(FOLDER_KEY, 0);
		external = intent.getBooleanExtra(EXTERNAL_KEY, false);
		passcode = intent.getStringExtra(PASSCODE_KEY);
		if (passcode == null) passcode = "";
		delete = intent.getBooleanExtra(DELETE_KEY, false);
		logPath = intent.getStringExtra(LOGPATH_KEY);
		if (logPath == null) logPath = "";
		errorCount = intent.getIntExtra(ERRORCOUNT_KEY, 0);
		String[] itemsIntent = intent.getStringArrayExtra(ITEMS_KEY);
		items().clear();
		if (itemsIntent != null && itemsIntent.length > 0) {
			for (int i=0; i<itemsIntent.length; i++) {
				BackgroundActionImportItem item = new BackgroundActionImportItem();
				item.setJSONString(itemsIntent[i]);
				items().add(item);
			}
		}
		status().listTotal = items().size();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	protected void set2JSON(JSONObject json) {
		//status is processed, just ignore it
		try { json.put(BackgroundService.ACTION_KEY, action); } catch (Exception e) { }
		try { json.put(ACTION_SUB_KEY, actionSub); } catch (Exception e) { }
		try { json.put(TYPE_KEY, type); } catch (Exception e) { }
		try { json.put(FOLDER_KEY, folder); } catch (Exception e) { }
		try { json.put(EXTERNAL_KEY, external); } catch (Exception e) { }
		try { json.put(PASSCODE_KEY, passcode); } catch (Exception e) { }
		try { json.put(DELETE_KEY, delete); } catch (Exception e) { }
		try { json.put(LOGPATH_KEY, logPath); } catch (Exception e) { }
		try { json.put(ERRORCOUNT_KEY, errorCount); } catch (Exception e) { }
		JSONArray list = new JSONArray();
		for (int i=0; i<items().size(); i++) {
			try {
				list.put(items().get(i).toJSON());
			} catch (Exception e) { }
		}
		try {
			json.put(ITEMS_KEY, list);
		} catch (Exception e) { }
	}
	
	@Override
	public void set(JSONObject json) {
		super.set(json);
		//set status().listTotal
		try { actionSub = json.getInt(ACTION_SUB_KEY); } catch (Exception e) { }
		try { type = json.getInt(TYPE_KEY); } catch (Exception e) { }
		try { folder = json.getInt(FOLDER_KEY); } catch (Exception e) { }
		try { external = json.getBoolean(EXTERNAL_KEY); } catch (Exception e) { }
		try { passcode = json.getString(PASSCODE_KEY); } catch (Exception e) { }
		try { delete = json.getBoolean(DELETE_KEY); } catch (Exception e) { }
		try { logPath = json.getString(LOGPATH_KEY); } catch (Exception e) { }
		try { errorCount = json.getInt(ERRORCOUNT_KEY); } catch (Exception e) { }
		items().clear();
		try {
			JSONArray list = json.getJSONArray(ITEMS_KEY);
			for (int i=0; i<list.length(); i++) {
				try {
					JSONObject ij = list.getJSONObject(i);
					BackgroundActionImportItem item = new BackgroundActionImportItem();
					item.setJSON(ij);
					items().add(item);
				} catch (Exception e) { }
			}
		} catch (Exception e) { }
		status().listTotal = items().size();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

	@Override
	protected void doRunOne(int index) {
		//TODO run
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

}
