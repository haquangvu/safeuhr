package com.protectstar.timelock.pro.android.data.service;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;

import com.protectstar.timelock.pro.android.data.service.BackgroundService.BackgroundActionInterface;

public final class BackgroundActionFactory {
	
	public static BackgroundActionInterface from(Context context, JSONObject json) {
		BackgroundActionInterface action = null;
		try {
			int actionid = json.getInt(BackgroundService.ACTION_KEY);
			if (actionid == BackgroundService.ACTION_IMPORT) {
				action = new BackgroundActionImport(context);
				action.set(json);
			} else if (actionid == BackgroundService.ACTION_CHANGECODE) {
				action = new BackgroundActionChangeCode(context);
				action.set(json);
			}
		} catch (Exception e) { }
		return action;
	}
	
	public static BackgroundActionInterface from(Context context, Intent intent) {
		BackgroundActionInterface action = null;
		int actionid = intent.getIntExtra(BackgroundService.ACTION_KEY, 0);
		if (actionid == BackgroundService.ACTION_IMPORT) {
			action = new BackgroundActionImport(context);
			action.set(intent);
		} else if (actionid == BackgroundService.ACTION_CHANGECODE) {
			action = new BackgroundActionChangeCode(context);
			action.set(intent);
		}
		return action;
	}
	
	public static BackgroundActionInterface from(Context context, int act, Object[] items, int type, int folder, boolean external, String passcode, boolean delete) {
		BackgroundActionImport action = new BackgroundActionImport(context);
		action.set(items, type, folder, external, passcode, delete);
		return action;
	}
	
	public static BackgroundActionInterface from(Context context, int act, String oldCode, String newCode, int hour, int minute, int second) {
		BackgroundActionChangeCode action = new BackgroundActionChangeCode(context);
		action.set(oldCode, newCode, hour, minute, second);
		return action;
	}
	
}
