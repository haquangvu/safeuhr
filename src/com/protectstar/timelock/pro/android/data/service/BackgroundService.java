package com.protectstar.timelock.pro.android.data.service;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class BackgroundService extends Service implements Runnable {
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static interface BackgroundActionInterface {
		public void put2Intent(Intent intent);
		public void set(Intent intent);
		public void set(JSONObject json);
		public JSONObject toJSON();
		public void stop();
		public void runOne();
		public boolean isFinish();
		public void updateStatus(BackgroundActionStatus status);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	static final String ACTION_KEY = "action";
	static final int ACTION_STATUS = 0;
	static final int ACTION_IMPORT = 1;
	static final int ACTION_CHANGECODE = 2;
	
	public static void updateStatus(Activity act) {
		Intent intent = new Intent(act.getApplicationContext(), BackgroundService.class);
		act.startService(intent);
	}
	
	public static void startImport(Activity act, Object[] items, int type, int folder, boolean external, String passcode, boolean delete) {
		if (items == null || items.length <= 0) return;
		Intent intent = new Intent(act.getApplicationContext(), BackgroundService.class);
		BackgroundActionInterface action = BackgroundActionFactory.from(act, ACTION_IMPORT, items, type, folder, external, passcode, delete);
		action.put2Intent(intent);
		act.startService(intent);
	}
	
	public static void startChangeCode(Activity act, String oldCode, String newCode, int hour, int minute, int second) {
		if (oldCode == null || oldCode.length() <= 0 || newCode == null || newCode.length() <= 0) return;
		Intent intent = new Intent(act.getApplicationContext(), BackgroundService.class);
		BackgroundActionInterface action = BackgroundActionFactory.from(act, ACTION_CHANGECODE, oldCode, newCode, hour, minute, second);
		action.put2Intent(intent);
		act.startService(intent);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	private SharedPreferences quickPref() {
		return getApplicationContext().getSharedPreferences("BackgroundService.List", 0);
	}
	
	public static final String CACHE_ACTION_KEY = "cache_action";
	BackgroundActionInterface cacheLastAction = null;
	public static final String PENDING_ACTIONS_KEY = "pending_actions";
	ArrayList<BackgroundActionInterface> pendingActions = null;
	private ArrayList<BackgroundActionInterface> getPendingActions() {
		if (pendingActions == null) {
			pendingActions = new ArrayList<BackgroundActionInterface>();
			String jsonActions = quickPref().getString(PENDING_ACTIONS_KEY, "[]");
			try {
				JSONArray list = new JSONArray(jsonActions);
				for (int i=0; i<list.length(); i++) {
					try {
						JSONObject item = list.getJSONObject(i);
						BackgroundActionInterface action = BackgroundActionFactory.from(this, item);
						pendingActions.add(action);
					} catch (Exception e) { }
				}
			} catch (Exception e) { }
			String jsonActionCache = quickPref().getString(CACHE_ACTION_KEY, "{}");
			try {
				JSONObject item = new JSONObject(jsonActionCache);
				cacheLastAction = BackgroundActionFactory.from(this, item);
			} catch (Exception e) { }
		}
		return pendingActions;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		if (!getPendingActions().isEmpty()) {
			startWork();
		}
	}
	
	private void savePendingActions() {
		try {
			JSONArray list = new JSONArray();
			for (int i=0; i<getPendingActions().size(); i++) {
				try {
					JSONObject item = getPendingActions().get(i).toJSON();
					list.put(item);
				} catch (Exception e) { }
			}
			Editor edit = quickPref().edit();
			edit.putString(PENDING_ACTIONS_KEY, list.toString());
			try {
				if (cacheLastAction != null) {
					edit.putString(CACHE_ACTION_KEY, cacheLastAction.toJSON().toString());
				} else {
					edit.putString(CACHE_ACTION_KEY, "{}");
				}
			} catch (Exception e) { }
			edit.commit();
		} catch (Exception e) { }
	}
	
	@Override
	public void onDestroy() {
		Log.d("meobudebug", "background destroyed!");
		try { getPendingActions().get(0).stop(); } catch (Exception e) { }
		savePendingActions();
		super.onDestroy();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		boolean updateStatus = false;
		if (intent != null && intent.hasExtra(ACTION_KEY)) {
			switch (intent.getIntExtra(ACTION_KEY, ACTION_STATUS)) {
			case ACTION_IMPORT:
			case ACTION_CHANGECODE:
				BackgroundActionInterface action = BackgroundActionFactory.from(this, intent);
				addAction(action);
				startWork();
				break;
			case ACTION_STATUS:
			default:
				updateStatus = true;
			}
		} else {
			updateStatus = true;
		}
		if (updateStatus) {
			updateStatus();
			//return Service.START_NOT_STICKY;
		}
		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;//no need, use internal broadcast to work
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	private BackgroundActionStatus status;
	private BackgroundActionStatus status() {
		if (status == null) {
			status = new BackgroundActionStatus();
		}
		return status;
	}
	private void updateStatus() {
		BackgroundActionInterface action = null;
		if ((action = getAction()) == null) {
			//stopSelf();
			//START_NOT_STICKY mean stop immediately, maybe [WRONG]
			//START_NOT_STICKY mean the running Command is stopped (if the previous command is not sticky too, just stop service).
			// & START_STICKY mean the running Command will do background
			//[WRONG]
			//return ABC mean update the status of service to ABC
			//-> always return START_STICKY
			Intent intent = new Intent("my-event");
			intent.putExtra("working", false);
			boolean clear = false;
			if (cacheLastAction != null) {
				cacheLastAction.put2Intent(intent);
				clear = true;
			}
			LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
			if (clear) {
				clear = false;
				cacheLastAction = null;
				savePendingActions();
				stopSelf();
			}
			return;
		}
		//Log.d("meobudebug", "some action here: " + action.toString());
		action.updateStatus(status());
		Intent intent = new Intent("my-event");
		intent.putExtra("working", true);
		status().get(intent);
		LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	private final Object threadNewLock = new Object();
	private boolean threadWorking = false;
	private void startWork() {
		boolean shouldNew = false;
		synchronized (threadNewLock) {
			if (threadWorking) {
				shouldNew = false;
			} else {
				shouldNew = true;
				threadWorking = true;
			}
		}
		if (shouldNew) {
			newThread();
		}
	}
	
	private Thread thread;
	private void newThread() {
		thread = new Thread(this);
		thread.start();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	private final Object stackChangeLock = new Object();
	private void addAction(BackgroundActionInterface action) {
		synchronized (stackChangeLock) {
			getPendingActions().add(action);
			savePendingActions();
		}
	}
	
	public void removeAction(BackgroundActionInterface action) {
		synchronized (stackChangeLock) {
			getPendingActions().remove(action);
			savePendingActions();
		}
	}
	
	public BackgroundActionInterface getAction() {
		BackgroundActionInterface action = null;
		synchronized (stackChangeLock) {
			if (!getPendingActions().isEmpty()) {
				action = getPendingActions().get(0);
			}
		}
		return action;
	}
	
	//----------//----------//----------//----------//----------//----------//----------//
	
	public void run() {
		BackgroundActionInterface action = null;
		while ((action = getAction()) != null) {
			cacheLastAction = action;
			if (action.isFinish()) {
				Log.d("meobudebug", "finished cmn roi");
				removeAction(action);
			} else {
				Log.d("meobudebug", "run run run");
				action.runOne();
				savePendingActions();
			}
			try { Thread.sleep(100); } catch (Exception e) { }
		}
		stopSelf();
	}
	
	//----------//----------//----------//----------//----------//----------//----------//

}
