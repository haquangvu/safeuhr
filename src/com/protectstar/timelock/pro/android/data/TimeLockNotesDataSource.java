package com.protectstar.timelock.pro.android.data;

import java.util.ArrayList;

import meobu.android.base.MeobuSQLiteDataSource;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

public class TimeLockNotesDataSource  implements MeobuSQLiteDataSource {

	// ----------//----------//----------//----------//----------//

	SQLiteOpenHelper helper;
	public TimeLockNotesDataSource(Context context) {
		helper = new TimeLockSQLiteOpenHelper(context);
	}
	
	@Override
	public void open() {
		//Log.d("meobudebug", "did open!!!");
	}
	
	@Override
	public void close() {
		helper.close();
	}

	// ----------//----------//----------//----------//----------//

	public ArrayList<TimeLockNote> retrieve(int from, int size) {
		ArrayList<TimeLockNote> contents = new ArrayList<TimeLockNote>();
		SQLiteDatabase db = null;
		Cursor cursor = null;

		try {
			db = helper.getReadableDatabase();
			String query = SQLiteQueryBuilder.buildQueryString(false,
					TimeLockSQLiteOpenHelper.TABLE_NOTES,
					TimeLockNote.NOTE_KEYS, null, null, null,
					TimeLockNote.NOTE_PRIMARY + " desc", from + ", " + size);
			cursor = db.rawQuery(query, null);
	
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				TimeLockNote content = TimeLockNote.fromCursor(cursor);
				contents.add(content);
				cursor.moveToNext();
			}
		} catch (Exception e) {
		} finally {
			if (cursor != null) try { cursor.close(); } catch (Exception e) { }
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return contents;
	}
	
	public TimeLockNote get(long id) {
		TimeLockNote note = null;
		SQLiteDatabase db = null;
		Cursor cursor = null;
		
		try {
			db = helper.getReadableDatabase();
			String query = SQLiteQueryBuilder.buildQueryString(false,
					TimeLockSQLiteOpenHelper.TABLE_NOTES, TimeLockNote.NOTE_KEYS,
					TimeLockNote.NOTE_PRIMARY + "=" + id, null, null,
					TimeLockNote.NOTE_PRIMARY + " desc", null);
			cursor = db.rawQuery(query, null);
			
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				note = TimeLockNote.fromCursor(cursor);
				cursor.moveToNext();
			}
		} catch (Exception e) {
		} finally {
			if (cursor != null) try { cursor.close(); } catch (Exception e) { }
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return note;
	}
	
	public long put(TimeLockNote content) {
		SQLiteDatabase db = null;
		long id = -1;
		
		try {
			db = helper.getWritableDatabase();
			ContentValues values = content.toContentValues();
			id = db.insert(TimeLockSQLiteOpenHelper.TABLE_NOTES, null, values);
			if (id >= 0) {
				content.setId(id);
			}
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return id;
	}
	
	public int update(TimeLockNote content) {
		int count = 0;
		SQLiteDatabase db = null;
		
		try {
			db = helper.getWritableDatabase();
			ContentValues values = content.toContentValues();
			count = db.update(TimeLockSQLiteOpenHelper.TABLE_NOTES, values, TimeLockNote.NOTE_PRIMARY + "=" + content.getId(), null);
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return count;		
	}
	
	public int remove(TimeLockNote content) {
		int count = 0;
		SQLiteDatabase db = null;
		
		try {
			db = helper.getWritableDatabase();
			String whereClause = TimeLockNote.NOTE_PRIMARY + "=" + content.getId();
			String[] whereArgs = null;
			count = db.delete(TimeLockSQLiteOpenHelper.TABLE_NOTES, whereClause, whereArgs);
			if (count <= 0) {
				Log.e("meobuerror", "###delete record FAILED, don't know why? id:" + content.getId());
			}
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return count;
	}
	
	public int removeAll() {
		SQLiteDatabase db = null;
		int count = 0;
		
		try {
			db = helper.getWritableDatabase();
			count = db.delete(TimeLockSQLiteOpenHelper.TABLE_NOTES, null, null);
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return count;
	}
	
	public int remove(ArrayList<TimeLockNote> contents) {
		SQLiteDatabase db = null;
		int result = 0;
		
		try {
			db = helper.getWritableDatabase();
			for (TimeLockNote content : contents) {
				try {
					String whereClause = TimeLockNote.NOTE_PRIMARY + "=" + content.getId();
					String[] whereArgs = null;
					result += db.delete(TimeLockSQLiteOpenHelper.TABLE_NOTES, whereClause, whereArgs);
				} catch (Exception e) {}
			}
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		return result;
	}
//	public SQLiteDatabase doOpen() {
//		return helper.getWritableDatabase();
//	}
//	public void doClose(SQLiteDatabase db) {
//		db.close();
//	}
//	public int doRemove(int id, SQLiteDatabase db) {
//		String whereClause = TimeLockNote.NOTE_PRIMARY + "=" + id;
//		String[] whereArgs = null;
//		int result = db.delete(TimeLockSQLiteOpenHelper.TABLE_NOTES, whereClause, whereArgs);
//		if (result <= 0) {
//			Log.e("meobuerror", "###delete record FAILED, don't know why? id:" + id);
//		}
//		return result;
//	}
	
}