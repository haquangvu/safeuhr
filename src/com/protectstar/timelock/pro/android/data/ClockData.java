package com.protectstar.timelock.pro.android.data;

public class ClockData {
	
	public static final float DEGREE_PER_SECOND = 6.0f;
	public static final float[] DEGREE_HOURS = {0.0f, 30.0f, 60.0f, 90.0f, 120.0f, 150.0f, 180.0f, 210.0f, 240.0f, 270.0f, 300.0f, 330.0f};
	
	public static final float degreeHour(float degree) {//from 0 to 359
		return degree / (5 * DEGREE_PER_SECOND);
	}
	public static final float hourDegree(float hour) {
		return hourDegree(hour, 0);
	}
	public static final float hourDegree(float hour, float minute) {
		return (hour + minute / 60.0f) * 5 * DEGREE_PER_SECOND;
	}
	public static final float degreeMinute(float degree) {
		return degree / DEGREE_PER_SECOND;
	}
	public static final float minuteDegree(float minute) {
		return minuteDegree(minute, 0);
	}
	public static final float minuteDegree(float minute, float second) {
		return (minute + second / 60.0f) * DEGREE_PER_SECOND;
	}
	public static final float secondDegree(float second) {
		return second * DEGREE_PER_SECOND;
	}
}
