package com.protectstar.timelock.pro.android.data;

import com.protectstar.timelock.pro.android.TimeLockApplication.Data;

import android.content.ContentValues;
import android.database.Cursor;

public class TimeLockContent implements Data {

	private int id;
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	private int type;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	private String path;
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	private String thumbnail;
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	private long date;
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String fileext;
	public String getFileExt() {
		return fileext;
	}
	public void setFileExt(String fileext) {
		this.fileext = fileext;
	}
	private int folder;
	public int getFolder() {
		return folder;
	}
	public void setFolder(int folder) {
		this.folder = folder;
	}
	
	private int where;
	public int getWhere() {
		return where;
	}
	public void setWhere(int where) {
		this.where = where;
	}
	
	
	private String pathDecrypted;
	public String getPathDecrypted() {
		return pathDecrypted;
	}
	public void setPathDecrypted(String pathDecrypted) {
		this.pathDecrypted = pathDecrypted;
	}

	public static final String CONTENT_PRIMARY = TimeLockSQLiteOpenHelper.COLUMN_ID;
	public static final String[] CONTENT_KEYS = {
			TimeLockSQLiteOpenHelper.COLUMN_ID,
			TimeLockSQLiteOpenHelper.COLUMN_TYPE,
			TimeLockSQLiteOpenHelper.COLUMN_PATH,
			TimeLockSQLiteOpenHelper.COLUMN_THUMBNAIL,
			TimeLockSQLiteOpenHelper.COLUMN_DATE,
			TimeLockSQLiteOpenHelper.COLUMN_NAME,
			TimeLockSQLiteOpenHelper.COLUMN_FILEEXT,
			TimeLockSQLiteOpenHelper.COLUMN_FOLDER,
			TimeLockSQLiteOpenHelper.COLUMN_WHERE};

	public static TimeLockContent fromCursor(Cursor cursor) {
		TimeLockContent content = new TimeLockContent();
		content.id = cursor.getInt(0);
		content.type = cursor.getInt(1);
		content.path = cursor.getString(2);
		content.thumbnail= cursor.getString(3);
		content.date = cursor.getLong(4);
		content.name = cursor.getString(5);
		content.fileext = cursor.getString(6);
		content.folder = cursor.getInt(7);
		content.where = cursor.getInt(8);
		return content;
	}
	
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(TimeLockSQLiteOpenHelper.COLUMN_TYPE, type);
		values.put(TimeLockSQLiteOpenHelper.COLUMN_PATH, ((path==null)?"":path));
		values.put(TimeLockSQLiteOpenHelper.COLUMN_THUMBNAIL, ((thumbnail==null)?"":thumbnail));
		values.put(TimeLockSQLiteOpenHelper.COLUMN_DATE, date);
		values.put(TimeLockSQLiteOpenHelper.COLUMN_NAME, ((name==null)?"":name));
		values.put(TimeLockSQLiteOpenHelper.COLUMN_FILEEXT, ((fileext==null)?"":fileext));
		values.put(TimeLockSQLiteOpenHelper.COLUMN_FOLDER, folder);
		values.put(TimeLockSQLiteOpenHelper.COLUMN_WHERE, where);
		return values;
	}
	
	@Override
	public int getDataId() {
		return id;
	}
	@Override
	public int getDataType() {
		return type;
	}
	@Override
	public long getDataTime() {
		return date;
	}
	@Override
	public String getDataPath() {
		return path;
	}
	@Override
	public String getDataThumbnail() {
		return thumbnail;
	}
	@Override
	public String getDataPathDecrypted() {
		return pathDecrypted;
	}
	@Override
	public void setDataPathDecrypted(String path) {
		setPathDecrypted(path);
	}
}
