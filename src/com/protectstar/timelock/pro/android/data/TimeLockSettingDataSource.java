package com.protectstar.timelock.pro.android.data;

import android.content.Context;
import meobu.android.base.MeobuSettingsDataSource;

public class TimeLockSettingDataSource extends MeobuSettingsDataSource {

	public TimeLockSettingDataSource(Context context) {
		super(context);
	}
	
	public static final String[] keys = {"selfdestruction", "folder", "securedfolder", "notefolder", "spycamera", "ifiledesktop", "voicememo"};
	@Override
	protected String[] getKeys() {
		return keys;
	}

}
