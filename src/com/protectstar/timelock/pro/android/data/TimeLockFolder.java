package com.protectstar.timelock.pro.android.data;

import android.content.ContentValues;
import android.database.Cursor;

public class TimeLockFolder {
	
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private int folder;
	public int getFolder() {
		return folder;
	}
	public void setFolder(int folder) {
		this.folder = folder;
	}
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public static final String FOLDER_PRIMARY = TimeLockSQLiteOpenHelper.COLUMN_ID;
	public static final String[] FOLDER_KEYS = {
		TimeLockSQLiteOpenHelper.COLUMN_ID,
		TimeLockSQLiteOpenHelper.COLUMN_NAME,
		TimeLockSQLiteOpenHelper.COLUMN_FOLDER
	};

	public static TimeLockFolder fromCursor(Cursor cursor) {
		TimeLockFolder folder = new TimeLockFolder();
		folder.id = cursor.getInt(0);
		folder.name = cursor.getString(1);
		folder.folder = cursor.getInt(2);
		return folder;
	}
	
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(TimeLockSQLiteOpenHelper.COLUMN_NAME, ((name==null)?"":name));
		values.put(TimeLockSQLiteOpenHelper.COLUMN_FOLDER, folder);
		return values;
	}
}
