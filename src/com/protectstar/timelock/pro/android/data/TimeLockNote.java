package com.protectstar.timelock.pro.android.data;

import meobu.android.base.MeobuListViewData;
import android.content.ContentValues;
import android.database.Cursor;

public class TimeLockNote implements MeobuListViewData {
	
	@Override
	public int getType() {
		return 0;
	}
	
	private long id;
	private long date;
	private String name, content;
	private String nameReadable, contentReadable;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String nameEncrypted) {
		this.name = nameEncrypted;
	}
	public void setName(String nameReadable, String password) {
		this.name = encrypt(nameReadable, password);
	}
	public String getContent() {
		return content;
	}
	public void setContent(String contentEncrypted) {
		this.content = contentEncrypted;
	}
	public void setContent(String contentReadable, String password) {
		this.content = encrypt(contentReadable, password);
	}
	public String getNameReadable() {
		return nameReadable;
	}
	public void setNameReadable(String content) {
		this.nameReadable = content;
	}
	public void setNameReadable(String encrypt, String password) {
		this.nameReadable = decrypt(encrypt, password);
	}
	public String getContentReadable() {
		return contentReadable;
	}
	public void setContentReadable(String content) {
		this.contentReadable = content;
	}
	public void setContentReadable(String encrypt, String password) {
		this.contentReadable = decrypt(encrypt, password);
	}
	
	private static String encrypt(String content, String password) {
		return content;//TODO encrypt/decrypt
	}
	private static String decrypt(String content, String password) {
		return content;
	}
	
	public static final String NOTE_PRIMARY = TimeLockSQLiteOpenHelper.COLUMN_ID;
	public static final String[] NOTE_KEYS = {
			TimeLockSQLiteOpenHelper.COLUMN_ID,
			TimeLockSQLiteOpenHelper.COLUMN_DATE,
			TimeLockSQLiteOpenHelper.COLUMN_NAME,
			TimeLockSQLiteOpenHelper.COLUMN_CONTENT};

	
	public static TimeLockNote fromCursor(Cursor cursor) {
		TimeLockNote content = new TimeLockNote();
		content.id = cursor.getInt(0);
		content.date = cursor.getLong(1);
		content.name = cursor.getString(2);
		content.content = cursor.getString(3);
		return content;
	}
	
	public static TimeLockNote duplicate(TimeLockNote note) {
		TimeLockNote content = new TimeLockNote();
		content.id = note.id;
		content.name = note.name;
		content.content = note.content;
		content.nameReadable = note.nameReadable;
		content.contentReadable = note.contentReadable;
		content.date = note.date;
		return content;
	}
	
	public ContentValues toContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(TimeLockSQLiteOpenHelper.COLUMN_DATE, date);
		if (name != null && name.length() > 0) {
			cv.put(TimeLockSQLiteOpenHelper.COLUMN_NAME, name);
		}
		if (content != null && content.length() > 0) {
			cv.put(TimeLockSQLiteOpenHelper.COLUMN_CONTENT, content);
		}
		return cv;
	}

}
