package com.protectstar.timelock.pro.android.data;

import meobu.android.base.MeobuListViewData;
import android.content.ContentValues;
import android.database.Cursor;

public class TimeLockVoice implements MeobuListViewData {
	
	@Override
	public int getType() {
		return 0;
	}
	
	private long id;
	private long date;
	private String name;
	private String path, decryptedPath;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDecryptedPath() {
		return decryptedPath;
	}
	public void setDecryptedPath(String decryptedPath) {
		this.decryptedPath = decryptedPath;
	}
	
	public static final String NOTE_PRIMARY = TimeLockSQLiteOpenHelper.COLUMN_ID;
	public static final String[] NOTE_KEYS = {
			TimeLockSQLiteOpenHelper.COLUMN_ID,
			TimeLockSQLiteOpenHelper.COLUMN_DATE,
			TimeLockSQLiteOpenHelper.COLUMN_NAME,
			TimeLockSQLiteOpenHelper.COLUMN_PATH};

	
	public static TimeLockVoice fromCursor(Cursor cursor) {
		TimeLockVoice content = new TimeLockVoice();
		content.id = cursor.getInt(0);
		content.date = cursor.getLong(1);
		content.name = cursor.getString(2);
		content.path = cursor.getString(3);
		return content;
	}
	
	public static TimeLockVoice duplicate(TimeLockVoice note) {
		TimeLockVoice content = new TimeLockVoice();
		content.id = note.id;
		content.name = note.name;
		content.date = note.date;
		content.path = note.path;
		content.decryptedPath = note.decryptedPath;
		return content;
	}
	
	public ContentValues toContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(TimeLockSQLiteOpenHelper.COLUMN_DATE, date);
		cv.put(TimeLockSQLiteOpenHelper.COLUMN_NAME, name);
		cv.put(TimeLockSQLiteOpenHelper.COLUMN_PATH, path);
		return cv;
	}

}
