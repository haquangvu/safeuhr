package com.protectstar.timelock.pro.android.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import meobu.android.base.MeobuEncryption;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.protectstar.timelock.pro.android.TimeLockApplication;

public class TimeLockImageDownloader extends BaseImageDownloader {
	
	//----------//----------//----------//----------//----------//

	public TimeLockImageDownloader(Context context) {
		super(context);
	}
	public TimeLockImageDownloader(Context context, int connectTimeout, int readTimeout) {
		super(context, connectTimeout, readTimeout);
	}
	
	//----------//----------//----------//----------//----------//
	
	public String getPasscode() {
		return TimeLockApplication.cachePasscode();
	}
	
	//----------//----------//----------//----------//----------//
	
	public static final String NEW_SUPPORTED_SCHEME = "localaeh256://";
	
	@Override
	protected InputStream getStreamFromOtherSource(String imageUri, Object extra)
			throws IOException {
		if (imageUri != null) {
			if (imageUri.startsWith(NEW_SUPPORTED_SCHEME)) {
				String newImageUri = Uri.parse(imageUri).buildUpon().scheme("file").build().toString();
				String filePath = Scheme.FILE.crop(newImageUri);
//				InputStream is = new BufferedInputStream(new FileInputStream(filePath), BUFFER_SIZE);
				InputStream is = new FileInputStream(filePath);
				try {
					return MeobuEncryption.inStreamDecryptedAES(is, getPasscode());
//					return new BufferedInputStream(MeobuEncryption.inStreamDecryptedAES(is, getPasscode()), BUFFER_SIZE);
				} catch (Exception e) {
					Log.e("meobuerror", "TimeLockImageDownloader.getStreamFromOtherResource " + e.getMessage());
				}
				return is;
			}
		}
		return super.getStreamFromOtherSource(imageUri, extra);
	}

	//----------//----------//----------//----------//----------//
	
}
