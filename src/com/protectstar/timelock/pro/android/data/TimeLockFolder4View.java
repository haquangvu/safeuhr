package com.protectstar.timelock.pro.android.data;

import com.protectstar.timelock.pro.android.TimeLockApplication;
import com.protectstar.timelock.pro.android.TimeLockApplication.FolderItemViewData2;

public class TimeLockFolder4View implements FolderItemViewData2 {
	
	public TimeLockFolder folder;
	public TimeLockFolder4View(TimeLockFolder folder) {
		this.folder = folder;
	}
	
	@Override
	public int getDataId() {
		return folder.getId();
	}

	@Override
	public int getFolderViewType() {
		if (folder.getId() == TimeLockApplication.SECURED_FOLDER_ID) {
			return 1;
		}
		if (folder.getId() == TimeLockApplication.SPY_FOLDER_ID) {
			return 2;
		}
		if (folder.getId() == TimeLockApplication.NOTE_FOLDER_ID) {
			return 3;
		}
		if (folder.getId() == TimeLockApplication.VOICE_FOLDER_ID) {
			return 4;
		}
		//TODO when add new predefine folder 
		return 0;
	}
	
	@Override
	public String getFolderName() {
		return folder.getName();
	}

}
