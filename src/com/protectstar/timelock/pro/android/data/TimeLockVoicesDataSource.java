package com.protectstar.timelock.pro.android.data;

import java.util.ArrayList;

import meobu.android.base.MeobuSQLiteDataSource;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

public class TimeLockVoicesDataSource  implements MeobuSQLiteDataSource {

	// ----------//----------//----------//----------//----------//

	SQLiteOpenHelper helper;
	public TimeLockVoicesDataSource(Context context) {
		helper = new TimeLockSQLiteOpenHelper(context);
	}
	
	@Override
	public void open() {
		//Log.d("meobudebug", "did open!!!");
	}
	
	@Override
	public void close() {
		helper.close();
	}

	// ----------//----------//----------//----------//----------//

	public ArrayList<TimeLockVoice> retrieve(int from, int size) {
		ArrayList<TimeLockVoice> contents = new ArrayList<TimeLockVoice>();
		SQLiteDatabase db = null;
		Cursor cursor = null;

		try {
			db = helper.getReadableDatabase();
			String query = SQLiteQueryBuilder.buildQueryString(false,
					TimeLockSQLiteOpenHelper.TABLE_VOICES,
					TimeLockVoice.NOTE_KEYS, null, null, null,
					TimeLockVoice.NOTE_PRIMARY + " desc", from + ", " + size);
			cursor = db.rawQuery(query, null);
	
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				TimeLockVoice content = TimeLockVoice.fromCursor(cursor);
				contents.add(content);
				cursor.moveToNext();
			}
		} catch (Exception e) {
		} finally {
			if (cursor != null) try { cursor.close(); } catch (Exception e) { }
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return contents;
	}
	
	public TimeLockVoice get(long id) {
		TimeLockVoice note = null;
		SQLiteDatabase db = null;
		Cursor cursor = null;
		
		try {
			db = helper.getReadableDatabase();
			String query = SQLiteQueryBuilder.buildQueryString(false,
					TimeLockSQLiteOpenHelper.TABLE_VOICES, TimeLockVoice.NOTE_KEYS,
					TimeLockVoice.NOTE_PRIMARY + "=" + id, null, null,
					TimeLockVoice.NOTE_PRIMARY + " desc", null);
			cursor = db.rawQuery(query, null);
			
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				note = TimeLockVoice.fromCursor(cursor);
				cursor.moveToNext();
			}
		} catch (Exception e) {
		} finally {
			if (cursor != null) try { cursor.close(); } catch (Exception e) { }
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return note;
	}
	
	public long put(TimeLockVoice content) {
		SQLiteDatabase db = null;
		long id = -1;
		
		try {
			db = helper.getWritableDatabase();
			ContentValues values = content.toContentValues();
			id = db.insert(TimeLockSQLiteOpenHelper.TABLE_VOICES, null, values);
			if (id >= 0) {
				content.setId(id);
			}
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return id;
	}
	
	public int update(TimeLockVoice content) {
		int count = 0;
		SQLiteDatabase db = null;
		
		try {
			db = helper.getWritableDatabase();
			ContentValues values = content.toContentValues();
			count = db.update(TimeLockSQLiteOpenHelper.TABLE_VOICES, values, TimeLockVoice.NOTE_PRIMARY + "=" + content.getId(), null);
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return count;		
	}
	
	public int remove(TimeLockVoice content) {
		int count = 0;
		SQLiteDatabase db = null;
		
		try {
			db = helper.getWritableDatabase();
			String whereClause = TimeLockVoice.NOTE_PRIMARY + "=" + content.getId();
			String[] whereArgs = null;
			count = db.delete(TimeLockSQLiteOpenHelper.TABLE_VOICES, whereClause, whereArgs);
			if (count <= 0) {
				Log.e("meobuerror", "###delete record FAILED, don't know why? id:" + content.getId());
			}
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return count;
	}
	
	public int removeAll() {
		SQLiteDatabase db = null;
		int count = 0;
		
		try {
			db = helper.getWritableDatabase();
			count = db.delete(TimeLockSQLiteOpenHelper.TABLE_VOICES, null, null);
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		
		return count;
	}
	
	public int remove(ArrayList<TimeLockVoice> contents) {
		SQLiteDatabase db = null;
		int result = 0;
		
		try {
			db = helper.getWritableDatabase();
			for (TimeLockVoice content : contents) {
				try {
					String whereClause = TimeLockVoice.NOTE_PRIMARY + "=" + content.getId();
					String[] whereArgs = null;
					result += db.delete(TimeLockSQLiteOpenHelper.TABLE_VOICES, whereClause, whereArgs);
				} catch (Exception e) {}
			}
		} catch (Exception e) {
		} finally {
			if (db != null) try { db.close(); } catch (Exception e) { }
		}
		return result;
	}
//	public SQLiteDatabase doOpen() {
//		return helper.getWritableDatabase();
//	}
//	public void doClose(SQLiteDatabase db) {
//		db.close();
//	}
//	public int doRemove(int id, SQLiteDatabase db) {
//		String whereClause = TimeLockVoice.NOTE_PRIMARY + "=" + id;
//		String[] whereArgs = null;
//		int result = db.delete(TimeLockSQLiteOpenHelper.TABLE_VOICES, whereClause, whereArgs);
//		if (result <= 0) {
//			Log.e("meobuerror", "###delete record FAILED, don't know why? id:" + id);
//		}
//		return result;
//	}
	
}