package com.protectstar.timelock.pro.android.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TimeLockSQLiteOpenHelper extends SQLiteOpenHelper {
	
	//----------//----------//----------//----------//----------//

	private static final String DATABASE_NAME = "safe.db";
	private static final int DATABASE_VERSION = 6;

	public TimeLockSQLiteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	//----------//----------//----------//----------//----------//

	//1
	public static final String TABLE_CONTENTS = "contents";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_PATH = "path";
	public static final String COLUMN_THUMBNAIL = "thumbnail";
	public static final String COLUMN_DATE = "date";
	//2
	public static final String COLUMN_FILEEXT = "fileext";
	public static final String COLUMN_FOLDER = "folder";
	public static final String TABLE_FOLDERS = "folders";
	public static final String COLUMN_NAME = "name";
	//3
	public static final String TABLE_NOTES = "notes";
	public static final String COLUMN_CONTENT = "content";
	//4
	public static final String COLUMN_WHERE_OLD = "where";//WRONG
	//5
	public static final String COLUMN_WHERE = "location";
	//6
	public static final String TABLE_VOICES = "voicememo";
	
	
	// Database creation sql statement //1
	private static final String DATABASE_CREATE =
			"create table " + TABLE_CONTENTS
			+ "("
					+ COLUMN_ID + " integer primary key autoincrement, "
					+ COLUMN_TYPE + " integer not null, "
					+ COLUMN_PATH + " text not null, "
					+ COLUMN_THUMBNAIL + " text not null, "
					+ COLUMN_DATE + " integer not null, "
					+ COLUMN_FILEEXT + " text default null, "
					+ COLUMN_NAME + " text default null, "
					+ COLUMN_FOLDER + " integer default 0, "
					+ COLUMN_WHERE + " integer default 0"
			+");";
	//2
	private static final String DATABASE_CREATE_FOLDERS =
			"create table " + TABLE_FOLDERS
			+ "("
					+ COLUMN_ID + " integer primary key autoincrement, "
					+ COLUMN_NAME + " text default null, "
					+ COLUMN_FOLDER + " integer default 0"
			+");";
	//3
	private static final String DATABASE_CREATE_NOTES =
			"create table " + TABLE_NOTES
			+ "("
					+ COLUMN_ID + " integer primary key autoincrement, "
					+ COLUMN_DATE + " integer not null, "
					+ COLUMN_NAME + " text default null, "
					+ COLUMN_CONTENT+ " text default null"
			+");";
	//6
	private static final String DATABASE_CREATE_VOICES = 
			"create table " + TABLE_VOICES +
			"(" +
					COLUMN_ID + " integer primary key autoincrement, " +
					COLUMN_DATE + " integer not null, " +
					COLUMN_NAME + " text not null, " +
					COLUMN_PATH + " text not null" +
			");";

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
		db.execSQL(DATABASE_CREATE_FOLDERS);
		db.execSQL(DATABASE_CREATE_NOTES);
		db.execSQL(DATABASE_CREATE_VOICES);
	}
	
	//----------//----------//----------//----------//----------//

	private static final String DATABASE_UPGRADE_1to2_1 = "ALTER TABLE " + TABLE_CONTENTS + " ADD COLUMN " + COLUMN_FOLDER + " integer default 0;";
	private static final String DATABASE_UPGRADE_1to2_2 = "ALTER TABLE " + TABLE_CONTENTS + " ADD COLUMN " + COLUMN_FILEEXT + " text default null;";
	private static final String DATABASE_UPGRADE_1to2_3 = "ALTER TABLE " + TABLE_CONTENTS + " ADD COLUMN " + COLUMN_NAME + " text default null;";
	private static final String DATABASE_UPGRADE_3to4 = "ALTER TABLE " + TABLE_CONTENTS + " ADD COLUMN " + COLUMN_WHERE_OLD + " integer default 0;";
	private static final String DATABASE_UPGRADE_4to5 = "ALTER TABLE " + TABLE_CONTENTS + " ADD COLUMN " + COLUMN_WHERE + " integer default 0;";
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion < 2) {
			db.execSQL(DATABASE_UPGRADE_1to2_1);
			db.execSQL(DATABASE_UPGRADE_1to2_2);
			db.execSQL(DATABASE_UPGRADE_1to2_3);
			db.execSQL(DATABASE_CREATE_FOLDERS);
		}
		if (oldVersion < 3) {
			db.execSQL(DATABASE_CREATE_NOTES);
		}
		if (oldVersion < 4) {
			try { db.execSQL(DATABASE_UPGRADE_3to4); } catch (Exception e) { }
		}
		if (oldVersion < 5) {
			try { db.execSQL(DATABASE_UPGRADE_4to5); } catch (Exception e) { }
		}
		if (oldVersion < 6) {
			db.execSQL(DATABASE_CREATE_VOICES);
		}
	}
	
	//----------//----------//----------//----------//----------//

}
