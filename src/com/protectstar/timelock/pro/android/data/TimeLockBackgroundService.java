package com.protectstar.timelock.pro.android.data;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

public class TimeLockBackgroundService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null && intent.getExtras() != null) {
			doExecuteASync(intent.getExtras());
		}
		return super.onStartCommand(intent, flags, startId);
	}
	
	private void doExecuteASync(Bundle extras) {
		
	}
	
//	private void doBroadcastResult() {
//		//TODO don't know how to broadcast
//	}

}
