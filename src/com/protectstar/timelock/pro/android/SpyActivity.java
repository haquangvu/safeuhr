package com.protectstar.timelock.pro.android;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.protectstar.safeuhr.android.R;

public class SpyActivity extends TimeLockActivity {

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spycamera);
		setupClickable();
	}
	
	boolean activated;
	@Override
	protected void onResume() {
		super.onResume();
		TimeLockApplication app = (TimeLockApplication)getApplication();
		activated = app.getSpyCameraActivated();
		updateViews(activated);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	private void setupClickable() {
		findViewById(R.id.spycamActivated).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				activated = !activated;
				((TimeLockApplication)getApplication()).setSpyCameraActivated(activated);
				updateActivated(activated);
			}
		});
	}
	
	//----------//----------//----------//----------//----------//
	
	private void updateViews(boolean activated) {
		updateActivated(activated);
	}
	
	private Drawable activatedDrawable, unactivatedDrawable;
	private Drawable getActivatedDrawable() {
		if (activatedDrawable == null) {
			activatedDrawable = getResources().getDrawable(R.drawable.switch_on_big);
		}
		return activatedDrawable;
	}
	private Drawable getUnactivatedDrawable() {
		if (unactivatedDrawable == null) {
			unactivatedDrawable = getResources().getDrawable(R.drawable.switch_off_big);
		}
		return unactivatedDrawable;
	}
	private void updateActivated(boolean activated) {
		ImageView view = (ImageView)findViewById(R.id.spycamActivated);
		if (activated) {
			view.setImageDrawable(getActivatedDrawable());
		} else {
			view.setImageDrawable(getUnactivatedDrawable());
		}
	}
	
	//----------//----------//----------//----------//----------//
	
}
