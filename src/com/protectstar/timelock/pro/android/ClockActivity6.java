package com.protectstar.timelock.pro.android;

import java.io.File;
import java.io.FileOutputStream;

import org.json.JSONArray;

import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.TimeLockSettingDataSource;
import com.protectstar.timelock.pro.android.spy.SpyCameraUtil;
import com.protectstar.timelock.pro.android.spy.SpyCameraUtil.SpyCameraInfo;

/**
 * SpyCamera: capture photo at wrong code
 * @require	a SurfaceView somewhere user cannot see
 * @author meobu
 */
public class ClockActivity6 extends ClockActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sh = null;
		postSurfacingCapture = false;
		SurfaceView sv = (SurfaceView)findViewById(R.id.hiddenSurface);
		sv.getHolder().addCallback(surfaceCallback);
	}
	
	final Object surfacingLock = new Object();
	SurfaceHolder sh;
	private final Callback surfaceCallback = new Callback() {
		@Override
		public void surfaceCreated(SurfaceHolder paramSurfaceHolder) {
			Log.d("meobudebug", "surfaceholder");
			boolean shouldCapture = false;
			synchronized (surfacingLock) {
				sh = paramSurfaceHolder;
				if (postSurfacingCapture) {
					shouldCapture = true;
					postSurfacingCapture = false;
				}
			}
			if (shouldCapture) {
				captureASAP();
			}
		}
		@Override public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder) {
			synchronized (surfacingLock) {
				sh = null;
			}
		}
		@Override public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) { }
	};
	
	boolean postSurfacingCapture = false;
	@Override
	protected void postPasswordFail() {
		if (!((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]) ||
				!((TimeLockApplication)getApplication()).getSpyCameraActivated()) {
			Log.d("meobudebug", "postPasswordFail: " + ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[4]) +
					" " + ((TimeLockApplication)getApplication()).getSpyCameraActivated());
			return;
		}
		boolean shouldCapture = false;
		synchronized (surfacingLock) {
			if (sh == null) {
				postSurfacingCapture = true;
			} else {
				shouldCapture = true;
			}
		}
		if (shouldCapture) {
			captureASAP();
		}
	}
	
	final Object doingCaptureLock = new Object();
	boolean doingCapture;
	SpyCameraInfo info;
	protected void captureASAP() {
		info = SpyCameraUtil.getFaceCamera(this);
		if (info.id < 0) {
			Log.d("meobudebug", "no spy camera");
			return;
		}
		Log.d("meobudebug", "has camera");
		boolean shouldStart = false;
		synchronized (doingCaptureLock) {
			if (!doingCapture) {
				doingCapture = true;
				shouldStart = true;
			}
		}
		if (shouldStart) {
			capturingCancel = false;
			new Thread(doCapture).start();
		}
	}
	
	final Object capturingLock = new Object();
	boolean capturingCancel;
	Camera camera;
	private final Runnable doCapture = new Runnable() {
		@Override
		public void run() {
			try {
				Camera cam = Camera.open(info.id);
				setCameraOrientation(cam);
				cam.setPreviewDisplay(sh);
				cam.startPreview();
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					cam.enableShutterSound(false);
				}
				boolean shouldTake = false;
				synchronized (capturingLock) {
					if (!capturingCancel) {
						camera = cam;
						shouldTake = true;
					}
				}
				if (shouldTake) {
					Log.d("meobudebug", "takePicture");
					cam.takePicture(null, null, captureCallback);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	private void setCameraOrientation(Camera camera) {
		//find rotate
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {//TODO fix orientation
		    case Surface.ROTATION_0: degrees = 0; break; //Natural orientation
	        case Surface.ROTATION_90: degrees = 270; break; //Landscape left
	        case Surface.ROTATION_180: degrees = 180; break;//Upside down
	        case Surface.ROTATION_270: degrees = 90; break;//Landscape right
	    }
		int rotate = (info.orientation - degrees + 360) % 360;
		//set param
		Camera.Parameters params = camera.getParameters();
		params.setRotation(rotate); 
		camera.setParameters(params);
	}
	private final Camera.PictureCallback captureCallback = new Camera.PictureCallback() {
		@Override
		public void onPictureTaken(byte[] img, Camera cam) {
			//finish capturing
			try {
				cam.stopPreview();
				cam.release();
				camera = null;
			} catch (Exception e) {}
			doSave(img);
			//finish doingCapture
			synchronized (doingCaptureLock) {
				doingCapture = false;
			}
		}
	};
	
	private void doSave(final byte[] img) {
		if (img == null) return;
		Log.d("meobudebug", "doSave: " + img.length);
		new Thread(new Runnable() {
			@Override
			public void run() {
				String path = null;
				try {
					//find somewhere to store
					File file = new File(ClockActivity6.this.getApplicationContext().getFilesDir(), Long.toString(System.currentTimeMillis()));
					//transfer to file
					if (file.exists()) {
						file.createNewFile();
					}
					FileOutputStream fos = new FileOutputStream(file);
					fos.write(img);
					fos.flush();
					fos.close();
					//add to list
					path = file.getAbsolutePath();
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (path == null) return;
				try {
					String intrudersString = getApplication().getSharedPreferences("intruder", 0).getString("intruders", "[]");
					String intrudersStringNew = new JSONArray(intrudersString).put(path).toString();
					getApplication().getSharedPreferences("intruder", 0).edit().putString("intruders", intrudersStringNew).commit();
					Log.d("meobudebug", "instruders: " + intrudersStringNew);
				} catch (Exception e) {}
			}
		}).start();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		synchronized (surfacingLock) {
			postSurfacingCapture = false;
		}
		boolean shouldRelease = false;
		synchronized (capturingLock) {
			capturingCancel = true;
			if (camera != null) {
				shouldRelease = true;
			}
		}
		if (shouldRelease) {
			try {camera.stopPreview();camera.release();} catch (Exception e) {}
		}
	}
	
}
