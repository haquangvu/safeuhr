package com.protectstar.timelock.pro.android;

import java.text.SimpleDateFormat;
import java.util.Date;

import meobu.android.base.MeobuSQLiteDataSource;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.data.TimeLockNote;
import com.protectstar.timelock.pro.android.data.TimeLockNotesDataSource;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityNoteDetail extends TimeLockActivity {
	
	//----------//----------//----------//----------//----------//
	
	public static Intent createIntent(Context context, long id) {
		Intent intent = new Intent(context, ActivityNoteDetail.class);
		intent.putExtra("noteid", id);
		return intent;
	}
	
	//----------//----------//----------//----------//----------//

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note_edit);
		setupButtons();
	}
	
	@Override
	public void onBackPressed() {
		if (current.getDate() > original.getDate()) {
			DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					goBack();
				}
			};
			new AlertDialog.Builder(this)
				.setMessage(R.string.note_item_notsave)
				.setNegativeButton(R.string.btn_discard, onClick)
				.setPositiveButton(R.string.btn_cancel, null)
				.create().show();
		} else {
			goBack();
		}
	}
	
	TimeLockNote current, original;
	@Override
	protected void onResume() {
		super.onResume();
		long id = -1;
		try { id = getIntent().getLongExtra("noteid", -1); } catch (Exception e) { }
		if (id < 0) {
			finish();
			return;
		}
		if (original != null) {
			current = TimeLockNote.duplicate(original);
			updateNote();
			return;
		}
		loadNote(id);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (taskLoad != null) {
			taskLoad.cancel(true);
			taskLoad = null;
		}
		if (taskSave != null) {
			taskSave.cancel(false);
			taskSave = null;
		}
		if (taskDelete != null) {
			taskDelete.cancel(false);
			taskDelete = null;
		}
	}
	
	//----------//----------//----------//----------//----------//
	
	final SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
	String convertDateLong(long date) {
		return format.format(new Date(date));
	}

	private void setupButtons() {
		((EditText)findViewById(R.id.content)).addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (current != null) {
					current.setDate(System.currentTimeMillis());
				}
			}
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
			@Override public void afterTextChanged(Editable s) { }
		});
		findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String content = ((EditText)findViewById(R.id.content)).getText().toString();
				current.setContentReadable(content);
				saveNote(current);
			}
		});
		findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteNote(original);
				goBack();
			}
		});
	}
	
	private void updateNote() {
		String name = current.getNameReadable();
		if (name == null || name.length() <= 0) name = getString(R.string.note_item_notitle);
		String date = convertDateLong(current.getDate());
		String content = current.getContentReadable();
		if (content == null || content.length() <= 0) content = "";
		((TextView)findViewById(R.id.content)).setText(content);
		((TextView)findViewById(R.id.title)).setText(name);
		((TextView)findViewById(R.id.date)).setText(date);
		current.setDate(original.getDate());
	}
	
	//----------//----------//----------//----------//----------//
	
	AsyncTask<Long, Void, TimeLockNote> taskLoad;
	private void loadNote(long id) {
		taskLoad = new AsyncTask<Long, Void, TimeLockNote>() {
			@Override
			protected TimeLockNote doInBackground(Long... params) {
				TimeLockNote note = getDataSource().get(params[0]);
				if (note.getContent() == null || note.getContent().length() <= 0) {
					note.setContentReadable(null);
				} else {
					note.setContentReadable(note.getContent(), TimeLockApplication.cachePasscode());
				}
				if (note.getName() == null || note.getName().length() <= 0) {
					note.setNameReadable(null);
				} else {
					note.setNameReadable(note.getName(), TimeLockApplication.cachePasscode());
				}
				return note;
			}
			@Override
			protected void onPostExecute(TimeLockNote result) {
				super.onPostExecute(result);
				original = result;
				current = TimeLockNote.duplicate(result);
				if (!isCancelled()) {
					updateNote();
				}
				taskLoad = null;
			}
		};
		executeLong(taskLoad, id);
	}
	
	AsyncTask<Object, Void, Boolean> taskSave;
	private void saveNote(TimeLockNote note) {
		taskSave = new AsyncTask<Object, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Object... params) {
				TimeLockNote note = (TimeLockNote)params[0];
				if (note.getContentReadable() != null && note.getContentReadable().length() > 0) {
					note.setContent(note.getContentReadable(), TimeLockApplication.cachePasscode());
					
					String name = null;
					if (note.getContentReadable().length() <= 20) {
						name = note.getContentReadable();
					} else {
						name = note.getContentReadable().substring(0,20);
					}
					note.setNameReadable(name);
					note.setName(name, TimeLockApplication.cachePasscode());
				} else {
					note.setContentReadable(null);
					note.setContent(null);
					note.setNameReadable(null);
					note.setName(null);
				}
				
				int count = getDataSource().update(note);
				
				return count > 0;
			}
			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				if (result) {
					original = TimeLockNote.duplicate(current);
					if (!isCancelled()) {
						updateNote();
					}
				}
				taskSave = null;
			}
		};
		executeObject(taskSave, note);
	}
	
	AsyncTask<Object, Void, Boolean> taskDelete;
	private void deleteNote(TimeLockNote note) {
		taskDelete = new AsyncTask<Object, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Object... params) {
				int count = getDataSource().remove((TimeLockNote)params[0]);
				return count > 0;
			}
			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				taskDelete = null;
				goBack();
			}
		};
		executeObject(taskDelete, note);
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected boolean supportMeobuSQLite() {
		return true;
	}
	TimeLockNotesDataSource datasource;
	private TimeLockNotesDataSource getDataSource() {
		if (datasource == null) {
			datasource = new TimeLockNotesDataSource(this);
		}
		return datasource;
	}
	@Override
	protected MeobuSQLiteDataSource getMeobuDataSource() {
		return getDataSource();
	}
	
	//----------//----------//----------//----------//----------//
	
}
