package com.protectstar.timelock.pro.android;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import meobu.android.base.MeobuFileTools;
import meobu.android.base.MeobuGalleryHandler;
import meobu.android.base.MeobuListViewHandler;
import meobu.android.base.MeobuSQLiteDataSource;
import meobu.android.base.MeobuGalleryHandler.MeobuGalleryItem;

import com.protectstar.safeuhr.android.R;
import com.protectstar.timelock.pro.android.SettingActivity;
import com.protectstar.timelock.pro.android.TimeLockApplication.ContentViewData;
import com.protectstar.timelock.pro.android.TimeLockApplication.Data;
import com.protectstar.timelock.pro.android.TimeLockApplication.FolderItemViewData;
import com.protectstar.timelock.pro.android.TimeLockApplication.FolderItemViewData2;
import com.protectstar.timelock.pro.android.TimeLockApplication.FoldersViewData;
import com.protectstar.timelock.pro.android.TimeLockApplication.HeaderViewData;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData;
import com.protectstar.timelock.pro.android.TimeLockApplication.ViewData;
import com.protectstar.timelock.pro.android.TimeLockApplication.ItemViewData.ItemViewDataType;
import com.protectstar.timelock.pro.android.TimeLockApplication.ViewData.ViewDataType;
import com.protectstar.timelock.pro.android.data.TimeLockContentsDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockFoldersDataSource;
import com.protectstar.timelock.pro.android.data.TimeLockMediaFileLayer;
import com.protectstar.timelock.pro.android.data.TimeLockNode;
import com.protectstar.timelock.pro.android.data.TimeLockSettingDataSource;

import common.view.FullWidthImageView;
import common.view.OneByOnePercentAsyncTask;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SafeActivity_Full extends TimeLockActivity {
	
	private void goSetting() {
		Intent intent = new Intent(this, SettingActivity.class);
		intent.putExtra("intent_cache_passcode", getCachePasscode());
		startActivity(intent);
		overridePendingTransition(R.anim.hold_fade_in, R.anim.slide_out_to_right);
	}
	
	private void goFolder(int folder) {
		Intent intent = null;
		if (folder == TimeLockApplication.NOTE_FOLDER_ID) {
			intent = new Intent(this, NotesActivity.class);
		} else if (folder == TimeLockApplication.VOICE_FOLDER_ID) {
			intent = new Intent(this, VoicesActivity.class);
		} else {
			intent = new Intent(this, SafeActivity.class);
		}
		intent.putExtra("folder", folder);
		intent.putExtra("intent_cache_passcode", getCachePasscode());
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
	}
	private void goBack() {
		if (getFolder() == 0) {
			((TimeLockApplication)getApplication()).cleanCache();
			finish();
			overridePendingTransition(R.anim.slide_in_from_top, R.anim.hold_fade_out);
			if (((TimeLockApplication)getApplication()).getSoundSafe()) {
				playMeobuSoundPool(6);//reference TimeLockApplication soundpool
			}
		} else {
			finish();
			overridePendingTransition(R.anim.slide_in_from_left,
					R.anim.slide_out_to_right);
		}
	}
	private void goView(int index) {
		Intent intent = new Intent(this, ViewActivity.class);
		intent.putExtra(ViewActivity.PARAM_INDEX_KEY, index);
		intent.putExtra("folder", getFolder());
		intent.putExtra("intent_cache_passcode", getCachePasscode());
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.hold_fade_out);
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected int getFolder() {
		//int defaultFolder = 0;
		return getIntent().getIntExtra("folder", 0);
		//return defaultFolder;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		fromCreate = true;
		super.onCreate(savedInstanceState);
		if (getFolder() == 0) {
			setContentView(R.layout.activity_safe);
//			if (((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[1])) {
//				findViewById(R.id.folder).setVisibility(View.VISIBLE);
//			} else {
//				findViewById(R.id.folder).setVisibility(View.GONE);
//			}
		} else if (getFolder() == TimeLockApplication.SPY_FOLDER_ID) {
			setContentView(R.layout.activity_safe_folder);
			((TextView)findViewById(R.id.title)).setText(R.string.safe_folder_spy_title);
			findViewById(R.id.add).setVisibility(View.GONE);
		} else if (getFolder() == TimeLockApplication.SECURED_FOLDER_ID) {
			setContentView(R.layout.activity_safe_folder);
			((TextView)findViewById(R.id.title)).setText(R.string.safe_folder_secured_title);
		} else {//folder
			setContentView(R.layout.activity_safe_folder);
			String name = getString(R.string.safe_folder_title);
			try {name = ((TimeLockApplication)getApplication()).getHashNodeForSafeActivityFolderName().get(getFolder()).getName();} catch (Exception e) {}
			((TextView)findViewById(R.id.title)).setText(name);
		}
//		if (getFolder() == 0) {
//			findViewById(R.id.folder).setVisibility(View.VISIBLE);
//		} else {
//			findViewById(R.id.folder).setVisibility(View.GONE);
//		}
		setupList();
		setupButton();
		setupSetting();
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		if (fromCreate) {
			fromCreate = false;
		}
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey("cachePasscode")) {
			String cachePasscode = savedInstanceState.getString("cachePasscode");
			if (cachePasscode != null) {
				if (TimeLockApplication.cachePasscode() == null || TimeLockApplication.cachePasscode().length() <= 0) {
					TimeLockApplication.setCachePasscodeFromRestore(cachePasscode);
				}
			}
		}
	}
	
	boolean fromCreate = false;
	@Override
	protected void onResume() {
		super.onResume();
		onChangeMultipleMode(false);
		//update thumbnail size
		doResume();
	}
	
	protected void doResume() {
		doRefreshOnResume();
	}
	
	protected void doRefreshOnResume() {
		final AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Void... params) {
				boolean shouldUpdate = ((TimeLockApplication)getApplication()).updateData(getDataSource(), getDataSourceFolder(), getFolder());
				//calculate
				DisplayMetrics metrics = getResources().getDisplayMetrics();
				TimeLockMediaFileLayer.THUMBNAIL_SIZE = (int)(225.0f * metrics.widthPixels / 1000);
				return shouldUpdate;
			}
			@Override
			protected void onPostExecute(Boolean result) {
				if (result || fromCreate) {
					fromCreate = false;
					refreshListData();
				} else {
					//crash here, maybe because of this activity is destroyed by system
					//check before update, avoid crash, waiting what next?
					refreshListData(false);
				}
				onPostUpdateData();
			}
		};
		executeVoidVoidBoolean(task);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		onHideLoading();
    	hideAddingLoading();
//		if (getLoading().isShowing()) getLoading().dismiss();
		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		onChangeMultipleMode(false);
		if (getLoadingDelete().isShowing()) getLoadingDelete().dismiss();
		if (getLoadingSaveBack().isShowing()) getLoadingSaveBack().dismiss();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("cachePasscode", TimeLockApplication.cachePasscode());
	}
	
	@Override
	public void onBackPressed() {
		if (isMultiple) {
			onChangeMultipleMode(false);
		} else {
			goBack();
		}
		//super.onBackPressed();
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	protected void onPostUpdateData() {
		
	}
	
	ArrayList<ViewData> useList;
	protected void refreshListData() {
		refreshListData(true);
	}
	
	private long useListVersion;
	private void refreshListData(boolean refreshList) {
		TimeLockNode node = ((TimeLockApplication)getApplication()).getNode(getFolder());
		if (node.updatedDate != useListVersion) {
			refreshList = true;
		}
		useListVersion = node.updatedDate;
		ArrayList<ViewData> orgList = node.getListViewData();
		ArrayList<ViewData> useList = new ArrayList<ViewData>(orgList);
		if (useList.size() == 1) useList.clear();
		for (int i=useList.size(); i<5; i++) {
			useList.add(TimeLockApplication.emptyContentViewData());
		}
		if (this.useList != null && this.useList.size() != useList.size()) {
			refreshList = true;
		}
		this.useList = useList;
		if (refreshList) {
			//update whole list
			listHandler.refresh();
		} else {
			//update language only
			ListView list = (ListView)findViewById(R.id.list);
			int start = list.getFirstVisiblePosition();
			for(int i=start, j=list.getLastVisiblePosition();i<=j;i++) {
				if (this.useList.get(i) instanceof HeaderViewData) {
					View view = list.getChildAt(i-start);
					list.getAdapter().getView(i, view, list);
					break;
				}
			}
		}
	}
	private void clearMultiple() {
		for (View view : getSelectedList().values()) {
//			view.setBackgroundColor(0xff545454);
			for (int i=0; i<imgsid.length; i++) {
				View s = view.findViewById(imgsid[i]);
				if (s != null) s.setVisibility(View.INVISIBLE);
			}
		}
		getSelectedList().clear();
		isMultiple = false;
	}
	
	//----------//----------//----------//----------//----------//
	
	private void setupList() {
		listHandler.setListView(this, R.id.list);
	}
	static final int[] imgid = {R.id.img1, R.id.img2, R.id.img3, R.id.img4};
	static final int[] imgsid = {R.id.img1s, R.id.img2s, R.id.img3s, R.id.img4s};
	static final int[] txtid = {R.id.txt1, R.id.txt2, R.id.txt3, R.id.txt4};
	static final int[] imgcontainerid = {R.id.img1container, R.id.img2container, R.id.img3container, R.id.img4container};
	static final int[] listChildrenLayoutIds = {R.layout.activity_safe_empty,
		R.layout.activity_safe_header, R.layout.activity_safe_content,
		R.layout.activity_safe_header, R.layout.activity_safe_contentf};
	final MeobuListViewHandler<ViewData> listHandler = new MeobuListViewHandler<TimeLockApplication.ViewData>(listChildrenLayoutIds) {
		@Override
		public ArrayList<ViewData> getMeobuListData() {
			return useList;//((TimeLockApplication)getApplication()).getListViewData();
		}
		@Override
		public View createMeobuView(int position, int type, ViewGroup parent) {
			View view = super.createMeobuView(position, type, parent);
			if (type == ViewDataType.GroupContent.ordinal() || type == ViewDataType.GroupFolders.ordinal()) {
				DisplayMetrics metrics = getResources().getDisplayMetrics();
				float ratio = metrics.widthPixels * 1.0f / 640;
				int paddingLeftRight = (int)(10*ratio);
				int paddingBottom = (int)(16*ratio);
				for (int i=0; i<imgid.length; i++) {
					view.findViewById(imgid[i]).setPadding(paddingLeftRight, 0, paddingLeftRight, paddingBottom);
					view.findViewById(imgsid[i]).setPadding(paddingLeftRight, 0, paddingLeftRight, paddingBottom);
					View pv = view.findViewById(imgcontainerid[i]);
					pv.setOnClickListener(click);
					pv.setOnLongClickListener(longClick);
				}
			}
			if (type == ViewDataType.GroupFolderHeader.ordinal()) {
				((TextView)view.findViewById(R.id.title)).setText(R.string.safe_folder_title);
			}
			return view;
		}
		@Override
		public void updateMeobuView(int position, int type, View view) {
			ViewData data = (ViewData)getMeobuItem(position);
			switch (ViewDataType.values()[type]) {
			case GroupEmpty: break;//empty: no need update
			case GroupHeader://header: set the date
				TextView title = (TextView)view.findViewById(R.id.title);
				if (data != null && title != null) {
					title.setText(data.getTime());
				}
				break;
			case GroupFolderHeader: break;//folder header
			case GroupContent://item folder
			case GroupFolders://item content
			default:
				if (data == null) {
					for (int i=0; i<imgcontainerid.length; i++) {
						//clear background of selection
						View s = view.findViewById(imgsid[i]);
						if (s != null) s.setVisibility(View.INVISIBLE);
						//view.findViewById(imgcontainerid[i]).setBackgroundColor(0xff545454);
					}
				}
				for (int i=0; i<data.getCount(); i++) {
					Object dataItem = null;
					if (data instanceof ContentViewData) {
						dataItem = ((ContentViewData)data).getItems().get(i);
					} else if (data instanceof FoldersViewData) {
						dataItem = ((FoldersViewData)data).getFolderItems().get(i);
					}
					if (dataItem == null) continue;
					int id = 0;//negative number = folder, positive number = content
					if (dataItem instanceof ItemViewData) {
						id = ((ItemViewData)dataItem).getDataId();
					} else if (dataItem instanceof FolderItemViewData) {
						id = -((FolderItemViewData)dataItem).getDataId();
					}
					
					View container = view.findViewById(imgcontainerid[i]);
					container.setVisibility(View.VISIBLE);
					container.setTag(id);
					if (getSelectedList().containsKey(id)) {
						//selected
//						container.setBackgroundColor(0xffffffff);
						View s = container.findViewById(imgsid[i]);
						if (s != null) s.setVisibility(View.VISIBLE);
					} else {
						if (id < 0) {
//							container.setBackgroundColor(0);
							View s = container.findViewById(imgsid[i]);
							if (s != null) s.setVisibility(View.INVISIBLE);
						} else {
							View s = container.findViewById(imgsid[i]);
							if (s != null) s.setVisibility(View.INVISIBLE);
//							container.setBackgroundColor(0xff545454);
						}
					}
					
					FullWidthImageView imgview = (FullWidthImageView)view.findViewById(imgid[i]);
					if (dataItem instanceof ItemViewData) {
						switch (ItemViewDataType.values()[((ItemViewData)dataItem).getViewType()]) {
						case Photo:
							imgview.setImageBitmap(getDefaultPhotoBackground());
							break;
						case Video:
						default:
							imgview.setImageBitmap(getDefaultVideoBackground());
							break;
						}
						displayMeobuImage(((ItemViewData)dataItem).getViewThumbnailPath(), imgview);
					} else if (dataItem instanceof FolderItemViewData) {
						switch(((FolderItemViewData)dataItem).getFolderViewType()) {
						case 1:
							imgview.setImageBitmap(getSecuredFolder());
							break;
						case 2:
							imgview.setImageBitmap(getSpyFolder());
							break;
						case 3:
							imgview.setImageBitmap(getNotesFolder());
							break;
						case 4:
							imgview.setImageBitmap(getVoicesFolder());
							break;
						//TODO add new folder-type here
						default:
							imgview.setImageBitmap(getDefaultFolder());
						};
					}
					
					View txt = view.findViewById(txtid[i]);
					if (txt != null && txt instanceof TextView && dataItem instanceof FolderItemViewData2) {
						switch(((FolderItemViewData)dataItem).getFolderViewType()) {
						case 1:
						case 2:
						case 3:
						case 4:
							txt.setVisibility(View.INVISIBLE);
							break;
						default:
							txt.setVisibility(View.VISIBLE);
							((TextView)txt).setText(((FolderItemViewData2)dataItem).getFolderName());
							break;
						};						
					}
				}
				for (int i=data.getCount(); i<imgid.length; i++) {
					view.findViewById(imgcontainerid[i]).setVisibility(View.INVISIBLE);
				}
				break;
			}
		}
	};
	Bitmap defaultFolder, securedFolder, spyFolder, notesFolder, voicesFolder;
	Bitmap getDefaultFolder() {
		if (defaultFolder == null) {
			defaultFolder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_folder_normal);
		}
		return defaultFolder;
	}
	Bitmap getSecuredFolder() {
		if (securedFolder == null) {
			securedFolder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_folder_secured);
		}
		return securedFolder;
	}
	Bitmap getSpyFolder() {
		if (spyFolder == null) {
			spyFolder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_folder_intruder);
		}
		return spyFolder;
	}
	Bitmap getNotesFolder() {
		if (notesFolder == null) {
			notesFolder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_folder_notes);
		}
		return notesFolder;
	}
	Bitmap getVoicesFolder() {
		if (voicesFolder == null) {
			voicesFolder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_folder_voice);
		}
		return voicesFolder;
	}
	Bitmap defaultPhotoBackground, defaultVideoBackground;
	Bitmap getDefaultPhotoBackground() {
		if (defaultPhotoBackground == null) {
			defaultPhotoBackground = BitmapFactory.decodeResource(getResources(), R.drawable.default_img);
		}
		return defaultPhotoBackground;
	}
	Bitmap getDefaultVideoBackground() {
		if (defaultVideoBackground == null) {
			defaultVideoBackground = BitmapFactory.decodeResource(getResources(), R.drawable.default_video);
		}
		return defaultVideoBackground;
	}
//	Bitmap audioIcon;
//	Bitmap getAudioIcon() {
//		if (audioIcon == null) {
//			audioIcon = BitmapFactory.decodeResource(getResources(), R.drawable.micro_icon);
//		}
//		return audioIcon;
//	}
	
	//----------//----------//----------//----------//----------//
	
	private boolean isMultiple = false;
	private Hashtable<Integer, View> selectedList = null;
	private Hashtable<Integer, View> getSelectedList() {
		if (selectedList == null) {
			selectedList = new Hashtable<Integer, View>();
		}
		return selectedList;
	}
	
	private void onChangeMultipleMode(boolean isMultiple) {
		if (this.isMultiple != isMultiple) {
			doChangeMultipleMode(isMultiple);
			this.isMultiple = isMultiple;
		}
		if (!isMultiple) {
			if (getFolder() != TimeLockApplication.SPY_FOLDER_ID) {
				findViewById(R.id.add).setVisibility(View.VISIBLE);
			}
//			if (getFolder() == 0 && ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[1])) {
//				findViewById(R.id.folder).setVisibility(View.VISIBLE);
//			}
			findViewById(R.id.save).setVisibility(View.GONE);
			findViewById(R.id.delete).setVisibility(View.GONE);
		} else {
			if (getFolder() != TimeLockApplication.SPY_FOLDER_ID) {
				findViewById(R.id.add).setVisibility(View.GONE);
			}
//			if (getFolder() == 0 && ((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[1])) {
//				findViewById(R.id.folder).setVisibility(View.GONE);
//			}
			findViewById(R.id.save).setVisibility(View.VISIBLE);
			findViewById(R.id.delete).setVisibility(View.VISIBLE);
		}
	}
	private void doChangeMultipleMode(boolean isMultiple) {
		Toast.makeText(this, "Multiple Selection Mode: " + (isMultiple?"ON":"OFF"), Toast.LENGTH_SHORT).show();
		if (!isMultiple) {
			clearMultiple();
		}
	}
	
	private final View.OnLongClickListener longClick = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			if (isMultiple) {
				return false;
			}
			int id = getViewTag(v);
			if (id < 0) {
				return editFolder(-id);
			}
			getSelectedList().put(id, v);
			onChangeMultipleMode(true);
//			v.setBackgroundColor(0xffffffff);
			for (int i=0; i<imgsid.length; i++) {
				View s = v.findViewById(imgsid[i]);
				if (s != null) s.setVisibility(View.VISIBLE);
			}
			return true;
		}
	};
	
	private final View.OnClickListener click = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == null || view.getVisibility() != View.VISIBLE) return;
			int id = getViewTag(view);
			if (isMultiple) {
				if (id < 0) {//click on folder 
					onSelectFolderInMultipleMode(-id);
					return;
				}
				if (!getSelectedList().containsKey(id)) {
					getSelectedList().put(id, view);
					//refresh
					for (int i=0; i<imgsid.length; i++) {
						View s = view.findViewById(imgsid[i]);
						if (s != null) s.setVisibility(View.VISIBLE);
					}
//					view.setBackgroundColor(0xffffffff);
					if (getSelectedList().isEmpty()) {
						onChangeMultipleMode(false);
					}
					return;
				}
				getSelectedList().remove(id);

				for (int i=0; i<imgsid.length; i++) {
					View s = view.findViewById(imgsid[i]);
					if (s != null) s.setVisibility(View.INVISIBLE);
				}
//				view.setBackgroundColor(0xff545454);
				if (getSelectedList().isEmpty()) {
					onChangeMultipleMode(false);
				}
				return;
			}
			SafeActivity_Full.this.onClick(id);
		}
	};
	
	//----------//----------//----------//----------//----------//
	
	protected void onClick(int id) {
		Log.d("meobudebug", "clicked on id: " + id);
		if (id < 0) {
			goFolder(-id);
			return;
		}
		//find index
		ArrayList<Data> data = ((TimeLockApplication)getApplication()).getListData(getFolder());
//		StringBuilder logBuilder = new StringBuilder();
//		logBuilder.append("ids: ");
//		for (int i=0; i<data.size(); i++) {
//			logBuilder.append(data.get(i).getDataId() + " ");
//		}
//		Log.d("meobudebug", logBuilder.toString());
		int index = -1;
		for (int i=0; i<data.size(); i++) {
			if (data.get(i).getDataId() == id) {
				index = i;
				break;
			}
		}
//		Log.d("meobudebug", "id: " + id + "   index: " + index);
		//go
		if (index < 0) return;
		goView(index);
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	@Override
	protected boolean supportMeobuShakeDetection() {
		return true;
	}
	@Override
	protected void onMeobuShakeDetected() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private void setupButton() {
		findViewById(R.id.add).setOnClickListener(addClick);
		findViewById(R.id.folder).setOnClickListener(folderClick);
		findViewById(R.id.delete).setOnClickListener(deleteClick);
		findViewById(R.id.save).setOnClickListener(saveClick);
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private final View.OnClickListener folderClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			onFolderClick();
		}
	};
	private void onFolderClick() {
		createFolder();
	}
	
	private final View.OnClickListener addClick = new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			onAddClick();
		}
	};
	private void onAddClick() {
		String[] texts = getResources().getStringArray(R.array.safe_add_options);
		if (getFolder() != TimeLockApplication.SPY_FOLDER_ID &&
				((TimeLockApplication)getApplication()).getMeobuSettingsBoolean(TimeLockSettingDataSource.keys[1])) {
		} else {
			String[] temp = texts;
			texts = new String[temp.length-1];
			for (int i=0; i<texts.length; i++) {
				texts[i] = temp[i];
			}
		}
		final int[] icons = new int[]{R.drawable.icon_import_photo, R.drawable.icon_import_video, R.drawable.icon_import_capture, R.drawable.icon_import_record, R.drawable.icon_import_folder};
		ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, android.R.id.text1, texts){
	        public View getView(int position, View convertView, ViewGroup parent) {
	            View v = super.getView(position, convertView, parent);
	            TextView tv = (TextView)v.findViewById(android.R.id.text1);
	            tv.setBackgroundColor(0xff545454);
	            tv.setTextColor(0xfff7f7f7);
	            tv.setCompoundDrawablesWithIntrinsicBounds(icons[position], 0, 0, 0);
	            int dp10 = (int) (10 * getResources().getDisplayMetrics().density + 0.5f);
	            tv.setCompoundDrawablePadding(dp10);
	            return v;
	        }
	    };
		new AlertDialog.Builder(this).setTitle(R.string.safe_add_message)
		.setAdapter(adapter, new DialogInterface.OnClickListener() {
//		.setItems(R.array.safe_add_options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onHideLoading();
				if (which == 0) {
					startMeobuGalleryPhotosPick();
				} else if (which == 1) {
					startMeobuGalleryVideosPick();
				} else if (which == 2) {
					startMeobuCameraPhotoCapture();
				} else if (which == 3) {
					startMeobuCameraVideoCapture();
				} else {//if (which == 4) {
					createFolder();
				}
			}
		})
		.create().show();
		onShowLoading();
	}
	
	@Override
	public boolean supportMeobuGalleryDeleteAfterPicking() {
		return true;
	}
	@Override
	protected void onMeobuGalleryPhotosPicked(Uri[] uri) {
		startAddItem(uri, ItemViewDataType.Photo.ordinal());
	}
	@Override
	protected void onMeobuGalleryPhotosPicked(File[] file) {
		startAddItem(file, ItemViewDataType.Photo.ordinal());
	}
	@Override
	protected void onMeobuGalleryPhotosPicked(MeobuGalleryItem[] item) {
		startAddItem(item, ItemViewDataType.Photo.ordinal(), false);
	}
	@Override
	protected void onMeobuGalleryVideosPicked(Uri[] uri) {
		startAddItem(uri, ItemViewDataType.Video.ordinal());
	}
	@Override
	protected void onMeobuGalleryVideosPicked(File[] file) {
		startAddItem(file, ItemViewDataType.Video.ordinal());
	}
	@Override
	protected void onMeobuCameraPhotoCaptured(File file) {
		File[] files = new File[1];
		files[0] = file;
		startAddItem(files, ItemViewDataType.Photo.ordinal(), true);
	}
	@Override
	protected void onMeobuCameraVideoCaptured(File file) {
		File[] files = new File[1];
		files[0] = file;
		startAddItem(files, ItemViewDataType.Video.ordinal(), true);
	}
	@Override
	protected void onMeobuCameraVideoCaptured(Uri uri) {
		Uri[] uris = new Uri[1];
		uris[0] = uri;
		startAddItem(uris, ItemViewDataType.Video.ordinal(), true);
	}
	//----------//----------//----------//----------//----------//
	private AlertDialog loading;
	protected AlertDialog getLoading() {
		if (loading == null) {
			loading = new AlertDialog.Builder(this).setMessage(R.string.safe_add_processing).setCancelable(false).create();
		}
		return loading;
	}
	protected void showAddingLoading() {
		getLoading().setMessage(getString(R.string.safe_add_processing));
		getLoading().show();
		onShowLoading();
	}
	protected void hideAddingLoading() {
		if (getLoading().isShowing()) {
			getLoading().dismiss();
			onHideLoading();
		}
	}
	//----------//----------//----------//----------//----------//
	private void startAddItem(File[] mediaFiles, int type) {
		startAddItem(mediaFiles, type, false);
	}
	private void startAddItem(final File[] mediaFiles, final int type, final boolean delete) {
		startAddItem(mediaFiles, type, delete, getFolder());
	}
	//StringBuilder messageFile;
	protected void startAddItem(final File[] mediaFiles, final int type, final boolean delete, final int folder) {
		//final AlertDialog loading = new AlertDialog.Builder(this).setMessage(R.string.safe_add_processing).setCancelable(false).create();
		showAddingLoading();
//		getLoading().setMessage(getString(R.string.safe_add_processing));
//		getLoading().show();
//		onShowLoading();
//		if (type == ItemViewDataType.Video.ordinal()) {
//			//keeps screen on
//			//getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//		}
		final OneByOnePercentAsyncTask<Void, Integer> task = new OneByOnePercentAsyncTask<Void, Integer>() {
		//};
		//final AsyncTask<Void, Integer, Integer> task = new AsyncTask<Void, Integer, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				if (mediaFiles == null) return 0;
				//messageFile = new StringBuilder("File: ");
				int errorCount = 0;
				total = mediaFiles.length;
				boolean external = ((TimeLockApplication)getApplication()).getExternalStoreEnable();
				for (int i=0; i<mediaFiles.length; i++) {
					index = i+1;
					//messageFile.append(1).append(' ');
					doPublishProgressFull(-1,-1);
					//messageFile.append(2).append(' ');
					File file = mediaFiles[i];
					String[] paths = TimeLockMediaFileLayer.generateSecretPaths(SafeActivity_Full.this, external);
					//messageFile.append(3).append(' ');
					try {
						//doAddOneItem(file, type, paths);
						//messageFile.append(4).append(' ');
						TimeLockMediaFileLayer.storeMedia1(file, type, paths, this);
						//messageFile.append(5).append(' ');
						((TimeLockApplication)getApplication()).addData(paths, type, getDataSource(), getDataSourceFolder(), file.lastModified(), folder);
//						long date = file.lastModified();
//						TimeLockMediaFileLayer.storeMedia1(file, type, paths);
//						((TimeLockApplication)getApplication()).addData(paths, type, getDataSource(), date);
						
						if (delete) {
							try {file.delete();} catch (Exception e) {}
						}
						
					} catch (Exception e) {
						//messageFile.append(6).append(' ').append(e.toString());
						Log.w("meobuwarning", "Create file fail: " + e.getMessage());
						e.printStackTrace();
						errorCount++;
						//clean
						try {new File(paths[0]).delete();} catch (Exception ee) {}
						try {new File(paths[1]).delete();} catch (Exception ee) {}
					}
				}
				return errorCount;
			}
			
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (values[0] > 8888) {
					//messageFile.append(values[1]).append(' ');
					return;
				}
				if (getLoading().isShowing()) {
					String message = String.format(getString(R.string.safe_add_percentage_processing), values[1], values[2]);
					if (values[3] >= 0 || values[4] >= 0) {
						String value3 = String.format("%.2f", values[3] / 1048576f);
						String value4 = String.format("%.2f", values[4] / 1048576f);
						message = message + "\n" + (values[3]<0?"?":value3) + "/" + (values[4]<0?"?":value4);
					}
					getLoading().setMessage(message);
				}
			}

	        @Override
	        protected void onPostExecute(Integer result) {
	        	hideAddingLoading();
	    		//new AlertDialog.Builder(SafeActivity.this).setMessage(messageFile.toString()).setNeutralButton("OK", null).create().show();
	    		if (result > 0) {
	    			if (result == 1) {
	    				Toast.makeText(SafeActivity_Full.this, R.string.safe_add_notsupport, Toast.LENGTH_SHORT).show();
	    			} else {
		    			String message = String.format(getString(R.string.safe_add_notsupport_count), result);
		    			Toast.makeText(SafeActivity_Full.this, message, Toast.LENGTH_SHORT).show();
	    			}
	    		} else {
		    		refreshListData();
		    		if (!delete && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
		    			DialogInterface.OnClickListener delete = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								for (int i=0; i<mediaFiles.length; i++) {
									try {mediaFiles[i].delete();} catch (Exception e) {}
								}
							}
						};
		    			new AlertDialog.Builder(SafeActivity_Full.this)
		    				.setMessage(R.string.safe_import_delete_source)
		    				.setPositiveButton(R.string.safe_delete_confirm_yes, delete)
		    				.setNegativeButton(R.string.safe_delete_confirm_no, null)
		    				.create().show();
		    		}
	    		}
	        }
		};
		executeVoid(task);
	}
	//----------//----------//----------//----------//----------//
	private void startAddItem(Uri[] mediaUris, int type) {
		startAddItem(mediaUris, type, false);
	}
	private void startAddItem(final Uri[] mediaUris, final int type, final boolean delete) {
		startAddItem(mediaUris, type, delete, getFolder());
	}
	//StringBuilder messageUri;
	protected void startAddItem(final Uri[] mediaUris, final int type, final boolean delete, final int folder) {
		//final AlertDialog loading = new AlertDialog.Builder(this).setMessage(R.string.safe_add_processing).setCancelable(false).create();
		showAddingLoading();
//		getLoading().setMessage(getString(R.string.safe_add_processing));
//		getLoading().show();
//		onShowLoading();
//		if (type == ItemViewDataType.Video.ordinal()) {
//			//keeps screen on
//			//getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//		}
		//FileInputStream[] mediaInputs = new FileInputStream[mediaUris.length];
		//for (int i=0; i<)
		//FileInputStream sourceStream = new FileInputStream(from);
		
		//store, insert & update data, should be done in separate thread
////		findViewById(R.id.list).post(new Runnable() {
////			
////			@Override
////			public void run() {
		final OneByOnePercentAsyncTask<Void, Integer> task = new OneByOnePercentAsyncTask<Void, Integer>() {
			File logTemp = null;
			PrintStream logPS = null;
		//};
		//final AsyncTask<Void, Integer, Integer> task = new AsyncTask<Void, Integer, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				if (mediaUris == null) return 0;
				//messageUri = new StringBuilder("Uri: ");
				int errorCount = 0;
				boolean errorGoogleOffline = false;
				boolean errorFileNotFound = false;
				total = mediaUris.length;
				try {
					logTemp = MeobuFileTools.tempLog(SafeActivity_Full.this);
					logPS = new PrintStream(logTemp);
				} catch (Exception e) { }
				boolean external = ((TimeLockApplication)getApplication()).getExternalStoreEnable();
				for (int i=0; i<mediaUris.length; i++) {
					index = i+1;
					doPublishProgressFull(-1,-1);
					logPS.println("##########Item."+i+"##########" + mediaUris[i]);
					String[] paths = TimeLockMediaFileLayer.generateSecretPaths(SafeActivity_Full.this, external);
					
					long date = 0;
					boolean success = false;
					int fileNotFound = 0;
					{//first try, 1.0.7 code
						ParcelFileDescriptor pfd = null;
						FileDescriptor fd = null;
						FileInputStream fis = null;
						try {
							pfd = getContentResolver().openFileDescriptor(mediaUris[i], "r");
							fd = pfd.getFileDescriptor();
							fis = new FileInputStream(fd);
							TimeLockMediaFileLayer.storeMedia2(fd, fis, type, paths, this);
							success = true;
							Log.d("meobudebug", "import at 1st try");
						} catch (FileNotFoundException e) {
							fileNotFound++;
							if (logPS != null) try { 
							logPS.println("###1st try###");
							e.printStackTrace(logPS);
							} catch (Exception ee) {}
						} catch (Exception e) {
							if (logPS != null) try { 
							logPS.println("###1st try###");
							e.printStackTrace(logPS);
							} catch (Exception ee) {}
						} finally {
							fd = null;
							if (fis != null) { try { fis.close(); } catch (Exception e) { } fis = null; }
							if (pfd != null) { try { pfd.close(); } catch (Exception e) { } pfd = null; }
						}
						if (!success) {
							//clean
							try {new File(paths[0]).delete();} catch (Exception ee) {}
							try {new File(paths[1]).delete();} catch (Exception ee) {}
						}
					}
					
					if (!success) {//second try, 1.0.8 code, file
						if (mediaUris[i].getScheme().contains(ContentResolver.SCHEME_FILE)) {
							try {
								File file = new File(mediaUris[i].getPath());
								if (file.exists()) {
									//messageItems.append(4).append(' ');
									TimeLockMediaFileLayer.storeMedia1(file, type, paths, this);
									date = file.lastModified();
									success = true;
									Log.d("meobudebug", "import at 2nd try");
								}
							} catch (Exception e) {
								if (logPS != null) try { 
								logPS.println("###2nd try###");
								e.printStackTrace(logPS);
								} catch (Exception ee) {}
							}
							if (!success) {
								//clean
								try {new File(paths[0]).delete();} catch (Exception ee) {}
								try {new File(paths[1]).delete();} catch (Exception ee) {}
							}
						}
					}
					
					if (!success) {//third try, 1.0.9 code, with new openAssetFileDescriptor
						AssetFileDescriptor pfd = null;
						FileDescriptor fd = null;
						FileInputStream fis = null;
						try {
							pfd = getContentResolver().openAssetFileDescriptor(mediaUris[i], "r");
							fd = pfd.getFileDescriptor();
							fis = new FileInputStream(fd);
							TimeLockMediaFileLayer.storeMedia2(fd, fis, type, paths, this);
							success = true;
							Log.d("meobudebug", "import at 3rd try");
						} catch (FileNotFoundException e) {
							fileNotFound++;
							if (logPS != null) try { 
							logPS.println("###3rd try###");
							e.printStackTrace(logPS);
							} catch (Exception ee) {}
						} catch (Exception e) {
							if (logPS != null) try { 
							logPS.println("###3rd try###");
							e.printStackTrace(logPS);
							} catch (Exception ee) {}
						} finally {
							fd = null;
							if (fis != null) { try { fis.close(); } catch (Exception e) { } fis = null; }
							if (pfd != null) { try { pfd.close(); } catch (Exception e) { } pfd = null; }
						}
						if (!success) {
							//clean
							try {new File(paths[0]).delete();} catch (Exception ee) {}
							try {new File(paths[1]).delete();} catch (Exception ee) {}
						}
					}
					
					if (!success) {//forth try, 1.0.9 code, use openInputStream
						InputStream is = null;
						try {
							is = getContentResolver().openInputStream(mediaUris[i]);
							File tempFile = File.createTempFile("mbu", null);
							MeobuFileTools.copy(is, tempFile);
							TimeLockMediaFileLayer.storeMedia1(tempFile, type, paths, this);
							success = true;
							Log.d("meobudebug", "import at 4th try");
						} catch (FileNotFoundException e) {
							fileNotFound++;
							if (logPS != null) try { 
							logPS.println("###4th try###");
							e.printStackTrace(logPS);
							} catch (Exception ee) {}
						} catch (Exception e) {
							if (logPS != null) try { 
							logPS.println("###4th try###");
							e.printStackTrace(logPS);
							} catch (Exception ee) {}
						} finally {
							if (is != null) try {is.close();} catch(Exception e) {}
						}
						if (!success) {
							//clean
							try {new File(paths[0]).delete();} catch (Exception ee) {}
							try {new File(paths[1]).delete();} catch (Exception ee) {}
						}
					}
					
					if (!success) {//fifth try, 1.0.9 code, use XXXFileDescriptor to temp file
						if (fileNotFound < 3) {
							InputStream is = null;
							ParcelFileDescriptor pfd = null;
							AssetFileDescriptor afd = null;
							{//first try to open inputstream 1 time
								try {
									pfd = getContentResolver().openFileDescriptor(mediaUris[i], "r");
									is = new FileInputStream(pfd.getFileDescriptor());
								} catch (Exception e) {
									if (logPS != null) try { 
									logPS.println("###5th.1 try###");
									e.printStackTrace(logPS);
									} catch (Exception ee) {}
									if (pfd != null) try {pfd.close();} catch (Exception ee) {}
									if (is != null) try {is.close();} catch(Exception ee) {}
									is = null;
								}
							}
							if (is == null) {//second try to open inputstream 1 time
								try {
									afd = getContentResolver().openAssetFileDescriptor(mediaUris[i], "r");
									is = new FileInputStream(afd.getFileDescriptor());
								} catch (Exception e) {
									if (logPS != null) try { 
									logPS.println("###5th.2 try###");
									e.printStackTrace(logPS);
									} catch (Exception ee) {}
									if (afd != null) try {afd.close();} catch (Exception ee) {}
									if (is != null) try {is.close();} catch(Exception ee) {}
									is = null;
								}
							}
							if (is != null) {
								try {
									File tempFile = File.createTempFile("mbu", null);
									MeobuFileTools.copy(is, tempFile);
									TimeLockMediaFileLayer.storeMedia1(tempFile, type, paths, this);
									success = true;
									Log.d("meobudebug", "import at 5th try");
								} catch (Exception e) {
									if (logPS != null) try { 
									logPS.println("###5th.3 try###");
									e.printStackTrace(logPS);
									} catch (Exception ee) {}
								}
								if (!success) {
									//clean
									try {new File(paths[0]).delete();} catch (Exception ee) {}
									try {new File(paths[1]).delete();} catch (Exception ee) {}
								}
							}
							if (is != null) try {is.close();} catch(Exception ee) {}
							if (pfd != null) try {pfd.close();} catch (Exception ee) {}
							if (afd != null) try {afd.close();} catch (Exception ee) {}
						}
					}
					
					if (success) {
						if (date == 0) {
							try {
								date = MeobuGalleryHandler.queryMediaDate(SafeActivity_Full.this, mediaUris[i]);
							} catch (Exception e) {
								date = System.currentTimeMillis();
							}
						}
						if (date == 0) {
							date = System.currentTimeMillis();
						}
						try {
							((TimeLockApplication)getApplication()).addData(paths, type, getDataSource(), getDataSourceFolder(), date, folder);
						} catch (Exception e) {
							errorCount++;
							if (logPS != null) try { 
								logPS.println("###New problem###");
								e.printStackTrace(logPS);
							} catch (Exception ee) {}
						}
					} else {
						errorCount++;
						if (mediaUris[i].toString().contains("com.google.android.apps")) {
							if (!errorGoogleOffline) {
								errorGoogleOffline = true;
								errorCount += 2508;
							}
						} else if (fileNotFound > 1) {
							if (!errorFileNotFound) {
								errorCount += 5016;
							}
						}
					}
					
					try {//ignore exceptions
						if (delete) {
							try {
								if (mediaUris[i].getScheme().contains("file")) {
									new File(mediaUris[i].getPath()).delete();
								} else {
									long id = -1;
									try { id = ContentUris.parseId(mediaUris[i]); } catch (Exception e) {}
									if (id < 0) {//from somewhere
										getContentResolver().delete(mediaUris[i], null, null);
									} else {
										Uri deleteUri = null;
										if (type == ItemViewDataType.Photo.ordinal()) {
											deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
										} else {
											deleteUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id);
										}
									    getContentResolver().delete(deleteUri, null, null);
									}
								}
							} catch (Exception e) {}
						}
					} catch (Exception e) {}
				}
				
				if (logPS != null) {
					try { logPS.flush(); } catch (Exception e) {}
					try { logPS.close(); } catch (Exception e) {}
					logPS = null;
				}
				return errorCount;
			}
			
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (values[0] > 8888) {
					//messageUri.append(values[1]).append(' ');
					if (logPS != null) try { logPS.println("["+values[1]+"]"); } catch (Exception e) { }
					return;
				}
				if (getLoading().isShowing()) {
					String message = String.format(getString(R.string.safe_add_percentage_processing), values[1], values[2]);
					if (values[3] >= 0 || values[4] >= 0) {
						String value3 = String.format("%.2f", values[3] / 1048576f);
						String value4 = String.format("%.2f", values[4] / 1048576f);
						message = message + "\n" + (values[3]<0?"?":value3) + "/" + (values[4]<0?"?":value4);
					}
					getLoading().setMessage(message);
				}
			}

	        @Override
	        protected void onPostExecute(Integer result) {
	        	hideAddingLoading();
//	    		if (getLoading().isShowing()) {
//	    			getLoading().dismiss();
//	    			onHideLoading();
//		    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//	    		}
	    		//new AlertDialog.Builder(SafeActivity.this).setMessage(messageUri.toString()).setNeutralButton("OK", null).create().show();
	    		if (result > 0) {
	    			boolean notSupport = false;
	    			if (result > 7524) {
	    				new AlertDialog.Builder(SafeActivity_Full.this)
	    					.setMessage(R.string.safe_add_google_offline)
	    					.setNeutralButton(R.string.btn_ok1, null)
	    					.create().show();
	    				result -= 7524;
	    			} else if (result > 5016) {
	    				Toast.makeText(SafeActivity_Full.this, R.string.safe_add_notsupport_filebrowse, Toast.LENGTH_SHORT).show();
	    				result -= 5016;
	    			} else if (result > 2508) {
	    				new AlertDialog.Builder(SafeActivity_Full.this)
	    					.setMessage(R.string.safe_add_google_offline)
	    					.setNeutralButton(R.string.btn_ok1, null)
	    					.create().show();
	    				result -= 2508;
	    			} else if (result == 1) {
	    				notSupport = true;
	    				//Toast.makeText(SafeActivity.this, R.string.safe_add_notsupport, Toast.LENGTH_SHORT).show();
	    			} else {
	    				notSupport = true;
		    			//String message = String.format(getString(R.string.safe_add_notsupport_count), result);
		    			//Toast.makeText(SafeActivity.this, message, Toast.LENGTH_SHORT).show();
	    			}
	    			if (result < mediaUris.length) {
			    		refreshListData();
	    			}
	    			if (notSupport) {
	    				messageImpportSupport(logTemp, result);
	    			}
	    		} else {
		    		refreshListData();
    				//messageImpportSupport(logTemp, result);
		    		if (!delete && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
		    			DialogInterface.OnClickListener delete = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								for (int i=0; i<mediaUris.length; i++) {
									try {
										if (mediaUris[i].getScheme().contains("file")) {
											new File(mediaUris[i].getPath()).delete();
										} else {
											long id = -1;
											try { id = ContentUris.parseId(mediaUris[i]); } catch (Exception e) {}
											if (id < 0) {//from somewhere
												getContentResolver().delete(mediaUris[i], null, null);
											} else {
												Uri deleteUri = null;
												if (type == ItemViewDataType.Photo.ordinal()) {
													deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
												} else {
													deleteUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id);
												}
											    getContentResolver().delete(deleteUri, null, null);
											}
										}
									} catch (Exception e) {}
								}
							}
						};
		    			new AlertDialog.Builder(SafeActivity_Full.this)
		    				.setMessage(R.string.safe_import_delete_source)
		    				.setPositiveButton(R.string.safe_delete_confirm_yes, delete)
		    				.setNegativeButton(R.string.safe_delete_confirm_no, null)
		    				.create().show();
		    		}
	    		}
	        }
		};
		executeVoidVoidInteger(task);
////			}
////		});

	}
	//----------//----------//----------//----------//----------//
	protected void messageImpportSupport(final File logFile, int result) {
		String message = ((result==1)?getString(R.string.safe_add_notsupport_count):String.format(getString(R.string.safe_add_notsupport_count), result));
		DialogInterface.OnClickListener click = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String emailAddress = getString(R.string.settingSupportEmailTo);
				String content = "";
				content += getString(R.string.settingSupportEmailContent1) + android.os.Build.MODEL;
				content += getString(R.string.settingSupportEmailContent3)
						+ android.os.Build.VERSION.RELEASE;
				content += getString(R.string.settingSupportEmailContent4);
				String subject = getString(R.string.settingSupportEmailSubject) + " / " + getString(R.string.safe_import_faile_support_title);
				
	    		Intent emailIntent = new Intent(Intent.ACTION_SEND);
	    		emailIntent.setType("message/rfc822");
	    		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
	    		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
	    		emailIntent.putExtra(Intent.EXTRA_TEXT, content);
	    		emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(logFile));
	    		startActivity(Intent.createChooser(emailIntent, "Send email"));//emailIntent);//
			}
		};
		new AlertDialog.Builder(SafeActivity_Full.this)
			.setMessage(message)
			.setNegativeButton(R.string.btn_ok1, null)
			.setPositiveButton(R.string.safe_import_faile_support, click)
			.create().show();
	}
	//----------//----------//----------//----------//----------//
	//StringBuilder messageItems;
	protected void startAddItem(final MeobuGalleryItem[] mediaItems, final int type, final boolean delete) {
		showAddingLoading();
//		getLoading().setMessage(getString(R.string.safe_add_processing));
//		getLoading().show();
//		onShowLoading();
		//getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		final OneByOnePercentAsyncTask<Void, Integer> task = new OneByOnePercentAsyncTask<Void, Integer>() {
		//};
		//final AsyncTask<Void, Integer, Integer> task = new AsyncTask<Void, Integer, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				if (mediaItems == null) return 0;
				//messageItems = new StringBuilder("File: ");
				int errorCount = 0;
				total = mediaItems.length;
				boolean external = ((TimeLockApplication)getApplication()).getExternalStoreEnable();
				for (int i=0; i<mediaItems.length; i++) {
					if (mediaItems[i] == null) continue;
					index = i+1;
					//messageItems.append(1).append(' ');
					doPublishProgressFull(-1,-1);
					//messageItems.append(2).append(' ');
					String[] paths = TimeLockMediaFileLayer.generateSecretPaths(SafeActivity_Full.this, external);
					//messageItems.append(3).append(' ');
					try {
						File file = new File(mediaItems[i].path);
						//messageItems.append(4).append(' ');
						TimeLockMediaFileLayer.storeMedia1(file, type, paths, this);
						//messageItems.append(5).append(' ');
						((TimeLockApplication)getApplication()).addData(paths, type, getDataSource(), getDataSourceFolder(), mediaItems[i].date, getFolder());
						
						if (delete) {
							try {
								Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, mediaItems[i].id);
							    getContentResolver().delete(deleteUri, null, null);
							} catch (Exception e) {}
							try {file.delete();} catch (Exception e) {}
						}
						
					} catch (Exception e) {
						//messageItems.append(6).append(' ').append(e.toString());
						Log.w("meobuwarning", "Create file fail: " + e.getMessage());
						e.printStackTrace();
						errorCount++;
						//clean
						try {new File(paths[0]).delete();} catch (Exception ee) {}
						try {new File(paths[1]).delete();} catch (Exception ee) {}
					}
				}
				return errorCount;
			}
			
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (values[0] > 8888) {
					//messageItems.append(values[1]).append(' ');
					return;
				}
				if (getLoading().isShowing()) {
					String message = String.format(getString(R.string.safe_add_percentage_processing), values[1], values[2]);
					if (values[3] >= 0 || values[4] >= 0) {
						String value3 = String.format("%.2f", values[3] / 1048576f);
						String value4 = String.format("%.2f", values[4] / 1048576f);
						message = message + "\n" + (values[3]<0?"?":value3) + "/" + (values[4]<0?"?":value4);
					}
					getLoading().setMessage(message);
				}
			}

	        @Override
	        protected void onPostExecute(Integer result) {
	        	hideAddingLoading();
//	    		if (getLoading().isShowing()) {
//	    			getLoading().dismiss();
//	    			onHideLoading();
//		    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//	    		}
	    		//new AlertDialog.Builder(SafeActivity.this).setMessage(messageItems.toString()).setNeutralButton("OK", null).create().show();
	    		if (result > 0) {
	    			if (result == 1) {
	    				Toast.makeText(SafeActivity_Full.this, R.string.safe_add_notsupport, Toast.LENGTH_SHORT).show();
	    			} else {
		    			String message = String.format(getString(R.string.safe_add_notsupport_count), result);
		    			Toast.makeText(SafeActivity_Full.this, message, Toast.LENGTH_SHORT).show();
	    			}
	    		} else {
	    			if (!delete && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
		    			DialogInterface.OnClickListener delete = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								for (int i=0; i<mediaItems.length; i++) {
									try {
										Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, mediaItems[i].id);
									    getContentResolver().delete(deleteUri, null, null);
									} catch (Exception e) {}
									try {new File(mediaItems[i].path).delete();} catch (Exception e) {}
								}
							}
						};
		    			new AlertDialog.Builder(SafeActivity_Full.this)
		    				.setMessage(R.string.safe_import_delete_source)
		    				.setPositiveButton(R.string.safe_delete_confirm_yes, delete)
		    				.setNegativeButton(R.string.safe_delete_confirm_no, null)
		    				.create().show();
	    			}
		    		refreshListData();
	    		}
	        }
		};
		executeVoid(task);
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private final View.OnClickListener deleteClick = new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			onDeleteClick();
		}
	};
	private void onDeleteClick() {
		if (getSelectedList().isEmpty()) return;
		//are you sure
		DialogInterface.OnClickListener delete = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onHideLoading();
				int[] selected = new int[getSelectedList().size()];
				int index = 0;
				ArrayList<Data> list = ((TimeLockApplication)getApplication()).getListData(getFolder());
				for (Object id : getSelectedList().keySet()) {
					try {
						int idvalue = (Integer)id;
						if (idvalue > 0) {
							for (int i=0; i<list.size(); i++) {
								if (list.get(i).getDataId() == idvalue) {
									selected[index] = i;
									break;
								}
							}
						} else {//delete folder
							selected[index] = idvalue;
						}
					} catch (Exception e) {}
					index++;
				}
				delete(selected);
				onChangeMultipleMode(false);
			}
		};
		String message = String.format(getString(R.string.safe_delete_confirm), getSelectedList().size());
		new AlertDialog.Builder(this)
				.setNegativeButton(R.string.safe_delete_confirm_no, null)
				.setPositiveButton(R.string.safe_delete_confirm_yes, delete)
				.setMessage(message)
				.create().show();
		onShowLoading();
	}

	private AlertDialog loadingDelete;
	private AlertDialog getLoadingDelete() {
		if (loadingDelete == null) {
			loadingDelete = new AlertDialog.Builder(this).setMessage(R.string.view_share_delete_processing).setCancelable(false).create();
		}
		return loadingDelete;
	}
	
	protected void delete(final int[] index) {
		if (index == null) {
			//show message
			return;
		}
		getLoadingDelete().show();
		onShowLoading();
		final AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				//delete in database, thumbnail, full file
				ArrayList<Data> list = new ArrayList<Data>(((TimeLockApplication)getApplication()).getListData(getFolder()));
				for (int i=0; i<index.length; i++) {
					Log.d("meobudebug", "delete: " + index[i]);
					if (index[i] >= 0) {
						try {
							Data data = list.get(index[i]);
							((TimeLockApplication)getApplication()).removeData(data, getDataSource(), getFolder());
							//((TimeLockApplication)getApplication()).getListData().remove(data);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {//delete folder
						try {
							((TimeLockApplication)getApplication()).removeFolder(-index[i], getDataSourceFolder(), getDataSource());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				return 1;
			}

	        @Override
	        protected void onPostExecute(Integer result) {
	        	if (getLoadingDelete().isShowing()) {
	        		getLoadingDelete().dismiss();
	        		onHideLoading();
		    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		        	if (((TimeLockApplication)getApplication()).getSoundDeletion()) {
		        		playMeobuSoundPool(5);//reference TimeLockApplication soundpool
		        	}
		    		if (result > 0) {
		    			//update useList & refresh view
		    			refreshListData(true);
		    		}
	        	}
	        }
		};
		executeVoidVoidInteger(task);
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private final View.OnClickListener saveClick = new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			onSaveClick();
		}
	};
	private void onSaveClick() {
		if (getSelectedList().isEmpty()) return;
		ArrayList<Data> selectedData = new ArrayList<TimeLockApplication.Data>();
		ArrayList<Integer> selectedFolder = new ArrayList<Integer>();
		//fill content this folder
		ArrayList<Data> list = ((TimeLockApplication)getApplication()).getListData(getFolder());
		Enumeration<Integer> keys = getSelectedList().keys();
		while (keys.hasMoreElements()) {
			try {
				Integer id = keys.nextElement();
				if (id > 0) {
					for (Data data : list) {
						if (data.getDataId() == id) {
							selectedData.add(data);
							break;
						}
					}
				} else {
					selectedFolder.add(-id);
				}
			} catch (Exception e) {}
		}
		//looking at children folders
		Hashtable<Integer, TimeLockNode> nodes = ((TimeLockApplication)getApplication()).getHashNodeForSafeActivitySavingBackOnly();
		for (int i=0; i<selectedFolder.size(); i++) {
			try {
				TimeLockNode node = nodes.get(selectedFolder.get(i));
				//fill content this child folder
				selectedData.addAll(node.getContents());
				//go down for child
				for (int j=0; j<node.getFolders().size(); j++) {
					int childOfChild = node.getFolders().get(j).getId();
					boolean already = false;
					for (int k=0; k<selectedFolder.size(); k++) {
						if (selectedFolder.get(k) == childOfChild) {
							already = true;
							break;
						}
					}
					if (!already) {
						selectedFolder.add(childOfChild);
					}
				}
			} catch (Exception e) {}
		}
		//go delete
		Data[] selected = new Data[selectedData.size()];
		for (int i=0; i<selectedData.size(); i++) {
			selected[i] = selectedData.get(i);
		}
		saveBackGallery(selected);
		onChangeMultipleMode(false);
	}
	
	private AlertDialog loadingSaveBack;
	private AlertDialog getLoadingSaveBack() {
		if (loadingSaveBack == null) {
			loadingSaveBack = new AlertDialog.Builder(this).setMessage(R.string.view_save_back_processing).setCancelable(false).create();
		}
		return loadingSaveBack;
	}
	
	private void saveBackGallery(final Data[] itemData) {
		if (itemData == null) {
			return;
		}
		if (Environment.getExternalStorageState() == null || !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			new AlertDialog.Builder(this).setNeutralButton(R.string.btn_ok1, null).setMessage(R.string.view_saveback_sdcard).create().show();
			return;
		}
		getLoadingSaveBack().setMessage(getString(R.string.view_save_back_processing));
		getLoadingSaveBack().show();
		onShowLoading();
		final OneByOnePercentAsyncTask<Void, Integer> task = new OneByOnePercentAsyncTask<Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... arg0) {
				total = itemData.length;
				for (int i=0; i<itemData.length; i++) {
					index = i+1;
					if (itemData[i] == null) continue;
					doPublishProgressFull(-1,-1);
					//save file to gallery
					String filename = "timelock" + System.currentTimeMillis();// + ".png";
					File outputFile = null;
					if (itemData[i].getDataType() == ItemViewDataType.Photo.ordinal()) {
						//getd
						filename += ".png";
						outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), filename);
					} else {
						filename += ".mp4";
						outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), filename);
					}
					try {
						boolean created = outputFile.createNewFile();
						if (!created) {
							outputFile.delete();
							created = outputFile.createNewFile();
							if (!created) {
								deleteFile(outputFile.getAbsolutePath());
								outputFile.createNewFile();
							}
						}
					} catch (Exception e) { }
					File inputFile = new File(itemData[i].getDataPath());
					MeobuFileTools.copyDecrypt(inputFile, outputFile, getCachePasscode(), this);//TimeLockApplication.cachePasscode()
					//scan to gallery
					Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				    Uri contentUri = Uri.fromFile(outputFile);
				    mediaScanIntent.setData(contentUri);
				    sendBroadcast(mediaScanIntent);
				}
			    return 1;
			}
			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				if (values[0] > 8888) {
					//messageFile.append(values[1]).append(' ');
					return;
				}
				if (getLoadingSaveBack().isShowing()) {String.format(getString(R.string.safe_add_percentage_processing), values[1], values[2]);
					String message = String.format(getString(R.string.view_save_back_processing_many), values[1], values[2]);
					if (values[3] >= 0 || values[4] >= 0) {
						String value3 = String.format("%.2f", values[3] / 1048576f);
						String value4 = String.format("%.2f", values[4] / 1048576f);
						message = message + "\n" + (values[3]<0?"?":value3) + "/" + (values[4]<0?"?":value4);
					}
					getLoadingSaveBack().setMessage(message);
				}
			}
	        @Override
	        protected void onPostExecute(Integer result) {
	        	Log.d("meobudebug", "saveBackGallery finish.");
	        	if (getLoadingSaveBack().isShowing()) {
	        		getLoadingSaveBack().dismiss();
	        		onHideLoading();
		    		//getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	        	}
	        }
		};
		executeVoidVoidInteger(task);
    }

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	
	@Override
	protected boolean supportMeobuSQLite() {
		return true;
	}
	TimeLockContentsDataSource datasource;
	private TimeLockContentsDataSource getDataSource() {
		if (datasource == null) {
			datasource = new TimeLockContentsDataSource(this);
		}
		return datasource;
	}
	@Override
	protected MeobuSQLiteDataSource getMeobuDataSource() {
		return getDataSource();
	}
	TimeLockFoldersDataSource datasourceFolder;
	protected TimeLockFoldersDataSource getDataSourceFolder() {
		if (datasourceFolder == null) {
			datasourceFolder = new TimeLockFoldersDataSource(this);
		}
		return datasourceFolder;
	}
	@Override
	protected void resumeMeobuSQLite() {
		super.resumeMeobuSQLite();
		if (supportMeobuSQLite()) {
			getDataSourceFolder().open();
		}
	}
	@Override
	protected void pauseMeobuSQLite() {
		super.pauseMeobuSQLite();
		if (supportMeobuSQLite()) {
			getDataSourceFolder().close();
		}
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
	private void setupSetting() {
		findViewById(R.id.setting).setOnClickListener(settingClick);
	}
	private final View.OnClickListener settingClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			onSettingClick();
		}
	};
	private void onSettingClick() {
		goSetting();
	}

	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//
	
//	private void updateData() {
//		//it's sample now, replace with real data loader
//		if (new Random().nextBoolean()) {
//			//random empty
//			getListData().add(new EmptyViewData());
//		} else {
//			//random data
//			getListData().add(new HeaderViewData());
//			getListData().add(new ContentViewData());
//		}
//		((TimeLockApplication)getApplication()).updateData();
//	}
	
//	private void addData(Object object) {
//		//update
//	}
//	
//	private void removeData(Object object) {
//		//update
//	}
	
	//----------//----------//----------//----------//----------//
	
//	private ArrayList<Object> getData() {
//		return null;
//	}
//	private ArrayList<ViewData> convertToViewObject(ArrayList<Object> data) {
//		//group by date, month
//		ArrayList<ViewData> list = new ArrayList<ViewData>();
//		list.add(new EmptyViewData());
//		return list;
//	}
//	ArrayList<ViewData> listData;
//	private ArrayList<ViewData> getListData() {
//		if (listData == null) {
//			listData = new ArrayList<ViewData>();
//			//listData.add(new EmptyViewData());
//		}
//		return listData;
//		return ((TimeLockApplication)getApplication()).getListViewData();
//	}
	
	//----------//----------//----------//----------//----------//
	
//	public static class Data {
//		int id;
//		Date time;
//		String path;
//		public void set(Data data) {
//			this.id = data.id;
//			this.time = data.time;
//			this.path = data.path;
//		}
//		public Data() { }
//		public Data(Data data) {
//			set(data);
//		}
//	}
//	public static class EncryptedData extends Data {
//		public InputStream getDecryptedStream() {
//			return null;
//		}
//	}
//	static class ThumbnailData extends Data {
//		public ThumbnailData(Data data) {
//			super(data);
//		}
//		@Override
//		public void set(Data data) {
//			super.set(data);
//			//thumbnail = BitmapDrawable.createFromPath(path);
//		}
//		BitmapDrawable thumbnail;
//	}
	
	//----------//----------//----------//----------//----------//
	
//	public static interface ViewData {
//		enum DataViewType {
//			GroupEmpty,
//			GroupHeader,
//			GroupContent
//		}
//		public int getType();
//		public String getTime();
//		public int getCount();
//		public ArrayList<BitmapDrawable> getThumbnails();
//	}
//	
//	public static class EmptyViewData implements ViewData {
//		@Override
//		public int getType() { return DataViewType.GroupEmpty.ordinal(); }
//		@Override
//		public String getTime() { return null; }
//		@Override
//		public int getCount() { return 0; }
//		@Override
//		public ArrayList<BitmapDrawable> getThumbnails() { return thumbnails; }
//		ArrayList<BitmapDrawable> thumbnails = new ArrayList<BitmapDrawable>();
//	}
//	
//	public static class HeaderViewData implements ViewData {
//		@Override
//		public int getType() { return DataViewType.GroupHeader.ordinal(); }
//		@Override
//		public String getTime() { return "Dec 21, 2013"; }
//		@Override
//		public int getCount() { return 0; }
//		@Override
//		public ArrayList<BitmapDrawable> getThumbnails() { return thumbnails; }
//		ArrayList<BitmapDrawable> thumbnails = new ArrayList<BitmapDrawable>();
//	}
//		
//	public static class ContentViewData implements ViewData {
//		public ContentViewData() {
//			thumbnails = new ArrayList<BitmapDrawable>();
//			for (int i=0; i<getCount(); i++) {
//				thumbnails.add(new BitmapDrawable(Resources.getSystem(), "file:///android_asset/sample.png"));
//			}
//		}
//		@Override
//		public int getType() { return DataViewType.GroupContent.ordinal(); }
//		@Override
//		public String getTime() { return null; }
//		@Override
//		public int getCount() { return 3; }
//		@Override
//		public ArrayList<BitmapDrawable> getThumbnails() { return thumbnails; }
//		ArrayList<BitmapDrawable> thumbnails;
//	}
	
	//----------//----------//----------//----------//----------//
	
	protected void createFolder() { }
	
	protected boolean editFolder(int folderid) { return false; }
	
	protected void onSelectFolderInMultipleMode(int folderid) { }
	
}
