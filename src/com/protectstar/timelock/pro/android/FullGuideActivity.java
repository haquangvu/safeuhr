package com.protectstar.timelock.pro.android;

import com.protectstar.safeuhr.android.R;
import common.view.HackyViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import meobu.android.base.MeobuActivity;

public class FullGuideActivity extends MeobuActivity {
	
	//----------//----------//----------//----------//----------//
	
	public static final String FIRST_GUIDE_KEY = "FIRSTGUIDE";
	public static final int[] guideLayoutIds = {R.layout.activity_guide1, R.layout.activity_guide2, R.layout.activity_guide3, R.layout.activity_guide4, R.layout.activity_guide6, R.layout.activity_guide7};
	boolean firstGuide;
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guide);
		
		firstGuide = getIntent().getBooleanExtra(FIRST_GUIDE_KEY, true);
		if (firstGuide) {
			((TimeLockApplication)getApplication()).offGuide();
		}
		
		pager = (HackyViewPager)findViewById(R.id.pager);
		pager.setAdapter(adapter);
	}
	
	@Override
	public void onBackPressed() {
		goClose();
	}
	
	//----------//----------//----------//----------//----------//
	
	HackyViewPager pager;
	private final PagerAdapter adapter = new PagerAdapter() {
		@Override
		public int getCount() {
			if (firstGuide) return guideLayoutIds.length;
			return guideLayoutIds.length - 1;
		}
		@Override
		public View instantiateItem(ViewGroup container, int position) {
			LayoutInflater li = LayoutInflater.from(container.getContext());
			View view = li.inflate(guideLayoutIds[position], container, false);
			//TODO out-of-memory crash because of 3 background & 3 item at the same time => create background manually & set to each page
			//setup views
			view.findViewById(R.id.close).setOnClickListener(onClose);
			view.findViewById(R.id.next).setOnClickListener(onNext);
			//just add
			container.addView(view, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			return view;
		}
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}
		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	};
	
	//----------//----------//----------//----------//----------//
	
	private final View.OnClickListener onClose = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			goClose();
		}
	};
	private void goClose() {
		if (firstGuide) {
			finish();
			Intent intent = new Intent(FullGuideActivity.this, ClockActivity6.class);
			startActivity(intent);
			overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
		} else {
			finish();
			overridePendingTransition(R.anim.hold_fade_in, R.anim.hold_fade_out);
		}
	}
	
	private final View.OnClickListener onNext = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			goNext();
		}
	};
	private void goNext() {
		final int current = pager.getCurrentItem();
		if (current + 1 >= adapter.getCount()) {
			goClose();
		} else {
			pager.setCurrentItem(current + 1, true);
		}
	}
	
	//----------//----------//----------//----------//----------//
	
}
