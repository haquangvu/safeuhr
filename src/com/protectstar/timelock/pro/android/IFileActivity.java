package com.protectstar.timelock.pro.android;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.protectstar.safeuhr.android.R;

public class IFileActivity extends TimeLockActivity {

	private void goBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);
	}
	
	//----------//----------//----------//----------//----------//
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ifile);
		setupTouchable();
	}
	
	boolean activated;
	@Override
	protected void onResume() {
		super.onResume();
		activated = false;
		updateView(activated);
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}

	//----------//----------//----------//----------//----------//
	
	private void setupTouchable() {
		findViewById(R.id.ifileActivated).setOnClickListener(itemClick);
	}
	private final View.OnClickListener itemClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			IFileActivity.this.onClick();
		}
	};
	
	//----------//----------//----------//----------//----------//
	
	Drawable on, off;
	private Drawable getOnDrawable() {
		if (on == null) {
			on = getResources().getDrawable(R.drawable.switch_on);
		}
		return on;
	}
	private Drawable getOffDrawable() {
		if (off == null) {
			off = getResources().getDrawable(R.drawable.switch_off);
		}
		return off;
	}
	private void updateView(boolean activated) {
		ImageView view = (ImageView)findViewById(R.id.ifileActivated);
		if (activated && view != null) {
			view.setImageDrawable(getOnDrawable());
		} else {
			view.setImageDrawable(getOffDrawable());
		}
		TextView status = (TextView)findViewById(R.id.status);
		TextView url = (TextView)findViewById(R.id.url);
		if (activated) {
			url.setVisibility(View.VISIBLE);
			url.setText("http://192.168.1.1:1234");
			status.setText(R.string.ifile_status_enable);
		} else {
			url.setVisibility(View.GONE);
			status.setText(R.string.ifile_status_disable);
		}
	}
	
	private void onClick() {
		activated = !activated;
		updateView(activated);
	}
	
	//----------//----------//----------//----------//----------//
	

}
