package com.protectstar.timelock.pro.android.spy;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;

public class SpyCameraUtil {
	
	public static class SpyCameraInfo extends CameraInfo {
		public int id = -1;
	}

	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	
	public static SpyCameraInfo getFaceCamera(Context context) {
		SpyCameraInfo faceCameraInfo = new SpyCameraInfo();
		try {
//			if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
				//go through all camera
				int cameraCount = Camera.getNumberOfCameras();
				for (int i=0; i<cameraCount; i++) {
					Camera.getCameraInfo(i, faceCameraInfo);
					//check info
					if (faceCameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
						faceCameraInfo.id = i;
						break;
					}
				}
//			}
		} catch (Exception e) {}
		return faceCameraInfo;
	}

	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//
	//----------//----------//----------//----------//----------//----------//----------//

}
